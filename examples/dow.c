/* Example in DoW */

#include <string.h>

double *heatmap;
double *heatmap_tmp;

void initmap(double *heatmap) {
}

double heatcell(double *cells) {
  return 0.0;
}

double *neighbours(double *cells, int x) {
  return NULL;
}

#pragma func heatspread heatmap = \
  zipWith (+) (heatcell (neighbours heatmap)) heatmap
void heatspread(double **heatmap)
{
  int x;
  for (x=0; x<10; x++)
    {
      double dphi = heatcell(neighbours(*heatmap,x));
      heatmap_tmp[x] = *heatmap[x] + dphi;
    }
}

#pragma func main() = iterate 100 heatspread (initmap(heatmap))
void main()
{
  int iter;
  initmap(heatmap);
  for (iter=0; iter<100; iter++) /* 100 iterations */
    {
      heatspread(&heatmap);
      memcpy(heatmap, heatmap_tmp, 10);
    }
}
