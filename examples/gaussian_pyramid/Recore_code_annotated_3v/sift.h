/*
 * Copyright (c) 2014, Recore Systems B.V., The Netherlands,
 * web: www.recoresystems.com, email: info@recoresystems.com
 *
 * Any reproduction in whole or in parts is prohibited
 * without the written consent of the copyright owner.
 *
 * All Rights Reserved.
 *
 */
#ifndef SIFT_H_
#define SIFT_H_

#include <stdint.h>

#define MAX_OCTAVES       (2)       /* Maximum number of octaves */
#define MAX_SCALES        (7)       /* Maximum number of scales */
#define MAX_FILTER_LEN    (51)      /* Maximum filter length */
#define MAX_IMAGE_SIZE    (256*256) /* Maximum input image size in pixels */

/* Fixed point math operations */
#define fmul32(X, Y, SHIFT) (int32_t)(((int64_t) X * Y) >> SHIFT)
#define sadd32(X, Y)        (((int64_t) X + Y > INT32_MAX) ? INT32_MAX : \
                            (((int64_t) X + Y < INT32_MIN) ? INT32_MIN : X + Y))

#define fix2double(X, frac) ((double) (X)/(1<<frac))
#define double2fix(X, frac) ((int32_t) ((X) * (1<<frac)))

/* Input parameters structure */
typedef struct
{
  /* Number of scales and octaves */
  uint8_t noOfOctaves, noOfScales;
  /* Fixed point fractional length */
  uint8_t fracLen;
  /* Array of filter lengths */
  uint8_t filterLen[MAX_SCALES];
  /* Array of filter coefficients */
  int32_t filter[MAX_SCALES][MAX_FILTER_LEN * MAX_FILTER_LEN];
  /* Input image width and height */
  uint8_t imageWidth, imageHeight;
  /* Input image */
  int32_t image[MAX_IMAGE_SIZE];
  /* Initial sigma*/
  double sigma_init;
} InputParams;


#endif /* SIFT_H_ */
