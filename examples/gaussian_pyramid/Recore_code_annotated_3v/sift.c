/* 
 * Copyright (c) 2014, Recore Systems B.V., The Netherlands,
 * web: www.recoresystems.com, email: info@recoresystems.com
 *
 * Any reproduction in whole or in parts is prohibited
 * without the written consent of the copyright owner.
 *
 * All Rights Reserved.
 *
 */

// ABOUT THE ANNOTATIONS
// This is one proposal of annotations for this code, having in mind the needed
// information for its tranformation and trying to use annotation as similar as
// possible as the proposed in the WP3 at the wiki. The only function annotated is 
// getGaussianKernels (the rest are annotated with previous ideas). This is because
// we think that this proposal could be too much difficult and tedious to be used.
// Additionally, we provide a parallel version of the getGaussianKernels function
// (getGaussianKernelsPar) that could be obtained using the informations of these 
// annotations. We also include some comments about the most interesting annotations.
// These comments are prefixed by POLCA to help to distinguish between others 
// comments. 

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "pgm_import.h"
#include "sift.h"

/* comment to skip saving images*/
#define SAVE_IMAGE_PGM
//#define WRITE_IMAGE_STD_OUT

/* Function to print image */
void printImage(uint8_t width,
    uint8_t height,
    int32_t *pImage)
{
  uint8_t w, h;
  /* Loop over image height */
  for (h = 0; h < height; h++) {
    /* Loop over image width */
    for (w = 0; w < width; w++) {
      printf("%d ", pImage[h * width + w]);
    }
    printf("\n");
  }
  printf("\n");
}

int saveImage(char *fileName,
    uint8_t width,
    uint8_t height,
    uint8_t fracLen,
    int32_t *pImage)
{
  uint16_t i, imageSize;
  uint32_t half = (1u << (fracLen - 1));
  GraylevelImage grayOut = DefaultPgmImage;

  imageSize = width * height;
  grayOut.width = width;
  grayOut.height = height;
  grayOut.depth = 1;
  grayOut.data = (uint16_t *)malloc(imageSize * sizeof(uint16_t));

  /* Scale pixels to 8 bit and copy to grayOut.data */
  for (i = 0; i < imageSize; i++) {
    grayOut.data[i] = (uint16_t)(sadd32((pImage[i] * IMAGE_SCALING(grayOut.depth)),
        half) >> fracLen);
  }
	printf("Writing image to %s\n", fileName);
  if (writePgm(fileName, &grayOut)) {
    printf("ERROR: Could not write to %s\n", fileName);
    return -1;
  }
  freePgm(&grayOut);
  return 0;
}

void getGaussianKernels(InputParams *param)
{
  int32_t x, y, normCoef;
  int32_t scale, filterLen, r;
  double sigma, sigmaPre, temp, k;
  double sigmaSq, preFactor, norm;
  const double CONST_PI = 3.14159265359;
  int32_t maxFilterSize = ((MAX_FILTER_LEN << 1) + 1);
  maxFilterSize *= maxFilterSize;
  double coeffFloat[maxFilterSize];
  k = pow(2, 1./(param->noOfScales - 3));
  sigmaPre = sqrt(k*k - 1.)*param->sigma_init;
  //Changed by Salvador Tamarit: removed initialization of sigma from inside the next loop
  sigma = param->sigma_init;
/*Filter coefficients */
#pragma polca def FS
#pragma polca map G (itnscanl MULTK sigma:i (param->noOfScales - 1) )
  for (scale = 0; scale < param->noOfScales; scale++) {
    // if (scale > 0) {
#pragma polca def MULTK
      sigma = sigmaPre * k;
      sigmaPre = sigma;
    // }
    // else {
    //   sigma = param->sigma_init;
    // }
    filterLen = ceil(6*sigma);
    filterLen = (filterLen % 2) ? filterLen : filterLen + 1;
    assert(filterLen < MAX_FILTER_LEN);
    param->filterLen[scale] = filterLen;

#pragma polca def G
{
    // generate Gaussian kernel
    r = filterLen>>1;
    sigmaSq = 2*sigma*sigma;
    preFactor = 1. / (sigmaSq * CONST_PI);
    norm = 0;
    // Since Gaussian coefficients are symmetric
    for(y = 0; y <= r; ++y) {
      for (x = 0; x <= r; ++x) {
        temp = (x * x) + (y * y);
        temp = temp / sigmaSq;
        temp = preFactor * exp(-temp);
        coeffFloat[y * (r + 1) + x] = temp;
        if(x && y) norm += (temp*4);
        else if(x || y) norm += (temp*2);
        else norm += temp;
      }
    }
    //normalize coefficients
    for(y = 0; y <= r; ++y) {
      for (x = 0; x <= r; ++x) {
        normCoef = double2fix(coeffFloat[y * (r + 1) + x] / norm, param->fracLen);
        param->filter[scale][(r + x) * filterLen + (r + y)] = normCoef;
        param->filter[scale][(r + x) * filterLen + (r - y)] = normCoef;
        param->filter[scale][(r - x) * filterLen + (r + y)] = normCoef;
        param->filter[scale][(r - x) * filterLen + (r - y)] = normCoef;
      }
    }
  }
  }
}

/* Function to load input parameters */
int loadInputParams(InputParams *pParams, char *inputFileName)
{
  int32_t i;
  int32_t imSize;

  /* Parameters */
  pParams->noOfOctaves = 2;
  pParams->noOfScales = 5;
  pParams->fracLen = 20;
  pParams->sigma_init = 1.6;

  assert(pParams->noOfScales <= MAX_SCALES);
  assert(pParams->noOfOctaves <= MAX_OCTAVES);

  /*import image data*/
  GraylevelImage gray = DefaultPgmImage;
  if (readPgm(inputFileName, &gray)) {
    printf("ERROR: Could not read from %s\n", inputFileName);
    return -1;
  }
  imSize = gray.width * gray.height;
  assert(imSize <= MAX_IMAGE_SIZE);
  pParams->imageWidth = gray.width;
  pParams->imageHeight = gray.height;
  /* Scale pixels to fixed point fractional length and load into input image buffer */
  for (i = 0; i < imSize; i++) {
    pParams->image[i] = fmul32((gray.data[i] << pParams->fracLen),
        (double2fix((1./IMAGE_SCALING(gray.depth)), pParams->fracLen)),
        pParams->fracLen);
  }
  freePgm(&gray);
  //initialize Gaussian Filter Kernels
  getGaussianKernels(pParams); // Calculate Gaussian Filter Kernels

  return 0;
}

/* Function to perform 2D convolution */
void convolution2D(uint8_t width,
    uint8_t height,
    uint8_t filterLen,
    uint8_t fracLen,
    int32_t *pFilter,
    int32_t *pImage,
    int32_t *pGauss)
{
  uint8_t w, h, k, l;
  int16_t x, y, xstart, ystart;
  int32_t sum;
  uint8_t filterLenBy2 = filterLen >> 1;

#pragma polca itn CALCULATE_PGAUSS_H width
//ST: Here we can add a extra loop parallelizable to run the whole calculus in 
// smaller squares
  /* Loop over processed image height indices */
  for (h = 0; h < height; h++) 
  {
    /* Loop over processed image width indices */
//#pragma polca zip from pImage[(h - filterLenBy2..filterLenBy2) * width + (w - filterLenBy2..filterLenBy2)] pFilter[(0..filterLen) * filterLen + (0..filterLen)] to pGauss[h * width + w]
#pragma polca def CALCULATE_PCAUSS_H
#pragma polca itn CALCULATE_PGAUSS_W width
    for (w = 0; w < width; w++) 
    {
#pragma polca def INIT_SUM
      sum = 0; /* Initialize sum to 0 */
      /* Pixel row index to start with */
#pragma polca def INIT_YTSTART
      ystart = h - filterLenBy2;
      /* Loop over filter length height-wise */
#pragma polca def CALCULATE_PGAUSS_W
#pragma polca itn CALCULATE_NEW_Y_XSTART filterLen y:i ystart:i 
#pragma polca itn INIT_XTSTART filterLen xstart:i  
#pragma polca itn ADD_PIXELFILTER filterLen
#pragma polca itn ASSIGN_PGAUSS filterLen
      for (l = 0; l < filterLen; l++) 
      {
#pragma polca def CALCULATE_NEW_Y_XSTART
{
        y = ystart;
        ystart++; /* Increment pixel row index */
}
        /* Pixel column index to start with */
#pragma polca def INIT_XTSTART
        xstart = w - filterLenBy2;
        /* Loop over filter length width-wise */
#pragma polca def ADD_PIXELFILTER
#pragma itn CALCULATE_NEW_X_XSTART filterLen x:i xstart:i 
#pragma itn ADD_PIXEL_X_FILTER filterLen sum:i with BOUNDARY:boundary
        for (k = 0; k < filterLen; k++) 
        {
#pragma polca def CALCULATE_NEW_X_XSTART
{
          x = xstart;
          xstart++; /* Increment pixel column index */
}
          /* Skip pixels that are outside the image boundary (Zero-padding) */
#pragma polca def BOUNDARY
          if ((x < 0) || (y < 0)) continue;
          if ((x > width - 1) || (y > height - 1)) continue;
          /* Multiply image pixel with filter coefficient and add to sum */
#pragma polca def ADD_PIXEL_X_FILTER
#pragma polca def op (+ (pFilter[l * filterLen + k] * pImage[y * width + x]))
          sum = sadd32(sum,
              fmul32(pImage[y * width + x],
                  pFilter[l * filterLen + k],
                  fracLen)
          );
        }
      }
#pragma polca def ASSIGN_PGAUSS
      /* Copy sum to output gaussian image location */
      pGauss[h * width + w] = sum;
    }
  }
}

/* Function to downsample an image by a factor of 2 */
void downsample(uint8_t width,
    uint8_t height,
    int32_t *pInputImage,
    int32_t *pOutputImage)
{
  uint8_t w, h;
  /* Temporary pointers */
  int32_t *pIn;
  int32_t *pOut = pOutputImage;
  /* Loop over image height skipping every other index */
#pragma polca itn INIT_PIN·ASSIGN_POUT_AND_INC_LOOP height/2
  for (h = 0; h < height; h += 2) 
  {
#pragma polca INIT_PIN
    /* Set pIn to point to current row */
    pIn = pInputImage + h * width;
    /* Loop over image width skipping every other index */
#pragma polca def ASSIGN_POUT_AND_INC_LOOP
#pragma polca map ASSIGN_POUT_AND_INC (pIn:i,inc:2) pOut:o iters(width/2)
//pragma polca map ASSIGN_POUT pIn:i pOut:o
//#pragma polca itn INC_POUT width/2 pOut:io  
//#pragma polca itn INC_PIN width/2 pIn:io  
    for(w = 0; w < width; w += 2) 
#pragma polca def ASSIGN_POUT_AND_INC
    {
      /* Copy input image pixel value to output image */
//pragma polca def ASSIGN_POUT 
      *pOut = *pIn;
//#pragma polca def INC_POUT
      ++pOut; /* Increment output image pointer */
//#pragma polca def INC_PIN
      pIn += 2; /* Increment input image pointer */
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc < 2) {
    printf("USAGE: ./sift path/to/input/image\n");
    return -1;
  }
  uint8_t scale, octave;
  uint8_t width, height;
  uint16_t pixel, size;
  InputParams params;
  int32_t gauss[MAX_OCTAVES][MAX_SCALES][MAX_IMAGE_SIZE];
  int32_t dog[MAX_OCTAVES][MAX_SCALES][MAX_IMAGE_SIZE];
  int32_t *pImage;

#ifdef SAVE_IMAGE_PGM
  char outFileName[80];
#endif
  /* Load input parameters into structure */
#pragma polca def SIGMAS params.filter:o
  if (loadInputParams(&params, argv[1])) {
    printf("ERROR: Invalid Input\n");
    return -1;
  }
  width = params.imageWidth;
  height = params.imageHeight;
  size = width * height;
  /* Set pImage to point to input image */
  pImage = params.image;
  /* Set scale loop index to start from 0 for first octave */
  scale = 0;
  /* Loop over number of octaves */
#pragma polca def P
#pragma polca mapacc F pImage:i SIGMAS:i
  for (octave = 0; octave < params.noOfOctaves; octave++) 
#pragma polca def F I_:o IS:o
  { 
    if (octave > 0) {
      scale = 0;
      /* Downsample the (S + 1)th image from previous octave
       * Store the downsampled image as the first gaussian image
       * for current octave
       */
#pragma polca def I_ gauss[octave][scale]:o
      downsample(width,
          height,
          gauss[octave - 1][params.noOfScales - 4],
          gauss[octave][scale]);
      /* Adjust image dimensions */
      width = (width % 2) ? (width + 1) >> 1 : width >> 1;
      height = (height % 2) ? (height + 1) >> 1 : height >> 1;
      size = width * height;
      /* First gaussian image is input image for next scale */
      pImage = gauss[octave][scale];
#ifdef SAVE_IMAGE_PGM
      sprintf(outFileName, "data/gauss_%d_%d.pgm", octave, scale);
      saveImage(outFileName, width, height, params.fracLen, gauss[octave][scale]);
#endif
#ifdef WRITE_IMAGE_STD_OUT
      printf("Gaussian image for octave %d scale 0\n", octave);
      printImage(width, height, gauss[octave][0]);
#endif
      /* Set scale loop index to start from 1 */
      ++scale;
    }
    /* Construct Gaussian images */ 
#pragma polca def IS
#pragma polca scanl B pImage:i params.filter:i pImage:o
    for ( ; scale < params.noOfScales; scale++) 
    {
      /* Perform 2D convolution to obtain a gaussian image */
#pragma polca def B 
      convolution2D(width,
          height,
          params.filterLen[scale],
          params.fracLen,
          params.filter[scale],
          pImage,
          gauss[octave][scale]);
      /* Current Gaussian image is input image for next scale */
      pImage = gauss[octave][scale];
#ifdef SAVE_IMAGE_PGM
      sprintf(outFileName, "data/gauss_%d_%d.pgm", octave, scale);
      saveImage(outFileName, width, height, params.fracLen, gauss[octave][scale]);
#endif
#ifdef WRITE_IMAGE_STD_OUT
      printf("Gaussian image for octave %d scale %d\n", octave, scale);
      printImage(width, height, gauss[octave][scale]);
#endif
    }

    /* Construct DoG images */
#pragma polca def DIFFS 
#pragma polca zipWith DIFF gauss[octave]:i gauss[octave]:i
    for (scale = 0; scale < params.noOfScales - 1; scale++) 
    {
#pragma polca def DIFF 
#pragma polca map2 (-)  gauss[octave][scale + 1]:i gauss[octave][scale]:i dog[octave][scale]
      for (pixel = 0; pixel < size; pixel++) 
      {
        dog[octave][scale][pixel] = gauss[octave][scale + 1][pixel] -
            gauss[octave][scale][pixel];
      }
#ifdef SAVE_IMAGE_PGM
      sprintf(outFileName, "data/dog_%d_%d.pgm", octave, scale);
      saveImage(outFileName, width, height, params.fracLen, dog[octave][scale]);
#endif
#ifdef WRITE_IMAGE_STD_OUT
      printf("DoG image for octave %d scale %d\n", octave, scale);
      printImage(width, height, dog[octave][scale]);
#endif
    }
  }
  return 0;
}
