/* 
 * Copyright (c) 2014, Recore Systems B.V., The Netherlands,
 * web: www.recoresystems.com, email: info@recoresystems.com
 *
 * Any reproduction in whole or in parts is prohibited
 * without the written consent of the copyright owner.
 *
 * All Rights Reserved.
 *
 */

// ABOUT THE ANNOTATIONS
// This is one proposal of annotations for this code, having in mind the needed
// information for its tranformation and trying to use annotation as similar as
// possible as the proposed in the WP3 at the wiki. The only function annotated is 
// getGaussianKernels (the rest are annotated with previous ideas). This is because
// we think that this proposal could be too much difficult and tedious to be used.
// Additionally, we provide a parallel version of the getGaussianKernels function
// (getGaussianKernelsPar) that could be obtained using the informations of these 
// annotations. We also include some comments about the most interesting annotations.
// These comments are prefixed by POLCA to help to distinguish between others 
// comments. 

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "pgm_import.h"
#include "sift.h"

/* comment to skip saving images*/
#define SAVE_IMAGE_PGM
//#define WRITE_IMAGE_STD_OUT

/* Function to print image */
void printImage(uint8_t width,
    uint8_t height,
    int32_t *pImage)
{
  uint8_t w, h;
  /* Loop over image height */
  for (h = 0; h < height; h++) {
    /* Loop over image width */
    for (w = 0; w < width; w++) {
      printf("%d ", pImage[h * width + w]);
    }
    printf("\n");
  }
  printf("\n");
}

int saveImage(char *fileName,
    uint8_t width,
    uint8_t height,
    uint8_t fracLen,
    int32_t *pImage)
{
  uint16_t i, imageSize;
  uint32_t half = (1u << (fracLen - 1));
  GraylevelImage grayOut = DefaultPgmImage;

  imageSize = width * height;
  grayOut.width = width;
  grayOut.height = height;
  grayOut.depth = 1;
  grayOut.data = (uint16_t *)malloc(imageSize * sizeof(uint16_t));

  /* Scale pixels to 8 bit and copy to grayOut.data */
  for (i = 0; i < imageSize; i++) {
    grayOut.data[i] = (uint16_t)(sadd32((pImage[i] * IMAGE_SCALING(grayOut.depth)),
        half) >> fracLen);
  }
	printf("Writing image to %s\n", fileName);
  if (writePgm(fileName, &grayOut)) {
    printf("ERROR: Could not write to %s\n", fileName);
    return -1;
  }
  freePgm(&grayOut);
  return 0;
}

void getGaussianKernels(InputParams *param)
{
  int32_t x, y, normCoef;
  int32_t scale, filterLen, r;
  double sigma, sigmaPre, temp, k;
  double sigmaSq, preFactor, norm;
  const double CONST_PI = 3.14159265359;
  int32_t maxFilterSize = ((MAX_FILTER_LEN << 1) + 1);
  maxFilterSize *= maxFilterSize;
  double coeffFloat[maxFilterSize];
// POLCA: Initializations of several variables in the same block
#pragma polca INIT_K_SIGMAPRE input(param->noOfScales,param->sigma_init) output(k,sigmaPre,sigma)
{
  k = pow(2, 1./(param->noOfScales - 3));
  sigmaPre = sqrt(k*k - 1.)*param->sigma_init;
  //Changed by Salvador Tamarit: removed initialization of sigma from inside the next loop
  sigma = param->sigma_init;
}
/*Filter coefficients */
// POLCA: Iteration with function NORMALIZE 
#pragma polca itn NORMALIZE INIT_K_SIGMAPRE param->noOfScales input(param->fracLen,param->noOfScales,k,sigmaPre,sigma) output(param->filter[scale],param->filterLen[scale])
  for (scale = 0; scale < param->noOfScales; scale++) {
// POLCA: Not used
#pragma polca INIT_SIGMA_SIGMAPRE input(sigma,sigmaPre,k) output(sigma,sigma_pre)
{
    // if (scale > 0) {
      sigma = sigmaPre * k;
      sigmaPre = sigma;
    // }
    // else {
    //   sigma = param->sigma_init;
    // }
}
// POLCA: the two following annotations has been separated deliberately
// to allow to separate two different behaviours in the followng loop.
#pragma polca INIT_FILTERLEN input(sigma) output(filterLen,param->filterLen[scale])
{
    filterLen = ceil(6*sigma);
    filterLen = (filterLen % 2) ? filterLen : filterLen + 1;
    assert(filterLen < MAX_FILTER_LEN);
    param->filterLen[scale] = filterLen;
}
#pragma polca INIT_R_SIGMASQ_PREFACTOR_NORM input(filterLen,sigma) output(r,sigmaSq,preFactor,norm)
{
    // generate Gaussian kernel
    r = filterLen>>1;
    sigmaSq = 2*sigma*sigma;
    preFactor = 1. / (sigmaSq * CONST_PI);
    norm = 0;
}
    // Since Gaussian coefficients are symmetric
// POLCA: This loop has three annotations.
//   1 - It is a initilization of data that will be used later
//   2 and 3 - Two different calculations are made in this loop. Describe
//       the separately can allow as to separate them and treat them individually
//       See the parallel version of this function bellow to see a proposal.
#pragma polca INIT_COEFFFLOAT_NORM input(r,sigmaSq,preFactor,norm) output(norm,coeffFloat[y * (r + 1) + x])
#pragma polca itn CALC_COEFFFLOAT INIT_R_SIGMASQ_PREFACTOR_NORM r input(x,y,r,sigmaSq,preFactor)  output(coeffFloat[y * (r + 1) + x]) 
#pragma polca itn CALC_NORM INIT_R_SIGMASQ_PREFACTOR_NORM r input(r,norm)  output(norm) 
    for(y = 0; y <= r; ++y) {
      for (x = 0; x <= r; ++x) {
// POLCA: each calculation is also annotated 
#pragma polca CALC_COEFFFLOAT input(x,y,r,sigmaSq,preFactor) output(coeffFloat[y * (r + 1) + x])
{
        temp = (x * x) + (y * y);
        temp = temp / sigmaSq;
        temp = preFactor * exp(-temp);
        coeffFloat[y * (r + 1) + x] = temp;
}
#pragma polca CALC_NORM input(norm,temp) output(norm)
{
        if(x && y) norm += (temp*4);
        else if(x || y) norm += (temp*2);
        else norm += temp;
}
      }
    }
    //normalize coefficients
// POLCA: Again a loop with more than one annotation. First the defintion 
// to give a name to be used (in this case be the first loop for scales).
// The second describes its semantics. Note that the second annotation uses
// INIT_COEFFFLOAT_NORM as initialization block, that in this case is the previous 
// loop
#pragma polca NORMALIZE input(coeffFloat[y * (r + 1) + x],norm, param->fracLen,r,filterLen) output(param->filter[scale][(r - x) * filterLen + (r - y)],param->filter[scale][(r - x) * filterLen + (r + y)],param->filter[scale][(r + x) * filterLen + (r - y)],param->filter[scale][(r + x) * filterLen + (r + y)])
#pragma polca itn NORMALIZE_STEP INIT_COEFFFLOAT_NORM r input(coeffFloat[y * (r + 1) + x],norm, param->fracLen,r,filterLen) output(param->filter[scale][(r - x) * filterLen + (r - y)],param->filter[scale][(r - x) * filterLen + (r + y)],param->filter[scale][(r + x) * filterLen + (r - y)],param->filter[scale][(r + x) * filterLen + (r + y)])
    for(y = 0; y <= r; ++y) {
      for (x = 0; x <= r; ++x) {
#pragma polca NORMALIZE_STEP input(coeffFloat[y * (r + 1) + x],norm, param->fracLen,r,filterLen) output(param->filter[scale][(r - x) * filterLen + (r - y)],param->filter[scale][(r - x) * filterLen + (r + y)],param->filter[scale][(r + x) * filterLen + (r - y)],param->filter[scale][(r + x) * filterLen + (r + y)])
{
        normCoef = double2fix(coeffFloat[y * (r + 1) + x] / norm, param->fracLen);
        param->filter[scale][(r + x) * filterLen + (r + y)] = normCoef;
        param->filter[scale][(r + x) * filterLen + (r - y)] = normCoef;
        param->filter[scale][(r - x) * filterLen + (r + y)] = normCoef;
        param->filter[scale][(r - x) * filterLen + (r - y)] = normCoef;
}
      }
    }
  }
}

void getGaussianKernels_parallel(InputParams *param)
{
  int32_t x, y, normCoef;
  int32_t scale, filterLen, r;
  double sigma[param->noOfScales], sigmaPre[param->noOfScales], 
  //double temp, k;
  double k;
  double sigmaSq, preFactor, norm;
  const double CONST_PI = 3.14159265359;
  int32_t maxFilterSize = ((MAX_FILTER_LEN << 1) + 1);
  maxFilterSize *= maxFilterSize;
  double coeffFloat[maxFilterSize];
#pragma polca INIT_K_SIGMAPRE input(param->noOfScales,param->sigma_init) output(k,sigmaPre,sigma)
{
  k = pow(2, 1./(param->noOfScales - 3));
  sigmaPre[0] = sqrt(k*k - 1.)*param->sigma_init;
  //Changed by Salvador Tamarit: removed initialization of sigma from inside the next loop
  sigma[0] = param->sigma_init;
}
  /*Filter coefficients */
  for (scale = 0; scale < param->noOfScales; scale++) {
#pragma polca INIT_SIGMA_SIGMAPRE input(sigma,sigmaPre,k) output(sigma,sigmaPre)
{
    if (scale > 0) {
      //We will need an anlysis to know that the sigmaPre that we need is the previous
      sigma[scale] = sigmaPre[scale-1] * k;
      sigmaPre[scale] = sigma[scale];
    }
    // else {
    //   sigma = param->sigma_init;
    // }
   
}
  }
#pragma polca itn NORMALIZE INIT_K_SIGMAPRE param->noOfScales input(param->fracLen,param->noOfScales,k,sigmaPre,sigma) output(param->filter[scale],param->filterLen[scale])
#pragma omp parallel for
for (scale = 0; scale < param->noOfScales; scale++) {
#pragma polca INIT_FILTERLEN input(sigma) output(filterLen,param->filterLen[scale])
{
    filterLen = ceil(6*sigma[scale]);
    filterLen = (filterLen % 2) ? filterLen : filterLen + 1;
    assert(filterLen < MAX_FILTER_LEN);
    param->filterLen[scale] = filterLen;
}
#pragma polca INIT_R_SIGMASQ_PREFACTOR_NORM input(filterLen,sigma) output(r,sigmaSq,preFactor,norm)
{
    // generate Gaussian kernel
    r = filterLen>>1;
    sigmaSq = 2*sigma[scale]*sigma[scale];
    preFactor = 1. / (sigmaSq * CONST_PI);
    norm = 0;
}
    // Since Gaussian coefficients are symmetric
    double temp[r+1][r+1];
#pragma polca INIT_COEFFFLOAT_NORM input(r,sigmaSq,preFactor,norm) output(norm,coeffFloat[y * (r + 1) + x])
#pragma polca itn CALC_COEFFFLOAT INIT_R_SIGMASQ_PREFACTOR_NORM r input(x,y,r,sigmaSq,preFactor)  output(coeffFloat[y * (r + 1) + x]) 
#pragma omp parallel for
    for(y = 0; y <= r; ++y) {
#pragma omp parallel for
      for (x = 0; x <= r; ++x) {
#pragma polca CALC_COEFFFLOAT input(x,y,r,sigmaSq,preFactor) output(coeffFloat[y * (r + 1) + x])
{
        double new_temp = (x * x) + (y * y);
        new_temp = new_temp / sigmaSq;
        temp[x][y] = preFactor * exp(-new_temp);
        coeffFloat[y * (r + 1) + x] = temp[x][y];
}
      }
    }
#pragma polca itn CALC_NORM INIT_R_SIGMASQ_PREFACTOR_NORM r input(r,norm)  output(norm) 
    for(y = 0; y <= r; ++y) {
      for (x = 0; x <= r; ++x) {
#pragma polca CALC_NORM input(norm,temp) output(norm)
{
        if(x && y) norm += (temp[x][y]*4);
        else if(x || y) norm += (temp[x][y]*2);
        else norm += temp[x][y];
}
      }
    }
    //normalize coefficients
#pragma polca NORMALIZE input(coeffFloat[y * (r + 1) + x],norm, param->fracLen,r,filterLen) output(param->filter[scale][(r - x) * filterLen + (r - y)],param->filter[scale][(r - x) * filterLen + (r + y)],param->filter[scale][(r + x) * filterLen + (r - y)],param->filter[scale][(r + x) * filterLen + (r + y)])
#pragma polca itn NORMALIZE_STEP INIT_COEFFFLOAT_NORM r input(coeffFloat[y * (r + 1) + x],norm, param->fracLen,r,filterLen) output(param->filter[scale][(r - x) * filterLen + (r - y)],param->filter[scale][(r - x) * filterLen + (r + y)],param->filter[scale][(r + x) * filterLen + (r - y)],param->filter[scale][(r + x) * filterLen + (r + y)])
#pragma omp parallel for
    for(y = 0; y <= r; ++y) {
#pragma omp parallel for
      for (x = 0; x <= r; ++x) {
#pragma polca NORMALIZE_STEP input(coeffFloat[y * (r + 1) + x],norm, param->fracLen,r,filterLen) output(param->filter[scale][(r - x) * filterLen + (r - y)],param->filter[scale][(r - x) * filterLen + (r + y)],param->filter[scale][(r + x) * filterLen + (r - y)],param->filter[scale][(r + x) * filterLen + (r + y)])
{
        normCoef = double2fix(coeffFloat[y * (r + 1) + x] / norm, param->fracLen);
        param->filter[scale][(r + x) * filterLen + (r + y)] = normCoef;
        param->filter[scale][(r + x) * filterLen + (r - y)] = normCoef;
        param->filter[scale][(r - x) * filterLen + (r + y)] = normCoef;
        param->filter[scale][(r - x) * filterLen + (r - y)] = normCoef;
}
      }
    }
  }
}

/* Function to load input parameters */
int loadInputParams(InputParams *pParams, char *inputFileName)
{
  int32_t i;
  int32_t imSize;

  /* Parameters */
  pParams->noOfOctaves = 2;
  pParams->noOfScales = 5;
  pParams->fracLen = 20;
  pParams->sigma_init = 1.6;

  assert(pParams->noOfScales <= MAX_SCALES);
  assert(pParams->noOfOctaves <= MAX_OCTAVES);

  /*import image data*/
  GraylevelImage gray = DefaultPgmImage;
  if (readPgm(inputFileName, &gray)) {
    printf("ERROR: Could not read from %s\n", inputFileName);
    return -1;
  }
  imSize = gray.width * gray.height;
  assert(imSize <= MAX_IMAGE_SIZE);
  pParams->imageWidth = gray.width;
  pParams->imageHeight = gray.height;
  /* Scale pixels to fixed point fractional length and load into input image buffer */
  for (i = 0; i < imSize; i++) {
    pParams->image[i] = fmul32((gray.data[i] << pParams->fracLen),
        (double2fix((1./IMAGE_SCALING(gray.depth)), pParams->fracLen)),
        pParams->fracLen);
  }
  freePgm(&gray);
  //initialize Gaussian Filter Kernels
  getGaussianKernels(pParams); // Calculate Gaussian Filter Kernels

  return 0;
}

/* Function to perform 2D convolution */
void convolution2D(uint8_t width,
    uint8_t height,
    uint8_t filterLen,
    uint8_t fracLen,
    int32_t *pFilter,
    int32_t *pImage,
    int32_t *pGauss)
{
  uint8_t w, h, k, l;
  int16_t x, y, xstart, ystart;
  int32_t sum;
  uint8_t filterLenBy2 = filterLen >> 1;

  /* Loop over processed image height indices */
  for (h = 0; h < height; h++) 
  {
    /* Loop over processed image width indices */
#pragma polca zip from pImage[(h - filterLenBy2..filterLenBy2) * width + (w - filterLenBy2..filterLenBy2)] pFilter[(0..filterLen) * filterLen + (0..filterLen)] to pGauss[h * width + w]
    for (w = 0; w < width; w++) 
#pragma polca def W
    {
      sum = 0; /* Initialize sum to 0 */
      /* Pixel row index to start with */
      ystart = h - filterLenBy2;
      /* Loop over filter length height-wise */
      for (l = 0; l < filterLen; l++) 
      {
        y = ystart;
        ystart++; /* Increment pixel row index */
        /* Pixel column index to start with */
        xstart = w - filterLenBy2;
        /* Loop over filter length width-wise */
#pragma polca foldl from pImage[y * width + x] pFilter[l * filterLen + k] sum:i to sum:o
        for (k = 0; k < filterLen; k++) 
#pragma polca def K
        {
          x = xstart;
          xstart++; /* Increment pixel column index */
          /* Skip pixels that are outside the image boundary (Zero-padding) */
          if ((x < 0) || (y < 0)) continue;
          if ((x > width - 1) || (y > height - 1)) continue;
          /* Multiply image pixel with filter coefficient and add to sum */
          sum = sadd32(sum,
              fmul32(pImage[y * width + x],
                  pFilter[l * filterLen + k],
                  fracLen)
          );
        }
      }
      /* Copy sum to output gaussian image location */
      pGauss[h * width + w] = sum;
    }
  }
}

/* Function to downsample an image by a factor of 2 */
void downsample(uint8_t width,
    uint8_t height,
    int32_t *pInputImage,
    int32_t *pOutputImage)
{
  uint8_t w, h;
  /* Temporary pointers */
  int32_t *pIn;
  int32_t *pOut = pOutputImage;
  /* Loop over image height skipping every other index */
#pragma polca zip from pIn to pOut
  for (h = 0; h < height; h += 2) 
#pragma polca def H
  {
    /* Set pIn to point to current row */
    pIn = pInputImage + h * width;
    /* Loop over image width skipping every other index */
#pragma polca zip from pIn to pOut
    for(w = 0; w < width; w += 2) 
#pragma polca def W
    {
      /* Copy input image pixel value to output image */
      *pOut = *pIn;
      ++pOut; /* Increment output image pointer */
      pIn += 2; /* Increment input image pointer */
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc < 2) {
    printf("USAGE: ./sift path/to/input/image\n");
    return -1;
  }
  uint8_t scale, octave;
  uint8_t width, height;
  uint16_t pixel, size;
  InputParams params;
  int32_t gauss[MAX_OCTAVES][MAX_SCALES][MAX_IMAGE_SIZE];
  int32_t dog[MAX_OCTAVES][MAX_SCALES][MAX_IMAGE_SIZE];
  int32_t *pImage;

#ifdef SAVE_IMAGE_PGM
  char outFileName[80];
#endif
  /* Load input parameters into structure */
  if (loadInputParams(&params, argv[1])) {
    printf("ERROR: Invalid Input\n");
    return -1;
  }
  width = params.imageWidth;
  height = params.imageHeight;
  size = width * height;
  /* Set pImage to point to input image */
  pImage = params.image;
  /* Set scale loop index to start from 0 for first octave */
  scale = 0;
  /* Loop over number of octaves */
#pragma polca itn gauss[octave-1]:i gauss[octave]:o
#pragma polca itn_inital_value pImage
  for (octave = 0; octave < params.noOfOctaves; octave++) 
#pragma polca def F
  {
    if (octave > 0) {
      scale = 0;
      /* Downsample the (S + 1)th image from previous octave
       * Store the downsampled image as the first gaussian image
       * for current octave
       */
      downsample(width,
          height,
          gauss[octave - 1][params.noOfScales - 4],
          gauss[octave][scale]);
      /* Adjust image dimensions */
      width = (width % 2) ? (width + 1) >> 1 : width >> 1;
      height = (height % 2) ? (height + 1) >> 1 : height >> 1;
      size = width * height;
      /* First gaussian image is input image for next scale */
      pImage = gauss[octave][scale];
#ifdef SAVE_IMAGE_PGM
      sprintf(outFileName, "data/gauss_%d_%d.pgm", octave, scale);
      saveImage(outFileName, width, height, params.fracLen, gauss[octave][scale]);
#endif
#ifdef WRITE_IMAGE_STD_OUT
      printf("Gaussian image for octave %d scale 0\n", octave);
      printImage(width, height, gauss[octave][0]);
#endif
      /* Set scale loop index to start from 1 */
      ++scale;
    }
    /* Construct Gaussian images */ 
#pragma polca zip from params.filterLen[scale] params.filter[scale] gauss[octave][scale] to gauss[octave][scale]
    for ( ; scale < params.noOfScales; scale++) 
#pragma polca def CONVULTION
    {
      /* Perform 2D convolution to obtain a gaussian image */
      convolution2D(width,
          height,
          params.filterLen[scale],
          params.fracLen,
          params.filter[scale],
          pImage,
          gauss[octave][scale]);
      /* Current Gaussian image is input image for next scale */
      pImage = gauss[octave][scale];
#ifdef SAVE_IMAGE_PGM
      sprintf(outFileName, "data/gauss_%d_%d.pgm", octave, scale);
      saveImage(outFileName, width, height, params.fracLen, gauss[octave][scale]);
#endif
#ifdef WRITE_IMAGE_STD_OUT
      printf("Gaussian image for octave %d scale %d\n", octave, scale);
      printImage(width, height, gauss[octave][scale]);
#endif
    }
    /* Construct DoG images */
#pragma polca zip from gauss[octave][scale + 1] gauss[octave][scale + 1] to dog[octave][scale]
    for (scale = 0; scale < params.noOfScales - 1; scale++) 
#pragma polca def DOG_EXT
    {
#pragma polca zip from gauss[octave][scale + 1][pixel] gauss[octave][scale + 1][pixel] to dog[octave][scale][pixel]
      for (pixel = 0; pixel < size; pixel++) 
#pragma polca def DOG_INT
      {
        dog[octave][scale][pixel] = gauss[octave][scale + 1][pixel] -
            gauss[octave][scale][pixel];
      }
#ifdef SAVE_IMAGE_PGM
      sprintf(outFileName, "data/dog_%d_%d.pgm", octave, scale);
      saveImage(outFileName, width, height, params.fracLen, dog[octave][scale]);
#endif
#ifdef WRITE_IMAGE_STD_OUT
      printf("DoG image for octave %d scale %d\n", octave, scale);
      printImage(width, height, dog[octave][scale]);
#endif
    }
  }
  return 0;
}
