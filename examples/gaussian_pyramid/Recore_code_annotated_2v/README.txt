Content
=========
This folder includes the following files :
  sift.c        : implementation of Gaussian and DoG construction 
  pgm_import.c  : contains routines to read and write images in PGM format.
  Makefile      : compiles sources
  data/         : conatins sample input image
  README        : this file
       

Steps to run
============
1. Compile C sources using 'make'
2. Run the executable using './sift "path/to/input/image"'.


Output
======
The application saves Gaussian and DoG images in the folder 'data/' in PGM format.
  
