/* *
 * Copyright (c) 2013, Recore Systems B.V., The Netherlands,
 * web: www.recoresystems.com, email: info@recoresystems.com
 *
 * Any reproduction in whole or in parts is prohibited
 * without the written consent of the copyright owner.
 *
 * All Rights Reserved.
 *
*/
#include <stdio.h>
#include <stdint.h>
#define MAX_OCTAVES     (5) // Maximum number of octaves
#define MAX_SCALES      (10) // Maximum number of scales
#define MAX_FILTER_LEN  (15) // Maximum filter length
#define MAX_IMAGE_SIZE  (100*100) // Maximum number of pixels in input image

// Fixed point math operations
#define fmul(X, Y, SHIFT) (int32_t)(((int64_t) X * Y) >> SHIFT)
#define sadd(X, Y)        (((int64_t) X + Y > INT32_MAX) ? INT32_MAX : \
                           (((int64_t) X + Y < INT32_MIN) ? INT32_MIN : X + Y))


// Input parameters structure
typedef struct
{
  // Number of scales and octaves
  uint8_t noOfOctaves, noOfScales;
  // Fixed point fractional length
  uint8_t fracLen;
  // Array of filter lengths
  uint8_t filterLen[MAX_OCTAVES][MAX_SCALES];
  // Array of filter coefficients
  int32_t filter[MAX_OCTAVES][MAX_SCALES][MAX_FILTER_LEN * MAX_FILTER_LEN];
  // Input image width and height
  uint8_t imageWidth, imageHeight;
  // Input image
  int32_t image[MAX_IMAGE_SIZE];
} InputParams;

// Function to perform 2D convolution
void convolution2D(uint8_t width,
                   uint8_t height,
                   uint8_t filterLen,
                   uint8_t fracLen,
                   int32_t *pFilter,
                   int32_t *pImage,
                   int32_t *pGauss)
{
  uint8_t w, h, k, l;
  int16_t x, y, xstart, ystart;
  int32_t sum;
  uint8_t filterLenBy2 = filterLen >> 1;
  // Loop over processed image height indices
  for (h = 0; h < height; h++)
  {
    // Loop over processed image width indices
    for (w = 0; w < width; w++)
    {
      sum = 0; // Initialize sum to 0
      ystart = h - filterLenBy2;
      // Loop over filter length height-wise
      for (l = 0; l < filterLen; l++)
      {
        y = ystart;
        ystart++;
        xstart = w - filterLenBy2;
        // Loop over filter length width-wise
        for (k = 0; k < filterLen; k++)
        {
          x = xstart;
          xstart++;
          // Skip pixels that are outside the image boundary (Cropping)
          if ((x < 0) || (y < 0)) continue;
          if ((x > width - 1) || (y > height - 1)) continue;
          // Multiply image pixel with filter coefficient and add to sum
          sum = sadd(sum,
		     fmul(pImage[y * width + x],
		     pFilter[l * filterLen + k],
		     fracLen));
	}
      }
      // Copy sum to output gaussian image location
      pGauss[h * width + w] = sum;
    }
  }
}

void main(void)
{
  uint8_t scale, octave;
  uint16_t pixel, imageSize;
  InputParams params;
  int32_t gauss[MAX_OCTAVES][MAX_SCALES][MAX_IMAGE_SIZE];
  int32_t dog[MAX_OCTAVES][MAX_SCALES - 1][MAX_IMAGE_SIZE];
  int32_t *pImage;
  // Load input parameters into structure
  loadInputParams(&params);
  // Set pImage to point to input image
  pImage = params.image;
  imageSize = params.imageWidth * params.imageHeight;
  for (octave = 0; octave < params.noOfOctaves; octave++)
  {
    // Construct gaussian images
    for (scale = 0; scale < params.noOfScales; scale++)
    {
      // Perform 2D convolution to obtain a gaussian scale image
      convolution2D(params.imageWidth,
                    params.imageHeight,
                    params.filterLen[octave][scale],
                    params.fracLen,
                    params.filter[octave][scale],
                    pImage,
                    gauss[octave][scale]);
      // Current gaussian scale image is input image for next scale
      pImage = gauss[octave][scale];
    }
    // Construct DoG images
    for (scale = 0; scale < params.noOfScales - 1; scale++)
    {
      for (pixel = 0; pixel < imageSize; pixel++)
      {
        dog[octave][scale][pixel] = gauss[octave][scale + 1][pixel] -
                                    gauss[octave][scale][pixel];
      }
    }
  }
}
