/*
 * Copyright (c) 2014, Recore Systems B.V., The Netherlands,
 * web: www.recoresystems.com, email: info@recoresystems.com
 *
 * Any reproduction in whole or in parts is prohibited
 * without the written consent of the copyright owner.
 *
 * All Rights Reserved.
 *
 */

#ifndef PGM_IMPORT_H_
#define PGM_IMPORT_H_

#include <stdint.h>

#define IMAGE_SCALING(depth) ((1 << (8 * depth)) - 1)

typedef struct {
  int width;  // width
  int height; // height
  int depth;  // pixel depth in byte (1 or 2)
  uint16_t *data;
} GraylevelImage;

extern GraylevelImage DefaultPgmImage;

int readPgm(const char *fileName, GraylevelImage *pGray);
int writePgm(const char *fileName, GraylevelImage *pGray);
void freePgm(GraylevelImage *pGray);

#endif
