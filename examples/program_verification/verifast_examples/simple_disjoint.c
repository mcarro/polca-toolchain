/*@
predicate integers(int *start, int count) =
    count <= 0 ? true : integer(start, _) &*& integers(start + 1, count - 1);
@*/

/*@

lemma void span_integer_array(int *start)
    requires integers(start, ?count) &*& count > 0;
    ensures integers(start, count);
{


}
@*/

int *malloc(int count);
    //@ requires true;
    //@ ensures integers(result, count);
    
//------------------------------------------------------------------------------
// We verify that input pointers dst and src point to different memory addresses
// by checking that on the symbolic heap there are two different heap chunks for
// both pointers
//------------------------------------------------------------------------------
void arrayAsignment(int *dst,int *src, int count)
    //@ requires integers(dst,count) &*& integers(src,count) &*& count > 0;
    //@ ensures integers(dst,count) &*& integers(src,count);
{

    //@ open integers(dst,count);
    //@ open integers(src,count);
    //@ integer_distinct(dst,src);
    //@ close integers(dst,count);
    //@ close integers(src,count);
    
    return;
}


int main()
    //@ requires true;
    //@ ensures true;
{
    // -------------------------------------------------------
    // This block of code should not verfiy since
    // v and w point to the same memory address
    // -------------------------------------------------------
    int p = 19;
    int *v = malloc(1);
    int *w = malloc(1);
    v = &p;
    w = &p;
    arrayAsignment(v,w, 1);

    // -------------------------------------------------------
    // This block of code verfies since
    // v and w do not point to the same memory address
    // -------------------------------------------------------        
    //int *v = malloc(1);
    //int *w = malloc(1);
    //arrayAsignment(v,w, 1);
   
    //@ leak integers( w, 1);    
    //@ leak integers( v, 1);
    return 0;
}
