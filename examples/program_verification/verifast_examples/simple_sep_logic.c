/*@
predicate characters(char *start, int count) =
    count <= 0 ? true : character(start, _) &*& characters(start + 1, count - 1);
@*/

/*@

lemma void span_array_chunk(char *start, int i)
    requires characters(start, ?count) &*& 0 <= i &*& i <= count;
    ensures characters(start, i) &*& characters(start + i, count - i);
{
    if (i == 0) {
        close characters(start,0);
    } else {
        open characters(start,count);
        span_array_chunk(start+1,i-1);
        close characters(start,i);
    }
}

lemma void colapse_array_chunk(char *start)
    requires characters(start, ?i) &*& characters(start + i, ?count) &*& 0 <= i &*& 0 <= count;
    ensures characters(start, i + count);
{
    open characters(start, i);
    if (i != 0) {
        colapse_array_chunk(start + 1);
        close characters(start, i + count);
    }
}

@*/

char *malloc(int count);
    //@ requires true;
    //@ ensures characters(result, count);
    
//------------------------------------------------------------------------------
// We verify that input pointers dst and src point to different memory addresses
// by checking that on the symbolic heap there are two different heap chunks for
// both pointers
//------------------------------------------------------------------------------
void arrayAsignment(char *dst,char *src, int count)
    //@ requires characters(src,count) &*& characters(dst,count);
    //@ ensures characters(src,count) &*& characters(dst,count);
{
    for(int i=0;i<count;i++)
    //@ invariant characters(src, count) &*& characters(dst, count) &*& 0 <= i;
    // invariant true;
    {    
        //@ span_array_chunk(src,i);
        //@ open characters(src+i,count-i);
        //*(dst+i) = *(src+i);
        *(src+i) = 'a';
        //@ close characters(src+i,count-i);
        //@ colapse_array_chunk(src);
    }
}


int main()
    //@ requires true;
    //@ ensures true;
{
    // -------------------------------------------------------
    // This block of code should not verfiy since
    // v and w point to the same memory address
    // -------------------------------------------------------
    //char *v = malloc(10);
    //char *w = v;
    //arrayAsignment(v,w, 10);

    // -------------------------------------------------------
    // This block of code verfies since
    // v and w do not point to the same memory address
    // -------------------------------------------------------        
    char *v = malloc(10);
    char *w = malloc(10);
    arrayAsignment(v,w, 10);
    
    //@ leak characters(w, 10);    
    //@ leak characters(v, 10);
    return 0;
}
