#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//@ #include <listex.gh>

/*@

lemma void span_array_chunk(void **start, int i)
    requires pointers(start, ?count, _) &*& 0 <= i &*& i <= count;
    ensures pointers(start, i, _) &*& pointers(start + i, count - i, _);
{
    if (i == 0) {
        close pointers(start,0,_);
    } else {
        open pointers(start,count,_);
        span_array_chunk(start+1,i-1);
        close pointers(start,i,_);
    }
}

lemma void colapse_array_chunk(void **start)
    requires pointers(start, ?i,_) &*& pointers(start + i, ?count,_) &*& 0 <= i &*& 0 <= count;
    ensures pointers(start, i + count,_);
{
    open pointers(start, i,_);
    if (i != 0) {
        colapse_array_chunk(start + 1);
        close pointers(start, i + count,_);
    }
}

@*/

//@ predicate number(int *array;) = integer(array, _) &*& malloc_block_ints(array, 1);

int main() //@ : main
    //@ requires true;
    //@ ensures true;
{
    int n = 5;
    int **numbers = malloc(n * sizeof(int **));
    if (numbers == 0) abort();
    for (int i = 0; ; i++)
        //@ requires pointers(numbers, n, _) &*& 0<=i &*& i<=n;
        //@ ensures pointers(numbers, n, _) &*& numbers[old_i..n] |-> ?ps &*& foreachp(ps, number);
    {
        if (i == n) {
            break;
        }
        int *num = malloc(sizeof(int));
        //*(num) = i;
        //@ span_array_chunk(numbers,i);
        //@ open pointers(numbers+i, n-i, _);
        numbers[i] = num;
        //if(i==3)
        //    numbers[i] = numbers[i-1];
            
        // pointer_distinct(numbers[i],numbers[i-1]);
            
        //printf("\t%p -> %d\n",numbers[i],*(numbers[i]));
        //@ close pointers(numbers+i, n-i, _);
        //@ colapse_array_chunk(numbers);
    }
    
    for (int i = 0; ; i++)
        //@ requires numbers[i..n] |-> ?ps &*& foreachp(ps, number);
        //@ ensures numbers[old_i..n] |-> _;
    {
        //@ open pointers(_, _, _);
        if (i == n) {
            break;
        }
        
        //@ open foreachp(_, _);
        free(numbers[i]);
    }
    
    free(numbers);
    // leak pointers(numbers,_,_); 
    
    return 0;
}
