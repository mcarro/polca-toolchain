
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

//#define SCALAR int
#define SCALAR double
#define M 1000

SCALAR A[M][M], B[M][M], B2[M][M], C[M][M];
int i, j, k;

clock_t start_time, stop_time;

#define START if ( (start_time = clock()) == -1) { 	\
    printf("Error calling clock");			\
    exit(1);						\
  }
#define STOP if ( (stop_time = clock()) == -1) {	\
    printf("Error calling clock");			\
    exit(1);						\
  }
#define RETURN_TIME_DBL ((double)stop_time - start_time)/CLOCKS_PER_SEC

void init_matrix (SCALAR mat[M][M]) {
for (i = 0; i< M; i++)
  for (j = 0; j < M; j++) {
    mat[i][j] = (SCALAR)(((i + j) % 7 == 0) ? 0 : i + j);
  }
}

void zero_matrix (SCALAR mat[M][M]) {
  for (i = 0; i< M; i++)
    for (j = 0; j < M; j++) {
      mat[i][j] = (SCALAR)0;
    }
}


void print_matrix (SCALAR mat[M][M]) {
  printf("\n");

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) 
      printf(" %.2f ", (double) mat[i][j]);
    printf("\n");
  }

  printf("\n");
}

// Regular
void regular_matrix_mult(void) {
  for (i = 0; i< M; i++)
    for (j = 0; j < M; j++)
      for(k = 0; k < M; k++)
	C[i][j] += A[i][k] * B[k][j];
}

void transpose_matrix(void) {
  for (i = 0; i< M; i++)
    for (j = 0; j < M; j++)
      B2[i][j] = B[j][i];
}


void transposed_matrix_mult(void) {
  for (i = 0; i< M; i++)
    for (j = 0; j < M; j++)
      for(k = 0; k < M; k++)
	C[i][j] += A[i][k] * B2[j][k];
}


SCALAR d[M];
#define P 10
int t, m;

void index_splitting_matrix_mult(void){
  for(i=0;i<M;i++)
    for(j=0;j<M;j++)   
      for(t=0;t<P;t++)
	for(m=t*(M/P);m<(t*(M/P))+M/P;m++) {         
	  d[m] = A[i][m] * B[m][j];
	  C[i][j] += d[m];
	}
}


void loop_inversion_matrix_multiplication(void) {
  for (i = 0; i< M; i++)
    for(k = 0; k < M; k++)
      for (j = 0; j < M; j++)
	C[i][j] += A[i][k] * B[k][j];
}

//SCALAR accum;
SCALAR A_point;
void cprop_loop_inversion_matrix_multiplication(void) {
  for (i = 0; i< M; i++)  
    for(k = 0; k < M; k++) {
      A_point = A[i][k];
      for (j = 0; j < M; j++)
	C[i][j] += A_point * B[k][j];
    }
}

void sparse_cprop_loop_inversion_matrix_multiplication(void) {
  for (i = 0; i< M; i++)  
    for(k = 0; k < M; k++) 
      if ((A_point = A[i][k]) != (SCALAR) 0)
      for (j = 0; j < M; j++)
	C[i][j] += A_point * B[k][j];
}


int main(int argc, char *argv[])  {

  init_matrix(A);
  init_matrix(B);

  //  print_matrix(A);
  //  print_matrix(B);
  printf("Heating up memory\n");
  zero_matrix(C);
  regular_matrix_mult();
  zero_matrix(C);
  printf("Multiplying\n");
  START;
  regular_matrix_mult();
  STOP;
  //  print_matrix(C);
  printf("Regular multiplication: %f\n\n", RETURN_TIME_DBL);

  transpose_matrix();

  printf("Heating up memory\n");
  zero_matrix(C);
  transposed_matrix_mult();
  printf("Multiplying\n");
  zero_matrix(C);
  START;
  transposed_matrix_mult();
  STOP;
  //  print_matrix(C);
  printf("Transposed matrix multiplication: %f\n\n", RETURN_TIME_DBL);

  printf("Heating up memory\n");
  zero_matrix(C);
  index_splitting_matrix_mult();
  printf("Multiplying\n");
  zero_matrix(C);
  START;
  index_splitting_matrix_mult();
  STOP;
  //  print_matrix(C);
  printf("Split index matrix multiplication: %f\n\n", RETURN_TIME_DBL);

  printf("Heating up memory\n");
  zero_matrix(C);
  loop_inversion_matrix_multiplication();
  printf("Multiplying\n");
  zero_matrix(C);
  START;
  loop_inversion_matrix_multiplication();
  STOP;
  //  print_matrix(C);
  printf("Reversed loop matrix multiplication: %f\n\n", RETURN_TIME_DBL);

  printf("Heating up memory\n");
  zero_matrix(C);
  cprop_loop_inversion_matrix_multiplication();
  printf("Multiplying\n");
  zero_matrix(C);
  START;
  cprop_loop_inversion_matrix_multiplication();
  STOP;
  //  print_matrix(C);
  printf("Reversed loop cprop matrix multiplication: %f\n\n", RETURN_TIME_DBL);

  printf("Heating up memory\n");
  zero_matrix(C);
  sparse_cprop_loop_inversion_matrix_multiplication();
  printf("Multiplying\n");
  zero_matrix(C);
  START;
  sparse_cprop_loop_inversion_matrix_multiplication();
  STOP;
  //  print_matrix(C);
  printf("Reversed loop cprop sparse matrix multiplication: %f\n\n", RETURN_TIME_DBL);


  return 0;
}


