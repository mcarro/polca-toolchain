// 1-dimensional heat dissipation (Daniel Rubio)
//MCAPI version (Salvador Tamarit)

/* 
  The problem with this version is that it does not seem not able to
  find the endpoints in different nodes. The problem appears to be
  related to the OPENMCAPI implementation or configuration. (Note:
  does it happens as well with other implementations?)  The process
  exits when trying to find the direction of a endpoint in the
  heatspread function.
 */
 
 


#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <signal.h>

#include <mcapi.h>

#define N_ELEM 12
#define N_ITER 10
#define SIZE_CHUNK 3
#define C 0.1


#define WAIT_TIMEOUT 0xFFFFFFFF

#define EP_SEND 0
#define EP_RECV 1

#define mcapi_assert_success(s) \
  if (s != MCAPI_SUCCESS) { printf("%s:%d status %d\n", __FILE__, __LINE__, s); abort(); }

static const int num_chunks = 
    N_ELEM % SIZE_CHUNK == 0? 
      N_ELEM / SIZE_CHUNK : 
      (N_ELEM / SIZE_CHUNK) + 1;
  
static const int BUFF_SIZE = sizeof(float);   
      
typedef struct {
  int  thread_id;
  float *chunk;
} thread_data;
  
void chunk(float heatmap[],float heattmap_chanked [][SIZE_CHUNK])
{
  int i,j;
  
  for(i=0; i < num_chunks; i++) 
    for(j=0; j < SIZE_CHUNK && (i*SIZE_CHUNK) + j < N_ELEM; j++) 
      heattmap_chanked[i][j] = heatmap[(i*SIZE_CHUNK) + j];
}

void join_chunks(float heatmap[],float heattmap_chanked [][SIZE_CHUNK])
{
  int i,j;
  
  for(i=0; i < num_chunks; i++) 
    for(j=0; j < SIZE_CHUNK && (i*SIZE_CHUNK) + j < N_ELEM; j++) 
      heatmap[(i*SIZE_CHUNK) + j] = heattmap_chanked[i][j];
}

void init(float heatmap[]) {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}


// Use only one array.  For large arrays this helps cache hit ration,
// which has a large impact in performance.
void heatspread_all(float heatmap[]) {
  int i;
  float prev, curr;

  prev = heatmap[0];

  for(i=1; i < N_ELEM-1; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }
}


void heatspread(float heatmap[],
    mcapi_endpoint_t local_recv_endpoint,
    mcapi_endpoint_t local_send_endpoint,
    int tid) {
  int i;
  float prev, curr;
  mcapi_status_t status;
  mcapi_endpoint_t remote_recv_endpoint_pre;
  mcapi_endpoint_t remote_recv_endpoint_fol;
  mcapi_request_t request;
  
  size_t size;
  float first;
  float last;
  
  prev = 0.0;
  
  
  if(tid != 1)
  {
    status = 0;
    while(status!=1){
      remote_recv_endpoint_fol = mcapi_get_endpoint(tid - 1,EP_RECV, &status);
      sleep(1);
    }
    mcapi_assert_success(status);
    mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint_fol,
      (void *)&heatmap[0],BUFF_SIZE,1,&request,&status);
    mcapi_assert_success(status);
  }
  if(tid != num_chunks)
  {
    status = 0;
    while(status!=1){
      remote_recv_endpoint_pre = mcapi_get_endpoint(tid + 1, EP_RECV, &status);
      sleep(1);
    }
    mcapi_assert_success(status);
    mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint_pre,
        (void *)&heatmap[SIZE_CHUNK -1],BUFF_SIZE,0,&request,&status);
    mcapi_assert_success(status);
  }
  
  if(tid != 1)
  {
    mcapi_msg_recv(local_recv_endpoint, &first, BUFF_SIZE , &size, &status);
    mcapi_assert_success(status);
    
    
    curr = heatmap[0];
    heatmap[0] = curr + (C * phi(first, curr, heatmap[1]));
    prev = curr;
  }

  for(i=1; i < SIZE_CHUNK -1 ; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }
  
  if(tid != num_chunks)
  {
    mcapi_msg_recv(local_recv_endpoint, &last, BUFF_SIZE , &size, &status);
    mcapi_assert_success(status);
    
    heatmap[i] = heatmap[i] + (C * phi(prev, heatmap[i], last));
    
  }
  
}

void printList(float *heatmap, int iter, int limit, int n) {
  int i;
  
  if(n!=-1 )
    printf("Thread %d; ", n);
  printf("i %02d:", iter);
  for(i=0; i<limit; i++) {
    printf(" %6.3f", heatmap[i]);
  }
  printf("\n");
}


void printCells(float *heatmap, int n) {
  printList(heatmap, n, N_ELEM, -1);
}

void printChunk(float *heatmap, int n, int iter) {
  printList(heatmap, iter, SIZE_CHUNK, n);
}


int main () {
  int t,i;
  pid_t pid[num_chunks+1];
  
  mcapi_status_t status;
  mcapi_version_t version;
  mcapi_endpoint_t local_send_endpoint; 
  mcapi_endpoint_t local_recv_endpoint;


  // N_ELEM is a constant.  Let's use it as a constant.
  float heatmap[N_ELEM];
  // If we want to have a better design, the heatspread function
  // should receive only the initial map and return the final one.
  // Whoever calls heatspread should not be concerned with
  // intermediate / temporal vectors / memory areas.

      
  float heatmap_chunked[num_chunks][SIZE_CHUNK];

  //could be integrated with function chunk
  init(heatmap);
  
  //*************************************
  //Sequential
  //*************************************
  
  clock_t start_s = clock();

  //printCells(heatmap, 0);
  for(i=0; i<N_ITER; i++) {
    heatspread_all(heatmap);
    //printCells(heatmap, i+1);
  }
  
  clock_t end_s = clock();
  float seconds_s = (float)(end_s - start_s) / CLOCKS_PER_SEC;
  
  //*************************************
  //Parallel
  //*************************************
  
  
  chunk(heatmap,heatmap_chunked);
 
  
  
  //for (i = 1; i <= num_chunks ; i ++)
  for (i = 1; i <= 2 ; i ++)
  {
   
 
   pid[i] = fork();
 
   if (pid[i] == -1) {
      perror("fork failed");
      exit(EXIT_FAILURE);
   }
   else if (pid[i] == 0) {
      mcapi_initialize(i, &version, &status);
      mcapi_assert_success(status);
       
      
      printf("Thread %d: Create endpoints.\n",i);
      local_send_endpoint = mcapi_create_endpoint(EP_SEND, &status);
      mcapi_assert_success(status);
      local_recv_endpoint = mcapi_create_endpoint(EP_RECV, &status);
      mcapi_assert_success(status);
      
      
      printf("Thread %d: Created.\n",i);
      
      
      for(t=0; t<N_ITER; t++) {
        heatspread(heatmap_chunked[i-1],local_recv_endpoint,local_send_endpoint,i);
        //printChunk(chunk, tid, i+1);
      }
      
      _exit(EXIT_SUCCESS);  
   } 
  }
  
  
  printf("Node 0: MCAPI Initialized\n");
  mcapi_initialize(0, &version, &status);
  mcapi_assert_success(status);
  
  
  clock_t start_p = clock();
  
  
  for(i = 1; i < num_chunks;i++)
  {
      int statusPid;
      (void)waitpid(pid[i], &statusPid, 0);
  }
  
  clock_t end_p = clock();
  float seconds_p = (float)(end_p - start_p) / CLOCKS_PER_SEC;
  
  mcapi_finalize(&status);
  

  //*************************************
  //Results
  //*************************************
  
  
  printf("Sequential answer.\n");
  //printCells(heatmap, i);
  printf("Time: %f\n.",seconds_s);
  
  join_chunks(heatmap,heatmap_chunked);
  printf("Parallel answer.\n");
  //printCells(heatmap, i);
  printf("Time: %f\n.",seconds_p);

  return 0;
}
