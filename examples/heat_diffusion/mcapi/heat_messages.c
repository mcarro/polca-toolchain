// 1-dimensional heat dissipation (Daniel Rubio)

/* 
   Comments (MCL): I am not sure what the program does, in terms of
   physical phenomena.  A heat pulse is injected at the center and the
   edges are kept at a constant temperature.  However, the heat
   dissipates in a way which is strange: there is a heat transfer
   constant between adjacent cells.  Since we aleady have this, there
   should be a heat transfer constant between the edges and the
   "external" world, and may be have this external world to be @
   constant temperature.  This constant determines the rate at which
   energy is dissipated from the plate.

   Also, the notion of iteration is strange, since no fixpoint is
   reached.  It is rather time, as we are simulating (discretizing) a
   phenomenon which happens in time.  It's clear in the math
   formulation, where there is a time differential, but it is not so
   clear in the code.  I suppose that this should be encoded in the
   units of the transfer constant C, which should be something like
   energy / time, so every iteration (= time?) some amount of energy
   is transferred / dissipated.
 */
 
 


#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

#include <mcapi.h>

#define N_ELEM 240000
#define N_ITER 1500
#define SIZE_CHUNK 30000
#define C 0.1


#define WAIT_TIMEOUT 0xFFFFFFFF

#define MAIN_SEND 1000
#define MAIN_RECV 1001

#define mcapi_assert_success(s) \
  if (s != MCAPI_SUCCESS) { printf("%s:%d status %d\n", __FILE__, __LINE__, s); abort(); }

static const int num_chunks = 
    N_ELEM % SIZE_CHUNK == 0? 
      N_ELEM / SIZE_CHUNK : 
      (N_ELEM / SIZE_CHUNK) + 1;
  
static const int BUFF_SIZE = sizeof(float);   
      
typedef struct {
  int  thread_id;
  float *chunk;
} thread_data;
  
void chunk(float heatmap[],float heattmap_chanked [][SIZE_CHUNK])
{
  int i,j;
  
  for(i=0; i < num_chunks; i++) 
    for(j=0; j < SIZE_CHUNK && (i*SIZE_CHUNK) + j < N_ELEM; j++) 
      heattmap_chanked[i][j] = heatmap[(i*SIZE_CHUNK) + j];
}

void join_chunks(float heatmap[],float heattmap_chanked [][SIZE_CHUNK])
{
  int i,j;
  
  for(i=0; i < num_chunks; i++) 
    for(j=0; j < SIZE_CHUNK && (i*SIZE_CHUNK) + j < N_ELEM; j++) 
      heatmap[(i*SIZE_CHUNK) + j] = heattmap_chanked[i][j];
}

void init(float heatmap[]) {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}


// Use only one array.  For large arrays this helps cache hit ration,
// which has a large impact in performance.
void heatspread_all(float heatmap[]) {
  int i;
  float prev, curr;

  prev = heatmap[0];

  for(i=1; i < N_ELEM-1; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }
}


void heatspread(float heatmap[],
    mcapi_endpoint_t local_recv_endpoint,
    mcapi_endpoint_t local_send_endpoint,
    int tid) {
  int i;
  float prev, curr;
  mcapi_status_t status;
  mcapi_endpoint_t remote_recv_endpoint_pre;
  mcapi_endpoint_t remote_recv_endpoint_fol;
  mcapi_request_t request;
  
  size_t size;
  float first;
  float last;
  
  prev = 0.0;
  
   /*
  For all the threads but the first, the last element from the previous 
  chunk is needed in order to compute the new value of the first element. 
  This element is received through a message and then sent for be used
  in the following iteration.
  */
  if(tid != 0)
  {
    mcapi_msg_recv(local_recv_endpoint, &first, BUFF_SIZE , &size, &status);
    mcapi_assert_success(status);
    
    
    curr = heatmap[0];
    heatmap[0] = curr + (C * phi(first, curr, heatmap[1]));
    prev = curr;
    
    remote_recv_endpoint_fol = mcapi_get_endpoint(0, tid - 1, &status);
    mcapi_assert_success(status);
    
    // Note we are using priorities to distinguish the messages to the
    // left and right of the chunk.  We should improve this: it works
    // at the moment, but it may fail if the higher-priority message
    // is sent after the lower-priority has been sent; therefore it is
    // a fragile solution. This is improved in the channels version.
    mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint_fol,
      (void *)&heatmap[0],BUFF_SIZE,1,&request,&status);
    mcapi_assert_success(status);
    
    //printf("Thread %d; First %f\n",tid,first);
  }
  

  /*
  Loop with the internal data. Not need to receive any element
  from other threads.
  */
  for(i=1; i < SIZE_CHUNK -1 ; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }


  /*
  For all the threads but the last, the firt element from the following chunk
  is needed in order to compute the new value of the last element. 
  This element is received through a message and then sent for be used
  in the following iteration.
  */
  if(tid != num_chunks - 1)
  {
    mcapi_msg_recv(local_recv_endpoint, &last, BUFF_SIZE , &size, &status);
    mcapi_assert_success(status);
    
    heatmap[i] = heatmap[i] + (C * phi(prev, heatmap[i], last));
    
    remote_recv_endpoint_pre = mcapi_get_endpoint(0, tid + 1, &status);
    mcapi_assert_success(status);
    
    mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint_pre,
      (void *)&heatmap[i],BUFF_SIZE,0,&request,&status);
    mcapi_assert_success(status);
    
    //printf("Thread %d; Last %f\n",tid,last);
  }
  
}

void printList(float *heatmap, int iter, int limit, int n) {
  int i;
  
  if(n!=-1 )
    printf("Thread %d; ", n);
  printf("i %02d:", iter);
  for(i=0; i<limit; i++) {
    printf(" %6.3f", heatmap[i]);
  }
  printf("\n");
}


void printCells(float *heatmap, int n) {
  printList(heatmap, n, N_ELEM, -1);
}

void printChunk(float *heatmap, int n, int iter) {
  printList(heatmap, iter, SIZE_CHUNK, n);
}


void* run_thread (void *t)
{
  mcapi_status_t status;
  mcapi_endpoint_t local_send_endpoint; 
  mcapi_endpoint_t local_recv_endpoint;
  mcapi_endpoint_t remote_recv_endpoint;
  mcapi_request_t request;

  int tid,i;
  float * chunk;
  float initial_msg;
  
  
  
  
  thread_data *my_data = (thread_data *)t;
  tid = my_data->thread_id;
  chunk = my_data->chunk;

  
  printf("Thread %d: Create endpoints.\n",tid);
  local_send_endpoint = mcapi_create_endpoint(tid + num_chunks, &status);
  mcapi_assert_success(status);
  local_recv_endpoint = mcapi_create_endpoint(tid, &status);
  mcapi_assert_success(status);
  
  
  
  remote_recv_endpoint = mcapi_get_endpoint(0, MAIN_RECV, &status);
  mcapi_assert_success(status);

  // Send the initial message
  initial_msg = 0.0;
  mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint,
      (void *)&initial_msg,BUFF_SIZE,1,&request,&status);
  mcapi_assert_success(status);
  
  

  /*
    All is ready now. The following loop performs the calculation of
    each chunk and the communication between threads.
  */
  for(i=0; i<N_ITER; i++) {
    heatspread(chunk,local_recv_endpoint,local_send_endpoint,tid);
    //printChunk(chunk, tid, i+1);
  }
  
  
  return NULL;
}


int main () {
  pthread_t threads[num_chunks];
  thread_data thread_data_array[num_chunks];
  int t;
  
  
  int i;
  //char msg[BUFF_SIZE];
  float initial_msg;
  size_t size;
  
  mcapi_status_t status;
  mcapi_version_t version;
  mcapi_endpoint_t local_send_endpoint; 
  mcapi_endpoint_t local_recv_endpoint;
  mcapi_endpoint_t remote_recv_endpoint_1;
  mcapi_endpoint_t remote_recv_endpoint_2;
  mcapi_request_t request;


  // N_ELEM is a constant.  Let's use it as a constant.
  float heatmap[N_ELEM];
  // If we want to have a better design, the heatspread function
  // should receive only the initial map and return the final one.
  // Whoever calls heatspread should not be concerned with
  // intermediate / temporal vectors / memory areas.

      
  float heatmap_chunked[num_chunks][SIZE_CHUNK];

  //could be integrated with function chunk
  init(heatmap);
  
  //*************************************
  //Sequential
  //*************************************
  
  clock_t start_s = clock();

  //printCells(heatmap, 0);
  for(i=0; i<N_ITER; i++) {
    heatspread_all(heatmap);
    //printCells(heatmap, i+1);
  }
  
  clock_t end_s = clock();
  float seconds_s = (float)(end_s - start_s) / CLOCKS_PER_SEC;
  
  //*************************************
  //Parallel
  //*************************************

  /*
  The idea here is to create chunks of the original heatmap and
  process each chunk in an individual thread (or, ideally, a process).
  In this computation, the processes need to communicate with each
  other in order to share their elements at both ends of the chunk.
  */

  chunk(heatmap,heatmap_chunked);

  /*
  Chunks should not be in shared memory (as they are now).  They
  should be sent to each thread and each thread should send the result
  back.
  */
  
  for(t=0; t<num_chunks; t++){
    thread_data_array[t].thread_id = t;
    thread_data_array[t].chunk = heatmap_chunked[t];
  }
  
  printf("Node 0: MCAPI Initialized\n");
  mcapi_initialize(0, &version, &status);
  mcapi_assert_success(status);
  
  printf("Create main endpoints.\n");
  local_send_endpoint = mcapi_create_endpoint(MAIN_SEND, &status);
  mcapi_assert_success(status);
  local_recv_endpoint = mcapi_create_endpoint(MAIN_RECV, &status);
  mcapi_assert_success(status);
  


  /* run all the threads */
  for(t=0; t<num_chunks; t++){
    // We create threads instead of using MTAPI because the simulation
    // API we are using only implements the MCAPI API
    pthread_create(&threads[t],NULL, 
      run_thread,(void *)&thread_data_array[t]);
  }
  

  // Here an initial message is received from all the tasks in 
  // order to assure that all threads have created their endpoints.
  for(t=0; t<num_chunks; t++){
    mcapi_msg_recv(local_recv_endpoint, &initial_msg, BUFF_SIZE, &size, &status);
    mcapi_assert_success(status);
  }
  
  printf("Received confirmation\n");
  
  clock_t start_p = clock();
  
  //In the following loop the initial chunks are sent to each task.
  for(t = SIZE_CHUNK; t < N_ELEM; t += SIZE_CHUNK)
  {
    remote_recv_endpoint_1 = mcapi_get_endpoint(0, (t/SIZE_CHUNK) -1 , &status);
    mcapi_assert_success(status); 
    
    remote_recv_endpoint_2 = mcapi_get_endpoint(0, t/SIZE_CHUNK, &status);
    mcapi_assert_success(status);
    
    mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint_1,
      (void *)&heatmap[t],BUFF_SIZE,1,&request,&status);
    mcapi_assert_success(status);
    
    mcapi_msg_send_i(local_send_endpoint,remote_recv_endpoint_2,
      (void *)&heatmap[t-1],BUFF_SIZE,0,&request,&status);
    mcapi_assert_success(status);
    
  }
  
  printf("Initial data sent.\n");
  
  for (t = 0; t < num_chunks; t++) {
    pthread_join(threads[t],NULL);
  }
  
  clock_t end_p = clock();
  float seconds_p = (float)(end_p - start_p) / CLOCKS_PER_SEC;
  
  mcapi_finalize(&status);
  

  //*************************************
  //Results
  //*************************************
  
  
  printf("Sequential answer.\n");
  //printCells(heatmap, i);
  printf("Time: %f\n.",seconds_s);
  
  join_chunks(heatmap,heatmap_chunked);
  printf("Parallel answer.\n");
  //printCells(heatmap, i);
  printf("Time: %f\n.",seconds_p);

  return 0;
}
