// 1-dimensional heat dissipation (Daniel Rubio)
//MCAPI version (Salvador Tamarit)
 


#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>

#include <mcapi.h>

#define N_ELEM 12

// More than 15 iterations make the system fail.  This does not happen
// with the simple messages version. Maybe it is related to some
// (OPENMCAPI) limit in the number of messages that could be send
// through a channel?

#define N_ITER 15
#define SIZE_CHUNK 3
#define C 0.1


#define WAIT_TIMEOUT 0xFFFFFFFF

#define MAIN_SEND 1000
#define MAIN_RECV 1001

#define mcapi_assert_success(s) \
  if (s != MCAPI_SUCCESS) { printf("%s:%d status %d\n", __FILE__, __LINE__, s); abort(); }

static const int num_chunks = 
    N_ELEM % SIZE_CHUNK == 0? 
      N_ELEM / SIZE_CHUNK : 
      (N_ELEM / SIZE_CHUNK) + 1;
  
static const int BUFF_SIZE = sizeof(float);     
      
typedef struct {
  int  thread_id;
  float *chunk;
} thread_data;
  
void chunk(float heatmap[],float heattmap_chunked [][SIZE_CHUNK])
{
  int i,j;
  
  for(i=0; i < num_chunks; i++) 
    for(j=0; j < SIZE_CHUNK && (i*SIZE_CHUNK) + j < N_ELEM; j++) 
      heattmap_chunked[i][j] = heatmap[(i*SIZE_CHUNK) + j];
}

void join_chunks(float heatmap[],float heattmap_chunked [][SIZE_CHUNK])
{
  int i,j;
  
  for(i=0; i < num_chunks; i++) 
    for(j=0; j < SIZE_CHUNK && (i*SIZE_CHUNK) + j < N_ELEM; j++) 
      heatmap[(i*SIZE_CHUNK) + j] = heattmap_chunked[i][j];
}

void init(float heatmap[]) {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}


// Use only one array.  For large arrays this helps cache hit ratio,
// which has a large impact in performance.

void heatspread_all(float heatmap[]) {
  int i;
  float prev, curr;

  prev = heatmap[0];

  for(i=1; i < N_ELEM-1; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }
}


void heatspread(float heatmap[],
  mcapi_pktchan_recv_hndl_t   send_handle_left,
  mcapi_pktchan_recv_hndl_t   recv_handle_left,
  mcapi_pktchan_recv_hndl_t   send_handle_right,
  mcapi_pktchan_recv_hndl_t   recv_handle_right,
  int tid) 
{
  int i;
  float prev, curr;
  mcapi_status_t status;
  mcapi_request_t request;
  
  size_t size;
  float* first;
  float* last;
  float first_sent;
  float last_sent;
  
  prev = 0.0;
  
  /*
  First of all, data from previous iterations are sent through the channels,
  avoiding the leftmost and righmost chunks sends. 
  */
  if(tid != 0)
  {
    first_sent = heatmap[0];
    mcapi_pktchan_send_i(send_handle_left, (void *)&first_sent,BUFF_SIZE,
            &request,&status);
    mcapi_assert_success(status);
  }
  
  if(tid != num_chunks - 1)
  {
    last_sent = heatmap[SIZE_CHUNK -1];
    mcapi_pktchan_send_i(send_handle_right, (void *)&last_sent,BUFF_SIZE,
            &request,&status);
    mcapi_assert_success(status);
  }
  
  
   /*
  For all the threads but the first, the last element from the previous 
  chunk is needed in order to compute the new value of the first element. 
  This element is received through the correponding channel.
  */
  if(tid != 0)
  {
    mcapi_pktchan_recv(recv_handle_left, (void **)&first, &size,
        &status);
    mcapi_assert_success(status);
    
    curr = heatmap[0];
    heatmap[0] = curr + (C * phi(*first, curr, heatmap[1]));
    prev = curr;
  }

  /*
  Loop with the internal data. Not need to receive any element
  from other threads.
  */
  for(i=1; i < SIZE_CHUNK -1 ; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }
  
  /*
  For all the threads but the last, the firt element from the following chunk
  is needed in order to compute the new value of the last element. 
  This element is received through the correponding channel.
  */
  if(tid != num_chunks - 1)
  {
  
    mcapi_pktchan_recv(recv_handle_right, (void **)&last, &size,
        &status);
    mcapi_assert_success(status);
    
    heatmap[i] = heatmap[i] + (C * phi(prev, heatmap[i], *last));
  }
  
}

void printList(float *heatmap, int iter, int limit, int n) {
  int i;
  
  if(n!=-1 )
    printf("Thread %d; ", n);
  printf("i %02d:", iter);
  for(i=0; i<limit; i++) {
    printf(" %6.3f", heatmap[i]);
  }
  printf("\n");
}


void printCells(float *heatmap, int n) {
  printList(heatmap, n, N_ELEM, -1);
}

void printChunk(float *heatmap, int n, int iter) {
  printList(heatmap, iter, SIZE_CHUNK, n);
}


void* run_thread (void *t)
{
  mcapi_status_t status;
  mcapi_endpoint_t local_send_endpoint_left; 
  mcapi_endpoint_t local_recv_endpoint_left;
  mcapi_endpoint_t local_send_endpoint_right; 
  mcapi_endpoint_t local_recv_endpoint_right;
  mcapi_endpoint_t remote_recv_endpoint_left;
  mcapi_endpoint_t remote_recv_endpoint_right;
  
  mcapi_request_t  request_left;
  mcapi_request_t  request_right;
  
  mcapi_request_t  send_request_left;
  mcapi_request_t  recv_request_left;
  mcapi_request_t  send_request_right;
  mcapi_request_t  recv_request_right;
  
  mcapi_pktchan_recv_hndl_t   send_handle_left;
  mcapi_pktchan_recv_hndl_t   recv_handle_left;
  mcapi_pktchan_recv_hndl_t   send_handle_right;
  mcapi_pktchan_recv_hndl_t   recv_handle_right;

  int tid,i;
  float * chunk;
  size_t size;
  
  
  
  
  thread_data *my_data = (thread_data *)t;
  tid = my_data->thread_id;
  chunk = my_data->chunk;

  /*
  Create four endpoints that ara used to create two channels.  Two
  senders, which send the side elements of the previous iteration, and
  two receivers to receive this data in a crossed way as per the
  following diagram:
  
  
CHUNK i  |  CHUNK i + 1
      S    S
        \/
        /\
      R    R
  
  The identifiers of the endpoints could be simplified (e.g. 0,1,2,3)
  using diferent MCAPI nodes for each thread (process).
  */

  printf("Thread %d: Create endpoints.\n",tid);
  local_send_endpoint_left = mcapi_create_endpoint(tid + (2 * num_chunks), &status);
  mcapi_assert_success(status);
  local_send_endpoint_right = mcapi_create_endpoint(tid + (3 * num_chunks), &status);
  mcapi_assert_success(status);
  local_recv_endpoint_left = mcapi_create_endpoint(tid, &status);
  mcapi_assert_success(status);
  local_recv_endpoint_right = mcapi_create_endpoint(tid + num_chunks, &status);
  mcapi_assert_success(status);
  
  
  /*
  The channels through which the current thread/process sends are
  created here.  The channels through which the current thread /
  process receives will be created by someone else.  We do not create
  channels for the leftmost & righmost chunks (tid==0 and tid ==
  num_chunks - 1, resp.) since there are no receivers at that end.
  */

  printf("Thread %d: Create channels.\n",tid);
  if (tid != 0)
  {
    remote_recv_endpoint_right = mcapi_get_endpoint(0, (tid - 1) + num_chunks , &status);
    mcapi_assert_success(status);
    mcapi_connect_pktchan_i(local_send_endpoint_left, 
      remote_recv_endpoint_right, &request_left, &status);
    mcapi_assert_success(status);
  }
  
  if(tid != num_chunks - 1)
  {
    remote_recv_endpoint_left = mcapi_get_endpoint(0, tid + 1, &status);
    mcapi_assert_success(status);
    mcapi_connect_pktchan_i(local_send_endpoint_right, 
      remote_recv_endpoint_left, &request_right, &status);
    mcapi_assert_success(status);
  }
  
  if (tid != 0)
  { 
    mcapi_wait(&request_left, &size, &status, WAIT_TIMEOUT); 
    mcapi_assert_success(status);
  }
  
  if(tid != num_chunks - 1)
  {
    mcapi_wait(&request_right, &size, &status, WAIT_TIMEOUT); 
    mcapi_assert_success(status);
  }
  
  
  /*
  Open the handlers
  */
  printf("Thread %d: Create handlers.\n",tid);
  if (tid != 0)
  {
    mcapi_open_pktchan_send_i(&send_handle_left, local_send_endpoint_left, 
         &send_request_left,&status);

    mcapi_open_pktchan_recv_i(&recv_handle_left, local_recv_endpoint_left, 
         &recv_request_left,&status);
  }
  
  if (tid != num_chunks - 1)
  {  
    mcapi_open_pktchan_send_i(&send_handle_right, local_send_endpoint_right, 
       &send_request_right,&status);

    mcapi_open_pktchan_recv_i(&recv_handle_right, local_recv_endpoint_right, 
       &recv_request_right,&status);
  }
   
  if (tid != 0)
  {    
    mcapi_wait(&send_request_left, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
      
    mcapi_wait(&recv_request_left, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
  }
  
  
  if (tid != num_chunks - 1)
  { 
    mcapi_wait(&send_request_right, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
    
    mcapi_wait(&recv_request_right, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
  }
  
  
   /*
    All is ready now. The following loop performs the calculation of
    each chunk and the communication between threads.
   */
  for(i=0; i<N_ITER; i++) {
    heatspread(chunk, send_handle_left, recv_handle_left,
      send_handle_right, recv_handle_right,tid);
    //printChunk(chunk, tid, i+1);
  }
  
  
  /*
  Close the channel and finish
  */
  printf("Thread %d: Close channels.\n",tid);
  if (tid != 0)
  {
    mcapi_packetchan_send_close_i(send_handle_left, 
         &send_request_left,&status);

    mcapi_packetchan_recv_close_i(recv_handle_left, 
         &recv_request_left,&status);
  }
  
  if (tid != num_chunks - 1)
  {  
    mcapi_packetchan_send_close_i(send_handle_right,
       &send_request_right,&status);

    mcapi_packetchan_recv_close_i(recv_handle_right, 
       &recv_request_right,&status);
  }
   
  if (tid != 0)
  {    
    mcapi_wait(&send_request_left, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
      
    mcapi_wait(&recv_request_left, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
  }
  
  
  if (tid != num_chunks - 1)
  { 
    mcapi_wait(&send_request_right, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
    
    mcapi_wait(&recv_request_right, &size, &status, WAIT_TIMEOUT);    
    mcapi_assert_success(status);
  }
  
  return NULL;
}


int main () {
  pthread_t threads[num_chunks];
  thread_data thread_data_array[num_chunks];
  int t, i;
  
  mcapi_status_t status;
  mcapi_version_t version;

  float heatmap[N_ELEM];  
  float heatmap_chunked[num_chunks][SIZE_CHUNK];
  
  init(heatmap);
  
  
  //*************************************
  //Parallel
  //*************************************
  
  
  /*
  The idea here is to create chunks of the original heatmap and
  process each chunk in an individual thread (or, ideally, a process).
  In this computation, the processes need to communicate with each
  other in order to share their elements at both ends of the chunk.
  */

  clock_t start_p = clock();
  
  /*
  Chunks should not be in shared memory (as they are now).  They
  should be sent to each thread and each thread should send the result
  back.
  */
  
  chunk(heatmap,heatmap_chunked);
  
  for(t=0; t<num_chunks; t++){
    thread_data_array[t].thread_id = t;
    thread_data_array[t].chunk = heatmap_chunked[t];
  }
  
  printf("Node 0: MCAPI Initialized\n");
  mcapi_initialize(0, &version, &status);
  mcapi_assert_success(status);

  /* run all the threads */
  for(t=0; t<num_chunks; t++){
    pthread_create(&threads[t],NULL, 
      run_thread,(void *)&thread_data_array[t]);
  }
  
  for (t = 0; t < num_chunks; t++) {
    pthread_join(threads[t],NULL);
  }
  
  
  mcapi_finalize(&status);
  mcapi_assert_success(status);
  
  clock_t end_p = clock();
  float seconds_p = (float)(end_p - start_p) / CLOCKS_PER_SEC;
  
    
  //*************************************
  //Sequential
  //*************************************
  
  init(heatmap);
  
  clock_t start_s = clock();

  //printCells(heatmap, 0);
  for(i=0; i<N_ITER; i++) {
    heatspread_all(heatmap);
    //printCells(heatmap, i+1);
  }
  
  clock_t end_s = clock();
  float seconds_s = (float)(end_s - start_s) / CLOCKS_PER_SEC;

  //*************************************
  //Results
  //*************************************
  
  
  printf("Sequential answer.\n");
  //printCells(heatmap, i);
  printf("Time: %f\n.",seconds_s);
  
  join_chunks(heatmap,heatmap_chunked);
  printf("Parallel answer.\n");
  //printCells(heatmap, i);
  printf("Time: %f\n.",seconds_p);

  return 0;
}
