#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#define LOCAL_N_ELEM 10
#define N_ITER 10
#define C 0.1

void create(float **heatmap, float **heatmap_tmp) {
  *heatmap = malloc(sizeof(float) * LOCAL_N_ELEM+2);
  *heatmap_tmp = malloc(sizeof(float) * LOCAL_N_ELEM+2);
}

void destroy(float *heatmap, float *heatmap_tmp) {
  free(heatmap);
  free(heatmap_tmp);
}

void init(int size, int rank, float *heatmap) {
  int i;
  for(i=0; i<LOCAL_N_ELEM+2; i++) {
    heatmap[i] = 0.0;
  }

  if(rank == size/2)
    heatmap[LOCAL_N_ELEM/2] = 100;
}

float phi(float x, float y, float z) {
  return x - (2*y) + z;
}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// THIS IS THE COMPUTING KERNEL                       //
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
void heatspread(int size, int rank, float **heatmap, float **heatmap_tmp) {
  MPI_Status status;
  int i;
  float *h;
  float *ht;

  h = *heatmap;
  ht = *heatmap_tmp;

  /* Send up unless I'm at the top, then receive from below */
  if (rank < size - 1)
    MPI_Send(&h[LOCAL_N_ELEM], 1, MPI_FLOAT, rank + 1, 0,
	     MPI_COMM_WORLD );
  if (rank > 0)
    MPI_Recv( &h[0], 1, MPI_FLOAT, rank - 1, 0, MPI_COMM_WORLD,
	      &status );
  /* Send down unless I'm at the bottom */
  if (rank > 0)
    MPI_Send( &h[1], 1, MPI_FLOAT, rank - 1, 1, MPI_COMM_WORLD );
  if (rank < size - 1)
    MPI_Recv( &h[LOCAL_N_ELEM+1], 1, MPI_FLOAT, rank + 1, 1,
	      MPI_COMM_WORLD, &status );

  for(i=1; i<LOCAL_N_ELEM+1; i++) {
    ht[i] = h[i] + (C * phi(h[i-1], h[i], h[i+1]));
  }

  //  ht[0] = h[0];
  //  ht[N_ELEM] = h[N_ELEM];

  *heatmap = ht;
  *heatmap_tmp = h;
}
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=1; i<LOCAL_N_ELEM+1; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}

int main (int argc, char *argv[]) {
  int size, rank;

  MPI_Init( &argc, &argv );

  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );

  printf("The problem size is : %i(num_of_mpi_tasks)*%i(LOCAL_N_ELEM)=%i\n", size, LOCAL_N_ELEM, size*LOCAL_N_ELEM);

  int i;
  float *heatmap;
  float *heatmap_tmp;

  create(&heatmap, &heatmap_tmp);
  init(size,rank,heatmap);

  printCells(heatmap, 0);
  for(i=0; i<N_ITER; i++) {
    heatspread(size,rank, &heatmap, &heatmap_tmp);
    printCells(heatmap, i+1);
  }

  destroy(heatmap, heatmap_tmp);

  MPI_Finalize();

  return 0;
}
