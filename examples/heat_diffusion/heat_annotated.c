// 1-dimensional heat dissipation (Daniel Rubio)

/* 
   Comments (MCL): I am not sure what the program does, in terms of
   physical phenomena.  A heat pulse is injected at the center and the
   edges are kept at a constant temperature.  However, the heat
   dissipates in a way which is strange: there is a heat transfer
   constant between adjacent cells.  Since we aleady have this, there
   should be a heat transfer constant between the edges and the
   "external" world, and may be have this external world to be @
   constant temperature.  This constant determines the rate at which
   energy is dissipated from the plate.

   Also, the notion of iteration is strange, since no fixpoint is
   reached.  It is rather time, as we are simulating (discretizing) a
   phenomenon which happens in time.  It's clear in the math
   formulation, where there is a time differential, but it is not so
   clear in the code.  I suppose that this should be encoded in the
   units of the transfer constant C, which should be something like
   energy / time, so every iteration (= time?) some amount of energy
   is transferred / dissipated.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N_ELEM 21
#define N_ITER 1
#define K 0.1


#pragma polca function init(heatmap:N_ELEM:o)
#pragma polca calc heatmap[N_ELEM/2] = 100.0
#pragma polca calc heatmap[i | {0..N_ELEM/2 - 1} U {N_ELEM/2 + 1..N_ELEM-1}] = 0.0  
void init(float *heatmap) {
  int i;

  for(i=0; i<N_ELEM; i++) {
    heatmap[i] = 0.0;
  }

  heatmap[10] = 100.0;
}

#pragma polca kernel g(l,c,r)
float g(float l, float c, float r) {
  return c + K * (l + r - 2 * c);
}


#pragma polca function heatspread(heatmap:N_ELEM:io)
#pragma polca calc heatmap[0] = 0.0
#pragma polca calc heatmap[N_ELEM-1] = 0.0
#pragma polca calc heatmap[i | i in {1..N_ELEM-2}] = g(heatmap[i-1],heatmap[i],heatmap[i+1])  
void heatspread(float *heatmap) {
  int i;
  float heatmap_tmp[N_ELEM];

  for(i=1; i<N_ELEM-1; i++) {
    heatmap_tmp[i] = g(heatmap[i-1], heatmap[i], heatmap[i+1]);
  }

  heatmap_tmp[0] = 0;
  heatmap_tmp[N_ELEM-1] = 0;

  memcpy((void*)heatmap, (void*)heatmap_tmp, N_ELEM * sizeof(float));
}

// void heatspread(float **heatmap, float **heatmap_tmp) {
//   int i;
//   float *h;
//   float *ht;

//   h = *heatmap;
//   ht = *heatmap_tmp;


//   for(i=1; i<N_ELEM-1; i++) {
//     ht[i] = g(h[i-1], h[i], h[i+1]);
//   }

//   ht[0] = h[0];
//   ht[N_ELEM-1] = h[N_ELEM-1];

//   *heatmap = ht;
//   *heatmap_tmp = h;
// }

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}


#pragma polca function main(N_ELEM,N_ITER) = core(heatmap,N_ITER) $ init(heatmap) $ def heatmap:N_ELEM:float
int main(void) {
  int i;
  float *heatmap;

  heatmap = malloc(sizeof(float) * N_ELEM);

  init(heatmap);


#pragma polca function core(heatmap:N_ELEM:io, N_ITER) = (iteration heatspread heatmap)!!N_ITER 
  {
    for(i=0; i<N_ITER; i++) {
      heatspread(heatmap);
      //printCells(heatmap, i+1);
    }
  }

  printCells(heatmap, N_ITER);
  free(heatmap);

  return 0;
}
