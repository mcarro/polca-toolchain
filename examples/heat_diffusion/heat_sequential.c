// 1-dimensional heat dissipation (Daniel Rubio)

/* 
   Comments (MCL): I am not sure what the program does, in terms of
   physical phenomena.  A heat pulse is injected at the center and the
   edges are kept at a constant temperature.  However, the heat
   dissipates in a way which is strange: there is a heat transfer
   constant between adjacent cells.  Since we aleady have this, there
   should be a heat transfer constant between the edges and the
   "external" world, and may be have this external world to be @
   constant temperature.  This constant determines the rate at which
   energy is dissipated from the plate.

   Also, the notion of iteration is strange, since no fixpoint is
   reached.  It is rather time, as we are simulating (discretizing) a
   phenomenon which happens in time.  It's clear in the math
   formulation, where there is a time differential, but it is not so
   clear in the code.  I suppose that this should be encoded in the
   units of the transfer constant C, which should be something like
   energy / time, so every iteration (= time?) some amount of energy
   is transferred / dissipated.
 */


#include <stdlib.h>
#include <stdio.h>

#define N_ELEM 10
#define N_ITER 10
#define C 0.1

void init(float heatmap[]) {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

// Use only one array.  For large arrays this helps cache hit ration,
// which has a large impact in performance.
void heatspread(float heatmap[]) {
  int i;
  float prev, curr;

  prev = heatmap[0];

  for(i=1; i < N_ELEM-1; i++) {
    curr = heatmap[i];
    heatmap[i] = curr + (C * phi(prev, curr, heatmap[i+1]));
    prev = curr;
  }
}

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}

int main(void) {
  int i;
  // N_ELEM is a constant.  Let's use it as a constant.
  float heatmap[N_ELEM];

  // If we want to have a better design, the heatspread function
  // should receive only the initial map and return the final one.
  // Whoever calls heatspread should not be concerned with
  // intermediate / temporal vectors / memory areas.

  init(heatmap);

  printCells(heatmap, 0);
  for(i=0; i<N_ITER; i++) {
    heatspread(heatmap);
    printCells(heatmap, i+1);
  }

  return 0;
}
