#pragma polca def XXX
Do not touch the lines of this code block unless there are other polca pragma's inside
(but you can touch the functions called within this block)

//TODO:
// - Boundary
// - Memory



#pragma polca itn heatmap:i N_ITER heatmap:o
let heatmap' = itn HEATSPREAD heatmap N_ITER


//we assume that the tool detects that within the block of the for-loop there is a function call thus the tool will search for polca annotation inside the callee function body
//HEATSPREAD = printCells(heatmap, i+1) . heatspread(heatmap) 
HEATSPREAD v = heatspread v



#pragma polca zipWith3 heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
heatspread heatmap = zipWith3 G slide(heatmap, -1) heatmap slide(heatmap, +1) 

//let heatmap_tmp = zipWith3 G (heatmap-1) heatmap (heatmap+1)  
//let heatmap_tmp = zipWith3 G slide(heatmap, -1) heatmap slide(heatmap, +1) 

// We assume here that the callee function will be treated as a black box so we the user has not to provide a function definition of g in the polca annotation.
// Since this function is treated as a black box, then it will be inserted "as it is" in the final transformed code
G x y z = g x y z


/////////////////////////////////////////////////////////////////////////////
// Jan's stencil:
/////////////////////////////////////////////////////////////////////////////

slWindow f x0 xs
foldl f x0 xs
slWindow g xs
slW g n (x:xs) = if (length (x:xs) < n)   then   [ ]   else  g (take n (x:xs)  :  slW g n xs




