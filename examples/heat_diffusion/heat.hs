module DoW where

g l c r =
	 c + 0.1 * (l + r - 2 * c)

left xs = tail xs ++ [0]

right xs = 0 : init xs 

heatspread1 heatmap = 
	0:(mid_result ++ [0])
  where 
  	heatmap' = tail(init heatmap) --[heatmap!!i | i <- [1 .. length heatmap - 2]]
  	mid_result = zipWith3 g (left heatmap') heatmap' (right heatmap')
 
heatspread2 heatmap = 
	0:(mid_result ++ [0])
 where
 	mid_result = 
 		 (map (\i -> g  (heatmap!!(i-1)) (heatmap!!i)  (heatmap!!(i+1)) )
           [1 .. length heatmap - 2]) 

heat heatmap =
   iterate heatspread1 heatmap

main =
  (heat ([0 | x <- [1..10]] ++ (100 : [0 | x <- [1..10]]))) !! 100