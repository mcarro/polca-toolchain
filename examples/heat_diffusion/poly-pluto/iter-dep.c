#define N 10
#define K 2

main () {

  // Ejemplo 1:

  int i, j;

  for (i = 0; i < N-K; i++) {
    w[i] = 0;
    for (j = 0; j < K; j++)
      // w[i] depende solo de {w[i], v[i], v[i+1], ..., v[i+K-1]}
      w[i] = w[i] + v[j+i];
  }

  // para convertirlo en 

  for (i = 0; i < N-K; i++) {
    w[i] = 0;
    w[i] = v[i] + v[i+1] + /* ... + */ v[i+K-1] ;
  }

  // Ejemplo 2:

  w[0] = 0;
  for (i = 0; i < N-K; i++) {
    if (i > 0)
      w[i] = w[i-1];
    for (j = 0; j < K; j++)
      // w[i] depende solo de {w[i], v[i], v[i+1], ..., v[i+K-1]}
      w[i] = w[i] + v[j+i];
  }

  // para convertirlo en 

  for (i = 0; i < N-K; i++) {
    w[i] = 0;
    w[i] = v[i] + v[i+1] + ... + v[i+K-1] ;
  }

}
