#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>


#define N_ELEM 10
#define N_ITER 10
#define C 0.1
float heatmap[N_ELEM];
//#define N 1000
//#define T 500
//double a[N][N];

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

void init() {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

/*void init_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            a[i][j] = i*i+j*j;
        }
    }
}*/

/*
void printCells(int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}
*/

/*void print_array()
{
    int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            fprintf(stderr, "%0.2lf ", a[i][j]);
            if (j%80 == 20) fprintf(stderr, "\n");
        }
    }
    fprintf(stderr, "\n");
}*/

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

int main()
{
    //int i, j, k, t;
    int i, t;
    float prev, curr;

    double t_start, t_end;

    //init_array() ;
    init(heatmap);

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

#pragma scop
    for (t=0; t<=N_ITER; t++)  {
    		prev = heatmap[0];
        for (i=1; i<N_ELEM-1; i++)  {
        		curr = heatmap[i];
    				heatmap[i] = curr + (C * (prev + heatmap[i+1] - 2*curr)); 
    				prev = curr;
        		/*
            for (j=1; j<=N-2; j++)  {
                a[i][j] = (a[i-1][j-1] + a[i-1][j] + a[i-1][j+1] 
                        + a[i][j-1] + a[i][j] + a[i][j+1]
                        + a[i+1][j-1] + a[i+1][j] + a[i+1][j+1])/9.0;
            }*/
        }
    }
#pragma endscop

    IF_TIME(t_end = rtclock());
    IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

#ifdef TEST
    print_array();
#endif
    return 0;
}
