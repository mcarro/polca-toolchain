#include <stdlib.h>
#include <stdio.h>

#define N_ELEM 10
#define N_ITER 10
#define C 0.1

void create(float **heatmap, float **heatmap_tmp) {
  *heatmap = malloc(sizeof(float) * N_ELEM);
  *heatmap_tmp = malloc(sizeof(float) * N_ELEM);
}

void destroy(float *heatmap, float *heatmap_tmp) {
  free(heatmap);
  free(heatmap_tmp);
}

void init(float *heatmap) {
  int i;

  for(i=0; i<N_ELEM; i++) {
    heatmap[i] = 0.0;
  }

  heatmap[5] = 100.0;
}

float phi(float x, float y, float z) {
  return x - (2*y) + z;
}

void heatspread(float **heatmap, float **heatmap_tmp) {
  int i;
  float *h;
  float *ht;

  h = *heatmap;
  ht = *heatmap_tmp;

  for(i=1; i<N_ELEM-1; i++) {
    ht[i] = h[i] + (C * phi(h[i-1], h[i], h[i+1]));
  }

  ht[0] = h[0];
  ht[N_ELEM-1] = h[N_ELEM-1];

  *heatmap = ht;
  *heatmap_tmp = h;
}

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}

int main(void) {
  int i;
  float *heatmap;
  float *heatmap_tmp;

  create(&heatmap, &heatmap_tmp);
  init(heatmap);

  printCells(heatmap, 0);
  for(i=0; i<N_ITER; i++) {
    heatspread(&heatmap, &heatmap_tmp);
    printCells(heatmap, i+1);
  }

  destroy(heatmap, heatmap_tmp);

  return 0;
}
