#include <stdlib.h>
#include <stdio.h>

#define N_ELEM 10
#define N_ITER 100
#define C 0.1

void init(float *heatmap) {
  int i;

  for(i=0; i<N_ELEM; i++) {
    heatmap[i] = 0.0;
  }

  heatmap[5] = 100.0;
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

void swap_input(float *source, float *target, int size) {
  int i;

  for(i=0;i<size;i++)
    target[i] = source[i];
}

void heatspread(float **heatmap, float **heatmap_tmp) {
}

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}

int main(void) {
  int i,t;

  // Annotation to define the scalar field over which the vector space is defined
  // This annotation can be automatically generated. Scalar operations can be defined
  // In case the scalar ops. are the C ops. associated to the type (float) it can be 
  // omitted (i.e. associate default C ops.)
  // Possibility to define to use different ones is given to the user 
#pragma polca scalar_field(F,float,+,-,*,/)
  // Annotation to define the vector space V over the previous scalar field F
  // Operations associated to V are stated
#pragma polca vector_space(V,F,OP1,OP2)
  // C variables that belong to V space
#pragma polca vector_space_vars(V,[heatmap,heatmap_tmp])
  float heatmap[N_ELEM];
  float heatmap_tmp[N_ELEM];

  init(heatmap);

  printCells(heatmap, 0);
  for(t=0;t<N_ITER;t++) {   

  // Definition of OP1 with input and output variables 
#pragma polca def OP1 in:heatmap out:heatmap_tmp 
{ 
    for(i=1; i<N_ELEM-1; i++) {
      heatmap_tmp[i] = heatmap[i] + (C*heatmap[i-1] - 2*C*heatmap[i] + C*heatmap[i+1]));
    }

    heatmap_tmp[0] = heatmap[0];
    heatmap_tmp[N_ELEM-1] = heatmap[N_ELEM-1];
}

  // Definition of OP2 with input and output variables
#pragma polca def OP2 in:heatmap_tmp out:heatmap 
{ 
    swap_input(heatmap_tmp,heatmap,N_ELEM);
}
  }

  printCells(heatmap_tmp, t-1);

  return 0;
}
