let heatmap' = itn HEATSPREAD heatmap N_ITER


// In this case HEATSPREAD is the composition of heatspread, B (boundary update) and Mem
HEATSPREAD v = heatspread v; B v; Mem v


heatspread heatmap = zipWith3 G slide(heatmap, -1) heatmap slide(heatmap, +1) 

G x y z = g x y z

B xs =  boundary xs len(xs) len(xs)  0.0

boundary x:xs n1 n2 val = 
	 if n1==n2  [val, boundary xs n1 len(xs) val]
	 if n2==0 val
 	 else [x, boundary xs n1 len(xs) val] 

Mem x:heatmap y:heatmap_tmp = [y, Mem heatmap heatmap_tmp]
Mem x:[] y:[] = y  

------------------------------------------------------------------------------

let heatmap' = itn HEATSPREAD heatmap N_ITER


// In this case HEATSPREAD is the composition of heatspread, B (boundary update) and Mem
HEATSPREAD v = heatspread v; B v; Mem v


-- There should be a enclosing call to concat function to have exactly the same result
-- SIZE_OF_CHUNK will be a variable (could be a parameter) that will be given a value in the final MPI code
-- From this transformated specification we considered two options: 
-- 		- Generate the MPI code directly from this specification 
--		- Generate an intermediate imperative sequenctial version and then generate the MPI form that one
-- In both cases we need a way to idenitify the relevant parts for the MPI transformation, i.e. init, etc.
heatspread heatmap = map applyG chunks
	where
		applyG v = zipWith3 G slide(v, -1) slide(v,0) slide(v, +1) 
		chunks = splitEvery SIZE_OF_CHUNK heatmap

-- PSEUDO SEQUENTIAL CODE
--for splitEvery
--for chunks 
--	for 0..SIZE_OF_CHUNK
--		G(,,)



G x y z = g x y z

B xs =  boundary xs len(xs) len(xs)  0.0

boundary x:xs n1 n2 val = 
	 if n1==n2  [val, boundary xs n1 len(xs) val]
	 if n2==0 val
 	 else [x, boundary xs n1 len(xs) val] 

Mem x:heatmap y:heatmap_tmp = [y, Mem heatmap heatmap_tmp]
Mem x:[] y:[] = y  