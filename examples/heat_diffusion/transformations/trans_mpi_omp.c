// 1-dimensional heat dissipation

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <mpi.h>

int rank;
int size;
int local_n_elem;
int n_iter;
int n_elem;

#define K 0.1

#pragma polca def init heatmap:o
void init(float **heatmap,float **heatmap_tmp) {
	n_elem = 21;
	n_iter = 10;
	local_n_elem = n_elem/size;

	*heatmap = malloc(sizeof(float) * local_n_elem+2);
	*heatmap_tmp = malloc(sizeof(float) * local_n_elem+2);

	int i;
	for(i=0; i<local_n_elem+2; i++) {
		(*heatmap)[i] = 0.0;
		(*heatmap_tmp)[i] = 0.0;
	}
	
	if((n_elem/2)/local_n_elem == rank)
		(*heatmap)[(n_elem/2)%local_n_elem + 1] = 100;
}



#pragma polca kernel g l:i c:i r:i
float g(float l, float c, float r) {
  return c + K * (l + r - 2 * c);
}

  
void heatspread(float *heatmap,float *heatmap_tmp) {
//   int i;

// #pragma polca zipWith3 G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
//   for(i=1; i<n_elem-1; i++)
//     {
// #pragma polca def G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
//       heatmap_tmp[i] = g(heatmap[i-1], heatmap[i], heatmap[i+1]);
//     }

  int iter;

#pragma polca zipWith3 G heatmap[iter-1]:i heatmap[iter]:i heatmap[iter+1]:i heatmap_tmp[iter]:o
{
  MPI_Status status;
  if (rank < size - 1) 
    MPI_Send(&heatmap[local_n_elem], 1, MPI_FLOAT, rank + 1, 0,
       MPI_COMM_WORLD );
  if (rank > 0)
    MPI_Recv( &heatmap[0], 1, MPI_FLOAT, rank - 1, 0, MPI_COMM_WORLD,
        &status );
  
  if (rank > 0)
    MPI_Send( &heatmap[1], 1, MPI_FLOAT, rank - 1, 1, MPI_COMM_WORLD );
  if (rank < size - 1)
    MPI_Recv( &heatmap[local_n_elem+1], 1, MPI_FLOAT, rank + 1, 1,
        MPI_COMM_WORLD, &status );

#pragma omp parallel for 
  for(iter=1; iter<local_n_elem+1; iter++)
  #pragma polca def G heatmap[iter-1]:i heatmap[iter]:i heatmap[iter+1]:i heatmap_tmp[iter]:o 
  {
    heatmap_tmp[iter] = g(heatmap[iter-1], heatmap[iter], heatmap[iter+1]);
  }
}


#pragma polca def B
    if (0/local_n_elem == rank)
    heatmap_tmp[0%local_n_elem + 1] = 0.0;
  if ((n_elem-1)/local_n_elem == rank)
    heatmap_tmp[(n_elem-1)%local_n_elem + 1] = 0.0;


#pragma polca MemCopy heatmap_tmp:i heatmap:i
  memcpy((void*)heatmap, (void*)heatmap_tmp, (local_n_elem+2) * sizeof(float));

}


void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<n_elem; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}


int main(void) {
  int i;
  float *heatmap,*heatmap_tmp;

#pragma polca def INIT heatmap:o
  MPI_Init( &argc, &argv );
  
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );
  init(&heatmap,&heatmap_tmp);
  
#pragma polca itn HEATSPREAD INIT N_ITER heatmap:o
  for(i=0; i<n_iter; i++)
  {
#pragma polca def HEATSPREAD heatmap:i heatmap:o
    heatspread(heatmap, heatmap_tmp);
    //printCells(heatmap, i+1);
  }

  printCells(heatmap, n_iter);
  free(heatmap);
  free(heatmap_tmp);

  return 0;
}
