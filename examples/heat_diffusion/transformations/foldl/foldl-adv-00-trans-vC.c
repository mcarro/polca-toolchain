#include <stdio.h>

#define NELEM 12


int addAll(int *vs, size_t n) {
  int i;
  int total;

  int j; //we need to add and extra iterator variable
  int pts[3]; // partial accumulate, the 3 comes from the splitEvery parm.

  //original annotation:
  //#pragma polca foldl (+) 0 (map (*2) vs)
  //is transformed into: (see D2.1 page 19)
  //#pragma polca foldl2vec (+) 0 (splitEvery 3 (map (*2) vs))
  {
    for(j=0; j<3; j++)
      pts[j] = 0;

    for(i=0; i<n/3; i++) {
      for(j=0; j<3; j++) {
	pts[j] = pts[j] + (2 * vs[i*3+j]);
      }
    }

    total = 0;
    for(j=0; j<3; j++) {
      total = total + pts[j];
    }
  }
  //SALVA: Same than VB
  return total;
}


int main (void) {
  int vs[NELEM];
  int i;
  int result;

  //INIT array... not really important for the example
  for(i=0; i<NELEM; i++) {
    vs[i] = i+1;
  }


  result = addAll(vs, NELEM);

  printf("Total: %d\n", result);
  

  return 0;
}
