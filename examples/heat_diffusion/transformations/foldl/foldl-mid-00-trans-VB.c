#include <stdio.h>

#define NELEM 12


int addAll(int *vs, size_t n) {
  int i;
  int total;

  int j; //we need to add and extra iterator variable
  int pts[n/3]; // partial accumulate, the 3 comes from the splitEvery parm.

  //original annotation:
  //#pragma polca foldl (+) 0 vs
  //is transformed into: (see D2.1 page 15)
  //#pragma polca foldl2par (+) 0 (splitEvery 3 vs)
  {
    for(i=0; i<n/3; i++) {
      pts[i] = 0;
      for(j=0; j<3; j++) {
	pts[i] = pts[i] + vs[i*3+j];
3:  copy_vs[i*3+j] = vs[i*3+j];
      }
    }

    total = 0;
    for(i=0; i<n/3; i++) {
      total = total + pts[i];
    }
  }
  //SALVA: It is not possible to apply transformations for statements 1 and 2. There is a problem with sum if we change the iteration domain.
  //SALVA: The statement 3 needs to be placed in a particular loop. We need a way to know where to place this statement
  return total;
}


int main (void) {
  int vs[NELEM];
  int i;
  int result;

  //INIT array... not really important for the example
  for(i=0; i<NELEM; i++) {
    vs[i] = i+1;
  }


  result = addAll(vs, NELEM);

  printf("Total: %d\n", result);
  

  return 0;
}
