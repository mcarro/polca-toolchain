#include <stdio.h>

#define NELEM 12


int addAll(int *vs, size_t n) {
  int i;
  int total;
  int int_res[n];
  int copy_vs[n];

  // this pragma states the next block is a foldl, with operation (+)
  // it might be important to know what operation it is
  // as if it is not associative, we might not be able to transform it
#pragma polca foldl (+) 0 (map (*2) vs)
  {
    total = 0;
    for(i=0; i<n; i++) {
      total = total + (2 * vs[i]);
    }
  }


  return total;
}


int main (void) {
  int vs[NELEM];
  int i;
  int result;

  //INIT array... not really important for the example
  for(i=0; i<NELEM; i++) {
    vs[i] = i+1;
  }


  result = addAll(vs, NELEM);

  printf("Total: %d\n", result);
  

  return 0;
}
