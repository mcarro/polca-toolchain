#include <stdio.h>

#define NELEM 12


int addAll(int *vs, size_t n) {
  int i;
  int total;

  int j; //we need to add and extra iterator variable

  //original annotation:
  //#pragma polca foldl (+) 0 (map (*2) vs)
  //is transformed into: (see D2.1 page 11)
  //#pragma polca foldl2seq (+) 0 (splitEvery 3 (map (*2) vs))
  {
    total = 0;
    for(i=0; i<n/3; i++) {
      for(j=0; j<3; j++) {
//SALVA: The transformation is straightforward
    total = total + (2 * vs[i*3+j]);
      }
    }
  }

  return total;
}


int main (void) {
  int vs[NELEM];
  int i;
  int result;

  //INIT array... not really important for the example
  for(i=0; i<NELEM; i++) {
    vs[i] = i+1;
  }


  result = addAll(vs, NELEM);

  printf("Total: %d\n", result);
  

  return 0;
}
