// 1-dimensional heat dissipation (Daniel Rubio)

/* 
   Comments (MCL): I am not sure what the program does, in terms of
   physical phenomena.  A heat pulse is injected at the center and the
   edges are kept at a constant temperature.  However, the heat
   dissipates in a way which is strange: there is a heat transfer
   constant between adjacent cells.  Since we aleady have this, there
   should be a heat transfer constant between the edges and the
   "external" world, and may be have this external world to be @
   constant temperature.  This constant determines the rate at which
   energy is dissipated from the plate.

   Also, the notion of iteration is strange, since no fixpoint is
   reached.  It is rather time, as we are simulating (discretizing) a
   phenomenon which happens in time.  It's clear in the math
   formulation, where there is a time differential, but it is not so
   clear in the code.  I suppose that this should be encoded in the
   units of the transfer constant C, which should be something like
   energy / time, so every iteration (= time?) some amount of energy
   is transferred / dissipated.
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N_ELEM 21
#define N_ITER 1
#define K 0.1

void init(float *heatmap) {
  int i;

  for(i=0; i<N_ELEM; i++) {
    heatmap[i] = 0.0;
  }

  heatmap[10] = 100.0;
}


float g(float l, float c, float r) {
  return c + K * (l + r - 2 * c);
}

void heatspread(float *heatmap) {
  int i;
  float heatmap_tmp[N_ELEM];


//********************************************

//  PSEUDO SEQUENTIAL CODE
// for splitEvery
// for chunks 
//   for 0..SIZE_OF_CHUNK
//     G(,,) 

  int t1,t2;
  float chunk[N_ELEM/SIZE_OF_CHUNK][SIZE_OF_CHUNK];

  for(t1 = 0; t1 < N_ELEM/SIZE_OF_CHUNK; t1++)
    for(t2 = 0; t2 < SIZE_OF_CHUNK; t2++)
      chunk[t1][t2] = heatmap[t1*SIZE_OF_CHUNK + t2];
  for(t1 = 0; t1 < N_ELEM/SIZE_OF_CHUNK; t1++)
    for(t2 = 0; t2 < SIZE_OF_CHUNK; t2++)
      heatmap_tmp[t1*SIZE_OF_CHUNK + t2] = 
        g(chunk[t1][t2-1],chunk[t1][t2],chunk[t1][t2+1])


//********************************************

//********************************************

//  PSEUDO SEQUENTIAL CODE
// for splitEvery
// for chunks 
//   for 0..SIZE_OF_CHUNK
//     G(,,) 

  int t1,t2;

  for(t1 = 0; t1 < N_ELEM/SIZE_OF_CHUNK; t1++)
    for(t2 = 0; t2 < SIZE_OF_CHUNK; t2++)
      heatmap_tmp[t1*SIZE_OF_CHUNK + t2] = 
        g(heatmap[t1*SIZE_OF_CHUNK + t2 - 1],
          heatmap[t1*SIZE_OF_CHUNK + t2],
          heatmap[t1*SIZE_OF_CHUNK + t2 + 1])


//********************************************

  // for(i=1; i<N_ELEM-1; i++)
  // {
  //     heatmap_tmp[i] = g(heatmap[i-1], heatmap[i], heatmap[i+1]);
  // }

  heatmap_tmp[0] = 0;
  heatmap_tmp[N_ELEM-1] = 0;

  memcpy((void*)heatmap, (void*)heatmap_tmp, N_ELEM * sizeof(float));
}

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}


int main(void) {
  int i;
  float *heatmap;

  heatmap = malloc(sizeof(float) * N_ELEM);

  init(heatmap);

    for(i=0; i<N_ITER; i++)
    {
	heatspread(heatmap);
	//printCells(heatmap, i+1);
    }


  printCells(heatmap, N_ITER);
  free(heatmap);

  return 0;
}
