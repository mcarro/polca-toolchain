// 1-dimensional heat dissipation (Daniel Rubio)

/* 
   Comments (MCL): I am not sure what the program does, in terms of
   physical phenomena.  A heat pulse is injected at the center and the
   edges are kept at a constant temperature.  However, the heat
   dissipates in a way which is strange: there is a heat transfer
   constant between adjacent cells.  Since we aleady have this, there
   should be a heat transfer constant between the edges and the
   "external" world, and may be have this external world to be @
   constant temperature.  This constant determines the rate at which
   energy is dissipated from the plate.

   Also, the notion of iteration is strange, since no fixpoint is
   reached.  It is rather time, as we are simulating (discretizing) a
   phenomenon which happens in time.  It's clear in the math
   formulation, where there is a time differential, but it is not so
   clear in the code.  I suppose that this should be encoded in the
   units of the transfer constant C, which should be something like
   energy / time, so every iteration (= time?) some amount of energy
   is transferred / dissipated.
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define LOCAL_N_ELEM 10
#define N_ITER 1
#define K 0.1

void init(int size, int rank, float *heatmap) {
  int i;
  for(i=0; i<LOCAL_N_ELEM+2; i++) {
    heatmap[i] = 0.0;
  }

  if(rank == size/2)
    heatmap[LOCAL_N_ELEM/2] = 100;
}


float g(float l, float c, float r) {
  return c + K * (l + r - 2 * c);
}

void heatspread(float *heatmap) {
  int i;
  float heatmap_tmp[LOCAL_N_ELEM+2];

  /* Send up unless I'm at the top, then receive from below */
  if (rank < size - 1)
    MPI_Send(&h[LOCAL_N_ELEM], 1, MPI_FLOAT, rank + 1, 0,
	     MPI_COMM_WORLD );
  if (rank > 0)
    MPI_Recv( &h[0], 1, MPI_FLOAT, rank - 1, 0, MPI_COMM_WORLD,
	      &status );
  /* Send down unless I'm at the bottom */
  if (rank > 0)
    MPI_Send( &h[1], 1, MPI_FLOAT, rank - 1, 1, MPI_COMM_WORLD );
  if (rank < size - 1)
    MPI_Recv( &h[LOCAL_N_ELEM+1], 1, MPI_FLOAT, rank + 1, 1,
	      MPI_COMM_WORLD, &status );

  for(i=1; i<LOCAL_N_ELEM+1; i++)
  {
      heatmap_tmp[i] = g(heatmap[i-1], heatmap[i], heatmap[i+1]);
  }

  if (rank == 0)
    heatmap_tmp[1] = 0.0;
  if (rank == size - 1)
    heatmap_tmp[LOCAL_N_ELEM] = 0.0;

  memcpy((void*)heatmap, (void*)heatmap_tmp, (LOCAL_N_ELEM+2) * sizeof(float));
}

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}


int main(void) {
  int i;
  float *heatmap;
  int size, rank;

  MPI_Init( &argc, &argv );

  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &size );

  heatmap = malloc(sizeof(float) * LOCAL_N_ELEM+2);

  init(size,rank,heatmap);

    for(i=0; i<N_ITER; i++)
    {
	heatspread(heatmap);
	//printCells(heatmap, i+1);
    }


  printCells(heatmap, N_ITER);
  free(heatmap);

  return 0;
}
