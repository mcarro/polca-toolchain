module MapStrideGeneral where

-- mapStride (foldl (+) 0 ) 1 [1..20]
-- mapStrideT (foldl (+) 0 ) 1 [1..20] 5

mapStride :: ([a] -> b) -> Int -> [a] -> Bool -> [b]
mapStride f offset vs both =
	map f (windowed window vs)
	where 
		window = 
			if both
			then (2*offset + 1)
			else offset + 1

windowed :: Int -> [a] -> [[a]]
windowed size [] = []
windowed size ls@(x:xs) = 
	if length ls >= size 
	then (take size ls) : windowed size xs 
	else windowed size xs

mapStrideT :: ([a] -> b) -> Int -> [a] -> Bool -> Int -> [b]
mapStrideT f offset vs both size_chunk =
	concat (map applyG chunks)
	where
		applyG v = mapStride f offset v both
		chunks = splitEveryOffset size_chunk offset both vs

mapStrideT2 :: ([a] -> b) -> Int -> [a] -> Bool -> Int -> [b]
mapStrideT2 f offset vs both size_chunk =
	mapSplit applyG size_chunk offset both vs 
	where
		applyG v = mapStride f offset v both

mapSplit :: ([a] -> [b]) -> Int -> Int -> Bool -> [a] -> [b]
mapSplit f size_chunk offset both vs = 
	concat (map f chunks)
	where 
		chunks = splitEveryOffset size_chunk offset both vs

splitEveryOffset :: Int -> Int -> Bool -> [a] -> [[a]]
splitEveryOffset _ _ _ [] = [] 
splitEveryOffset size_chunk offset both vs = 
	(take (size_chunk + window) vs):splitEveryOffset size_chunk offset both (drop size_chunk vs)
	where
		window = 
			if both
			then 2*offset
			else offset 

-- Not used
splitEvery :: Int -> [a] -> [[a]]
splitEvery _ [] = [] 
splitEvery n vs = 
	(take n vs):splitEvery n (drop n vs)
