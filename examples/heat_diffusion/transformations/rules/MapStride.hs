module MapStride where

-- mapStride (foldl (+) 0 ) 1 [1..20]
-- mapStrideT (foldl (+) 0 ) 1 [1..20] 5

mapStride :: ([a] -> b) -> Int -> [a] -> [b]
mapStride f offset vs =
	map f (windowed (2*offset + 1) vs)

windowed :: Int -> [a] -> [[a]]
windowed size [] = []
windowed size ls@(x:xs) = 
	if length ls >= size 
	then (take size ls) : windowed size xs 
	else windowed size xs

mapStrideT :: ([a] -> b) -> Int -> [a] -> Int -> [b]
mapStrideT f offset vs size_chunk =
	concat (map applyG chunks)
	where
		applyG v = mapStride f offset v
		chunks = splitEveryOffset size_chunk offset vs

mapStrideT2 :: ([a] -> b) -> Int -> [a] -> Int -> [b]
mapStrideT2 f offset vs size_chunk =
	mapSplit applyG size_chunk offset vs 
	where
		applyG v = mapStride f offset v

mapSplit :: ([a] -> [b]) -> Int -> Int -> [a] -> [b]
mapSplit f size_chunk offset vs = 
	concat (map f chunks)
	where 
		chunks = splitEveryOffset size_chunk offset vs

--splitEveryOffset :: Int -> Int -> [a] -> [[a]]
--splitEveryOffset _ _ [] = [] 
--splitEveryOffset size_chunk offset vs = 
--	(take (size_chunk + (2 * offset)) vs):splitEveryOffset size_chunk offset (drop size_chunk vs)

splitEveryOffset :: Int -> Int -> [a] -> [[a]]
splitEveryOffset _ _ [] = [] 
splitEveryOffset size_chunk offset vs = 
	if length (drop size_chunk vs) >= size_chunk + (2 * offset)
	then (take (size_chunk + (2 * offset)) vs):ntail
	else [vs]
	where 
		ntail = splitEveryOffset size_chunk offset (drop size_chunk vs)

-- Not used
splitEvery :: Int -> [a] -> [[a]]
splitEvery _ [] = [] 
splitEvery n vs = 
	(take n vs):splitEvery n (drop n vs)