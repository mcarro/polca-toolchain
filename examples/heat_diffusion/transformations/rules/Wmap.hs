-- wmap :: o:N -> (Vec_(2o+1)(a) -> b) -> Vec_n(a) -> Vec_(n-2o)(b)  
wmap :: Int -> ([a] -> b) -> [a] -> [b]
wmap offset op vector = map op (windows (2*offset+1) vector) 

windows :: Int -> [a] -> [[a]]
windows _ []     = []
windows n (h:ts) = 
  if   length first_n < n
  then []
  else first_n:(windows n ts)
       where first_n = take n (h:ts)
             
-- splitEveryOffset can be seen as a generalization of 
-- windows
-- splitEveryOffset chunk_size offset xs
-- splitEveryOffset :: c:N -> o:N -> 
splitEveryOffset :: Int -> Int -> [a] -> [[a]]
splitEveryOffset c o xs =
  if   length xs < 2*(o+c) 
  then [xs]
  else (take (o+c+o) xs):(splitEveryOffset c o (drop (o+c) xs))

-- transformation rule 1:
-- forall c in ??
-- wmap o f xs = concat (map (wmap o f) (splitEveryOffset c o xs)) 
