module CSI where

-- Examples to run
-- mapM_ print (iteSpace 4 12 2)
-- mapStrideNTimes 3 (foldl (+) 0 ) 2 [1..20]
-- mapStrideNTimesT 3 (foldl (+) 0 ) 2 [1..20]


-- **************************************
-- Antecedent Rules
-- **************************************

mapStrideNTimes :: Num a => Int -> ([a] -> a) -> Int -> [a] -> [a]
mapStrideNTimes n f offset vs =   
	iter n calcIter (removeBorders offset vs)
	where
		calcIter = (mapStride f offset (take offset vs) (takeR offset vs))

iter :: Int -> (a -> a) -> a -> a
iter 0 _ x = x 
iter n f x =
	iter (n - 1) f (f x)

mapStride :: ([a] -> b) -> Int -> [a] -> [a] -> [a] -> [b]
mapStride f offset left right vs =
	map f (splitEveryOffset 1 offset (left ++ vs ++ right) )


-- **************************************
-- Consequent Rules
-- **************************************

mapStrideNTimesT :: Num a => Int -> ([a] -> a) -> Int -> [a] -> [a]
mapStrideNTimesT n f offset vs =
	removeBorders offset (last (iterSeq iterations f offset initial))
	where  
		middleElems = (replicate ((length vs) - (2 * offset)) 0)
		initialList vss = 
			((take offset vs) ++ middleElems ++ (takeR offset vs)):vss
		initial = vs : (iter n initialList [])
		iterations = iteSpace n ((length vs) - (2 * offset)) offset

iteSpace :: Int -> Int -> Int -> [[(Int,Int)]]
iteSpace n len offset = 
	iteSpaceAuxFirst ++ iteSpaceAuxMiddle ++ iteSpaceAuxLast 
	where 
		iteSpaceAuxFirst = 
			[(1,i) | i <- [1..offset+1]]:[(iteSpaceEle (1,i) n len offset) | i <- [offset+2..len]]
		iteSpaceAuxMiddle = 
			[(iteSpaceEle (i,j) n len offset) | i <- [2..n-1], j <- [len-offset..len]] 
		iteSpaceAuxLast =
			[[(n,i) | i <- [(len - offset)..len]]]

iteSpaceEle :: (Int,Int) -> Int -> Int -> Int -> [(Int,Int)]
iteSpaceEle (currIte, currElem) n len offset | currIte <= n && currElem >= 1 = 
	(currIte, currElem):iteSpaceEle (currIte + 1, currElem - (offset+1)) n len offset
iteSpaceEle _ _ _ _ | otherwise = 
	[]

windows :: [(Int,Int)] -> Int -> [[a]] -> [[a]]
windows [] offset vs = 
	[]
windows ((ite,ele):xs) offset vs = 
	window:(windows xs offset vs)
	where
		ele' = (ele - 1) + offset
		window = ([vs!!(ite-1)!!i | i <- [ele'-offset..ele'+offset]])

iterSeq :: [[(Int,Int)]] -> ([a] -> a) -> Int -> [[a]] -> [[a]]
iterSeq [] f offset vs = 
	vs
iterSeq (x:xs) f offset vs = 
	iterSeq xs f offset (foldl replace vs values)
	where
		values = (zip x (map f (windows x offset vs)))
		replace vs' ((ite,ele), value) = 
			replaceAt ite (replaceAt (ele + offset - 1) value (vs'!!ite)) vs'

-- **************************************
-- Other Functions
-- **************************************

splitEveryOffset :: Int -> Int -> [a] -> [[a]]
splitEveryOffset _ _ [] = [] 
splitEveryOffset size_chunk offset vs |length vs >= 1 + (2 * offset) = 
	if length (drop size_chunk vs) >= size_chunk + (2 * offset)
	then (take (size_chunk + (2 * offset)) vs):ntail
	else [vs]
	where 
		ntail = splitEveryOffset size_chunk offset (drop size_chunk vs)
splitEveryOffset _ _ _ | otherwise = 
	[]

takeR :: Int -> [a] -> [a]
takeR n = reverse . take n . reverse 

replaceAt :: Int -> a -> [a] -> [a]
replaceAt n item ls = 
	a ++ (item:tail(b)) where (a, b) = splitAt n ls

removeBorders :: Int -> [a] -> [a]
removeBorders offset vs =
	take ((length vs) - 2*offset) (drop offset vs)


-- **************************************
-- Not used Rules
-- **************************************

windowed :: Int -> [a] -> [[a]]
windowed size [] = []
windowed size ls@(x:xs) = 
	if length ls >= size 
	then (take size ls) : windowed size xs 
	else windowed size xs

iterBound :: Int -> ([a] -> [a]) -> [a] -> [a] -> [a] -> [a]
iterBound 0 _ _ _ x = x 
iterBound n f left right x =
	iterBound (n - 1) f left right (f (left ++ x ++ right))