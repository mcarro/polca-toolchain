let heatmap' = itn HEATSPREAD heatmap N_ITER


// In this case HEATSPREAD is the composition of heatspread, B (boundary update) and Mem
HEATSPREAD v = heatspread v; B v; Mem v


heatspread heatmap = zipWith3 G slide(heatmap, -1) heatmap slide(heatmap, +1) 

G x y z = g x y z

B xs =  boundary xs len(xs) len(xs)  0.0

boundary x:xs n1 n2 val = 
	 if n1==n2  [val, boundary xs n1 len(xs) val]
	 if n2==0 val
 	 else [x, boundary xs n1 len(xs) val] 

Mem x:heatmap y:heatmap_tmp = [y, Mem heatmap heatmap_tmp]
Mem x:[] y:[] = y  

------------------------------------------------------------------------------

-- For transforming the code and generate OpenMP,  high order functions offering parallelism will be searched and then corresponding OpenMP directives will be inserted. For this case zipWith will be replaced by "omp parallel for"