/**
 * Document: MaxCompiler Training (maxcompiler-training.pdf)
 * Chapter: 2      Exercise: 1      Name: Mean
 * MaxFile name: Mean
 * Summary:
 * 	   Computes 1D Heat Diffusion
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "Maxfiles.h"
#include <MaxSLiCInterface.h>

#define N_ELEM 24
#define N_ITER 10
#define C 0.1

void init(float heatmap[]) {
  int i;

  for(i=0; i < N_ELEM; i++)
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

void swapHeatMap(float srcMap[], float dstMap[]) {
  int i;

  for(i=0; i < N_ELEM; i++)
    dstMap[i] = srcMap[i];

}

void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}

int main(void)
{
	float_t inMap[N_ELEM];

	printf("Initializing heatmap.\n");
	init(inMap);

	int dataSize = N_ELEM * sizeof(float);
	// Allocate a buffer for the output image
	float_t *outMap = malloc(dataSize);

	int inAddr = 0;
	int outAddr = sizeof(float) * N_ELEM;
	Heat_writeLMem(inAddr, sizeof(float) * N_ELEM, inMap);

	printCells(inMap,0);
	for(int i=0;i<N_ITER;i++)
	{
		Heat_derp(
			inAddr,
			N_ELEM,
			outAddr);
		//printCells(outMap,i+1);

		int tmp = outAddr;
		outAddr = inAddr;
		inAddr = tmp;

		//swapHeatMap(outMap,inMap);
	}

	Heat_readLMem(inAddr, dataSize, outMap);
	printCells(outMap,N_ITER+1);

	printf("Exiting\n");
	return 0;
}
