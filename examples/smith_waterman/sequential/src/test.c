#include <stdio.h>
#include <stdlib.h>

#define N 5
#define M 5

int main (int argc, const char * argv[]) {


  for(int i=0;i<N;i++){
    for(int j=0;j<M;j++)
      printf("%2d, ",i*M+j);
    printf("\n");
  }

  printf("------------------------------\n");

  for(int i=0;i<(N+M-1);i++){
    int new_i = i<N ? i : N-1;
    int off_i = 0;
    for(int j=(i<N ? 0 : i-(N-1));j< (i<M ? (i+1) : M);j++)
    {
      printf("%2d, ",(new_i-off_i)*M+j);
      off_i++;
    }
    printf("\n");
  }

  printf("------------------------------\n");

  /* for(int i=0;i<(N+M-1);i++){ */
  /*   int new_i = i<N ? i : N-1; */
  /*   int off_i = 0; */
  /*   for(int j=(i<N ? 0 : i-(N-1));j< (i<M ? (i+1) : M);j++) */
  /*   { */
  /*     printf("(%2d,%2d), ",new_i-off_i,j); */
  /*     off_i++; */
  /*   } */
  /*   printf("\n"); */
  /* } */


  /* printf("Hola mundano!\n"); */

  return 0;
  
}
