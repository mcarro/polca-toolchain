#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


/*Definitions*/
#define TRUE 1
#define FALSE 0

#define DEF_MATCH 10 //5
#define DEF_MISSMATCH -5 //-4
#define DEF_GAP_PENALTY 10
#define DEF_GAP_EXT_PEANLTY 8

#define SEQSIZE 101

#define DEF_STR_A "AABBAABBAA"
#define DEF_STR_B "BBBAAABBBA"

/*Global Variables*/

int MatchScore,MissmatchScore;
int GapOpenPenalty;
int GapExtPenalty;
int StrLen1,StrLen2;
int i,j,k,l,m,n,lenA,lenB,compval;
const char dash = '-';

char sequenceA[SEQSIZE];			//holds 1st string to be aligned in character array
char sequenceB[SEQSIZE];			//holds 2nd string to be aligned in character array
int HiScore;			    //holds value of highest scoring alignment(s).
int HiScorePos[2];		    //holds the position of the HiScore
int SWArray[SEQSIZE+1][SEQSIZE+1];	//S-W Matrix

char MaxA[SEQSIZE];
char MaxB[SEQSIZE];
char OptA[SEQSIZE];
char OptB[SEQSIZE];

int MaxAcounter = 0;	//MaxA counter
int MaxBcounter = 0;	//MaxB counter


void ReadStrings()
{
    printf("Strings should be no longer than %d  symbols. Program is case sensitive\n",SEQSIZE-1);
    printf("Please enter 1st string or press ENTER to keep default one:");
    fgets(sequenceA,sizeof(sequenceA),stdin);
    if(sequenceA[strlen(sequenceA)-1] == '\n') // removing possible \n from string got through fgets
        sequenceA[strlen(sequenceA)-1] = '\0';

    if (strlen(sequenceA) ==0) // if empty set to default
    {
        strcpy(sequenceA, DEF_STR_A);
        printf("1st string defaults to :%s\n",sequenceA );
    }



    printf("Please enter 2nd string or press ENTER to keep default one:");
    fgets(sequenceB,sizeof(sequenceB),stdin);

    if(sequenceB[strlen(sequenceB)-1] == '\n')
        sequenceB[strlen(sequenceB)-1] = '\0';	//edit '\n' out of string

    if (strlen(sequenceB) == 0)
    {
        strcpy(sequenceB, DEF_STR_B);
        strcpy(sequenceA, DEF_STR_A);
        printf("2nd string defaults to :%s\n",sequenceB );
    }

}

void InitSWscoreMatrix()
{
    lenA = strlen(sequenceA);
    lenB = strlen(sequenceB);
    //Create empty table
    for(i=0; i<=lenA; ++i)
    {
        SWArray[0][i]=0;
    }
    for(i=0; i<=lenB; ++i)
    {
        SWArray[i][0]=0;
    }
}

void ComputeSWscoreMatrix()
{

    for(i = 1; i <= lenA; ++i)
    {
        for(j = 1; j <= lenB; ++j)
        {
            compval = 0;
            if(sequenceA[i-1] == sequenceB[j-1])  //MATCH
                compval = (SWArray[i-1][j-1] + MatchScore);
            else    //MISMATCH
                if(compval < (SWArray[i-1][j-1] + MissmatchScore))
                    compval = SWArray[i-1][j-1] + MissmatchScore;


            for(k = i-1; k > 0; --k) //check all sub rows for better scores through gap penalty / gap extension
                if(compval < ((SWArray[i-k][j]) - (GapOpenPenalty + (GapExtPenalty * k))))
                    compval = ((SWArray[i-k][j]) - (GapOpenPenalty + (GapExtPenalty * k)));

            for(k=j-1; k>0; --k) //check all sub columns for better scores through gap penalty / gap extension
                if(compval < ((SWArray[i][j-k]) - (GapOpenPenalty + (GapExtPenalty * k))))
                    compval = ((SWArray[i][j-k]) - (GapOpenPenalty + (GapExtPenalty * k)));

            if(compval < 0)  // keep only positive values for scores
                compval = 0;
            SWArray[i][j] = compval;	//set current cell score
        }
    }

}

void PrintSWscoreMatrix() // deprecated
{
    printf("lenA = %d\n", lenA);
    printf("lenB = %d\n", lenB);
    printf("\n\n");

    printf("   +");

    for(i = 0; i < lenB; ++i)
        printf("  %c",sequenceB[i]);

    printf("\n");
    printf("+");

    for(j = 0; j <= lenB; ++j)
        printf("  +");
    printf("\n");

    for(i = 1; i <= lenA; ++i)
    {
        printf("%c",sequenceA[i-1]);

        printf("  +");
        for(j = 1; j <= lenB; ++j)
        {
            printf("%3i",SWArray[i][j]);
        }
        printf("\n");
    }
    printf("\n\n");
}

void PrintSWscoreMatrix2()
{
    printf("lenA = %d\n", lenA);
    printf("lenB = %d\n", lenB);
    printf("\n\n");
    // line with indexes
    printf("    |");

    for(i = 1; i <= lenB; ++i)
        printf("%3i",i);

    printf("\n");
    // first line with String B
    printf("    |");

    for(i = 0; i < lenB; ++i)
        printf("  %c",sequenceB[i]);

    printf("\n");
    // separation line
    printf("____|");

    for(j = 1; j <= lenB; ++j)
        printf("___");
    printf("\n");

    // the following lines starting with matching value from string B then score matrix values
    for(i = 1; i <= lenA; ++i)
    {
        printf("%2i|%c|",i, sequenceA[i-1]);
        for(j = 1; j <= lenB; ++j)
        {
            printf("%3i",SWArray[i][j]);
        }
        printf("\n");
    }
    printf("\n\n");
}


void Align(int PosA, int PosB)
{

    /*Function Variables*/
    int relmax = -1;		//hold highest value in sub columns and rows
    int relmaxpos[2];		//holds position of relmax

    // printf("Align(%d,%d), cont =%d \n",PosA, PosB, cont);
    if (SWArray[PosA][PosB] == 0)
        return;

    relmax = SWArray[PosA-1][PosB-1];
    relmaxpos[0]=PosA-1;
    relmaxpos[1]=PosB-1;

    /*Find relmax in sub columns and rows*/
    for(i=PosA; i>0; --i)
    {

        if(relmax < SWArray[i-1][PosB/*-1*/])
        {

            relmax = SWArray[i-1][PosB/*-1*/];
            relmaxpos[0]=i-1;
            relmaxpos[1]=PosB/*-1*/;
            //printf("position(%d,%d) = %d\n",i-1,PosB-1, relmax);
        }
    }

    for(j=PosB; j>0; --j)
    {

        if(relmax < SWArray[PosA/*-1*/][j-1])
        {

            relmax = SWArray[PosA/*-1*/][j-1];
            relmaxpos[0]=PosA/*-1*/;
            relmaxpos[1]=j-1;
            //printf("position(%d,%d) = %d\n",PosA-1, j-1, relmax);
        }
    }

    printf("position \t(%d,%d) = \t%d\n",relmaxpos[0], relmaxpos[1], relmax);

    /*Align strings to relmax*/
    if((relmaxpos[0] == PosA-1) && (relmaxpos[1] == PosB-1))  	//if relmax position is diagonal from current position simply align and increment counters
    {

        MaxA[MaxAcounter] = sequenceA[relmaxpos[0]/*-1*/];
        ++MaxAcounter;
        MaxB[MaxBcounter] = sequenceB[relmaxpos[1]/*-1*/];
        ++MaxBcounter;

    }

    else
    {

        if((relmaxpos[1] == PosB/*-1*/) /*&& (relmaxpos[0] != PosA-1) */ )  	//maxB needs at least one '-'
        {

            for(i=PosA-1; i>relmaxpos[0]-1; --i)  	//for all elements of sequenceA between PosA and relmaxpos[0]
            {

                MaxA[MaxAcounter]= sequenceA[i/*-1*/];
                MaxB[MaxBcounter] = dash;
                ++MaxAcounter;
                ++MaxBcounter;
            }
        }

        if((relmaxpos[0] == PosA/*-1*/) /* && (relmaxpos[1] != PosB-1) */ )  	//MaxA needs at least one '-'
        {

            for(j=PosB-1; j>relmaxpos[1]-1; --j)  	//for all elements of sequenceB between PosB and relmaxpos[1]
            {

                MaxB[MaxBcounter] = sequenceB[j/*-1*/];
                MaxA[MaxAcounter] = dash;
                ++MaxAcounter;
                ++MaxBcounter;
            }
        }
    }

    //printf("(%i,%i)",relmaxpos[0],relmaxpos[1]);
    Align(relmaxpos[0],relmaxpos[1]);

    return;
}




void Align2(int StartPosA, int StartPosB)
{

    /*Function Variables*/
    int relmax = -1;		//hold highest value in sub columns and rows
    int relmaxpos[2];		//holds position of relmax

    int PosA = StartPosA;
    int PosB = StartPosB;

    while (SWArray[PosA][PosB] != 0)
{


    // printf("Align(%d,%d), cont =%d \n",PosA, PosB, cont);
    if (SWArray[PosA][PosB] == 0)
        return;

    relmax = SWArray[PosA-1][PosB-1];
    relmaxpos[0]=PosA-1;
    relmaxpos[1]=PosB-1;

    /*Find relmax in sub columns and rows*/
    for(i=PosA; i>0; --i)
    {

        if(relmax < SWArray[i-1][PosB/*-1*/])
        {

            relmax = SWArray[i-1][PosB/*-1*/];
            relmaxpos[0]=i-1;
            relmaxpos[1]=PosB/*-1*/;
            //printf("position(%d,%d) = %d\n",i-1,PosB-1, relmax);
        }
    }

    for(j=PosB; j>0; --j)
    {

        if(relmax < SWArray[PosA/*-1*/][j-1])
        {

            relmax = SWArray[PosA/*-1*/][j-1];
            relmaxpos[0]=PosA/*-1*/;
            relmaxpos[1]=j-1;
            //printf("position(%d,%d) = %d\n",PosA-1, j-1, relmax);
        }
    }

    printf("position \t(%d,%d) = \t%d\n",relmaxpos[0], relmaxpos[1], relmax);

    /*Align strings to relmax*/
    if((relmaxpos[0] == PosA-1) && (relmaxpos[1] == PosB-1))  	//if relmax position is diagonal from current position simply align and increment counters
    {

        MaxA[MaxAcounter] = sequenceA[relmaxpos[0]/*-1*/];
        ++MaxAcounter;
        MaxB[MaxBcounter] = sequenceB[relmaxpos[1]/*-1*/];
        ++MaxBcounter;

    }

    else
    {

        if((relmaxpos[1] == PosB/*-1*/) /*&& (relmaxpos[0] != PosA-1) */ )  	//maxB needs at least one '-'
        {

            for(i=PosA-1; i>relmaxpos[0]-1; --i)  	//for all elements of sequenceA between PosA and relmaxpos[0]
            {

                MaxA[MaxAcounter]= sequenceA[i/*-1*/];
                MaxB[MaxBcounter] = dash;
                ++MaxAcounter;
                ++MaxBcounter;
            }
        }

        if((relmaxpos[0] == PosA/*-1*/) /* && (relmaxpos[1] != PosB-1) */ )  	//MaxA needs at least one '-'
        {

            for(j=PosB-1; j>relmaxpos[1]-1; --j)  	//for all elements of sequenceB between PosB and relmaxpos[1]
            {

                MaxB[MaxBcounter] = sequenceB[j/*-1*/];
                MaxA[MaxAcounter] = dash;
                ++MaxAcounter;
                ++MaxBcounter;
            }
        }
    }

    //printf("(%i,%i)",relmaxpos[0],relmaxpos[1]);
    //Align(relmaxpos[0],relmaxpos[1]);
    PosA = relmaxpos[0];
    PosB = relmaxpos[1];
    //return;

    }
}


void TrackBackBestAlignement()
{
    /*MAKE ALIGNMENTT*/
    for(i=0; i<=lenA; ++i)  	//find highest score in matrix: this is the starting point of an optimal local alignment
    {

        for(j=0; j<=lenB; ++j)
        {

            if(SWArray[i][j] > HiScore)
            {

                HiScore = SWArray[i][j];
                HiScorePos[0]=i;
                HiScorePos[1]=j;

            }
        }
    }
    printf("HiScorePos(%d,%d) = %d\n",HiScorePos[0],HiScorePos[1],HiScore);
    /*send Position to alignment function*/

//    MaxA[0] = sequenceA[HiScorePos[0]-1];
//    MaxB[0] = sequenceB[HiScorePos[1]-1];

    //Align(HiScorePos[0],HiScorePos[1]); // recursive version of Align function
    Align2(HiScorePos[0],HiScorePos[1]); // rewritten Align function (without recursion)

    /*in the end reverse Max A and B*/
    k=0;
    for(i = strlen(MaxA)-1; i > -1; --i)
    {
        OptA[k] = MaxA[i];
        ++k;
    }

    k=0;
    for(j=strlen(MaxB)-1; j > -1; --j)
    {
        OptB[k] = MaxB[j];
        ++k;
    }
}

void PrintSWfinalAlignement()
{
    int nb_dashes_A = 0;
    int nb_dashes_B = 0;

    char spaces[SEQSIZE] ="";
    int nb_spaces  =0 ;

    printf("ALIGNED SUBSEQUENCES ARE:\n");
    printf("%s\n%s\n\n",OptA,OptB);

    printf("FORMATTED ALIGNEMENT OF FULL SEQUENCES GIVES:\n");
    // count gaps in both strings

    for(i = 0; i<strlen(OptA); i++)
        OptA[i]==dash?nb_dashes_A++:nb_dashes_A;

    for(i = 0; i<strlen(OptB); i++)
        OptB[i]==dash?nb_dashes_B++:nb_dashes_B;

    nb_spaces = nb_dashes_B- nb_dashes_A + HiScorePos[1] - HiScorePos[0];

    for(i = 0; i<abs(nb_spaces); i++)
        strcat(spaces," ");

    //---------


    if (nb_spaces>0) // equivalent to : if (nb_dashes_A + HiScorePos[0] <nb_dashes_B + HiScorePos[1])
        printf("%s",spaces);

    for(i = 0; i<HiScorePos[0] - (strlen(OptA)-nb_dashes_A); i++)
        printf("%c", sequenceA[i]);
    for(i = 0; i<strlen(OptA); i++)
        printf("%c", ' ');
    for(i = HiScorePos[0]; i<lenA; i++)
        printf("%c", sequenceA[i]);
    printf("\n");

    //---------
    if (nb_spaces>0)
        printf("%s",spaces);

    for(i = 0; i<HiScorePos[0] - (strlen(OptA)-nb_dashes_A); i++)
        printf("%c", ' ');
    for(i = 0; i<strlen(OptA); i++)
        printf("%c",OptA[i] );
    for(i = HiScorePos[0]; i<lenA; i++)
        printf("%c", ' ');
    printf("\n");

    //---------

    if (nb_spaces<0)
        printf("%s",spaces);

    for(i = 0; i<HiScorePos[1] - (strlen(OptB)-nb_dashes_B); i++)
        printf("%c", ' ');
    for(i = 0; i<strlen(OptB); i++)
        printf("%c",OptB[i] );
    for(i = HiScorePos[1]; i<lenB; i++)
        printf("%c", ' ');
    printf("\n");

    //---------
    if (nb_spaces<0)
        printf("%s",spaces);


    for(i = 0; i<HiScorePos[1] - (strlen(OptB)-nb_dashes_B); i++)
        printf("%c", sequenceB[i]);
    for(i = 0; i<strlen(OptB); i++)
        printf("%c",' ');
    for(i = HiScorePos[1]; i<lenB; i++)
        printf("%c", sequenceB[i]);
    printf("\n\n\n");


}
void SetSWparams()
{

    printf("\n\nSetting scores for Match, MissMatch, gap Opening penalty and gap extension penalty:\n");
    printf("(default values):(10,-5,10,8)\n\n");
    printf("Enter the 4 parameters (comma seperated and within parenthesis like above) \n"
           "or press ENTER to keep defaults:\n");
    int ret = scanf("(%d,%d,%d,%d)",&MatchScore, &MissmatchScore, &GapOpenPenalty, &GapExtPenalty);

    if (ret !=4) //ret !=0 && ret != 4)
    {
        //set  to default values
        MatchScore      = DEF_MATCH;
        MissmatchScore  = DEF_MISSMATCH;
        GapOpenPenalty  = DEF_GAP_PENALTY;
        GapExtPenalty   = DEF_GAP_EXT_PEANLTY;

        printf("parameters set to DEFAULTS : (%d,%d,%d,%d)\n\n\n",MatchScore, MissmatchScore, GapOpenPenalty, GapExtPenalty);
    }
    else
        printf("parameters set to USER DEFINED VALUES : (%d,%d,%d,%d)\n\n\n",MatchScore, MissmatchScore, GapOpenPenalty, GapExtPenalty);
}

/*MAIN */

int main()
{
    FILE *ptr_logfile;
    clock_t begin, end;
    double SW_compute_time, SW_align_time;

    /*Introduction*/
    printf("Smith-Waterman String Alignment \n\n");

    ReadStrings();

    SetSWparams();

    InitSWscoreMatrix();

//----------------------------
    begin = clock();

    ComputeSWscoreMatrix();

    end = clock();
    SW_compute_time = (double)(end - begin) / CLOCKS_PER_SEC;
//----------------------------

    PrintSWscoreMatrix2();

//----------------------------
    begin = clock();
    TrackBackBestAlignement();
    end = clock();
    SW_align_time = (double)(end - begin) / CLOCKS_PER_SEC;
//----------------------------
    printf("SW_compute_time = %6f sec\n",SW_compute_time);
    printf("SW_align_time   = %6f sec\n",SW_align_time);
    printf("---------------------\n\n\n");
    PrintSWfinalAlignement();

    // saving results to log file
    ptr_logfile = fopen("log.txt", "w+");
    if(ptr_logfile == NULL)
    {
        printf("Error opening 'log.txt'\n");
        system("PAUSE");
        exit(8);
    }
    fprintf(ptr_logfile,"\
            SEQ A = %s\n\
            SEQ B = %s\n\
            ALIGNEMENT : \n%s\n%s\n--------\n ",sequenceA,sequenceB, OptA, OptB);
    fclose(ptr_logfile);

    //system("PAUSE");

    return(0);

}
