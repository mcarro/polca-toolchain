sum :: [Int] -> Int
sum = foldl (+) 0

sum_transform_1 :: [Int] -> Int
sum_transform_1 v =
  let m = 2
      f = (length v) `div` m
      s_0 = take m (repeat 0)
      vv = snd ((iterate (\ (v',vv') -> (drop m v', vv' ++ [take m v'])) (v,[]))!!f)
  in foldl (+) 0 (foldl (zipWith (+)) s_0 vv)

-- foldl (+) 0 [1,2,3,4,5,6,7,8,9,0]
-- sum_transform_1 [1,2,3,4,5,6,7,8,9,0] 10
