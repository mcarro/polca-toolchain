/*
Version of the heat diffusion proposed by Uday R Bondhugula (from Pluto team). 
The main goal of this version is to overcome with the limitation of Pluto with
pointers. This code replicates twice the innermost loop avoiding the swapping
of the two lists (what was the most problematic part).
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>

#define TEST

#define N_ELEM 10
#define N_ITER 10
#define C 0.1

float heatmap[N_ELEM];
float heatmap_tmp[N_ELEM];

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

void init() {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

void print_array()
{
  int i;

  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

int main()
{
    //int i, j, k, t;
    int i, t;

    double t_start, t_end;

    //init_array() ;
    init(heatmap);

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

float *h = heatmap;
float *ht = heatmap_tmp; 

int half = N_ITER/2;
#pragma scop
for (t=0; t<half; t++)  {
        for (i=1; i<N_ELEM-1; i++)  {
                ht[i] =
                    h[i] + (C * phi(h[i-1], h[i], h[i+1]));
        }

				for (i=1; i<N_ELEM-1; i++)  {
                h[i] =
                    ht[i] + (C * phi(ht[i-1], ht[i], ht[i+1]));
        }
 }
#pragma endscop

    IF_TIME(t_end = rtclock());
    IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

#ifdef TEST
    print_array();
#endif
    return 0;
}
