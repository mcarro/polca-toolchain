#include <omp.h>
#include <math.h>
#define ceild(n,d)  ceil(((double)(n))/((double)(d)))
#define floord(n,d) floor(((double)(n))/((double)(d)))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

/*
Version of the heat diffusion proposed by Uday R Bondhugula (from Pluto team). 
The main goal of this version is to overcome with the limitation of Pluto with
pointers. This code replicates twice the innermost loop avoiding the swapping
of the two lists (what was the most problematic part).
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>

#define TEST

#define N_ELEM 10
#define N_ITER 10
#define C 0.1

float heatmap[N_ELEM];
float heatmap_tmp[N_ELEM];

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

void init() {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

void print_array()
{
  int i;

  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

int main()
{
    //int i, j, k, t;
    int i, t;

    double t_start, t_end;

    //init_array() ;
    init(heatmap);

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

float *h = heatmap;
float *ht = heatmap_tmp; 

int half = N_ITER/2;




  int t1, t2, t3;

 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;

/* Start of CLooG code */
if ((N_ELEM >= 3) && (half >= 1)) {
  ht[1]=h[1]+(C*phi(h[1 -1],h[1],h[1 +1]));;
  for (t1=2;t1<=min(N_ELEM-2,3*half-2);t1++) {
    if ((2*t1+1)%3 == 0) {
      ht[1]=h[1]+(C*phi(h[1 -1],h[1],h[1 +1]));;
    }
    lbp=ceild(2*t1+2,3);
    ubp=t1;
#pragma omp parallel for private(lbv,ubv)
    for (t2=lbp;t2<=ubp;t2++) {
      ht[-2*t1+3*t2]=h[-2*t1+3*t2]+(C*phi(h[-2*t1+3*t2-1],h[-2*t1+3*t2],h[-2*t1+3*t2+1]));;
      h[-2*t1+3*t2-1]=ht[-2*t1+3*t2-1]+(C*phi(ht[-2*t1+3*t2-1 -1],ht[-2*t1+3*t2-1],ht[-2*t1+3*t2-1 +1]));;
    }
  }
  if (N_ELEM == 3) {
    for (t1=2;t1<=3*half-2;t1++) {
      if ((2*t1+1)%3 == 0) {
        ht[1]=h[1]+(C*phi(h[1 -1],h[1],h[1 +1]));;
      }
      if ((2*t1+2)%3 == 0) {
        h[1]=ht[1]+(C*phi(ht[1 -1],ht[1],ht[1 +1]));;
      }
    }
  }
  for (t1=3*half-1;t1<=N_ELEM-2;t1++) {
    lbp=t1-half+1;
    ubp=t1;
#pragma omp parallel for private(lbv,ubv)
    for (t2=lbp;t2<=ubp;t2++) {
      ht[-2*t1+3*t2]=h[-2*t1+3*t2]+(C*phi(h[-2*t1+3*t2-1],h[-2*t1+3*t2],h[-2*t1+3*t2+1]));;
      h[-2*t1+3*t2-1]=ht[-2*t1+3*t2-1]+(C*phi(ht[-2*t1+3*t2-1 -1],ht[-2*t1+3*t2-1],ht[-2*t1+3*t2-1 +1]));;
    }
  }
  if (N_ELEM >= 4) {
    for (t1=N_ELEM-1;t1<=3*half-2;t1++) {
      if ((2*t1+1)%3 == 0) {
        ht[1]=h[1]+(C*phi(h[1 -1],h[1],h[1 +1]));;
      }
      lbp=ceild(2*t1+2,3);
      ubp=floord(2*t1+N_ELEM-2,3);
#pragma omp parallel for private(lbv,ubv)
      for (t2=lbp;t2<=ubp;t2++) {
        ht[-2*t1+3*t2]=h[-2*t1+3*t2]+(C*phi(h[-2*t1+3*t2-1],h[-2*t1+3*t2],h[-2*t1+3*t2+1]));;
        h[-2*t1+3*t2-1]=ht[-2*t1+3*t2-1]+(C*phi(ht[-2*t1+3*t2-1 -1],ht[-2*t1+3*t2-1],ht[-2*t1+3*t2-1 +1]));;
      }
      if ((2*t1+N_ELEM+2)%3 == 0) {
        h[N_ELEM-2]=ht[N_ELEM-2]+(C*phi(ht[N_ELEM-2 -1],ht[N_ELEM-2],ht[N_ELEM-2 +1]));;
      }
    }
  }
  for (t1=max(N_ELEM-1,3*half-1);t1<=3*half+N_ELEM-5;t1++) {
    lbp=t1-half+1;
    ubp=floord(2*t1+N_ELEM-2,3);
#pragma omp parallel for private(lbv,ubv)
    for (t2=lbp;t2<=ubp;t2++) {
      ht[-2*t1+3*t2]=h[-2*t1+3*t2]+(C*phi(h[-2*t1+3*t2-1],h[-2*t1+3*t2],h[-2*t1+3*t2+1]));;
      h[-2*t1+3*t2-1]=ht[-2*t1+3*t2-1]+(C*phi(ht[-2*t1+3*t2-1 -1],ht[-2*t1+3*t2-1],ht[-2*t1+3*t2-1 +1]));;
    }
    if ((2*t1+N_ELEM+2)%3 == 0) {
      h[N_ELEM-2]=ht[N_ELEM-2]+(C*phi(ht[N_ELEM-2 -1],ht[N_ELEM-2],ht[N_ELEM-2 +1]));;
    }
  }
  h[N_ELEM-2]=ht[N_ELEM-2]+(C*phi(ht[N_ELEM-2 -1],ht[N_ELEM-2],ht[N_ELEM-2 +1]));;
}
/* End of CLooG code */

    IF_TIME(t_end = rtclock());
    IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

#ifdef TEST
    print_array();
#endif
    return 0;
}
