/*
Modified version of the heat difussion by Manuel Carro.
It avoids to use extra lists to store the data between iterations by using two
variables (previous and current value).
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>


#define N_ELEM 10
#define N_ITER 10
#define C 0.1
#define TEST
float heatmap[N_ELEM];

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

void init() {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

void print_array()
{
  int i;

  //printf("i %05d:", n);
  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

int main()
{
    int i, t;
    float prev, curr;

    double t_start, t_end;

    init(heatmap);

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

#pragma scop
    for (t=0; t<N_ITER; t++)  {
    		prev = heatmap[0];
        for (i=1; i<N_ELEM-1; i++)  {
        		curr = heatmap[i];
    				heatmap[i] = curr + (C * phi(prev,curr,heatmap[i+1])); 
    				prev = curr;
        }
    }
#pragma endscop

    IF_TIME(t_end = rtclock());
    IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

#ifdef TEST
    print_array();
#endif
    return 0;
}
