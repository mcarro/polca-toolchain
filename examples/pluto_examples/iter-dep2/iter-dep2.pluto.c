#include <omp.h>
#include <math.h>
#define ceild(n,d)  ceil(((double)(n))/((double)(d)))
#define floord(n,d) floor(((double)(n))/((double)(d)))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

/*
Example proposed by Manuel Carro.
The loop statement (w[i] = w[i] + v[j+i]) depends only on 
{w[i], v[i], v[i+1], ..., v[i+K-1]}
The goal is again to obtain the following code or an equivalent variant:
  for (i = 0; i < N-K; i++) {
    w[i] = 0;
    w[i] = v[i] + v[i+1] +  ... +  v[i+K-1] ;
  }
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>


#define N 15
#define K 5
#define TEST

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif



double rtclock()
{
  struct timezone Tzp;
  struct timeval Tp;
  int stat;
  stat = gettimeofday (&Tp, &Tzp);
  if (stat != 0) printf("Error return from gettimeofday: %d",stat);
  return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

int main()
{
  double t_start, t_end;





#ifdef PERFCTR
  PERF_INIT; 
#endif

  IF_TIME(t_start = rtclock());

	int i, j;
	int w[N-K];
	int v[N];

  for (i = 0; i < N; i++) {
    v[i] = i;
  }
	w[0] = 0;
	




  int t1, t2, t3;

 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;

/* Start of CLooG code */
if (N >= K+1) {
  if (K <= 0) {
    for (t1=1;t1<=N-K-1;t1++) {
      w[t1]=w[t1-1];;
    }
  }
  if (K >= 1) {
    for (t2=0;t2<=K-1;t2++) {
      w[0]=w[0]+v[t2+0];;
    }
  }
  if (K >= 1) {
    for (t1=1;t1<=N-K-1;t1++) {
      w[t1]=w[t1-1];;
      for (t2=0;t2<=K-1;t2++) {
        w[t1]=w[t1]+v[t2+t1];;
      }
    }
  }
}
/* End of CLooG code */

	

  IF_TIME(t_end = rtclock());
  IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
  PERF_EXIT; 
#endif

#ifdef TEST
  for(i=0; i<N-K; i++) {
    printf(" %d", w[i]);
  }
  printf("\n");
#endif
  return 0;
}
