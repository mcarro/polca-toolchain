/*
Example proposed by Manuel Carro.
The loop statement (w[i] = w[i] + v[j+i]) depends only on 
{w[i], v[i], v[i+1], ..., v[i+K-1]}
The goal is again to obtain the following code or an equivalent variant:
  for (i = 0; i < N-K; i++) {
    w[i] = 0;
    w[i] = v[i] + v[i+1] +  ... +  v[i+K-1] ;
  }
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>


#define N 15
#define K 5
#define TEST

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif



double rtclock()
{
  struct timezone Tzp;
  struct timeval Tp;
  int stat;
  stat = gettimeofday (&Tp, &Tzp);
  if (stat != 0) printf("Error return from gettimeofday: %d",stat);
  return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

int main()
{
  double t_start, t_end;





#ifdef PERFCTR
  PERF_INIT; 
#endif

  IF_TIME(t_start = rtclock());

	int i, j;
	int w[N-K];
	int v[N];

  for (i = 0; i < N; i++) {
    v[i] = i;
  }
	w[0] = 0;
	
#pragma scop
  for (i = 0; i < N-K; i++) {
    if (i > 0)
      w[i] = w[i-1];
    for (j = 0; j < K; j++)
      w[i] = w[i] + v[j+i];
  }
#pragma endscop

	

  IF_TIME(t_end = rtclock());
  IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
  PERF_EXIT; 
#endif

#ifdef TEST
  for(i=0; i<N-K; i++) {
    printf(" %d", w[i]);
  }
  printf("\n");
#endif
  return 0;
}
