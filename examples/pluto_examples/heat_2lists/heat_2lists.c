/*
Version of the heat difussion adapted from the original version of Daniel Rubio.
Two lists are used to store two generation of the heatmap, and each iteration
they are swapped.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <assert.h>


#define TEST
#define N_ELEM 10
#define N_ITER 10
#define C 0.1
float heatmap[N_ELEM];
float heatmap_tmp[N_ELEM];

#include <unistd.h>
#include <sys/time.h>

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

void init() {
  int i;

  for(i=0; i < N_ELEM; i++) 
    heatmap[i] = 0.0;

  heatmap[N_ELEM/2] = 100.0;
}

void print_array()
{
  int i;

  for(i=0; i<N_ELEM; i++) {
    printf(" %10.6f", heatmap[i]);
  }
  printf("\n");
}

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

float phi(float left, float center, float right) {
  return left + right - 2 * center;
}

int main()
{
    int i, t;

    double t_start, t_end;
    
    
    init(heatmap);

#ifdef PERFCTR
    PERF_INIT; 
#endif

    IF_TIME(t_start = rtclock());

float *h = heatmap;
float *ht = heatmap_tmp; 
float *swap ;

#pragma scop
    for (t=0; t<N_ITER; t++)  {
        for (i=1; i<N_ELEM-1; i++)  {
        		ht[i] = 
        			h[i] + (C * phi(h[i-1], h[i], h[i+1]));
        }
        ht[0] = h[0];
        ht[N_ELEM-1] = h[N_ELEM-1];
        
        swap = ht;
    		ht = h;
        h = swap;
        
    }
#pragma endscop

    IF_TIME(t_end = rtclock());
    IF_TIME(printf("%0.6lfs\n", t_end - t_start));

#ifdef PERFCTR
    PERF_EXIT; 
#endif

#ifdef TEST
    print_array();
#endif
    return 0;
}
