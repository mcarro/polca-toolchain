COMMENTS ABOUT PROGRAM heat_2lists

	- Command line used:
	polycc heat_2lists.c  --parallel  --debug

	- Command line to compile the resulting code:
	gcc -fopenmp heat_2lists.pluto.c -o heat_2lists.pluto -lm

	- Other comments:

The resulting code is not correct, and the result computed by this code is 
no valid. The reason is that Pluto does not go well with pointers. In this
case it gets code, but incorrect. When tiding flag is active, it does not even
obtain any code. 
