#ifndef CG_H
#define CG_H

typedef double dt;

dt ddot(dt* x, dt *y, int n);
dt norm(dt *v, int n);
void vvs(dt *v1, dt *v2, int n, dt* dest);
void vva(dt *v1, dt *v2, int n, dt* dest);
void vvc(dt *org, int n, dt *dest);
void mvm(dt *m, dt *v, int nCol, dt *dest);
void svm(dt s, dt* v, int n, dt* dest);
int allEqual(dt *v, int n, dt e);
int cg (dt* A, dt *b, dt *x, dt tol, int maxit, int n);

#endif
