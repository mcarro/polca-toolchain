#!/usr/bin/python

import scipy.sparse.linalg as la
import numpy as np
import datetime  as time
import random
import sys

def compute_Ident_matrix(N):
    A = np.zeros(shape=(N,N))

    for i in range(N):
        A[i,i] = 1.0

    print A
    return A

def compute_SPD_matrix(N):
    random.seed(10)
    
    print "\tComputing SPD matrix ..."
    D = np.zeros(shape=(N,N))

    S  = np.empty(shape=(N,N))
    for i in range(N):
        for j in range(N):
            S[i,j]=random.randint(1,10)

    St = S.transpose();

    for i in range(N):
        D[i,i] = 1 #random.randint(1,10)

# Take any diagonal matrix D with d_ii > 0 for all i and calculate 
# A:= S^t*D*S 
# for an arbitrary regular matrix S. A will be symmetric positive
# definite by Sylvester's law of inertia.

    D = np.dot(St,D)
    D = np.dot(D,S)
    print "\tDone!"
    # print D
    # print S
    # np.savetxt("spd_matrix.txt",D,fmt='%d',delimiter='\n',newline='\n')
    # np.savetxt("spd_matrix.txt",D,delimiter='\n',newline='\n')
    print "\tDumping SPD matrix to file ..."
    f = open('spd_matrix.txt', 'w')
    f.write('%d\n' % (N))
    for i in range(N):
        for j in range(N):
            f.write('%d\n' % (D[i,j]))
    f.closed
    print "\tDone!"

    return D


#############################################################
#                       Main function
#############################################################


# Parameter to control matrix size. Matrix will be rank x rank
# If provided in command line read it, otherwise use default value
if len(sys.argv) > 1:
    rank = int(sys.argv[1])
else:
    #default value for matrix size
    rank = 10
    print "\n----------------------------------------------------------"
    print "Note: no cmd line arg. given. Using %d as rank of matrix" % rank
    print "----------------------------------------------------------"
# Daniel example
# A = np.array([[4.,1.],
#               [1.,3.]])
# print A
# b = np.array([1, 2])
# x = np.array([0., 0.])

print("\nInitializing data for %dx%d matrix ..." % (rank,rank))
before = time.datetime.now()
A = compute_SPD_matrix(rank)
# exit(0)
b = np.ones([rank])
x = np.zeros([rank])
after  = time.datetime.now()
data_time = after - before
print "Done!"

print "\nSolving the system Ax=b ..."
# print x
before = time.datetime.now()
sol = la.cg(A,b,x,1e-15,10000)
after  = time.datetime.now()
comp_time = after - before
print "Done!"


print "----------------------------------"
# print x
print("\nCG terminated with exit code: %d" % (sol[1]))
if rank < 11:
    print("CG solution: %s" % (sol[0]))
print("\nComp Time: %f (ms)" % (comp_time.microseconds/1000.0))
print("Data Time: %f (ms)" % (data_time.microseconds/1000.0))

print "\n----------------------------------"
