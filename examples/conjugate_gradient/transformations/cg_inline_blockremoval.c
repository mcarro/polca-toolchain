#include <math.h>
#include "cg.h"
#include <stdlib.h>


dt ddot(dt * x, dt * y, int n)
{
    int i;
    dt final_sum = 0.0;
    for (i = 0; i < n; ++i)
        final_sum += x[i] * y[i];
    return final_sum;
}

void vvs(dt * v1, dt * v2, int n, dt * dest)
{
    int i;
    for (i = 0; i < n; ++i)
    {
        dest[i] = v1[i] - v2[i];
    }
}

void vva(dt * v1, dt * v2, int n, dt * dest)
{
    int i;
    for (i = 0; i < n; ++i)
    {
        dest[i] = v1[i] + v2[i];
    }
}

void vvc(dt * org, int n, dt * dest)
{
    int i;
    for (i = 0; i < n; ++i)
        dest[i] = org[i];
}

dt norm(dt * v, int n)
{
    int i;
    dt sum = 0.0;
    for (i = 0; i < n; ++i)
    {
        sum += v[i] * v[i];
    }
    return sqrt(sum);
}

void mvm(dt * m, dt * v, int n, dt * dest)
{
    int i;
    for (i = 0; i < n; i++)
    {
        dest[i] = ddot(&m[i * n], v, n);
    }
}

void svm(dt s, dt * v, int n, dt * dest)
{
    int i;
    for (i = 0; i < n; ++i)
    {
        dest[i] = s * v[i];
    }
}

int cg(dt * A, dt * b, dt * x, dt tol, int maxit, int n)
{
    int i, rval;
    dt tolb, normr, alpha, alpha2, vr, lambda;
    dt * r;
    dt * p;
    dt * v;
    dt * xt;
    dt * rt;
    dt * pt;
    r = (dt *) malloc(sizeof(dt) * n);
    p = (dt *) malloc(sizeof(dt) * n);
    v = (dt *) malloc(sizeof(dt) * n);
    xt = (dt *) malloc(sizeof(dt) * n);
    rt = (dt *) malloc(sizeof(dt) * n);
    pt = (dt *) malloc(sizeof(dt) * n);
    rval = 0;
    tolb = tol * norm(b, n);
    mvm(A, x, n, r);
    vvs(b, r, n, r);
    vvc(r, n, p);
    normr = norm(r, n);
    alpha = normr * normr;
    for (i = 0; i < maxit; ++i)
    {
        if (normr <= tolb)
        {
            break;
        }
        int polca_var_i_0;
        for (polca_var_i_0 = 0; polca_var_i_0 < n; polca_var_i_0++)
        {
            v[polca_var_i_0] = ddot(&A[polca_var_i_0 * n], p, n);
        }
        vr = ddot(v, r, n);
        lambda = alpha / vr;
        int polca_var_i_1;
        for (polca_var_i_1 = 0; polca_var_i_1 < n; ++polca_var_i_1)
        {
            xt[polca_var_i_1] = lambda * p[polca_var_i_1];
        }
        int polca_var_i_2;
        for (polca_var_i_2 = 0; polca_var_i_2 < n; ++polca_var_i_2)
        {
            x[polca_var_i_2] = x[polca_var_i_2] + xt[polca_var_i_2];
        }
        int polca_var_i_3;
        for (polca_var_i_3 = 0; polca_var_i_3 < n; ++polca_var_i_3)
        {
            rt[polca_var_i_3] = lambda * v[polca_var_i_3];
        }
        int polca_var_i_4;
        for (polca_var_i_4 = 0; polca_var_i_4 < n; ++polca_var_i_4)
        {
            r[polca_var_i_4] = r[polca_var_i_4] - rt[polca_var_i_4];
        }
        alpha2 = alpha;
        normr = norm(r, n);
        alpha = normr * normr;
        int polca_var_i_5;
        for (polca_var_i_5 = 0; polca_var_i_5 < n; ++polca_var_i_5)
        {
            pt[polca_var_i_5] = alpha / alpha2 * p[polca_var_i_5];
        }
        int polca_var_i_6;
        for (polca_var_i_6 = 0; polca_var_i_6 < n; ++polca_var_i_6)
        {
            p[polca_var_i_6] = r[polca_var_i_6] + pt[polca_var_i_6];
        }
    }
    mvm(A, x, n, r);
    vvs(b, r, n, r);
    normr = norm(r, n);
    if (normr > tolb)
    {
        rval = -3;
    }
    free(r);
    free(p);
    free(v);
    free(xt);
    free(rt);
    free(pt);
    return rval;
}

