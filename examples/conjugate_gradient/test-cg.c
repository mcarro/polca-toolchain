#include <stdio.h>
#include <stdlib.h>
#include "cg.h"
#include <sys/time.h>

/* #define N 2 */

void load_SPD_matrix(dt **M, int *rank)
{
  FILE *f;
  int value;

  char *fileName = "spd_matrix.txt";

  f=fopen(fileName, "r");
  
  if(!f){
    printf("ERROR: failed to open file %s\n",fileName);
    exit(0);
  }

  fscanf(f, "%d\n", rank);
  /* printf("%d\n",*rank); */

  printf("Loading %dx%d matrix ...",*rank,*rank);

  *M = malloc(sizeof(dt)*(*rank)*(*rank));

  int i=0;
  while (fscanf(f, "%d\n", &value) != EOF) {
    (*M)[i] = (float)value;
    /* printf("%e, ",value); */
    i++;
  }
  /* printf("\n"); */

  fclose(f);
  /* exit(0); */
}

/* void compute_SPD_matrix(dt **M, int *rank) */
/* { */
  
/* } */

int main(void) {
  int i;

  struct timeval t0,t1;

  /* dt A[N*N] = {4, 1, 1, 3}; */
  /* dt b[2] = {1, 2}; */
  /* dt x[2] = {0, 0}; */


  dt *A;
  int N;
  load_SPD_matrix(&A,&N);
  /* for(int i=0;i<N*N;i++) */
  /*   printf("%e\n",A[i]); */

  dt b[N];
  for(int i=0;i<N;i++)
    b[i] = 1;

  dt x[N];

  // Run multiple times for getting average
  long accTime = 0.0;
  int numReps = 30;
  for(int j=0;j<numReps;j++){

    for(int i=0;i<N;i++)
      x[i] = 0;

    gettimeofday(&t0, 0);

    int r = cg(A, b, x, 1e-6, 10000, N);

    gettimeofday(&t1, 0);
    long elapsed = (t1.tv_sec-t0.tv_sec)*1000000 + t1.tv_usec-t0.tv_usec;
    accTime += elapsed;

    if(r!=0)
      printf("\nCG returned value: %d", r);
    printf("\n");
    printf("CG Exec. Time: %ld", elapsed);
  }

  printf("\nCG Avg. Exec. Time: %ld\n", accTime/numReps);

  if(N<11){ //only print solution for small problems
  for(i=0; i<N; ++i)
    printf("%f ", x[i]);
  printf("\n");
  }

  return 0;
}
