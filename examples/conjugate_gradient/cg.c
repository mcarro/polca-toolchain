#include <stdlib.h>
#include <math.h>
#include "cg.h"


// calculate dot product
// foldl with zip (parallelizable)
//#pragma polca foldl MSUM zip(x, y)
dt ddot(dt* x, dt *y, int n) {
  int i;
  dt final_sum = 0.0;

  for (i = 0; i < n; ++i)
//#pragma polca def MSUM
//#pragma polca input x[i] y[i] final_sum
//#pragma polca output final_sum
    final_sum += x[i] * y[i];

  return final_sum;
}

// calculate vector-vector subtraction: dest = v1-v2
// ZipWith (parallelizable)
//#pragma polca zipWith SUB v1 v2 dest
void vvs(dt *v1, dt *v2, int n, dt* dest) {
  int i;

  for(i=0; i<n; ++i) {
//#pragma polca def SUB
//#pragma polca input v1[i] v2[i]
//#pragma polca output dest[i]
    dest[i] = v1[i] - v2[i];
  }
}

// calculate vector-vector adition: dest = v1+v2
// ZipWith (parallelizable)
//#pragma polca zipWith ADD v1 v2
void vva(dt *v1, dt *v2, int n, dt* dest) {
  int i;

  for(i=0; i<n; ++i) {
//#pragma polca def ADD
//#pragma polca input v1[i] v2[i]
//#pragma polca output dest[i]
    dest[i] = v1[i] + v2[i];
  }
}

// copy a vector into another dest := org
// memcopy (parallelizable)
//#pragma polca mem copy org sizeof(dt) n dest
void vvc(dt *org, int n, dt *dest) {
  int i;

  for(i=0; i<n; ++i)
    dest[i] = org[i];
}

// calculate norm of a vector
// foldl (parallelizable) + function call
//#pragma polca input v
dt norm(dt *v, int n) {
  int i;
  dt sum = 0.0;

//#pragma polca foldl DOUBLEADD sum v sum
  for(i=0; i<n; ++i) {
//#pragma polca DOUBLEADD
//#pragma polca input sum v[i]
//#pragma polca output sum
    sum += v[i]*v[i];
  }

  return sqrt(sum);
}

// calculate matrix vector multiplication dest = m*v
// map with zip (parallelizable)
//#pragma polca map DDOTMV zip(m, v) dest
void mvm(dt *m, dt *v, int n, dt *dest) {
  int i;

  for(i=0; i<n; i++) {
//#pragma polca def DDOTMV
//#pragma polca input &m[i*n] v
//#pragma polca output dest[i]
    dest[i] = ddot(&m[i*n], v, n);
  }
}

// calculate scalar vector multiplication dest = s*v
// map (parallelizable)
//#pragma polca map MULS v dest
void svm(dt s, dt* v, int n, dt* dest) {
  int i;
  for(i=0; i<n; ++i) {
//#pragma polca def MULS
//#pragma polca input v[i]
//#pragma polca output dest[i]
    dest[i] = s*v[i];
  }
}

// Solving Ax = b
// INPUT:
// A: nxn size Matrix (singular and positive-defined)
// b: n size vector
// x: n size vector (intial solution)
// tol: tolerance of the result
// maxit: maximum iterations
// n : size of the n-sized vectors (b and x) and the nxn A matrix
// OUTPUT:
// x: n-size vector with the final result
// RETURN:
//  0: correct result
// -3: Precision not reached after all iterations
int cg (dt* A, dt *b, dt *x, dt tol, int maxit, int n) {
  int i, rval;
  dt tolb, normr, alpha, alpha2, vr, lambda;
  dt *r;
  dt *p;
  dt *v;
  dt *xt;
  dt *rt;
  dt *pt;

  r  = (dt*) malloc(sizeof(dt) * n); // 01:
  p  = (dt*) malloc(sizeof(dt) * n);
  v  = (dt*) malloc(sizeof(dt) * n);
  xt = (dt*) malloc(sizeof(dt) * n);
  rt = (dt*) malloc(sizeof(dt) * n);
  pt = (dt*) malloc(sizeof(dt) * n);

  rval = 0;  // 02:
  tolb = tol * norm(b, n);  // 03:

  mvm(A, x, n, r); // 04: 
  vvs(b, r, n, r); // 05: 
  vvc(r, n, p);    // 06: 

  normr = norm(r, n);   // 07: 
  alpha = normr*normr;  // 08: 

  //itn -- break?
  for(i=0; i<maxit; ++i) { // 09:
    if(normr <= tolb) {    // 10:
      break;
    }
    mvm(A, p, n, v);    // 11:
    vr = ddot(v, r, n); // 12:
    lambda = alpha/vr;  //13
    
    svm(lambda, p, n, xt); //14
    vva(x, xt, n, x);      // 15
    svm(lambda, v, n, rt); // 16
    vvs(r, rt, n, r);      // 17

    alpha2 = alpha;      // 18
    normr = norm(r, n);  // 19
    alpha = normr*normr; // 20

    svm(alpha/alpha2, p, n, pt); // 21
    vva(r, pt, n, p); // 22
  }

  mvm(A, x, n, r);    // 23
  vvs(b, r, n, r);    // 24
  normr = norm(r, n); // 25

  if(normr > tolb) { // 26
    rval = -3; // 27
  }

  free(r); // 28
  free(p);
  free(v);
  free(xt);
  free(rt);
  free(pt);

  return rval; // 29
}
