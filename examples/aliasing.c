/* Manuel's example */

int main() {
  const int N = 3;
  int v[] = {1,1,1};
  int x = 1;
  int *w[] = {&x, &x, &x};
  int i;

  for (i = 1; i < N; i++) {
    v[i] = v[i-1] + v[i];
  }

  /*
   * Some transformation rules applied to the above loop might not be
   * applicable to the below one
   */

  for (i = 1; i < N; i++) {
    *w[i] = *w[i-1] + *w[i];
  }

  return 0;
}
