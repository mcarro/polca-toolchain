#include <stdio.h>

void f(int a)
{
	return;
}

void test_useless()
{
	int a,b,c,d;

	a = 3;
	b = 0;
	c = 2 + b;
	a = 4;
	f(a);
	a = 2;
	c = 3 * c;
	f(b);
	//f(d);
}

void test_sub_to_mult()
{
	int a,b,c;

	c = a * a - b * b;
	c = c * c - 2 * 2;
	c = ((a * a) * (a * a)) - ((b * b) * (b * b));
}

void test_remove_identity()
{
	int a,b,c;

	c = 0 + (a * (1 * 0))/1;
	b = (a - 0) * (1 + 0);
	a = ((c + (1 * 0)) * 1) + 0;
}

void test_remove_empty_for()
{
	int i,b;
	for(i=0;i<b;i+=2)
	{
	}
	for(i=(b++);i<b;i+=2)
	{
	}
	for(i=0;i<(b++);i+=2)
	{
	}
	for(i=0;i<b;i+=(2+(b++)))
	{
	}
}

void test_loop_rules()
{
	int a,b,c, i;

	for(i=0;i<b;i+=2)
		a*=(b+c);
}

void test_stregth_reduction()
{
	int a,b,c, i;

	for(i=0;i<b;i++)
	{
		a = b + (c*i);
	}
	a = i*2;
	for(i=0;i<b;i++)
	{
		a = b + (3*i);
	}

	// for(i=0;i<b;i++)
	// {
	// 	if (((a + 3) * i) > 5)
	// 		a++;
	// 	else
	// 		b++;
	// }

	// for(i=0;i<b;i++)
	// {
	// 	if ((3 * i) > 5)
	// 		a++;
	// 	else
	// 		b++;
	// }
}


void test_loop_fussion()
{
	int a,b,c, i,j;

	for(i=0;i<b;i++)
	{
		a++;
	}
	b = a*i;
	for(i=0;i<b;i++)
	{
		c++;
	}

	for(i=0;i<b;i++)
	{
		a++;
	}
	for(i=0;i<b;i++)
	{
		c++;
	}
}

void test_loop_interchange()
{
	int a,b,c, i,j;

	for(i=0;i<b;i++)
	{
		for(j=0;j<c;j++)
		{
			int d = i+j;
			a*=d;
		}
	}
}

void test_if_rules()
{
	int a,b,c;

	if(a>5)
	{
		a++;
		b+=2;
		if (b < 5)
			c++;
	}
	else
	{

	}
}

void test_replace()
{
	int a,b,c;

	if(a==b)
	{
		a++;
		b+=2;
		if (b < 5)
			a++;
	}

	if(a==b)
	{
		b = a * 3;
		b+=2;
		if (a < 5)
			b++;
	}
}

void test_join_assignments()
{
	int a,b,c;

	a = 3;
	b = 0;
	c = 2 + b;
	if(b > 0)
		a++;
	else
		a = 5*b;
	a = 4;
	b = 5 * a;
	a = 2;
	c = 3 * c;
}

void test_join_collapse()
{
	int i,j,a,b,p;

   for(i = 0; i < a; i++)
   {
       p = a;
        for(j = 0; j < b; j++)
        {
            p = a + b;
        }
        p = b;
   }


}





