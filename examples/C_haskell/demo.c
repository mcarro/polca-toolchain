int main()
{
    int a_r,a_c,b_r,b_c;
    int a[10][10], b[10][10], mult[10][10], i, j, k;

    a_c = 100;
    a_r = 100;
    b_c = 100; 
    b_r = a_c;

    for(i=0; i<a_r; i++)
        for(j=0; j<a_c; j++)
        {
            a[i][j] = i+j;
        }

    for(i=0; i<b_r; i++)
        for(j=0; j<b_c; j++)
        {
            b[i][j] = i==j?1:0;
        }


    for(i=0; i<a_r; i++)
        for(j=0; j<b_c; j++)
        {
            mult[i][j] = 0;
            for(k=0; k<a_c; k++)
            {
                mult[i][j]+=a[i][k]*b[k][j];
            }
        }

    return 0;
}
