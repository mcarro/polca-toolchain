--linkPolcaAnn :: CTranslUnit -> [Int]
--linkPolcaAnn :: Data b => b -> [Int]
--linkPolcaAnn :: CTranslUnit -> CTranslUnit
linkPolcaAnn ast polcaAnn = 
	[(linkOnePolcaAnn (line + 1) ast,ann) | (line,ann) <- polcaAnn]
	--gfoldl ($) id ast
	--everything id id ast
	--gshow ast
	--everything (++) ( [] `mkQ` count_fors) ast
	--node:(foldl extractDecl [] declarations)
	--where 
	--	list = everywhere (mkT (countBlocks)) (ast,0)
	--everywhere (mkT (countBlocks)) (ast,0)
	--gmapQ countBlocks ast 
	--gmapT applyRules ast
	--gmapQ count_fors ast
	--gsize2 ast

--gmapQ :: (forall d. Data d => d -> u) -> a -> [u]
--gmapT :: (forall b. Data b => b -> b) -> a -> a

--everywhere f x = map f (gmapT (everywhere f) x)

--everywhere f = f . gmapT (everywhere f)

--everywhere (mkT applyRules) ast = 
--	(mkT applyRules) .  gmapT (everywhere (mkT applyRules)) ast

--countBlocks :: ([CBlockItem],Int) -> ([CBlockItem],Int)
--countBlocks (bs,acc) = 
--	(bs,acc + sum(map count_fors bs))
--countBlocks :: [CBlockItem] -> Int
--countBlocks bs = 
--	sum (map count_fors bs)

--gsize2 :: Data a => a -> Int
--gsize2 x =
--	(if typeOf x == typeOf (undefined::CStat) 
--	 then case x of 
--	 	(CFor _ _ _ _ _) ->
--	 		1 
--	 	_ ->
--	 		0
--     else 0) + sum (gmapQ gsize2 x)
	--1 -- + sum (gmapQ gsize2 x)
--gsize2 x = 
--	0 + sum (gmapQ gsize2 x)
	--1 + sum (gmapQ gsize x)
	--case x:: of 
	--	Just b -> b:(gmapQ gsize2 x)
	--	Nothing -> gmapQ gsize2 x



--count_fors _ = 0

--extractDecl (CFDefExt node) = 
--	node

--applyRulesList :: [CBlockItem] -> [CBlockItem]
--applyRulesList bs 
--	| cond = 
--		case pats of
--			[(CBlockStmt (CExpr (Just (CAssign CAddAssOp v0 v1 _)) _)),
--		 	 (CBlockStmt (CExpr (Just (CAssign CMulAssOp v2 v3 _)) _))] ->
--				(b ++ m ++ [(CBlockStmt (CExpr (Just (CAssign CAddAssOp v0 (CBinary CAddOp v1 v3 undefNode) undefNode)) undefNode))] ++ a) 
--			_ -> 
--				usualResult
--	| otherwise = 
--		usualResult
--		--(map (applyRule) bs)
--	where 
--		(cond, inter,pats) = joinable bs []
--		[b,m,a] = inter
--		usualResult = 
--			(trace ("**********\n"++(prettyMyAST (CCompound [] bs undefNode))) (map (applyRule) bs))
--applyRulesList x = x

--applyRule
--	(CBlockStmt (CFor (Left (Just (CAssign CAssignOp var_i_0 (CBinary CSubOp var_n_1 (CConst (CIntConst var_1_2 _)) _) _))) (Just (CBinary CGeqOp var_i_3 (CConst (CIntConst var_0_4 _)) _)) (Just (CUnary CPostDecOp var_i_5 _)) (CCompound [] [CBlockStmt var_a_6,CBlockStmt var_b_7] _) _))
--	| (getCInteger var_1_2) == 1 && (getCInteger var_0_4) == 0 && (exprEqual var_i_5 var_i_3) && (exprEqual var_i_3 var_i_0) && True =
--	CBlockStmt (CFor (Left (Just (CAssign CAssignOp var_i_5 (CConst (CIntConst (cInteger 0 ) undefNode)) undefNode))) (Just (CBinary CLeOp var_i_5 var_n_1 undefNode)) (Just (CUnary CPostIncOp var_i_5 undefNode)) (CCompound [] [CBlockStmt var_b_7,CBlockStmt var_a_6] undefNode) undefNode)
--applyRule x = 
--	[]

--applyRules :: [(Name,[String])] -> CTranslUnit -> CTranslUnit
--applyRules :: CTranslUnit -> [(CStat,CStat)]
--applyRules ast =
--	 (everything (++) ( [] `mkQ` applyRules_) ast)

--applyRules_ :: [(Name,[String])] -> [CBlockItem] -> [CBlockItem]
--applyRules_ :: [CBlockItem] -> [CBlockItem]
--applyRules_ bs
--	| cond = 
--		trace (show bs) [v2,v1]
--	| otherwise = 
--		trace ("**********\n"++(prettyMyAST (CCompound [] bs undefNode))) (map (applyRule) bs)
--		--(map (applyRule) bs)
--	where 
--		(cond, list) = is_pair bs
--		[v1,v2] = list

-- a*a - b*b == (a - b) * (a + b)
--applyRule
--	(CBlockDecl (CDecl sp [(var,
--		Just
--            (CInitExpr
--            	(CBinary CSubOp
--					(CBinary CMulOp op11 op12 _)
--			        (CBinary CMulOp op21 op22 _)
--			        _)
--            	_
--            )
--		,expr)] ninfo))  
--	= 
--	(CBlockDecl (CDecl sp [(var,
--		Just
--            (CInitExpr
--			    (CBinary CMulOp
--					(CBinary CSubOp op11 op21 undefNode)
--			        (CBinary CAddOp op11 op21 undefNode)
--			        undefNode)
--			    undefNode
--            )
--		,expr)] ninfo)) 
--applyRule
--	(CBlockStmt
--		(CExpr
--			(Just
--				(CAssign CAssignOp var
--					(CBinary CSubOp
--						(CBinary CMulOp op11 op12 _)
--				        (CBinary CMulOp op21 op22 _)
--			        	_)
--	            _)
--	    	)
--		_))
--	= 
--	CBlockStmt
--		(CExpr
--			(Just
--				(CAssign CAssignOp var
--				    (CBinary CMulOp
--						(CBinary CSubOp op11 op21 undefNode)
--				        (CBinary CAddOp op11 op21 undefNode)
--				        undefNode)
--	            undefNode)
--	    	)
--		undefNode)

--applyRulesStmt :: CTranslUnit -> [(CStat,CStat)]
--applyRulesStmt ast =
--	 (everything (++) ( [] `mkQ` applyRule) ast)

--replaceNodesInfo = 
--	everywhere (mkT (replaceNodeInfo_)) 

--replaceNodeInfo_ :: NodeInfo -> NodeInfo
--replaceNodeInfo_ n = undefNode




--applyRuleExpr :: CBlockItem -> CBlockItem
--applyRuleExpr x =
	

--applyRules_ :: [(Name,[String])] -> [CBlockItem] -> [CBlockItem]
--applyRules_ polcaAnn n (b:bs) = 
--	(applyRule polcaAnn bs):nbs
--	where 
--		(rn,nbs) = (applyRules_ polcaAnn (n+1) bs)
--applyRules_ polcaAnn n [] = 
--	(n,[])

getPolcaAnn _ = unsafePerformIO (getPolcaAnn_)
getPolcaAnn_ =
	do 
		--handle <- openFile "state" ReadMode
		--contents <- hGetContents handle 
		--let linesFile = lines contents
		--rnf linesFile `seq` hClose handle
		linesFile <- readLinesFile "state"
		--print n
		let polcaAnn =  read (linesFile!!2) :: [(Name,[String])]
		return polcaAnn

--getCurrentIte = unsafePerformIO getCurrentIte_
--getCurrentIte_ =
--	do 
--		--handle <- openFile "state" ReadMode
--		--contents <- hGetContents handle 
--		--let linesFile = lines contents
--		--rnf linesFile `seq` hClose handle
--		linesFile <- readLinesFile "state"
--		--putStrLn ("Lines: " ++ (show linesFile))
--		let currentIte = read (head linesFile) :: Int
--		let newState = ((show (currentIte + 1))++"\n" ++ (linesFile!!1) ++ "\n" ++ (linesFile!!2) ++ "\n")
--		writeFile_ "state" newState
--		return currentIte

--getCurrentNode = unsafePerformIO getCurrentNode_
--getCurrentNode_ =
--	do 
--		--handle <- openFile "state" ReadMode
--		--contents <- hGetContents handle 
--		----print contents
--		--let linesFile = lines contents
--		--rnf linesFile `seq` hClose handle
--		linesFile <- readLinesFile "state"
--		--putStrLn ("Lines: " ++ (show linesFile))
--		let currentNode = read (linesFile!!1) :: Name
--		let newState = ((linesFile!!0)++"\n" ++ (show (incName currentNode)) ++ "\n" ++ (linesFile!!2) ++ "\n")
--		writeFile_ "state" newState
--		return currentNode

--storeNewAnn currentNode ann = unsafePerformIO (storeNewAnn_ currentNode ann)
----storeNewAnn currentNode ann = (mkNodeInfo nopos currentNode)
--storeNewAnn_ currentNode ann =
--	do 
--		print "antes"
--		--handle <- openFile "state" ReadMode
--		--contents <- hGetContents handle 
--		----print contents
--		--let linesFile = lines contents
--		--rnf linesFile `seq` hClose handle
--		linesFile <- readLinesFile "state"
--		print "despues"
--		--putStrLn ("Lines: " ++ (show linesFile))
--		let polcaAnn = read (linesFile!!2) :: [(Name,[String])]
--		let newState = ((linesFile!!0)++"\n" ++ (linesFile!!1) ++ "\n" ++ (show (polcaAnn ++ [(currentNode,ann)]) )++ "\n")
--		writeFile_ "state" newState
--		return (mkNodeInfo nopos currentNode)

--readUntilQuotes ('"':strtail) = 
--	("",strtail)
--readUntilQuotes (char:strtail) = 
--	((char:strread),strrest)
--	where
--		(strread,strrest) = readUntilQuotes strtail

--readUntilOpeningBracket ('(':strtail) = 
--	("",strtail)
--readUntilOpeningBracket (char:strtail) = 
--	((char:strread),strrest)
--	where
--		(strread,strrest) = readUntilOpeningBracket strtail