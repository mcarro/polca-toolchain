/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Dictionary of current functions
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////


PATTERN
both_cases(E_L Op E_R) -> 
    Match (E_L Op E_R) and (E_R Op E_L);

GENERATOR
subs((S|[S]|E),E_F,E_T) -> 
    Replace each occurrence of E_F in (S|[S]|E) to E_T;
guarded_gen(Cond,(S|[S]|E)) -> 
    If Cond, then it is possible to generate (S|[S]|E);
two_gens((S_1|[S_L1]|E_1),(S_2|[S_L2]|E_2)) -> 
    It is equivalent to define two different rules: one with (S_1|[S_L1]|E_1) and other with (S_2|[S_L2]|E_2);
inv_op(Op) ->
    The inverse operator of Op;

BOTH
bin_oper(Op,E_L,E_R) -> 
    Match/generate an operation (E_L Op E_R);
assign(Op,E_L,E_R) -> 
    Match/generate an assignment (E_L Op E_R);

CONDITIONS
is_identity(E) -> 
    True if the given array is respresenting an identity matrix;
no_defs(E_V,(S|[S]|E)) -> 
    True if there is not any assignment to E_V in (S|[S]|E);
no_defs((S|[S]|E)) -> 
    True if there is not any assignment in (S|[S]|E);
no_uses(E_V,(S|[S]|E)) -> 
    True if there is not any use of E_V in (S|[S]|E);
uses(E_V,(S|[S]|E)) -> 
    True if there is any use of E_V in (S|[S]|E);
is_const(E) -> 
    True if there is not any variable inside E;
is_comm(Op) -> 
    True if the given operator has the commutative property;
not(Cond) -> 
    True if Cond is False;
and(Cond_1,Cond_2) -> 
    True if both Cond_1 and Cond_2 are True;
or(Cond_1,Cond_2) -> 
    True if one of both Cond_1 or Cond_2 is True;

PATTERN AND/OR CONDITIONS
has_expr((S|[S]),E) -> 
    (S|[S]) has the expression E inside.

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for expressions
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// Remove a sum where one operator is 0
remove_0
{
	pattern:
	{
		both_cases(cexpr(a) + 0);
	}
	generate:
	{
		cexpr(a);
	}
}

// Remove a multiplication where one operator is 1
remove_1
{
	pattern:
	{
		both_cases(cexpr(a) * 1);
	}
	generate:
	{
		cexpr(a);
	}
}

// Reduce a operation to 0 when one operator is 0
reduce_to_0
{
    pattern:
    {
        both_cases(cexpr(a) * 0);
    }
    generate:
    {
        0;
    }
    condition:
    {
        // No assignments in the removed expression
        no_defs(cexpr(a));
    }
}

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Other rules
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// transform a element of a identity matrix in a ternary operation
identity_2_ternary
{
    pattern:
    {
        cexpr(v)[cexpr(i)][cexpr(j)];
    }
    generate:
    {
        cexpr(i)==cexpr(j)?1:0;
    }
    condition:
    {
        is_identity(cexpr(v));
    }
}

// move an external commutative operation inside a ternary expression
comm_op_2_ternary
{
    pattern:
    {
        both_cases(bin_oper(cop(op),cexpr(lop),(cexpr(cond)?cexpr(then_):cexpr(else_))));
    }
    generate:
    {
        cexpr(cond)? bin_oper(cop(op),cexpr(lop),cexpr(then_)):bin_oper(cop(op),cexpr(lop),cexpr(else_));
    }
    condition:
    {
        is_comm(cop(op));
    }
}

// move an external operation inside a ternary expression 
op_2_ternary_L
{
    pattern:
    {
        bin_oper(cop(op),cexpr(lop),(cexpr(cond)?cexpr(then_):cexpr(else_)));
    }
    generate:
    {
        cexpr(cond)? bin_oper(cop(op),cexpr(lop),cexpr(then_)):bin_oper(cop(op),cexpr(lop),cexpr(else_));
    }
    condition:
    {
        not(is_comm(cop(op)));
    }
}

// move an external operation inside a ternary expression
op_2_ternary_R
{
    pattern:
    {
        bin_oper(cop(op),(cexpr(cond)?cexpr(then_):cexpr(else_)),cexpr(lop));
    }
    generate:
    {
        cexpr(cond)? bin_oper(cop(op),cexpr(then_),cexpr(lop)):bin_oper(cop(op),cexpr(else_),cexpr(lop));
    }
    condition:
    {
        not(is_comm(cop(op)));
    }
}

// converts a assignement to a ternary in an if statement
assign_ternary_2_if
{
    pattern:
    {
        cstmts(ini);
        assign(cop(op),cexpr(lhs),cexpr(cond)?cexpr(then_):cexpr(else_));
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        if (cexpr(cond))
        {
            assign(cop(op),cexpr(lhs),cexpr(then_));
        }
        else
        {
            assign(cop(op),cexpr(lhs),cexpr(else_));
        }
        cstmts(end);
    }
}

// remove a useless assignment 
useless_assign
{
    pattern:
    {
        cstmts(ini);
        cexpr(lhs) = cexpr(lhs);
        cstmts(fin);
    }
    generate:
    {
        cstmts(ini);
        cstmts(fin);
    }
    conditions:
    {
        no_defs(cexpr(lhs));
    }
}

// remove an empty else
empty_else
{
    pattern:
    {
        if (cexpr(cond))
        {
            cstmts(then_);
        }
        else {}
    }
    generate:
    {
        if (cexpr(cond))
        {
            cstmts(then_);
        }
    }
}

// replace one expression by another knowing that 
replace_var_equal_generic
{
    pattern:
    {
        if (both_cases(cexpr(a) == cexpr(b)))
        {
            cstmts(body);
        }
    }
    generate:
    {
        if (cexpr(a) == cexpr(b))
        {
            // We can change apply two substitutions
            two_gens(
                //  a -> b
                guarded_gen(
                    no_defs(cexpr(a),cstmts(body)),
                    subs(cstmts(body),cexpr(a),cexpr(b))),
                //  b -> a
                guarded_gen(
                    no_defs(cexpr(b),cstmts(body)),
                    subs(cstmts(body),cexpr(b),cexpr(a))));
        }
    }
}

// remove a for loop that just run one statement during all its iterations
just_one_iteration_removal
{
    pattern:
    {
        for (cexpr(i) = cexpr(ini); 
            bin_oper(cop(cond),cexpr(i),cexpr(n)); 
            // Could be more general if it allows the (inc/dec)rement to be += or -=
            una_oper(cop(inc_dec),cexpr(i))
        {
            if (cexpr(i) == cexpr(other))
            {
                cstmt(one_stat);
            }
        }
    }
    generate:
    {
        // We need to check that cexpr(other) is in the for-loop iteration space
        if((cexpr(other) == cexpr(ini) || bin_oper(inv_op(cop(cond)),cexpr(other),cexpr(ini))) && bin_oper(cop(cond),cexpr(other),cexpr(n)) ) 
        {
            cstmt(one_stat);
        } 
    }
    condition:
    {
        // No def&use of cexpr(i) in the candidate statement
        no_uses(cexpr(i),cstmt(one_stat));
        no_defs(cexpr(i),cstmt(one_stat));
        // No any definition insede waht we are going to remove or replicate
        no_defs(cexpr(ini));
        no_defs(cexpr(n));
        no_defs(cexpr(other));
        no_defs(cexpr(other),cstmt(one_stat));
        // Just one step for the soundness of the condition
        or(
            op_type(cop(inc_dec),"--"),
            op_type(cop(inc_dec),"++") );
    }
}

// join two assignements to the same variable
join_assign
{
    pattern:
    {
        cstmts(ini);
        cexpr(v1) = cexpr(val_v1);
        cstmts(mid);
        cexpr(v1) = cexpr(val_v2);
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        cstmts(mid);
        two_gens(
                // We can remove the first assignment without problem.
                guarded_gen(
                    no_uses(cexpr(v1),cexpr(val_v2)),
                    cexpr(v1) = cexpr(val_v2)),
                // We need to apply a substitution. 
                // Each ocurrence of the var in the rhs is repleaced for its old value
                guarded_gen(
                    and(uses(cexpr(v1),cexpr(val_v2)),no_defs(cexpr(v1),cexpr(val_v2)) ),
                    subs(cexpr(val_v2),cexpr(v1),cexpr(val_v1)) ) );
        cstmts(end);
    }
    condition:
    {
        no_uses(cexpr(v1),cstmts(mid));
    }
}

