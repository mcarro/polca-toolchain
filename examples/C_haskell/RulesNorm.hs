{-# OPTIONS_GHC -w #-}

module RulesNorm where 

import RulesLib

import Language.C
import Language.C.Data.Ident

import Data.Generics



-- move_decl_outside_for
rule_move_decl_outside_for_0 state0 old@(CCompound ident bs nodeInfo)=
	(concat [(rule_move_decl_outside_for_3 item ident nodeInfo old state0) | item@(True,_,_) <- (rule_move_decl_outside_for_2 bs [] True )]) ++ []
rule_move_decl_outside_for_0 _ _ = []

rule_move_decl_outside_for_3 (True,[var_ini_9,var_body_10,var_end_11], [(CBlockStmt (CFor (Right (CDecl [CTypeSpec (CIntType _)] [(Just (CDeclr (Just var_i_4_ident) [] Nothing [] _),Just (CInitExpr var_init_5 _),Nothing)] _) ) (Just var_cond_6) (Just var_mod_7) (CCompound _ var_internal_8 _) _))]) ident nodeInfo old state0 | True && (and []) && True =
	concat ([[(("move_decl_outside_for", old, (CCompound ident (var_ini_9 ++ [(CBlockDecl (CDecl [CTypeSpec (CIntType undefNodeAnn)] [(Just (CDeclr (Just (var_i_4_ident)) [] Nothing [] undefNodeAnn), Nothing, Nothing)] undefNodeAnn))] ++ [(CBlockStmt (CFor (Left (Just (CAssign CAssignOp (CVar var_i_4_ident undefNodeAnn) var_init_5 undefNodeAnn))) (Just var_cond_6) (Just var_mod_7) (CCompound [] (var_body_10) undefNodeAnn) undefNodeAnn))] ++ var_end_11) nodeInfo)),state0, ((concat []) ++ (concat [])) )] | (and [])] ++ [])
rule_move_decl_outside_for_3 _ _ _ _ _ = []

rule_move_decl_outside_for_2 [] acc False =
	[(True,[acc],[])]
rule_move_decl_outside_for_2 [] acc _ =
	[(False,[acc],[])]
rule_move_decl_outside_for_2 (stat@(CBlockStmt (CFor (Right (CDecl [CTypeSpec (CIntType _)] [(Just (CDeclr (Just var_i_4_ident) [] Nothing [] _),Just (CInitExpr var_init_5 _),Nothing)] _) ) (Just var_cond_6) (Just var_mod_7) (CCompound _ var_internal_16 _) _)):tail_) accsl True =
	let
		listItems = rule_move_decl_outside_for_2 tail_ [] False 
		listItems17 = rule_move_decl_outside_for_17 var_internal_16 [] 
	in [(True, (accsl:(inter17 ++ inter)),(stat:(pats17 ++ pats))) | (True,inter,pats) <- listItems, (True, inter17, pats17) <- listItems17] ++ (rule_move_decl_outside_for_2 tail_ (accsl ++ [stat]) True)
rule_move_decl_outside_for_2 (other:tail_) acc  bool1  =
	rule_move_decl_outside_for_2 tail_ (acc ++ [other]) bool1

rule_move_decl_outside_for_17 all [] =
	[(True,[all],[])]