--  Copyright (C) 2015 The IMDEA Software Institute 
-- 	            and the Technical University of Madrid

-- All rights reserved.
-- For additional copyright information see below.

-- This file is part of the polca-transformation-rules package

-- License: This work is licensed under the Creative Commons
-- Attribution-NonCommercial-NoDerivatives 4.0 International
-- License. To view a copy of this license, visit
-- http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter
-- to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
--
-- Main author: Salvador Tamarit
-- Please address questions, etc. to: <polca-project-madrid@software.imdea.org>

{-# LANGUAGE DeriveDataTypeable #-}

module DataAnalysis where

import Language.C
import Language.C.Data.Position
import Language.C.Data.Node
import Language.C.Data.Ident
import Language.C.Data.Name

import RulesLib
import PragmaPolcaLib

import Data.Generics
import Data.List
import Data.Maybe

import Debug.Trace

import qualified Text.Groom as Gr


---------------------------------------------------------
-- Read/Write analysis
---------------------------------------------------------


data ModInfo = 
	ModInfo 
	{
		var_name :: String,
		var_id :: Int
    }
    deriving (Show, Data, Typeable)

data RWInfo = 
	RWInfo 
	{
		input :: [ModInfo],
		output :: [ModInfo]
    }
    deriving (Show, Data, Typeable)

data_ana name = 
	do
		let filename = name ++ ".c"
		ast <- parseMyFile filename
		--let ast2 = (synthesize ast (.) (mkQ id (\bl _ -> id))) ast
		let rwInfo = applyRulesGeneral get_rw_info_decl ast
		putStrLn $ show $ rwInfo


get_rw_info_decl (CFDefExt (CFunDef _ _ _ body _)) = 
	get_rw_info_stat body
get_rw_info_decl other = 
	[]


get_rw_info_stat :: CStat -> [(Name, RWInfo)]
get_rw_info_stat stat =
	case nameOfNode $ nodeInfo stat of 
		Nothing -> 
			empty_rw_info (head (namesStartingFrom 0))
		Just name -> 
			get_rw_info_stat_name stat name 

get_rw_info_stat_name :: CStat -> Name -> [(Name, RWInfo)]
get_rw_info_stat_name (CLabel _ s1 _ _) name = 
	get_rw_internal name [] [s1]
get_rw_info_stat_name (CCase e1 s1 _) name = 
	get_rw_internal name [e1] [s1]
get_rw_info_stat_name (CCases e1 e2 s1 _) name = 
	get_rw_internal name [e1, e2] [s1]
get_rw_info_stat_name (CDefault s1 _) name = 
	get_rw_internal name [] [s1]
get_rw_info_stat_name (CExpr mbe1 a) name = 
	get_rw_internal name [fromMaybe (CConst (CIntConst (cInteger 0) undefNode)) mbe1] []
get_rw_info_stat_name (CCompound _ cbitems _) name = 
	get_rw_internal name [] ([get_stats_from_block_item cbitem | cbitem <- cbitems] )
get_rw_info_stat_name (CIf e1 s1 mbs2 _) name = 
	let 
		s2 = fromMaybe (CBreak undefNode) mbs2
	in 
		get_rw_internal name [e1] [s1, s2]		
get_rw_info_stat_name (CSwitch e1 s1 _) name = 
	get_rw_internal name [e1] [s1]
get_rw_info_stat_name (CWhile e1 s1 _ _) name = 
	get_rw_internal name [e1] [s1]
get_rw_info_stat_name (CFor eitmbe1 mbe2 mbe3 s1 _) name = 
	let 
		e1 =
			case eitmbe1 of 
				(Left (Just e1_)) ->
					e1_ 
				_ -> 
					(CConst (CIntConst (cInteger 0) undefNode))
		e2 = fromMaybe (CConst (CIntConst (cInteger 0) undefNode)) mbe2
		e3 = fromMaybe (CConst (CIntConst (cInteger 0) undefNode)) mbe3
	in 
		get_rw_internal name [e1, e2, e3] [s1]
get_rw_info_stat_name (CGoto _ _) name = 
	empty_rw_info name
get_rw_info_stat_name (CGotoPtr e1 _) name = 
	get_rw_internal name [e1] []
get_rw_info_stat_name (CCont _) name = 
	empty_rw_info name
get_rw_info_stat_name (CBreak _) name = 
	empty_rw_info name
get_rw_info_stat_name (CReturn mbe1 _) name = 
	get_rw_internal name [fromMaybe (CConst (CIntConst (cInteger 0) undefNode)) mbe1] []
-- TODO: I'm not sure of the next ones
get_rw_info_stat_name (CAsm _ _) name = 
	empty_rw_info name
-- DEFAULT CASE: Not add it in order to report an error in case some case is forgotten
--get_rw_info_stat_name other _ = 
--	trace (show other) []

get_rw_info_expr :: CExpr -> [(Name, RWInfo)]
get_rw_info_expr expr = 
	case nameOfNode $ nodeInfo expr of 
		Nothing -> 
			empty_rw_info (head (namesStartingFrom 0))
		Just name -> 
			get_rw_info_expr_name expr name 

get_rw_info_expr_name :: CExpr -> Name -> [(Name, RWInfo)]
get_rw_info_expr_name (CComma es _) name = 
	get_rw_internal name es []
-- TODO: Dependign of the output lhs is read or not.
-- TODO: In single assignment not all the lhs is not read, e.g. v[i]...
get_rw_info_expr_name (CAssign _ e1 e2 _) name = 
	let 
		rwe1@((_,rw_e1):_) = get_rw_info_expr e1
		rwe2@((_,rw_e2):_) = get_rw_info_expr e2
	in 
		(name, RWInfo 
			{
				input = (input rw_e1) ++ (input rw_e2),
				output = (input rw_e1) ++ (output rw_e1) ++ (output rw_e2)
			}):(rwe1 ++ rwe2)  
get_rw_info_expr_name (CBinary _ e1 e2 _) name = 
	get_rw_internal name [e1, e2] []
get_rw_info_expr_name (CCast _ e1 a) name = 
	get_rw_internal name [e1] []
-- TODO: Not all operators write and not in all input, e.g., v[i]++ just writes in v 
get_rw_info_expr_name (CUnary op e1 _) name = 
	let 
		rwe1@((_,rw_e1):_) = get_rw_info_expr e1
	in 
		(name, RWInfo 
			{
				input = (input rw_e1),
				output = (input rw_e1) ++ (output rw_e1)
			}):rwe1
get_rw_info_expr_name (CSizeofExpr e1 _) name = 
	get_rw_internal name [e1] []
get_rw_info_expr_name (CSizeofType _ _) name = 
	empty_rw_info name
-- TODO: Not sure what the following two do
get_rw_info_expr_name (CAlignofExpr e1 _) name = 
	get_rw_internal name [e1] []
get_rw_info_expr_name (CAlignofType _ _) name = 
	empty_rw_info name
get_rw_info_expr_name (CComplexReal e1 _) name = 
	get_rw_internal name [e1] []
get_rw_info_expr_name (CComplexImag e1 _) name = 
	get_rw_internal name [e1] []
get_rw_info_expr_name (CIndex e1 e2 _) name = 
	get_rw_internal name [e1, e2] []
-- TODO: We assume now that all aruments are written
get_rw_info_expr_name (CCall e1 es _) name = 
	let 
		rwEs = 
			[let 
				rwE@((_,rw_e):_) = get_rw_info_expr e
			 in
			 	(rwE, rw_e) 
			 | e <- es]
		rwe1@((_,rw_e1):_) = get_rw_info_expr e1
	in 
		(name, RWInfo 
			{
				input = 
						(concat [input rw_e | (_, rw_e)  <- rwEs]) 
					++ 	(input rw_e1) ,
				output = 
						(concat [input rw_e | (_, rw_e)  <- rwEs]) 
					++	(concat [output rw_e | (_, rw_e)  <- rwEs]) 
					++ 	(output rw_e1)  
			}):(	(concat [rwE | (rwE, _)  <- rwEs]) 
				++ 	rwe1   ) 
get_rw_info_expr_name (CMember e1 _ _ _) name = 
	get_rw_internal name [e1] []
get_rw_info_expr_name (CVar (Ident varName varId _) _) name = 
	[(name, RWInfo 
			{input = 
				[ModInfo 
				{
					var_name = varName,
					var_id = varId
			    }],
			 output = []})]
get_rw_info_expr_name (CConst _) name = 
	empty_rw_info name
-- TODO : Remove default clause and add the following cases
--CCompoundLit (CDeclaration a) (CInitializerList a) a	
--CStatExpr (CStatement a) a	
--CLabAddrExpr Ident a	
--CBuiltinExpr (CBuiltinThing a)	
get_rw_info_expr_name _ name = 
	empty_rw_info name

empty_rw_info name = 
	[(name, RWInfo { input = [] , output = [] })]

get_rw_internal name exprs stats = 
	let 
		rwExrps = 
			[let 
				rwExpr@((_,rw_expr):_) = get_rw_info_expr expr
			 in
			 	(rwExpr, rw_expr) 
			 | expr <- exprs]
		rwStats = 
			[let 
				rwStat@((_,rw_stat):_) = (get_rw_info_stat stat)
					--trace ("Result " ++ (show (get_rw_info_stat stat)) ++ "\nStat: " ++ (show stat)) (get_rw_info_stat stat)
			 in
			 	(rwStat, rw_stat) 
			 | stat <- stats]
	in 
		(name, RWInfo 
			{
				input = 
						(concat [input rw_expr | (_, rw_expr)  <- rwExrps]) 
					++ 	(concat [input rw_stat | (_, rw_stat)  <- rwStats]) ,
				output = 
						(concat [output rw_expr | (_, rw_expr)  <- rwExrps]) 
					++ 	(concat [output rw_stat | (_, rw_stat)  <- rwStats]) 
			}):(	(concat [rwExpr | (rwExpr, _)  <- rwExrps]) 
				++ 	(concat [rwStat | (rwStat, _)  <- rwStats])   )  

get_stats_from_block_item (CBlockStmt s) = 
	s 
get_stats_from_block_item _ = 
	(CBreak undefNode)

-- TODO: Declarations can have expressions inside
-- CDecl [CDeclarationSpecifier a] [(Maybe (CDeclarator a), Maybe (CInitializer a), Maybe (CExpression a))] a

---------------------------------------------------------
-- Bounds analysis
---------------------------------------------------------

--data_ana name = 
--	do
--		let filename = name ++ ".c"
--		ast <- parseMyFile filename
--		let results = applyRulesGeneral getBounds ast
--		putStrLn (Gr.groom results)


--getBounds :: CStat -> [(Position, String, (Integer, Integer, [(Position, (String,[(String, Integer)]), (String,[(String, Integer)]))]))]
--getBounds (CFor 
--		(Left (Just (CAssign CAssignOp (ite1@(CVar iteIdent _)) ini _))) 
--		(Just (CBinary CLeOp ite2 fin _)) 
--		(Just (CUnary CPostIncOp ite3 _)) 
--		body node) 
--	| (exprEqual ite1 ite2) && (exprEqual ite2 ite3)= 
--	[(posOfNode node,identToString iteIdent,(extractValue ini,extractValue fin, getArrayAccess ite1 body))]
--getBounds _ = 
--	[]

--extractValue :: CExpr -> Integer
--extractValue (CConst (CIntConst int _)) = 
--	(getCInteger int)
--extractValue (CBinary CAddOp e1 e2 _) = 
--	(extractValue e1) + (extractValue e2) 
--extractValue (CBinary CSubOp e1 e2 _) = 
--	(extractValue e1) - (extractValue e2) 
--extractValue _ = 
--	0

--getArrayAccess ite body = 
--	applyRulesGeneral (searchAssignArrays ite) body

----(CAssign CAddAssOp var_a_463 var_b_464 _)

--searchAssignArrays :: CExpr -> CExpr -> [(Position, (String,[(String, Integer)]), (String,[(String, Integer)]))]
--searchAssignArrays ite (CAssign CAssignOp lhs rhs node) = 
--	let 
--		writeArr = nub (applyRulesGeneral (searchArrayAccess ite) lhs)
--		readArr = nub (applyRulesGeneral (searchArrayAccess ite) rhs)
--	in 
--		case (readArr,writeArr) of 
--			([],[]) -> 
--				[]
--			(_,_) ->
--				[(posOfNode node, ("r",readArr), ("w",writeArr))]
--searchAssignArrays other _ = 
--	--trace  ("OTHER: " ++ (show other)) []
--	[]

--searchArrayAccess ite (CIndex (CVar v _) i _) | not (null (uses ite [(CBlockStmt (CExpr (Just i) undefNode))])) =
--	[(identToString v, extractIndex  ite i)] 
--searchArrayAccess ite (CIndex (CVar v _) i _) | null (uses ite [(CBlockStmt (CExpr (Just i) undefNode))]) =
--	[] 
--searchArrayAccess ite (CIndex sub@(CIndex _ _ _) i _) | not (null (uses ite [(CBlockStmt (CExpr (Just i) undefNode))])) =
--	[(searchArrayName sub, extractIndex  ite i)]
--searchArrayAccess ite (CIndex sub@(CIndex _ _ _) i _) | null (uses ite [(CBlockStmt (CExpr (Just i) undefNode))]) =
--	searchArrayAccess ite sub 
--searchArrayAccess ite _ =
--	[]

--searchArrayName (CIndex (CVar v _) _ _) = 
--	identToString v
--searchArrayName (CIndex sub@(CIndex _ _ _) _ _) = 
--	searchArrayName sub

----(CIndex (CIndex var_v_466 var_i_467 _) var_j_468 _)

--extractIndex :: CExpr -> CExpr -> Integer
--extractIndex ite ind@(CVar i _) | (exprEqual ite ind) = 
--	0
--extractIndex ite (CBinary CAddOp e1 e2 _) | (exprEqual ite e1) = 
--	extractValue e2
--extractIndex ite (CBinary CSubOp e1 e2 _) | (exprEqual ite e1) = 
--	-extractValue e2 

-- Limitaciones 
-- * Como calcular el bound el ini o el end son llamadas a función o accesos a arrays?
-- * Arrays que no son afines? Accesos más complejos



