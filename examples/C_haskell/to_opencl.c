//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
// #include <stdlib.h>
#include <math.h>


#pragma polca type_size image
#pragma polca type_size redImage
#pragma polca type_size blueImage
#pragma polca type_size greenImage
size_t typeSize;

// num_items
#pragma polca total_size image
#pragma polca total_size redImage
#pragma polca total_size blueImage
#pragma polca total_size greenImage
int imageSize; 

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
    {
      r = image[i*rawWidth+j];
      g = image[i*rawWidth+j+1];
      b = image[i*rawWidth+j+2];

      (*outputImage)[i*rawWidth+j] = r;
      (*outputImage)[i*rawWidth+j+1] = 0;
      (*outputImage)[i*rawWidth+j+2] = 0;
    }
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
    {
      r = image[i*rawWidth+j];
      g = image[i*rawWidth+j+1];
      b = image[i*rawWidth+j+2];

      (*outputImage)[i*rawWidth+j] = 0;
      (*outputImage)[i*rawWidth+j+1] = g;
      (*outputImage)[i*rawWidth+j+2] = 0;
    }
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

    for(int j = 0; j < rawWidth; j+=3)
    {
      r = image[i*rawWidth+j];
      g = image[i*rawWidth+j+1];
      b = image[i*rawWidth+j+2];

      (*outputImage)[i*rawWidth+j] = 0;
      (*outputImage)[i*rawWidth+j+1] = 0;
      (*outputImage)[i*rawWidth+j+2] = b;
    }
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Applying red color filter...\n");
  printf("Applying green color filter...\n");
  printf("Applying blue color filter...\n");


  #pragma polca opencl kernel
  #pragma polca def squareOCL
  #pragma polca input image
  #pragma polca output redImage
  #pragma polca output greenImage
  #pragma polca output blueImage
  for (int j = 0; j < height * (rawWidth/3); j++)
    {
      char r,g,b;

      r = image[j*3];
      g = image[j*3+1];
      b = image[j*3+2];

      (*redImage)[j*3] = r;
      (*redImage)[j*3+1] = 0;
      (*redImage)[j*3+2] = 0;

      (*greenImage)[j*3] = 0;
      (*greenImage)[j*3+1] = g;
      (*greenImage)[j*3+2] = 0;

      (*blueImage)[j*3] = 0;
      (*blueImage)[j*3+1] = 0;
      (*blueImage)[j*3+2] = b;
    }

}

int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../../input/test.ppm";
  char* image;
  int width,height;

  LoadPPMImageArray(&image,&width,&height,filename);

  typeSize = sizeof(char);

  imageSize = width * height * 3; 

  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(image,width,height,&redImage,&greenImage,&blueImage);
  /* redcolorFilterImage(filename,"../output/img_redcolor.png"); */
  /* testFilterImage(filename,"../output/img_test.png"); */
  
  /* testFilterImage(inputImage,width,height,&blueImage); */

  SavePPMImageArray(redImage,width,height,"../../output/test_red.ppm");
  SavePPMImageArray(greenImage,width,height,"../../output/test_green.ppm");
  SavePPMImageArray(blueImage,width,height,"../../output/test_blue.ppm");

  return 0;
}
