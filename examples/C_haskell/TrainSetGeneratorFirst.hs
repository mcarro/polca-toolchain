-- Copyright (c) 2013-2016, The IMDEA Software Institute and
-- Copyright (c) 2013-2016, Universidad Politécnica de Madrid

-- See LICENSE.txt and AUTHORS.txt for licensing and authorship


{-# LANGUAGE BangPatterns #-}

module TrainSetGeneratorFirst where 

import Main as M (generateVersionsGetFirst)

import System.Environment( getArgs )

import Text.Read as TR

-- import Debug.Trace 

detFileName args = 
	case (Prelude.take 2 (Prelude.reverse (args!!0))) of 
		('c':('.':_)) ->
			(Prelude.reverse (Prelude.drop 2 (Prelude.reverse (args!!0)))) 
		_ ->
			(args!!0)
	

main = do
	args <- getArgs
	case length args of 
		4 -> 
			do 
				let mBdepth = 
					(TR.readMaybe (args!!2))::(Maybe Int)
				let mBbreath = 
					(TR.readMaybe (args!!3))::(Maybe Int)
				let (depth, breath, polcaBlock) =
					case (mBdepth, mBbreath) of 
						(Nothing, _) -> 
							(0, 0, (args!!1)) 
						(_, Nothing) -> 
							(0, 0, (args!!1)) 
						(Just vD, Just vB) ->
							(vD, vB, (args!!1))
				M.generateVersionsGetFirst (detFileName args) polcaBlock depth breath			
		_ ->
			putStrLn "Usage:\n\ttrain_set_first filename polca_block depth breath\nThere are four mandatory parameters.\n\t- filename: Name of the C file\n\t- polca_block: name of the block of code where the transformation should be performed\n\t- depth: Deepness of the branches\n\t- breath: Number of branches to be explored from the intial code."
			 