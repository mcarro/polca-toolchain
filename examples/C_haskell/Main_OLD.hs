{-# LANGUAGE OverloadedStrings, ScopedTypeVariables #-}

-- Why the expressions can not be iterated?
-- Use a language to define the rules
-- Think about what happen with the state after the transformation; Could be readed each time, and if not exists or it does not belong to the same file or the annotations stored does not correspond to the current state, then do it again.

module Main where


import Language.C
import Language.C.Data.Ident
import Language.C.System.GCC   -- preprocessor used
import Language.C.Analysis.AstAnalysis
import Language.C.Data.Name

import System.IO  
import Control.Monad
import Control.DeepSeq
import Control.Exception

import System.Environment
import Data.Generics
import Data.Generics.Twins
import Debug.Trace
import Data.Char (isSpace)

import qualified Text.Show.Pretty as Pr
import qualified Text.Groom as Gr

data PolcaAnns = PolcaAnns  [(Name,[String])] deriving (Show, Read)

main =
	trans "test"

ast name = 
	do
		ast <- parseMyFile (name ++ ".c")
		writeFile (name ++ ".ast") (Gr.groom ast)


trans name = 
	do
		--let result = geq (CVar (Ident "res" 1897203 undefNode) undefNode)  (CVar (Ident "res" 1897202 undefNode) undefNode)
		--print result
		handle <- openFile (name ++ ".c") ReadMode
		polcaAnn' <- parsePolcaAnn handle
		ast <- parseMyFile (name ++ ".c")
		let (errors,polcaAnn) = (errorsNclean polcaAnn') 
		--let polcaAnn = 
		--	case polcaAnnCleaned of 
		--		[] -> []
		--		(x:_) -> x
		--print [i | i@(n,_) <- polcaAnn, n /= -1]
		--let linkedPolcaAnn = linkPolcaAnn polcaAnn ast
		let linkedPolcaAnn = linkPolcaAnn ast polcaAnn
		--print linkedPolcaAnn
		--putStrLn "Parsed results"
		if errors /= [] 
		then error (unlines errors)
		else putStrLn ("Pragmas polca succefully read from " ++ name ++ ".c")
		putStrLn ("AST succefully read from " ++ name ++ ".c and stored in " ++ name ++ ".ast")
		--putStrLn "AST:"
		--putStrLn $ Gr.groom ast 
		writeFile (name ++ ".ast") (Gr.groom ast)
		--putStrLn "*************************************"
		--putStrLn "Original code:"
		--printMyAST ast
		--putStrLn "*************************************"
		--putStrLn "Transformed code:"
		let lastNode = getLastNode ast
		--print lastNode 
		writeFile "state" ("0\n"++ (show (incName lastNode)) ++"\n" ++ (show linkedPolcaAnn) ++ "\n")
		ast2 <- return (applyRules ast)
		ast3 <- return (applyRulesExpr ast2)
		--printMyAST ast2
		writeFile (name ++ "_transformed.c") (prettyMyAST ast3)
		hClose handle 
		putStrLn ("Transformed code stored in " ++ name ++ "_transformed.c")
		linesFile <- readLinesFile (name ++ "_transformed.c")
		let newLinkedPolcaAnn = getPolcaAnn 1
		--print newLinkedPolcaAnn
		let codeAnn = getAllNodes newLinkedPolcaAnn ast3
		--print (head linesFile)
		--print codeAnn
		let newFileContent = writePragmas linesFile (reverse codeAnn) []
		--print newFileContent
		--putStrLn (unlines newFileContent)
		writeFile (name ++ "_transformed.c") (unlines newFileContent)
		--print (getPolcaAnn 2)
		writeFile (name ++ ".ast2") (Gr.groom ast3)
		--putStrLn "*************************************"
		--putStrLn "Transformed code second iteration:"
		--ast4 <- return (applyRules ast2)
		--printMyAST ast2

getAllNodes ((nodeId,ann):xs) ast = 
		result
	where
		nodesId = (everything (++) ( [] `mkQ` (lookForNode nodeId)) ast)
		node = head nodesId
		ntail = (getAllNodes xs ast)
		result = 
			--case trace ((show nodeId) ++ " " ++ (show nodesId)) nodesId of 
			case  nodesId of 
				[] -> ntail
				_ -> (( lines( prettyMyAST node)),ann):ntail

getAllNodes [] _ = 
	[]

getLastNode :: CTranslUnit -> Name
getLastNode (CTranslUnit _ nI) = 
	case nameOfNode nI of 
		Just nodeId' -> nodeId'
		Nothing -> head newNameSupply

--lookForNode nodeId n@(CBlockStmt _ nodeInfo) =
lookForNode nodeId n@(CFor _ _ _ _ nI) =
	--case trace ((show (nameOfNode nI)) ++ " " ++ (show nodeId)) (nameOfNode nI) of 
	case (nameOfNode nI) of 
		Just nodeId' 
			| nodeId' == nodeId -> [n]
			| otherwise -> []
		Nothing -> []
lookForNode _ _ = []

parseMyFile :: FilePath -> IO CTranslUnit
parseMyFile input_file =
  do parse_result <- parseCFile (newGCC "gcc") Nothing [] input_file
     case parse_result of
       Left parse_err -> error (show parse_err)
       Right ast      -> return ast


parsePolcaAnn handle =
  do  
    --handle <- openFile input_file ReadMode
    contents <- hGetContents handle
    let linesFile = lines contents
    let numberedLines = zip [1..(length linesFile)] linesFile
    let pragmas = foldl readPolcaAnn [] numberedLines
    return pragmas

writePragmas :: [String] -> [([String],[String])] -> [String] -> [String]
writePragmas input codeAnn@((linesComp,polcaAnn):t) output =
	if (length cleanedInput) /= (length linesComp)
	then trace ("\nNOT FOUND: " ++ (show linesComp) ++ "\n")  (writePragmas (output ++ input) t [])
	else 
		if (trimList cleanedInput) == (trimList linesComp)
		then writePragmas (output ++ ["#pragma polca" ++ (foldl (\a s -> a ++ " " ++ s) "" polcaAnn)] ++ input) t []
		else writePragmas (tail input) codeAnn (output ++ [(head input)])
	where 
		cleanedInput = cleanedCode input (length linesComp)
		trimList = map trim 
writePragmas input [] _ =
	input

cleanedCode _ 0 = 
	[]
cleanedCode (x:xs) n = 
	if (getPragmaPolca x) == []
	then x:(cleanedCode xs (n-1))
	else cleanedCode xs n
cleanedCode [] n = 
	[]


errorsNclean polcaAnn =
	([(head l) | (-1,l) <- polcaAnn],
	 [i | i@(n,_) <- polcaAnn, n /= -1])

readPolcaAnn acc (numLine, line)
	| pragmaPolca /= [] =
		case (validPragma (head parsedPolcaAnn)) of 
			True ->
				acc ++ [(numLine, parsedPolcaAnn)]
			False ->
				acc ++ [(-1, ["Line " ++ (show numLine) ++ ": The following pragma polca is not recognized\n\t"++pragmaPolca])]
	| otherwise =
		acc
	where 
		pragmaPolca = 
			getPragmaPolca (trim line)
		parsedPolcaAnn = 
			words pragmaPolca

getPragmaPolca ('#':'p':'r':'a':'g':'m':'a':' ':'p':'o':'l':'c':'a':' ':tail_) = 
	tail_
getPragmaPolca _ = 
	[]

validPragma("def") = True
validPragma("mapStride") = True
validPragma(_) = False

--linkPolcaAnn :: CTranslUnit -> [Int]
--linkPolcaAnn :: Data b => b -> [Int]
--linkPolcaAnn :: CTranslUnit -> CTranslUnit
linkPolcaAnn ast polcaAnn = 
	[(linkOnePolcaAnn (line + 1) ast,ann) | (line,ann) <- polcaAnn]
	--gfoldl ($) id ast
	--everything id id ast
	--gshow ast
	--everything (++) ( [] `mkQ` count_fors) ast
	--node:(foldl extractDecl [] declarations)
	--where 
	--	list = everywhere (mkT (countBlocks)) (ast,0)
	--everywhere (mkT (countBlocks)) (ast,0)
	--gmapQ countBlocks ast 
	--gmapT applyRules ast
	--gmapQ count_fors ast
	--gsize2 ast

--Improve this. It is avoiding infinite loops just searching for the node till line 1000
linkOnePolcaAnn 1000 ast = 
	Name {nameId = -1}
linkOnePolcaAnn line ast = 
	case result of 
		list@(_:_) -> 
			last list
		[] ->
			linkOnePolcaAnn (line + 1) ast
	where 
		result = 
			everything (++) ( [] `mkQ` (lookForLine line)) ast


lookForLine :: Int -> NodeInfo -> [Name]
lookForLine line n = 
	if (posRow (posOfNode n)) == line
	then returnName
	else []
	where
		returnName = 
			case nameOfNode n of 
				Just name -> [name]
				Nothing -> []

--gmapQ :: (forall d. Data d => d -> u) -> a -> [u]
--gmapT :: (forall b. Data b => b -> b) -> a -> a

--everywhere f x = map f (gmapT (everywhere f) x)

--everywhere f = f . gmapT (everywhere f)

--everywhere (mkT applyRules) ast = 
--	(mkT applyRules) .  gmapT (everywhere (mkT applyRules)) ast

--countBlocks :: ([CBlockItem],Int) -> ([CBlockItem],Int)
--countBlocks (bs,acc) = 
--	(bs,acc + sum(map count_fors bs))
--countBlocks :: [CBlockItem] -> Int
--countBlocks bs = 
--	sum (map count_fors bs)

--gsize2 :: Data a => a -> Int
--gsize2 x =
--	(if typeOf x == typeOf (undefined::CStat) 
--	 then case x of 
--	 	(CFor _ _ _ _ _) ->
--	 		1 
--	 	_ ->
--	 		0
--     else 0) + sum (gmapQ gsize2 x)
	--1 -- + sum (gmapQ gsize2 x)
--gsize2 x = 
--	0 + sum (gmapQ gsize2 x)
	--1 + sum (gmapQ gsize x)
	--case x:: of 
	--	Just b -> b:(gmapQ gsize2 x)
	--	Nothing -> gmapQ gsize2 x



--count_fors _ = 0

--extractDecl (CFDefExt node) = 
--	node

printMyAST :: CTranslUnit -> IO ()
printMyAST ctu = (print . pretty) ctu

prettyMyAST ctu = show (pretty ctu)

--applyRules :: [(Name,[String])] -> CTranslUnit -> CTranslUnit
applyRules :: CTranslUnit -> CTranslUnit
applyRules =
	everywhere (mkT (applyRules_))

--applyRules_ :: [(Name,[String])] -> [CBlockItem] -> [CBlockItem]
--applyRules_ :: [CBlockItem] -> [CBlockItem]
--applyRules_ bs
--	| cond = 
--		trace (show bs) [v2,v1]
--	| otherwise = 
--		trace ("**********\n"++(prettyMyAST (CCompound [] bs undefNode))) (map (applyRule) bs)
--		--(map (applyRule) bs)
--	where 
--		(cond, list) = is_pair bs
--		[v1,v2] = list

applyRules_ :: CStat -> CStat
applyRules_ (CCompound ident bs nodeInfo) = 
	(CCompound ident (applyRulesList bs) nodeInfo)
applyRules_ x = x

applyRulesList :: [CBlockItem] -> [CBlockItem]
applyRulesList bs 
	| cond = 
		case pats of
			[(CBlockStmt (CExpr (Just (CAssign CAddAssOp v0 v1 _)) _)),
		 	 (CBlockStmt (CExpr (Just (CAssign CMulAssOp v2 v3 _)) _))] ->
				(b ++ m ++ [(CBlockStmt (CExpr (Just (CAssign CAddAssOp v0 (CBinary CAddOp v1 v3 undefNode) undefNode)) undefNode))] ++ a) 
			_ -> 
				usualResult
	| otherwise = 
		usualResult
		--(map (applyRule) bs)
	where 
		(cond, inter,pats) = joinable bs []
		[b,m,a] = inter
		usualResult = 
			(trace ("**********\n"++(prettyMyAST (CCompound [] bs undefNode))) (map (applyRule) bs))
--applyRulesList x = x


joinable :: [CBlockItem] -> [CBlockItem] -> (Bool, [[CBlockItem]],[CBlockItem])
joinable [] acc =
	(False ,[acc],[])
joinable (stat@(CBlockStmt (CExpr (Just (CAssign CAddAssOp _ _ _)) _)):tail_) accsl =
	let (bool,inter,pats) = joinable tail_ []
	in (bool && (length inter == 2),(accsl:inter),(stat:pats))
joinable (stat@(CBlockStmt (CExpr (Just (CAssign CMulAssOp _ _ _)) _)):tail_) accsl =
	let (_,inter,pats) = joinable tail_ []
	in (length inter == 1,(accsl:inter),(stat:pats))
joinable (other:tail_) acc =
	joinable tail_ (acc ++ [other])


applyRulesExpr :: CTranslUnit -> CTranslUnit
applyRulesExpr =
	everywhere (mkT (applyRulesExpr_))

applyRulesExpr_ :: CExpr -> CExpr
-- a*a - b*b == (a - b) * (a + b)
applyRulesExpr_ 
	(CBinary CSubOp
		(CBinary CMulOp op11 op12 _)
	    (CBinary CMulOp op21 op22 _)
		_) 
	| (exprEqual op11 op12) && (exprEqual op21 op22)
	= 
	(CBinary CMulOp
		(CBinary CSubOp op11 op21 undefNode)
        (CBinary CAddOp op11 op21 undefNode)
        undefNode)
applyRulesExpr_ x = 
	x --trace ("\nExpr:" ++ (show (cast x)) ++ "\n") x



--replaceNodesInfo = 
--	everywhere (mkT (replaceNodeInfo_)) 

--replaceNodeInfo_ :: NodeInfo -> NodeInfo
--replaceNodeInfo_ n = undefNode


exprEqual e1 e2 = 
	case e1 of 
		(CVar (Ident name1 _ _) _) ->
			case e2 of 
				(CVar (Ident name2 _ _) _) ->
					name1 == name2 
				_ -> 
					False
		(CConst (CIntConst num1 _)) -> 
			case e2 of 
				(CConst (CIntConst num2 _))  ->
					num1 == num2 
				_ -> 
					False
		_ ->
			False

--applyRuleExpr :: CBlockItem -> CBlockItem
--applyRuleExpr x =
	

--applyRules_ :: [(Name,[String])] -> [CBlockItem] -> [CBlockItem]
--applyRules_ polcaAnn n (b:bs) = 
--	(applyRule polcaAnn bs):nbs
--	where 
--		(rn,nbs) = (applyRules_ polcaAnn (n+1) bs)
--applyRules_ polcaAnn n [] = 
--	(n,[])


--applyRule :: [(Name,[String])] -> CBlockItem -> CBlockItem
applyRule :: CBlockItem -> CBlockItem
--mapStride
applyRule x@(CBlockStmt 
	(CFor 
		initDecl@(Left (Just 
			(CAssign CAssignOp 
				iter@(CVar (Ident iter1 _ _) _) 
				offset@(CConst (CIntConst offset1 _)) 
				_ ) ))  
		cond@(Just
			(CBinary CLeOp
			 (CVar (Ident iter2 _ _) _)
			 (CBinary CSubOp
			    size
			    (CConst (CIntConst offset2 _) )
			    _ )
			_ ))
		inc@(Just
			(CUnary CPostIncOp
			 (CVar (Ident iter3 _ _) _)
			 _))
      	stmt@(CCompound []
			[CBlockStmt
				(CExpr
					(Just
						(CAssign CAssignOp
							(CIndex
								(CVar outs _ )
		                        exprO _ )
		                	(CCall
		                    	(CVar f _ )
		                        [(CVar ins _),
		                         exprI]
			                _)
			        _))
			    _)]
			_)
      	ninfo)) 
	| iter1 == iter2 && iter2 == iter3 && offset1 == offset2 &&
	  [ann | (name,ann) <- polcaAnn, (nameOfNode ninfo) == Just name, (head ann) == "mapStride"] /= [] = 
	  	CBlockStmt 
		  	(CCompound []
	            [
	            CBlockDecl
	              (CDecl
	                 [CTypeSpec (CIntType undefNode)]
	                 [(Just
	                     (CDeclr
	                        (Just (mkIdent nopos ("ite"++(show currentIte)) (head newNameSupply)) )
	                        []
	                        Nothing
	                        []
	                        undefNode),
	                   Nothing, Nothing)]
	                 undefNode),
	             CBlockStmt 
					(CFor 
						(Left (Just 
							(CAssign CAssignOp 
								iteVar 
								(intConstant 0)
								undefNode) ))  
						(Just
							(CBinary CLeOp
							 iteVar
							 (CBinary CDivOp
							    size
							    (intConstant size_chunk)
							    undefNode )
							undefNode) )
						(Just
							(CUnary CPostIncOp
							 iteVar
							 undefNode))
					 	(CCompound [] 
					 		[CBlockStmt (forchunk 
					 			offset 
					 			(CBinary CSubOp (intConstant size_chunk) offset undefNode)
					 			newNode),
							CBlockStmt (CIf 
								(CBinary CNeqOp iteVar (intConstant 0) undefNode )
								(forchunk (intConstant 0) offset undefNode) 
								Nothing undefNode),
							CBlockStmt (CIf 
								(CBinary CNeqOp iteVar (CBinary CDivOp size (intConstant size_chunk) undefNode ) undefNode )
								(forchunk 
						 			(CBinary CSubOp (intConstant size_chunk) offset undefNode )
						 			(intConstant size_chunk)
						 			undefNode) 
								Nothing undefNode)
					 		]
					 		undefNode) 
					 	ninfo)
					] undefNode)
	| otherwise = 
		x
	where
		--currentIte = getCurrentIte
		polcaAnn = getPolcaAnn 1
		--currentNode = getCurrentNode
		(newNode,currentIte) = getCurrentInfo ["mapStride", "boundary:false"]
		iteVar = (CVar (mkIdent nopos ("ite"++(show currentIte)) (head newNameSupply)) undefNode) 
		forchunk ini end node = 
				(CFor 
					(Left (Just 
						(CAssign CAssignOp 
							iter 
							ini
							undefNode) ))  
					(Just
						(CBinary CLeOp
						 iter
						 end
						undefNode) )
					(Just
						(CUnary CPostIncOp
						 iter
						 undefNode))
						internalbody 
						node)
		internalbody = 
			CCompound []
				[CBlockStmt
					(CExpr
						(Just
							(CAssign CAssignOp
								(CIndex
									(CVar outs undefNode)
			                        (replace exprO iter1 iteVar) undefNode )
			                	(CCall
			                    	(CVar f undefNode )
			                        [(CVar ins undefNode),
			                         (replace exprI iter1 iteVar)]
				                undefNode)
				        undefNode))
				    undefNode)]
				undefNode
-- a*a - b*b == (a - b) * (a + b)
--applyRule
--	(CBlockDecl (CDecl sp [(var,
--		Just
--            (CInitExpr
--            	(CBinary CSubOp
--					(CBinary CMulOp op11 op12 _)
--			        (CBinary CMulOp op21 op22 _)
--			        _)
--            	_
--            )
--		,expr)] ninfo))  
--	= 
--	(CBlockDecl (CDecl sp [(var,
--		Just
--            (CInitExpr
--			    (CBinary CMulOp
--					(CBinary CSubOp op11 op21 undefNode)
--			        (CBinary CAddOp op11 op21 undefNode)
--			        undefNode)
--			    undefNode
--            )
--		,expr)] ninfo)) 
--applyRule
--	(CBlockStmt
--		(CExpr
--			(Just
--				(CAssign CAssignOp var
--					(CBinary CSubOp
--						(CBinary CMulOp op11 op12 _)
--				        (CBinary CMulOp op21 op22 _)
--			        	_)
--	            _)
--	    	)
--		_))
--	= 
--	CBlockStmt
--		(CExpr
--			(Just
--				(CAssign CAssignOp var
--				    (CBinary CMulOp
--						(CBinary CSubOp op11 op21 undefNode)
--				        (CBinary CAddOp op11 op21 undefNode)
--				        undefNode)
--	            undefNode)
--	    	)
--		undefNode)
-- Power of a number
applyRule x@(CBlockStmt 
	(CFor 
		_ 
		(Just (CBinary CLeOp _ ntimes _ ))
		_
      	(CCompound []
			[CBlockStmt
				(CExpr
					(Just
						(CAssign CAssignOp
							varResult@(CVar(Ident name1 _ _) _)
		                	(CBinary CMulOp
		                    	(CVar(Ident name2 _ _) _)
		                        varMult
			                _)
			        _))
			    _)]
			_)
      	ninfo)) 
	| name1 == name2 = 
		CBlockStmt 
			(CWhile
                 (CBinary CGrOp
                    ntimes (intConstant 0) undefNode)
                 (CCompound []
                    [CBlockStmt
                       (CIf
                          (CBinary CEqOp
                             (CBinary CRmdOp ntimes (intConstant 2) undefNode)
                             (intConstant 0)
                             undefNode)
                          (CCompound []
                             [CBlockStmt
                                (CExpr
                                   (Just
                                      (CAssign CAssignOp
                                         varResult
                                         (CBinary CMulOp
                                            varResult
                                            varResult
                                            undefNode)
                                        undefNode)
                                   )
                                undefNode),
                              CBlockStmt
                                (CExpr
                                   (Just
                                      (CAssign CAssignOp
                                         ntimes
                                         (CBinary CDivOp ntimes (intConstant 2) undefNode)
                                         undefNode))
                                   undefNode)]
                             undefNode)
                          (Just
                             (CCompound []
                                [CBlockStmt
                                   (CExpr
                                      (Just
                                         (CAssign CAssignOp
                                            varResult
                                            (CBinary CMulOp varResult (intConstant 3) undefNode)
                                            undefNode))
                                      undefNode),
                                 CBlockStmt
                                   (CExpr
                                      (Just
                                         (CAssign CAssignOp
                                            ntimes
                                            (CBinary CSubOp ntimes (intConstant 1) undefNode)
                                            undefNode))
                                      undefNode)]
                                undefNode))
                          undefNode)]
                    undefNode)
                 False
                 undefNode)
	| otherwise = 
		x
applyRule
	(CBlockStmt (CFor (Left (Just (CAssign CAssignOp var_i_0 (CBinary CSubOp var_n_1 (CConst (CIntConst var_1_2 _)) _) _))) (Just (CBinary CGeqOp var_i_3 (CConst (CIntConst var_0_4 _)) _)) (Just (CUnary CPostDecOp var_i_5 _)) (CCompound [] [CBlockStmt var_a_6,CBlockStmt var_b_7] _) _))
	| (getCInteger var_1_2) == 1 && (getCInteger var_0_4) == 0 && (exprEqual var_i_5 var_i_3) && (exprEqual var_i_3 var_i_0) && True =
	CBlockStmt (CFor (Left (Just (CAssign CAssignOp var_i_5 (CConst (CIntConst (cInteger 0 ) undefNode)) undefNode))) (Just (CBinary CLeOp var_i_5 var_n_1 undefNode)) (Just (CUnary CPostIncOp var_i_5 undefNode)) (CCompound [] [CBlockStmt var_b_7,CBlockStmt var_a_6] undefNode) undefNode)
applyRule x = 
	x


intConstant int =  (CConst (CIntConst (cInteger int) undefNode))

replace expr@(CVar (Ident iter _ _) _) iter' nite 
	| iter == iter' = CBinary CAddOp (CBinary CMulOp nite (intConstant size_chunk) undefNode) expr undefNode
	| otherwise = expr
replace expr _ _ = expr 

size_chunk = 3


trim :: String -> String
trim = f . f
   where f = reverse . dropWhile isSpace

incName (Name {nameId = id}) =
	 (Name {nameId = (id + 1)}) 


--getCurrentIte = unsafePerformIO getCurrentIte_
--getCurrentIte_ =
--	do 
--		--handle <- openFile "state" ReadMode
--		--contents <- hGetContents handle 
--		--let linesFile = lines contents
--		--rnf linesFile `seq` hClose handle
--		linesFile <- readLinesFile "state"
--		--putStrLn ("Lines: " ++ (show linesFile))
--		let currentIte = read (head linesFile) :: Int
--		let newState = ((show (currentIte + 1))++"\n" ++ (linesFile!!1) ++ "\n" ++ (linesFile!!2) ++ "\n")
--		writeFile_ "state" newState
--		return currentIte

--getCurrentNode = unsafePerformIO getCurrentNode_
--getCurrentNode_ =
--	do 
--		--handle <- openFile "state" ReadMode
--		--contents <- hGetContents handle 
--		----print contents
--		--let linesFile = lines contents
--		--rnf linesFile `seq` hClose handle
--		linesFile <- readLinesFile "state"
--		--putStrLn ("Lines: " ++ (show linesFile))
--		let currentNode = read (linesFile!!1) :: Name
--		let newState = ((linesFile!!0)++"\n" ++ (show (incName currentNode)) ++ "\n" ++ (linesFile!!2) ++ "\n")
--		writeFile_ "state" newState
--		return currentNode

--storeNewAnn currentNode ann = unsafePerformIO (storeNewAnn_ currentNode ann)
----storeNewAnn currentNode ann = (mkNodeInfo nopos currentNode)
--storeNewAnn_ currentNode ann =
--	do 
--		print "antes"
--		--handle <- openFile "state" ReadMode
--		--contents <- hGetContents handle 
--		----print contents
--		--let linesFile = lines contents
--		--rnf linesFile `seq` hClose handle
--		linesFile <- readLinesFile "state"
--		print "despues"
--		--putStrLn ("Lines: " ++ (show linesFile))
--		let polcaAnn = read (linesFile!!2) :: [(Name,[String])]
--		let newState = ((linesFile!!0)++"\n" ++ (linesFile!!1) ++ "\n" ++ (show (polcaAnn ++ [(currentNode,ann)]) )++ "\n")
--		writeFile_ "state" newState
--		return (mkNodeInfo nopos currentNode)


getPolcaAnn _ = unsafePerformIO (getPolcaAnn_)
getPolcaAnn_ =
	do 
		--handle <- openFile "state" ReadMode
		--contents <- hGetContents handle 
		--let linesFile = lines contents
		--rnf linesFile `seq` hClose handle
		linesFile <- readLinesFile "state"
		--print n
		let polcaAnn =  read (linesFile!!2) :: [(Name,[String])]
		return polcaAnn


getCurrentInfo ann = unsafePerformIO (getCurrentInfo_ ann)
getCurrentInfo_ ann =
	do 
		--handle <- openFile "state" ReadMode
		--contents <- hGetContents handle 
		--let linesFile = lines contents
		--rnf linesFile `seq` hClose handle
		linesFile <- readLinesFile "state"
		--putStrLn ("Lines: " ++ (show linesFile))
		let currentIte = read (linesFile!!0) :: Int
		let currentNode = read (linesFile!!1) :: Name
		let polcaAnn = read (linesFile!!2) ::  [(Name,[String])]
		let newState = ((show (currentIte + 1))++"\n" ++ (show (incName currentNode)) ++ "\n" ++ (show (polcaAnn ++ [(currentNode,ann)]) ) ++ "\n")
		writeFile_ "state" newState
		return ((mkNodeInfo nopos currentNode), currentIte)

writeFile_ file string = 
	do 
		handleW <- openFile file WriteMode
		hPutStr handleW string 
		rnf string `seq` hClose handleW

readLinesFile file = 
	do
	    result <- try (openFile file ReadMode) :: IO (Either SomeException Handle)
	    case result of
	        Left ex  -> 
	        	trace ("Can't read") (readLinesFile file)
	        Right handle -> 
	        	readLineFile_ handle 

readLineFile_ handle =
	do 
		contents <- hGetContents handle 
		let linesFile = lines contents
		-- force the whole file to be read, then close
		rnf linesFile `seq` hClose handle
		return linesFile
