
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#pragma polca type_size image
#pragma polca type_size redImage
#pragma polca type_size blueImage
#pragma polca type_size greenImage
size_t typeSize;

#pragma polca total_size image
#pragma polca total_size redImage
#pragma polca total_size blueImage
#pragma polca total_size greenImage

int imageSize; 

void kernelRedFilter(char* image,int i,int rawWidth, char *outputImage){
  char * im = image + i*rawWidth;
  char * ou = outputImage + i*rawWidth;
  int j;

  for(j = rawWidth / 3; j; j--) {
    *ou++ = *im;
    *ou++ = 0;
    *ou++ = 0;
    im += 3;
  }
}

void kernelGreenFilter(char* image,int i,int rawWidth,char *outputImage){
  char * im = image + i*rawWidth;
  char * ou = outputImage + i*rawWidth;
  int j;

  for(j = rawWidth / 3; j; j--) {
    *ou++ = 0;
    *ou++ = *(im+1);
    *ou++ = 0;
    im += 3;
  }

}

void kernelBlueFilter(char* image,int i,int rawWidth,char *outputImage){
  char * im = image + i*rawWidth;
  char * ou = outputImage + i*rawWidth;
  int j;

  for(j = rawWidth / 3; j; j--) {
    *ou++ = 0;
    *ou++ = 0;
    *ou++ = *(im+2);
    im += 3;
  }
}

void rgbImageFilter(char* image,int width,int height,char *redImage,char *greenImage,char *blueImage)
{

  unsigned rawWidth = width * 3;
  int i;

  printf("Applying red color filter...\n");

#pragma polca opencl kernel
#pragma polca def redFilter
#pragma polca map image redImage
  for (i = 0; i < height; i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
    }
  printf("Applying green color filter...\n");

#pragma polca opencl kernel
#pragma polca def greenFilter
#pragma polca map image greenImage
  for (i = 0; i < height; i++)
    {
      kernelGreenFilter(image,i,rawWidth,greenImage);
    }

  printf("Applying blue color filter...\n");
  
#pragma polca opencl kernel
#pragma polca def blueFilter
#pragma polca map image blueImage
  for (i = 0; i < height; i++)
    {
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }

}

int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../input/test.ppm";
  char* inputImage;
  int width,height;

  LoadPPMImageArray(&inputImage,&width,&height,filename);

  typeSize = sizeof(char);
  imageSize = width * height * 3;  
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,redImage,greenImage,blueImage);
  /* redcolorFilterImage(filename,"../output/img_redcolor.png"); */
  /* testFilterImage(filename,"../output/img_test.png"); */
  
  /* testFilterImage(inputImage,width,height,&blueImage); */

  SavePPMImageArray(redImage,width,height,"../output/test_red.ppm");
  SavePPMImageArray(greenImage,width,height,"../output/test_green.ppm");
  SavePPMImageArray(blueImage,width,height,"../output/test_blue.ppm");

  return 0;
}

