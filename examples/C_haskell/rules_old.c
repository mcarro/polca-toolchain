//  Copyright (C) 2015 The IMDEA Software Institute 
// 	            and the Technical University of Madrid
// 
// All rights reserved.
// For additional copyright information see below.
// 
// This file is part of the polca-transformation-rules package
// 
// License: This work is licensed under the Creative Commons
// Attribution-NonCommercial-NoDerivatives 4.0 International License. To
// view a copy of this license, visit
// http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
// Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
//
// Main author: Salvador Tamarit
// Please address questions, etc. to: <polca-project-madrid@software.imdea.org>


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for expressions
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// TODO: Remove. Just to test whether unknown is working
// fake_rule
// {
//     pattern:
//     {
//         cexpr(a) += cexpr(b) ;
//     }
//     condition:
//     {
//         fake1(cexpr(a));
//         fake2(cexpr(b));
//     }
//     generate:
//     {
//         cexpr(a) = cexpr(a) + cexpr(b);
//     }
// }


// Remove the identity of a binary operator
remove_identity
{
	pattern:
	{
		bin_op(cop(op),cexpr(a),cexpr(b));
	}
	condition:
	{
		is_identity(cop(op),cexpr(b));
        pure(cexpr(b));
	}
	generate:
	{
		cexpr(a);
	}
}

// Reduce a operation to 0 when the right operator is 0
reduce_to_0
{
    pattern:
    {
        cexpr(a) * 0;
    }
    condition:
    {
        pure(cexpr(a));
    }
    generate:
    {
        0;
    }
}

undo_distributive
{
    pattern:
    {
        //bin_op(cop(op1),bin_op(cop(op2),cexpr(b),cexpr(a)),bin_op(cop(op2),cexpr(c),cexpr(a)));
        (cexpr(b) * cexpr(a)) + (cexpr(c) * cexpr(a));
    }
    condition:
    {
        pure(cexpr(a));
        pure(cexpr(b));
        pure(cexpr(c));
        //is_distributive(cop(op2) or "*",cop(op1));
    }
    generate:
    {
        cexpr(a) * (cexpr(b) + cexpr(c));
        //bin_op(cop(op1),bin_op(cop(op2),cexpr(b),cexpr(a)),bin_op(cop(op2),cexpr(c),cexpr(a)));
    }
}

// Change a substraction aa - bb to multiplication (a-b) + (a+b)
sub_to_mult
{
    pattern:
    {
        cexpr(a) * cexpr(a) - cexpr(b) * cexpr(b);
    }
    generate:
    {
        (cexpr(a) - cexpr(b)) * (cexpr(a) + cexpr(b));
    }
}


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for loops
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// rule to normalize iteration step of a for loop, i.e. for example:
// for(i=0;i<N;i+=3){body}
// becomes
// for(i=0;i<(N/3);i++){subs(body,i,i*3)}
// This rule can be generalized in many ways. Left like this for the moment to get
// it working for the review at Oslo
normalize_iteration_step
{
    pattern:
    {
        for(cexpr(i) = cexpr(initial_value); cexpr(i) < cexpr(n); cexpr(i) += cexpr(step))
	{
            cstmts(body);
        }
    }
    condition:
    {
        no_writes(cexpr(i), cstmts(body));
    }
    generate:
    {
        for(cexpr(i) = cexpr(initial_value); cexpr(i) < (cexpr(n)/cexpr(step)); cexpr(i) ++)
        {
	       subs(cstmts(body), cexpr(i), cexpr(i)  * cexpr(step));
        }
    }
}



// Transform a for statement without block in a if statemst with block
for_wo_block_2_for_w_block
{
    pattern:
    {
        for(cexpr(i) = cexpr(initial_value); cexpr(cond); cexpr(inc))
            cstmt(body);
    }
    condition:
    {
    	not(is_block(cstmt(body)));
    }
    generate:
    {
        for(cexpr(i) = cexpr(initial_value); cexpr(cond); cexpr(inc))
        {
            cstmt(body);
        }
    }
}

// Empty for loop removal
remove_empty_for
{
    pattern:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(initial_value); cexpr(cond); cexpr(inc))
        {}
        cstmts(end);
    }
    condition:
    {
        pure(cexpr(initial_value));
        pure(cexpr(cond));
        pure(cexpr(i));
        // TODO: treat correctly to allow apply the rule when i++ o i+=expr
        //pure(cexpr(inc));
        // It should be that writes(inc) \subset {i}
        // end (and reachable statements) should not use i.
    }
    generate:
    {
        cstmts(ini);
        cstmts(end);
    }
}

// Change the order of the for loop. From decremental to incremental.
loop_reversal_d2i
{
	pattern:
	{
		for(cexpr(i) = cexpr(n) - 1; cexpr(i) >= 0; cexpr(i)--)
		{
			cstmts(a);
		}
	}
	condition:
	{
		pure(cexpr(n));
        no_writes(cexpr(i),cstmts(a));
	}
	generate:
	{
		for(cexpr(i) = 0; cexpr(i) < cexpr(n); cexpr(i)++)
		{
			cstmts(a);
		}
	}
}

// Change the order of the for loop. From incremental to decremental.
loop_reversal_i2d
{
	pattern:
	{
		for(cexpr(i) = 0; cexpr(i) < cexpr(n); cexpr(i)++)
		{
			cstmts(a);
		}
	}
	condition:
	{
		pure(cexpr(n));
        no_writes(cexpr(i),cstmts(a));
	}
	generate:
	{
		for(cexpr(i) = cexpr(n) - 1; cexpr(i) >= 0; cexpr(i)--)
		{
			cstmts(a);
		}
	}
    postcondition:
    {
        same_properties();
    }
}

// Transform a for into a while
for_to_while
{
    pattern:
    {
        for(cexpr(i) = cexpr(initial_value); cexpr(i) < cexpr(n); cexpr(i)++)
		{
			cstmts(body);
		}
    }
    generate:
    {
    	cexpr(i) = cexpr(initial_value);
        while(cexpr(i) < cexpr(n))
		{
			cstmts(body);
			cexpr(i)++;
		}
    }
}

// Transform a while into a for
while_to_for
{
    pattern:
    {
        cstmts(ini);
        cexpr(i) = cexpr(initial_value);
        cstmts(mid);
        while(cexpr(i) < cexpr(n))
        {
            cstmts(body1);
            cexpr(i)++;
            cstmts(body2);
        }
        cstmts(end);
    }
    condition:
    {
    	no_rw(cexpr(i),cstmts(mid));
    	no_rw(cexpr(i),cstmts(body2));
    }
    generate:
    {
        cstmts(ini);
        cstmts(mid);
        for(cexpr(i) = cexpr(initial_value); cexpr(i) < cexpr(n); cexpr(i)++)
        {
            cstmts(body1);
            cstmts(body2);
        }
        cstmts(end);
    }
}


// for_loop_fusions2
// {
//     pattern:
//     {
//         cstmts(ini);
//         for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
//         {
//             cstmts(bodyFOR1);
//         }
//         cstmts(mid);
//         for(cexpr(j) = cexpr(init);cexpr(j) < cexpr(n);cexpr(modj))
//         {
//             cstmts(bodyFOR2);
//         }
//         cstmts(fin);
//     }
//     condition:
//     {
//         // no_reads(cexpr(i),cstmts(mid));
//     // !anti_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
//     // !out_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
//     // !flow_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
//     // no_writes_except_arrays(cstmts(bodyFOR2),cstmts(bodyFOR1))
//         not(has_calls(cstmts(bodyFOR1)));
//         not(has_calls(cstmts(bodyFOR2)));
//     }
//     generate:
//     {
//         cstmts(ini);
//         cstmts(mid);
//         for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
//         {
//             cstmts(bodyFOR1);
//             subs(cstmts(bodyFOR2), cexpr(j), cexpr(i));
//         }
//         cstmts(fin);
//     }
// }

// Fusion of two for-loops into a single one. This rule only should be applied ...
for_loop_fusion
{
    pattern:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
        {
            cstmts(bodyFOR1);
        }
        cstmts(mid);
        for(cexpr(j) = cexpr(init);cexpr(j) < cexpr(n);cexpr(modj))
        {
            cstmts(bodyFOR2);
        }
        cstmts(fin);
    }
    condition:
    {
        no_reads(cexpr(i),cstmts(mid));
        // This only shuold be a condtion when i/= j
        // no_rw(cexpr(i),cstmts(bodyFOR2));
        no_writes(cexpr(i),cstmts(bodyFOR2));
        no_writes(cexpr(j),cstmts(bodyFOR1));


    // !anti_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
    // !out_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
    // !flow_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
    // no_writes_except_arrays(cstmts(bodyFOR2),cstmts(bodyFOR1))
        // not(has_calls(cstmts(bodyFOR1)));
        // not(has_calls(cstmts(bodyFOR2)));

    }
    generate:
    {
        cstmts(ini);
        cstmts(mid);
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
        {
            cstmts(bodyFOR1);
            subs(cstmts(bodyFOR2), cexpr(j), cexpr(i));
        }
        cstmts(fin);
    }
}


for_loop_fusion_pragma
{
    pattern:
    {
        cstmts(ini);
        #pragma polca def a
        #pragma polca map inputa outputa
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
        {
            cstmts(bodyFOR1);
        }
        cstmts(mid);
        #pragma polca def b
        #pragma polca map inputb outputb
        for(cexpr(j) = cexpr(init);cexpr(j) < cexpr(n);cexpr(modj))
        {
            cstmts(bodyFOR2);
        }
        cstmts(fin);
    }
    condition:
    {
        no_reads(cexpr(i),cstmts(mid));
        no_reads(cexpr(j),cstmts(fin));
        not(has_calls(cstmts(bodyFOR1)));
        not(has_calls(cstmts(bodyFOR2)));
    }
    generate:
    {
        cstmts(ini);
        cstmts(mid);
        #pragma polca same_properties a
        #pragma polca same_properties b
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
        {
            cstmts(bodyFOR1);
            subs(cstmts(bodyFOR2), cexpr(j), cexpr(i));
        }
        cstmts(fin);
    }
}



// Fusion of two for-loops into a single one. This rule only should be applied ...
// for_loop_fusion2
// {
//     pattern:
//     {
//         cstmts(ini);
//         #pragma polca def polcavar(a)
//         #pragma map polcavar(inputa) polcavar(outputa)
//         for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(mod))
//         {
//             cstmts(bodyFOR1);
//         }
//         #pragma polca def polcavar(b)
//         #pragma map polcavar(inputb) polcavar(outputb)
//         for(cexpr(i) = cexpr(init); cexpr(i) < cexpr(n);cexpr(mod))
//         {
//             cstmts(bodyFOR2);
//         }
//         cstmts(fin);
//     }
//     // condition:
//     // {
//     // // !anti_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
//     // // !out_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
//     // // !flow_dep(cstmts(bodyFOR1),cstmts(bodyFOR2),cexpr(i))
//     // // no_writes_except_arrays(cstmts(bodyFOR2),cstmts(bodyFOR1))
//     // }
//     generate:
//     {
//         cstmts(ini);
//         #pragma polca def concat(polcaV(a),polcaV(b))
//         #pragma polca same_properties polcaV(a)
//         #pragma polca same_properties polcaV(b)
//         for(cexpr(i) = cexpr(init); cexpr(i) < cexpr(n);cexpr(mod))
//         {
//             cstmts(bodyFOR1);
//             cstmts(bodyFOR2);
//         }
//         cstmts(fin);
//         cstmts(ini);
//     }
// }

// // Fusion of two for-loops into a single one. This rule only should be applied ...
// for_loop_fusion_pragma
// {
//     pattern:
//     {
//         cstmts(ini);

//         #pragma polca def(A)
//         #pragma polca can_be_fused_with(B)
//         for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
//         {
//             cstmts(bodyFOR1);
//         }
//         cstmts(mid);
//         #pragma polca def(B)
//         #pragma polca can_be_fused_with(A)
//         for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
//         {
//             cstmts(bodyFOR2);
//         }
//         cstmts(fin);
//     }
//     condition:
//     {
//         no_reads(cexpr(i),cstmts(mid));
//     }
//     generate:
//     {
//         cstmts(ini);
//         cstmts(mid);
//         for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
//         {
//             cstmts(bodyFOR1);
//             cstmts(bodyFOR2);
//         }
//         cstmts(fin);
//     }
// }

// Fission of two for-loops into a single one. AKA loop distribution or loop splitting
for_loop_fission
{
    pattern:
    {
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(body1);
            cstmt(mid);
            cstmts(body2);
        }
    }
    condition:
    {
        no_writes(cexpr(i),cstmts(body1));
        no_writes(cexpr(i),cstmt(mid));
        no_writes(cexpr(i),cstmts(body2));
        no_reads_in_written(cstmt(mid),cstmts(body2));
        no_reads_in_written(cstmts(body1),cstmts(body2));
        no_empty(cstmts(body2));
    }
    generate:
    {   
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(body1);
            cstmt(mid);
        }
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(body2);
        }
    }
}

// Loop-based strength reduction 
strength_reduction
{
    pattern:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(ini1);
            //has_expr(cstmt(goal),(cexpr(c) * cexpr(i)));
            // TODO: Generalize better
            cexpr(a) = cexpr(b) + (cexpr(c) * cexpr(i));
            cstmts(end1);
        }
        cstmts(end);
    }
    condition:
    {
        is_cons(cexpr(c));
    }
    generate:
    {   
        cstmts(ini);
        // TODO: Introduce a free variable
        cdecl(cint(),cexpr(t));
        cexpr(t) = 0;
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(ini1);
            //cstmt(goal);
            cexpr(a) = cexpr(b) + t;
            t += cexpr(c);
            cstmts(end1);
        }
        cstmts(end);
    }
}

// Loop interchange
loop_interchange
{
    pattern:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(init_i);cexpr(i) < cexpr(n_i);cexpr(i)++)
        {
            for(cexpr(j) = cexpr(init_j);cexpr(j) < cexpr(n_j);cexpr(j)++)
            {
                cstmts(body);
            }
        }
        cstmts(end);
    }
    condition:
    {
    	no_rw(cexpr(i),cexpr(init_j));
    	no_rw(cexpr(i),cexpr(n_j));
        no_rw(cexpr(i),cexpr(body));
        no_rw(cexpr(j),cexpr(init_i));
        no_rw(cexpr(j),cexpr(n_i));
        no_rw(cexpr(j),cexpr(body));
    }
    generate:
    {   
        cstmts(ini);
        for(cexpr(j) = cexpr(init_j);cexpr(j) < cexpr(n_j);cexpr(j)++)
        {
            for(cexpr(i) = cexpr(init_i);cexpr(i) < cexpr(n_i);cexpr(i)++)
            {
                cstmts(body);
            }
        }
        cstmts(end);
    }
}

// Loop interchange (pragma version)
loop_interchange_pragma
{
    pattern:
    {
        cstmts(ini);
        #pragma polca has_loop_carried_dependence false 
        #pragma polca def a
        for(cexpr(i) = cexpr(init_i);cexpr(cond_i) < cexpr(n_i);cexpr(i)++)
        {
            for(cexpr(j) = cexpr(init_j);cexpr(j) < cexpr(n_j);cexpr(j)++)
            {
                cstmts(body);
            }
        }
        cstmts(end);
    }
    condition:
    {
        no_rw(cexpr(i),cexpr(init_j));
        no_rw(cexpr(i),cexpr(n_j));
        no_rw(cexpr(i),cstmts(body));
        no_rw(cexpr(j),cexpr(init_i));
        no_rw(cexpr(j),cexpr(n_i));
        no_rw(cexpr(j),cstmts(body));
    }
    generate:
    {   
        cstmts(ini);
        #pragma polca has_loop_carried_dependence false 
        #pragma polca same_properties a
        for(cexpr(j) = cexpr(init_j);cexpr(j) < cexpr(n_j);cexpr(j)++)
        {
            for(cexpr(i) = cexpr(init_i);cexpr(i) < cexpr(n_i);cexpr(i)++)
            {
                cstmts(body);
            }
        }
        cstmts(end);
    }
}



/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for if statements
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// Transform a if statement without block in a if statemst with block
if_wo_block_2_if_w_block
{
    pattern:
    {
        if(cexpr(cond))
            cstmt(body);
    }
    condition:
    {
    	not(is_block(cstmt(body)));
        not(is_block(cstmt(body)));
    }
    generate:
    {
        if(cexpr(cond))
        {
            cstmt(body);
        }
    }
}

// Transform a if statement without else block into a if statement with
if_wo_else_2_if_w_else
{
    pattern:
    {
        if(cexpr(cond))
		{
			cstmts(body);
		}
    }
    generate:
    {
        if(cexpr(cond))
		{
			cstmts(body);
		}
		else
		{}
    }
}


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Partial Evaluation
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////




/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for Manuel's Transformation
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// Transform a addition assignment into a simple assignment
split_addition_assign
{
    pattern:
    {
        cexpr(a) += cexpr(b);
    }
    condition:
    {
        pure(cexpr(a));
    }
    generate:
    {
        cexpr(a) = cexpr(a) + cexpr(b);
    }
}

// Transform a simple assignment into an addition assignment  
join_addition_assign
{
    pattern:
    {
        cexpr(a) = cexpr(a) + cexpr(b);
    }
    condition:
    {
        pure(cexpr(a));
    }
    generate:
    {
        cexpr(a) += cexpr(b);
    }
}

// transform a element of a identity matrix in a ternary operation
// identity_2_ternary
// {
//     pattern:
//     {
//         cexpr(v)[cexpr(i)][cexpr(j)];
//     }
//     condition:
//     {
//         //is_identity(cexpr(v));
//     }
//     generate:
//     {
//         cexpr(i)==cexpr(j)?1:0;
//     }
// }

// move external multiplication operation inside a ternary expression
mult_ternary_2_ternary
{
    pattern:
    {
        cexpr(lop) * (cexpr(cond)?cexpr(then_):cexpr(else_));
    }
    generate:
    {
        cexpr(cond)? cexpr(lop) * cexpr(then_):cexpr(lop) * cexpr(else_);
    }
}

// move external multiplication operation inside a ternary expression
sum_ternary_2_ternary
{
    pattern:
    {
        cexpr(lop) + (cexpr(cond)?cexpr(then_):cexpr(else_));
    }
    generate:
    {
        cexpr(cond)? cexpr(lop) + cexpr(then_):cexpr(lop) + cexpr(else_);
    }
}

// converts a assignement to a ternary in an if statement
assign_ternary_2_if
{
    pattern:
    {
        cstmts(ini);
        cexpr(lhs) = cexpr(cond)?cexpr(then_):cexpr(else_);
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        if (cexpr(cond))
        {
            cexpr(lhs) = cexpr(then_);
        }
        else
        {
            cexpr(lhs) = cexpr(else_);
        }
        cstmts(end);
    }
}

// remove a useless assignment 
useless_assign
{
    pattern:
    {
        cstmts(ini);
        cexpr(lhs) = cexpr(lhs);
        cstmts(fin);
    }
    condition:
    {
        pure(cexpr(lhs));
    }
    generate:
    {
        cstmts(ini);
        cstmts(fin);
    }
}


// remove an empty else
empty_else
{
    pattern:
    {
        if (cexpr(cond))
        {
            cstmts(then_);
        }
        else {}
    }
    generate:
    {
        if (cexpr(cond))
        {
            cstmts(then_);
        }
    }
}

// Replace an expression that is equal to another in 
// an if statement context
replace_var_equal
{
    pattern:
    {
        if (cexpr(a) == cexpr(b))
        {
            cstmts(body);
        }
    }
    generate:
    {
        if (cexpr(a) == cexpr(b))
        {
        	gen_list:
        	{	
        		if_then:
        		{
        			no_writes(cexpr(a),cstmts(body));
        			subs(cstmts(body),cexpr(a),cexpr(b));
        		}
        		if_then:
        		{
        			no_writes(cexpr(b),cstmts(body));
        			subs(cstmts(body),cexpr(b),cexpr(a));
        		}
            }
        }
    } 
}

// remove a for loop that just run one statemnt during its iterations
just_one_iteration_removemal
{
    pattern:
    {
        for (cexpr(i) = cexpr(ini); cexpr(i) < cexpr(n); cexpr(i)++)
        {
            if (cexpr(i) == cexpr(other))
            {
                cstmt(one_stat);
            }
        }
    }
    generate:
    {
        subs(cstmt(one_stat),cexpr(i),cexpr(other));
    }
}

// Join two assignments to the same variable in a single one
join_assignments
{
    pattern:
    {
        cstmts(ini);
        cexpr(v1) = cexpr(val_v1);
        cstmts(mid);
        cexpr(v1) = cexpr(val_v2);
        cstmts(end);
    }
    condition:
    {
        //no_reads(cexpr(v1),cstmts(mid));
        // no_writes(cexpr(v1),cstmts(mid));
        // no_rw(cexpr(v1),cstmts(mid));
        no_writes_in_read(cexpr(v1),cstmts(mid));
        no_writes(cexpr(v1),cexpr(val_v1));        
    }
    generate:
    {
        cstmts(ini);
        cstmts(mid);
        cexpr(v1) = subs(cexpr(val_v2),cexpr(v1),cexpr(val_v1));
        cstmts(end);
    }  
}


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for Guillermo's Transformations
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// scalar expansion
introduce_aux_array
{
	pattern:
	{
		cstmts(ini);
		cexpr(c) = 0.0;
		cstmts(mid);
		for(cexpr(i) = 0;cexpr(i) < cexpr(n);cexpr(i)++)
		{
			cstmts(body1);
			cexpr(c) += cexpr(a)[cexpr(i)] * cexpr(b)[cexpr(i)];
			cstmts(body2);
		}
		cstmts(end);
	}
	generate:
	{
		cstmts(ini);
		cexpr(c) = 0.0;
		int d[cexpr(n)];
		cstmts(mid);
		for(cexpr(i) = 0;cexpr(i) < cexpr(n);cexpr(i)++)
		{
			cstmts(body1);
			d[cexpr(i)] = cexpr(a)[cexpr(i)] * cexpr(b)[cexpr(i)];
			cexpr(c) += d[cexpr(i)];
			cstmts(body2);
		}
		cstmts(end);
	}
}


// for_chunks
// {
// 	pattern:
// 	{
// 		cstmts(ini);
// 		for(cexpr(i) = cexpr(e0);cexpr(i) < cexpr(n);cexpr(i)++)
// 		{
// 			cstmts(body);
// 		}
// 		cstmts(end);
// 	}
//         condition:
//         {
// 	  no_writes(cexpr(i),cstmts(body));
//         }
// 	generate:
// 	{
// 		cstmts(ini);
// 		int j;
// 		int num_proc;
// 		for(cexpr(i) = 0;cexpr(i) < num_proc;cexpr(i)++)
// 		{
// 		  int prev_chunk_size = height / num_proc;
// 		  int curr_chunk_size = i != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

// 		  for(j=i*prev_chunk_size;j<((i+1)*curr_chunk_size);j++)
// 		  {
// 		    subs(cstmts(body),cexpr(i),j);
// 		  }
// 		}
// 		cstmts(end);
// 	}
// 	//post-condition:
// 	//{
// 	//  fresh(j);
// 	//}
// }

for_chunk
{
    pattern:
    {
        cstmts(ini);
        #pragma polca def a 
        for(cexpr(i) = 0; cexpr(i) < cexpr(n); cexpr(i)++)
        {
            cstmts(body);
        }
        cstmts(end);
    }
    condition:
    {
      no_writes(cexpr(i),cstmts(body));
    }
    generate:
    {
        //num_proc should be declared in the program
        int num_proc = 4;
        cstmts(ini);
        #pragma polca same_properties a
        #pragma polca chunked cexpr(n)
        #pragma polca prev_chunk_size cexpr(prev_chunk_size)
        #pragma polca curr_chunk_size cexpr(curr_chunk_size)        
        for(cexpr(i) = 0; cexpr(i) < num_proc; cexpr(i)++)
        {
          cdecl(cint(),cexpr(prev_chunk_size));
          cexpr(prev_chunk_size) = cexpr(n) / num_proc;
          cdecl(cint(),cexpr(curr_chunk_size));
          cexpr(curr_chunk_size) = 
            cexpr(i) != (num_proc-1) ? 
            cexpr(prev_chunk_size) : 
            cexpr(prev_chunk_size) + cexpr(n) % num_proc;
          cdecl(cint(),cexpr(j));

          for(cexpr(j)=cexpr(i)*cexpr(prev_chunk_size);
                cexpr(j) < ((cexpr(i)+1)*cexpr(curr_chunk_size));
                cexpr(j)++)
          {
            subs(cstmts(body), cexpr(i), cexpr(j));
          }
        }
        cstmts(end);
    }
}

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for FM paper
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

loop_inv_code_motion
{
    pattern:
    {
        cstmts(body0_1);
        for (cexpr(i) = cexpr(ini); cexpr(cond); cexpr(inv))
        {
            cstmts(body1_1);
            cexpr(c) = cexpr(a) * cexpr(b);
            cstmts(body2_1);
        }
        cstmts(body0_2);
    }
    condition:
    {
        no_writes(cexpr(i),cexpr(b));
    }
    generate:
    {   
        cstmts(body0_1);
        cdecl(cint(),cexpr(k));
        cexpr(k) = cexpr(b);
        for (cexpr(i) = cexpr(ini); cexpr(cond); cexpr(inv))
        {
            cstmts(body1_1);
            cexpr(c) = cexpr(a) * cexpr(k);
            cstmts(body2_1); 
        }
        cstmts(body0_2);
    }
}


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Rules for Gaussian Pyramid
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////



// Improved chunking of a for loop
// new_for_chunks
// {
//     pattern:
//     {
//         cstmts(s1);
//         for(cexpr(i) = cexpr(ini);cexpr(i) < cexpr(n);cexpr(inc))
//         {
//             cstmts(body);
//         }
//         cstmts(s2);
//     }
//     condition:
//     {
//         // cexpr(n) is pure
//         // cexpr(i) is pure
//         // cexpr(inc) only can modify cexpr(i)
//         // cstmts(body) cannot modify cexpr(i)
//     }
//     generate:
//     {
//         cstmts(s1);
//         cexpr(i) = cexpr(ini);
//         // num_proc is assigned to a value by the system when applying the rule
//         int P = num_proc;
//         int t;
//         for (t = 0; t < P; t++) 
//         {
//             int limit = t == (P-1) ? (t*(cexpr(n)/P)+(cexpr(n)/P)) + (cexpr(n)%P) : t*(cexpr(n)/P)+(cexpr(n)/P);
//             for (; cexpr(i) < limit; cexpr(inc)) {
//               cstmts(body);
//             }
//         }
//         cstmts(s2);
//     }
// }

// // Improved chunking of a for loop
// new_for_chunks_2
// {
//     pattern:
//     {
//         cstmts(s1);
//         for(cexpr(i) = cexpr(ini);cexpr(i) < cexpr(n);cexpr(inc))
//         {
//             cstmts(body);
//         }
//         cstmts(s2);
//     }
//     condition:
//     {
//         // cexpr(n) is pure
//         // cexpr(i) is pure
//         // cexpr(inc) only can modify cexpr(i)
//         // cstmts(body) cannot modify cexpr(i)
//     }
//     generate:
//     {
//         cstmts(s1);
//         cexpr(i) = cexpr(ini);
//         // num_proc is assigned to a value by the system when applying the rule
//         cdecl(cint(),cexpr(P));
//         cexpr(P) = num_proc;
//         cdecl(cint(),cexpr(t));
//         for (cexpr(t) = 0; cexpr(t) < cexpr(P); cexpr(t)++) 
//         {
//             int limit = 
//                 cexpr(t) == (cexpr(P)-1) ? 
//                 (cexpr(t)*(cexpr(n)/cexpr(P))+(cexpr(n)/cexpr(P))) + (cexpr(n)%cexpr(P)) : 
//                 cexpr(t)*(cexpr(n)/cexpr(P))+(cexpr(n)/cexpr(P));
//             for (; cexpr(i) < limit; cexpr(inc)) {
//               cstmts(body);
//             }
//         }
//         cstmts(s2);
//     }
// }


// for_interchange
// {
//     pattern:
//     {
//         cstmts(s1);
//         for(cexpr(i) = cexpr(ini_i); cexpr(i) < cexpr(n_i); cexpr(inc_i))
//         {
//             for(cexpr(j) = cexpr(ini_j); cexpr(j) < cexpr(n_j); cexpr(inc_j))
//             {
//                 cstmts(body);
//             }
//         }
//         cstmts(s2);
//     }
//     condition:
//     {
//         //cexpr(ini_j) is pure
//         //cexpr(ini_i) is pure
//         //cexpr(n_j) is pure
//         //cexpr(n_i) is pure
//         //cexpr(inc_j) only can modify cexpr(j)
//         //cexpr(inc_i) only can modify cexpr(i)
//         //cstmts(body) cannot modify cexpr(j) neither cexpr(i)
//     }
//     generate:
//     {
//         cstmts(s1);
//         for(cexpr(j) = cexpr(ini_j); cexpr(j) < cexpr(n_j); cexpr(inc_j))
//         {
//             for(cexpr(i) = cexpr(ini_i); cexpr(i) < cexpr(n_i); cexpr(inc_i))
//             {
//                 cstmts(body);
//             }
//         }
//         cstmts(s2);
//     }
// }

if_else_2_assign_ternary
{
    pattern:
    {
        cstmts(ini);
        if (cexpr(cond))
        {
            cexpr(lhs) = cexpr(then_);
        }
        else
        {
            cexpr(lhs) = cexpr(else_);
        }
        cstmts(end);
    }
    condition:
    {
        //cexpr(lhs) is pure
    }
    generate:
    {
        cstmts(ini);
        cexpr(lhs) = cexpr(cond)?cexpr(then_):cexpr(else_);
        cstmts(end);
    }
}

if_2_assign_ternary
{
    pattern:
    {
        cstmts(ini);
        if (cexpr(cond))
        {
            cexpr(lhs) = cexpr(then_);
        }
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        cexpr(lhs) = cexpr(cond)?cexpr(then_):cexpr(lhs);
        cstmts(end);
    }
}

remove_ternary
{
    pattern:
    {
        cexpr(cond)?cexpr(then_):cexpr(else_);
    }
    condition:
    {
        // cexpr(cond) is pure
        // cexpr(then_) and cexpr(else_) have numerical values
    }
    generate:
    {
        cexpr(cond) * cexpr(then_) + (1 - cexpr(cond)) * cexpr(else_);
    }
}


inlining
{
    pattern:
    {
        cstmts(ini);
        // #pragma stml has_function_calls true 
        cstmt(with_call);
        cstmts(fin);
    }
    condition:
    {
        has_calls(cstmt(with_call));
    }
    generate:
    {
        cstmts(ini);
        inline_stmt(cstmt(with_call));
        cstmts(fin);
    }
}

// inlining
// {
//     pattern:
//     {
//         cstmts(ini); 
//         cstmt(with_call);
//         cstmts(fin);
//     }
//     generate:
//     {
//         cstmts(ini);
//         inline_stmt(cstmt(with_call));
//         cstmts(fin);
//     }
// }

remove_block
{
    pattern:
    {
        cstmts(ini);
        {
            cstmts(block);
        }
        cstmts(fin);
    }
    generate:
    {
        cstmts(ini);
        fresh(cstmts(block));
        cstmts(fin);
    }
}

unrolling
{
    pattern:
    {
        cstmts(body0_1);
        for (cexpr(i) = cexpr(ini); cexpr(cond); cexpr(inc))
        {
            cstmts(body1);
        }
        cstmts(body0_2);
    }
    generate:
    {
        cstmts(body0_1);
        for (cexpr(i) = cexpr(ini); cexpr(cond); cexpr(inc))
        {
            cstmts(body1);
            cexpr(inc);
            if(cexpr(cond))
            {
               cstmts(body1); 
            }
        }
        cstmts(body0_2);
    }
}

// prova
// {
//     pattern:
//     {
//         cstmts(body0_1);
//         // all:
//         // {
//             cexpr(a) = cexpr(b);
//             for(cexpr(f1);cexpr(f2);cexpr(f3))
//             {
//                 cstmts(body1_1);
//                 cexpr(a) = cexpr(b);
//                 for(cexpr(f1);cexpr(f2);cexpr(f3))
//                 {
//                     cstmts(body1_1);
//                     all:
//                     {
//                         cexpr(r) = cexpr(t); 
//                         cexpr(s) += cexpr(u);
//                     }
//                     cstmts(body1_2);
//                 }
//                 cexpr(c) += cexpr(d);
//                 cstmts(body1_2);
//             }
//             cexpr(c) += cexpr(d);
//         // }
//         cstmts(body0_2);
//     }
//     generate:
//     {
//         cstmts(body0_1);
//         cexpr(a) = cexpr(d);
//         cstmts(body0_2);
//     }
// }


test_all
{
    pattern:
    {
        cstmts(body0_1);
        all:
        {
            cexpr(a) = cexpr(b) + cexpr(c); 
            cexpr(d) = cexpr(b) - cexpr(c);
        }
        cexpr(e) = cexpr(a) + cexpr(d);
        cstmts(body0_2);
    }
    generate:
    {
        cstmts(body0_1);
        cexpr(e) = 2 * cexpr(b);
        cstmts(body0_2);
    }
}

test_some
{
    pattern:
    {
        cstmts(body0_1);
        some:
        {
            cexpr(a) = cexpr(b) * 2;
            cexpr(a) = cexpr(b) + cexpr(b);
        }
        cexpr(c) = cexpr(a)/2;
        cstmts(body0_2);
    }
    generate:
    {
        cstmts(body0_1);
        cexpr(c) = cexpr(b);
        cstmts(body0_2);
    }
}


/* remove_continue */
/* { */
/*     pattern: */
/*     { */
/*         for(cexpr(i) = cexpr(ini); cexpr(i) < cexpr(n); cexpr(inc)) */
/*         { */
/* 	  cstmts(s1); */
/* 	  if (cexpr(cond)) */
/* 	  { */
/*             continue; */
/* 	  } */
/* 	  cstmts(s2); */
/* 	} */
/*     } */
/*     condition: */
/*     { */
/*         //cexpr(cond) is pure */
/*     } */
/*     generate: */
/*     { */
/*         for(cexpr(i) = cexpr(ini); cexpr(i) < cexpr(n); cexpr(inc)) */
/*         { */
/* 	  cstmts(s1); */
// Apply deMorgan Law
/* 	  if (!cexpr(cond)) */
/* 	  { */
/* 	    cstmts(s2); */
/* 	  } */
/* 	} */
/*     } */
/* } */


// move_value
// {
//     pattern:
//     {
//         cstmts(ini);
//         cexpr(lhs) = cexpr(rhs);
//         cstmts(mid);
//         cstmt(using_lhs);
//         cstmts(fin);
//     }
//     condition:
//     {
//         // cexpr(cond) is pure
//         // cstmts(mid) does not modify cexpr(lhs)
//         // cstmt(using_lhs) uses cexpr(lhs)
//     }
//     generate:
//     {
//         gen_list:
//         {
//             {
//                 cstmts(ini);
//                 cexpr(lhs) = cexpr(rhs);
//                 cstmts(mid);
//                 subs(cstmt(using_lhs),cexpr(lhs),cexpr(rhs));
//                 cstmts(fin);
//             }
//             {
//                 if_then:
//                 {
//                     no_reads(cexpr(lhs),cstmts(mid));
//                     {
//                         cstmts(ini);
//                         cstmts(mid);
//                         subs(cstmt(using_lhs),cexpr(lhs),cexpr(rhs));
//                         cexpr(lhs) = cexpr(rhs);
//                         cstmts(fin); 
//                     }
//                 }
//             }
//         }
//     }
// }


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// General rules
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////


// swap_statements
// {
//     pattern:
//     {
//         cstmts(sts1);
//         cstmt(s1);
//         cstmts(sts2);
//         cstmt(s2);
//         cstmts(sts3);
//     }
//     condition:
//     {
//         // no_rw(cstmt(s1), cstmts(sts2));
//         // no_rw(cstmt(s1), cstmts(s2));
//         // no_rw(cstmt(s2), cstmts(sts2));
//         // no_rw(cstmt(s2), cstmts(s1));
//     }
//     generate:
//     {
//         cstmts(sts1);
//         cstmt(s2);
//         cstmts(sts2);
//         cstmt(s1);
//         cstmts(sts3);
//     }
// }


// inline
// {
//     pattern:
//     {
//     	cdefs(ini);
//     	void f()
//     	{
//     		cstmts(body);
//     	}
//     	cdefs(mid);
//     	void main()
//     	{
//     		f();
//     	}
//     	cdefs(end);
//     }
//     generate:
//     {
//     	cdefs(ini);
//     	cdefs(mid);
//         void main()
//     	{
//     		cstmts(body);
//     	}
//     	cdefs(end);
//     }
// }

// // Remove a multiplication where the right operator is 1
// remove_1
// {
//  pattern:
//  {
//      cexpr(a) * 1;
//  }
//  generate:
//  {
//      cexpr(a);
//  }
// }

//TODO:
//Revisit: Compiler Transformations for High-Performance Computing 
//https://www.dropbox.com/s/cvjr2ws1hy5oo3r/High-Level%20Loop%20Optimizations%20for%20GCC%20.pdf?dl=0
//Fusion Fission for(i)-for(j)2for(j)-for(i), etc 
// Intersect with Guillermo's work on reverse engineering
// Evolving Transformation Sequences using Genetic Algorithms
//https://www.dropbox.com/s/hudix8ngu9n103p/Evolving%20Transformation%20Sequences%20using%20Genetic%20Algorithms.pdf?dl=0


// TODO: Revise. We need looking for a expression only in rhs of assignments
propagate_assignment
{
    pattern:
    {
        cstmts(ini);
        cexpr(lhs) = cexpr(rhs);
        cstmts(fin);
    }
    condition:
    {
        pure(cexpr(lhs));
        pure(cexpr(rhs));
        no_writes(cexpr(lhs), cstmts(fin));
        reads(cexpr(lhs), cstmts(fin));
        no_empty(cstmts(fin));
    }
    generate:
    {
        cstmts(ini);
        subs(cstmts(fin),cexpr(lhs),cexpr(rhs));
    }
}

move_inside_for
{
    pattern:
    {
        cstmts(pre);
        for (; cexpr(cond); cexpr(inc))
        {
            cstmts(body);
        }
        cstmts(fin);
    }
    generate:
    {
        cdecl(cint(),cexpr(first_iteration));
        cexpr(first_iteration) = 1;
        for (; cexpr(cond); cexpr(inc))
        {
            if(cexpr(first_iteration))
            {
                cstmts(pre);
                cexpr(first_iteration) = 0;
            }
            cstmts(body);
        }
        cstmts(fin);
    }
}

common_subexp_elimination
{
    pattern:
    {
        cstmts(ini);
        cexpr(a) = cexpr(c);
        cstmts(mid);
        cexpr(b) = cexpr(c);
        cstmts(fin);
    }
    condition:
    {
        not(is_cons(cexpr(c)));
        not(is_var(cexpr(c)));
    }
    generate:
    {
        cstmts(ini);
        cdecl(cint(),cexpr(t));
        cexpr(t) = cexpr(c);
        cexpr(a) = cexpr(t);
        cstmts(mid);
        cexpr(b) = cexpr(t);
        cstmts(fin);
    }
}


/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
// Needed properties
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

// // Move an invariant expression inside 
// move_loop_inv_into_for
// {
//     pattern:
//     {
//         cstmts(ini);
//         cexpr(lhs) = cexpr(rhs);
//         cstmts(mid);
//         cstmt(for_loop);
//         cstmts(fin);
//     }
//     condition:
//     {
//         pure(cexpr(lhs));
//         pure(cexpr(rhs));
//         no_rw(cexpr(lhs), cstmts(mid));
//         no_writes(cexpr(lhs), cstmts(for_loop));
//         no_writes(cexpr(lhs), cstmts(fin));
//         is_for_loop(cstmt(for_loop));
//     }
//     generate:
//     {
//         cstmts(ini);
//         cstmts(mid);
//         subs(cstmt(for_loop),cexpr(lhs),cexpr(rhs));
//         cstmts(fin);
//     }
// }


// // rule to collapse 2 nested for loops into one
// collapse_for_loops
// {
//     pattern:
//     {
//         #pragma polca def a
//         for(cexpr(i) = cexpr(initial_value); cexpr(i) < cexpr(limit1); cexpr(i)++)
//        {
//             cstmts(prelude);
//             for(cexpr(j) = cexpr(initial_value); cexpr(j) < cexpr(limit2); cexpr(j)++)
//             {
//                 cstmts(body);
//             }
//             cstmts(postlude);
//        }
//     }
//     condition:
//     {
//         no_writes(cexpr(i),cstmts(body));
//         no_writes(cexpr(j),cstmts(body));
//         no_writes(cexpr(i),cstmts(prelude));
//         side_effects_free(cstmts(prelude));
//         no_writes(cexpr(i),cstmts(postlude));
//     }
//     generate:
//     {
//         cstmts(prelude);
//         #pragma polca same_properties a
//         for(cexpr(j) = cexpr(initial_value); cexpr(j) < (cexpr(limit1) * cexpr(limit2)); cexpr(j)++)
//         {
//             subs(cstmts(body),cexpr(i),0);          
//         }
//         cstmts(postlude);
//     }
// }

collapse_2_for_loops
{
    pattern:
    {
        // #pragma polca def a
        for(cexpr(i) = cexpr(initial_value); cexpr(i) < cexpr(limit1); cexpr(i)++)
       {
           cstmts(prelude);
            for(cexpr(j) = cexpr(initial_value); cexpr(j) < cexpr(limit2); cexpr(j)++)
            {
                cstmts(body);
            }
            cstmts(postlude);
       }
    }
    condition:
    {
        no_writes(cexpr(i),cstmts(body));
        no_writes(cexpr(j),cstmts(body));
        no_writes(cexpr(i),cstmts(prelude));
        no_writes(cexpr(i),cstmts(postlude));
    }
    generate:
    {
        // #pragma polca same_properties a
        for(cexpr(j) = cexpr(initial_value); cexpr(j) < (cexpr(limit1) * cexpr(limit2)); cexpr(j)++)
        {
            if_then_else:
            {
                no_empty(cstmts(prelude));
                if(cexpr(j) % cexpr(limit2) == 0) 
                {
                    cstmts(prelude);
                }
                ;
            }
            subs(
                subs(cstmts(body),cexpr(j),cexpr(j)%cexpr(limit2)),
                cexpr(i),
                cexpr(j)/cexpr(limit2)
                ); 
            if_then_else:
            {
                no_empty(cstmts(postlude));
                if(cexpr(j) % cexpr(limit2) == 0) 
                {
                    cstmts(postlude);
                }
                ;
            }      
        }
    }
}

remove_empty_if
{
    pattern:
    {
        cstmts(ini);
        if(cexpr(cond))
        {}
        cstmts(end);
    }
    condition:
    {
        pure(cexpr(cond));
    }
    generate:
    {
        cstmts(ini);
        cstmts(end);
    }
}

remove_useless_statement
{
    pattern:
    {
        cstmts(ini);
        cstmt(mid);
        cstmts(end);
    }
    condition:
    {
        is_expr(cstmt(mid));
        pure(cstmt(mid));
    }
    generate:
    {
        cstmts(ini);
        cstmts(end);
    }    
}

// float temp2d[B][B];  
// float temp2d[B*B]; 

// temp2d[i][j] = sum;
// temp2d[i*B+j] = sum;


// never_applying_expr
// {
//     pattern:
//     {
//         cexpr(a) = cexpr(b) = cexpr(c) = cexpr(d);
//     }
//     generate:
//     {
//         cexpr(d) = cexpr(c) = cexpr(b) = cexpr(a);
//     }  
// }

flatten_float_array
{
    pattern:
    {
        cstmts(ini);
        cdecl(cfloat(),cexpr(v)[cexpr(l1)][cexpr(l2)]);
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        cdecl(cfloat(),cexpr(v)[cexpr(l1)*cexpr(l2)]);
        change_access(cstmts(end), cexpr(v), flatten(cexpr(l2)));
    }   
}

flatten_int_array
{
    pattern:
    {
        cstmts(ini);
        cdecl(cint(),cexpr(v)[cexpr(l1)][cexpr(l2)]);
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        cdecl(cint(),cexpr(v)[cexpr(l1)*cexpr(l2)]);
        change_access(cstmts(end), cexpr(v), flatten(cexpr(l2)));
    }   
}

remove_aux_2D_array
{
    pattern:
    {
        cstmts(ini);
        for (cexpr(k) = 0; cexpr(k) < cexpr(l1); cexpr(k)++) {
            for (cexpr(l) = 0; cexpr(l) < cexpr(l2); cexpr(l)++) {
                cexpr(v)[cexpr(k)][cexpr(l)] = 
                    cexpr(nv)[cexpr(m1)+cexpr(k)][cexpr(m2)+cexpr(l)];
            }
        }
        cstmts(end);
    }
    condition:
    {
        no_writes(cexpr(nv), cstmts(end));
    }
    generate:
    {
        cstmts(ini);
        change_array(cstmts(end), cexpr(v), cexpr(nv), cexpr(m1), cexpr(m2));
    }   
}
