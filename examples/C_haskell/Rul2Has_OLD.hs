module Rul2Has where

import Language.C
import Language.C.System.GCC   -- preprocessor used

import Data.List

import Debug.Trace

import qualified Text.Groom as Gr

-- TODOS
-- Conditions
-- Link with pragmas (conditions,etc...)
-- Allow to expres that certain expression is inside a statement
-- Rules for the whole program (e.g. void i(){...} main(){...i();...} )

-- NEW:
-- Implement all the cases of exprEqual function in RulesLib.hs
-- Allow to define a comparation between expressions or statements
-- Allow to define operators as a variable and avoid having to define left and right cases when the order is not important (e.g. (+ a b) -> (+ a b) o (+ b a))

-- Think how to define sequences of rules in the search tree and give a simple example
-- condition at the middle of the rulee
-- Small example for each rule
-- Syntactic sugar to define that a variable is not modified in the whole pattern

main =
	createHs "rules"

createHs name =  
	do
		ast <- parseMyFile (name ++ ".c")
		writeFile (name ++ ".ast") (Gr.groom ast)
		--putStrLn (show ast)
		--let astStr = (show ast)
		--let initial_string = 
		--	case stripPrefix "CTranslUnit [" astStr of 
		--		Just restOfString -> restOfString
		--		Nothing -> astStr
		let (rules,rulStmt,rulExp,rulNames) = createRules (show ast) 0
		putStrLn ("Rules read:\n**************\n" ++ (unlines rulNames) ++ "**************\n")
		let rulStmtStr = createStrListRuleNames rulStmt
		let rulExpStr = createStrListRuleNames rulExp
		let listRules = 
			--trace ("\n\n\n\n\n\n\n\n\n\n\n\n") "stmtRules = [" ++ rulStmtStr ++ "]\n"
			"stmtRules = [" ++ rulStmtStr ++ "]\n"
			++ "exprRules = [" ++ rulExpStr ++ "]\n"
		let modHeader = 
			"{-# OPTIONS_GHC -w #-}\n\n"
			++ "module Rules where \n\n"
			++ "import RulesLib\n\n"
			++ "import Language.C\nimport Language.C.Data.Ident\n\n"
			++ "import Data.Generics\n\n"
		writeFile "Rules.hs" (modHeader ++ listRules ++ rules)
		putStrLn "File Rules.hs created."


parseMyFile :: FilePath -> IO CTranslUnit
parseMyFile input_file =
  do parse_result <- parseCFile (newGCC "gcc") Nothing [] input_file
     case parse_result of
       Left parse_err -> error (show parse_err)
       Right ast      -> return ast

createRules "" _ =
	--"\n-- default\napplyRulesExpr n = n\n"
	("",[],[],[])
createRules string free_var =
	case stripPrefix "CFDefExt (CFunDef [] (CDeclr (Just (Ident \"" string of 
		Just restOfString -> 
			(ruleStr ++ nrules, rulesStmt,rulesExpr,(ruleName:rulesNames))
		Nothing -> createRules (tail string) free_var
	where
		(nrules, nrulesStmt,nrulesExpr,rulesNames) = (createRules restOfString (free_var + 1) )
		(rulesStmt,rulesExpr) = 
			case typePat of 
				"expr" -> (nrulesStmt, ruleNameHs:nrulesExpr)
				_ -> (ruleNameHs:nrulesStmt , nrulesExpr)
		(ruleName,dictJoined,patternsStr_,generatesStr_,wholeCondition,restOfString,lastFree,subs) =
			read_info_from_cml(string)
		ruleNbr = (free_var + 1)
		ruleNameHs = "rule_" ++ ruleName ++ "_" ++ (show ruleNbr)
		removeCExpr str = 
			let allStr = (fst (readUntilClosingBracketStoring 0 str))
			in case allStr of 
				('v':'a':'r':_) -> take ((length allStr) - 1) allStr
				_ -> allStr
		removeCExprIni str = 
			case stripPrefix "(CExpr (Just " str  of
				Just restOfCExpr ->
					restOfCExpr
				Nothing ->
					str
		(typePat,patternsStr,generatesStr,nfreeVar,augmentedDict) = 
			case (length patternsStr_) of 
				1 -> 
					case stripPrefix "(CExpr (Just " (head patternsStr_) of 
						Just restOfCExpr ->
							("expr",[(removeCExpr restOfCExpr)],[(removeCExpr (removeCExprIni (head generatesStr_)))],lastFree,dictJoined)
							--("expr",patternsStr_,generatesStr_)
						Nothing -> 
							("stmts",
								["var_internal_" ++ (show lastFree)] ++ patternsStr_++ ["var_internal_" ++ (show (lastFree+1))],
								["var_internal_" ++ (show lastFree)] ++ generatesStr_ ++ ["var_internal_" ++ (show (lastFree+1))],
								lastFree + 2,[("internal","cstmts",["var_internal_" ++ (show (lastFree + i)) | i <- [0..1]])] ++ dictJoined)
				_ -> 
					("stmts",patternsStr_,generatesStr_,lastFree,dictJoined)
		ruleStr =
			case typePat of 
				"stmts" -> 
					buildRuleStmts ruleName ruleNameHs patternsStr wholeCondition generatesStr augmentedDict nfreeVar subs
				_ ->
					 "\n-- " ++ ruleName ++ "\n" ++ ruleNameHs ++ "\n\told@" ++ (concat patternsStr )
					 ++ "\n\t| " ++ wholeCondition ++ " =\n\t[(\"" ++ ruleName ++ "\",old," 
					 ++ (concat generatesStr ) ++ ")]\n"++ ruleNameHs ++ " _ = []\n"

read_info_from_cml(string) =
	(ruleName,dictJoined,patternsStr_,generatesStr_,wholeCondition,restOfString4,lastFree,subs) 
	where
		initial_string = 
			case stripPrefix "CFDefExt (CFunDef [] (CDeclr (Just (Ident \"" string of 
				Just restOfString -> restOfString
				Nothing -> string
		(ruleName,restOfString) = readUntilQuotes initial_string
		(patterns,restOfString2) = searchPatGenCond "pattern" restOfString
		(patternsStr_, dict, lastFree) = (buildPatterns patterns [] 0 [])
		(generates,restOfString3) = searchPatGenCond "generate" restOfString2
		(generatesStr_, subs) = (buildGenerates generates [] [] dict)
		dictJoined = (joinDictEntries dict [] [])
		(conditionsStr,restOfString4) = 
			--([],restOfString3)
			case searchPatGenCond "condition" restOfString3 of 
				([],_) -> 
					([],restOfString3)
				(conditions,restOfString4_) ->
					let conditionsStr = buildConditions conditions [] dict
					in (conditionsStr,restOfString4_)
		conditionStr = --trace (show conditionsStr) (buildConditionHaskell conditionsStr)
			 (buildConditionHaskell conditionsStr)
		--consStr = buildConsDict (trace (show dict) dictJoined) ""
		consStr = buildConsDict dictJoined "" 
		wholeCondition = (consStr ++ " && " ++ conditionStr)


buildRuleStmts ruleName ruleNameHs patternsStr consStr generateStr dict freeVar subs = 
	-- main function generation
	"\n-- " ++ ruleName ++ "\n" ++ ruleNameHs ++ " old@(CCompound ident bs nodeInfo) =\n"
	++ "\tconcat [" ++ ruleNameAux ++ " item ident nodeInfo old | item@(True,_,_) <- (" ++ ruleNameAux ++ "2 bs [] " ++ (concat ["True " | _ <- [1..(length onlyPats)]]) ++ ")]\n"
	++ ruleNameHs ++ " _ = []\n\n"
	-- main auxiliar function generation
	++ (buildMainAuxiliaryFunction ruleName ruleNameAux allPatterns consStr generateStr onlyPats allInterListStr dict subs)
	++ strFuns
	where 
		ruleNameAux =  ruleNameHs ++ "_aux"
		onlyPats = filter (onlyPatterns dict) patternsStr
		--interListStr = "[" ++ (stringInterLists (trace (show allPatternsStr) allPatternsStr) dict "[]" [])++"]"
		interListStr = "[" ++ (stringInterLists allPatternsStr dict "[]" [])++"]"
		allPatterns = newPats
		allInterListStr = interListStr
		(strFuns,newPats,allPatternsStr,_) = (buildSecondaryAuxiliaryFunctions ruleNameAux patternsStr 2 dict freeVar)


buildMainAuxiliaryFunction ruleName ruleNameAux allPatterns consStr generateStr onlyPats allInterListStr dict subs =
	ruleNameAux ++ " (True," ++  allInterListStr ++ ", [" ++ (buildPatStr allPatterns) ++  "]) ident nodeInfo old | " ++ consStr ++ " =\n\t"
	++ body
	++ ruleNameAux ++ " _ _ _ _ = []\n\n"
	where 
		defaultBody = 
			"[(\"" ++ ruleName ++ "\", old, (CCompound ident (" ++ (buildGenerateStr generateStr dict) ++ ") nodeInfo))]\n"
		body = 
			case subs of 
				[] -> 
					defaultBody
				_ -> 
					"case " ++ (buildCondSubs subs) ++ " of\n\t\t"
					++ "True -> []\n\t\t" 
					++ "False -> " ++ defaultBody
	-- ++ "\t\t_ -> []\n"
	-- ++ "\twhere\n"
	-- ++ "\t\t" ++ allInterListStr ++ " = inter\n\n"

buildSecondaryAuxiliaryFunctions ruleNameAux [] number dict freeVar = 
	("",[],[],freeVar)
buildSecondaryAuxiliaryFunctions ruleNameAux patternsStr number dict freeVar = 
	-- secondary auxiliar function generation 
	--(buildSecondaryAuxiliaryFunction (trace ((show onlyPats) ++ "\n" ++ (show patsInPats))number) ruleNameAux newPats)
	--trace (show (ruleNameAux,allPatterns,patsInPats)) (auxFun ++ secAuxFun, newPatsFinal, allPatterns, nnfreVar)
	(auxFun ++ secAuxFun, newPatsFinal, allPatterns, nnfreVar)
	--(concat [buildSecondaryAuxiliaryFunctions ruleNameAux pats (number + 1) dict freeVar | pats <- otherPatterns])
	where 
		onlyPats = filter (onlyPatterns dict) patternsStr
		(patsInPats,nfreeVar) = searchAllPats onlyPats freeVar
		allPatterns = buildAllPatterns patternsStr 0
		newPats = [npat | (npat,_,_) <- patsInPats]
		--otherPatterns = [patList | (pat,listofPatList) <- patsInPats, patList <- listofPatList, patList /= []]
		auxFun = buildSecondaryAuxiliaryFunction number ruleNameAux patsInPats dict
		(secAuxFun,newPatsFinal,nnfreVar) = buildInternalSecondaryAuxiliaryFunctions patsInPats ruleNameAux dict nfreeVar

buildAllPatterns [] level =
	[]
buildAllPatterns (p:ps) level =
	let 
		([(_,lop,_)],_) = searchAllPats [p] 0
		lopRec = [buildAllPatterns ps_ (level + 1) | ps_ <- lop]
		levels = 
			case concat lopRec of 
				[] -> []
				_ -> concat [["level" ++ (show level)] ++ item ++ ["level" ++ (show level)]| item <- lopRec]
	in [p] ++ levels ++ (buildAllPatterns ps level)


buildInternalSecondaryAuxiliaryFunctions [] _ _ freeVar =
	("",[],freeVar) 
buildInternalSecondaryAuxiliaryFunctions ((p,lop,numbers):tail_) ruleNameAux dict freeVar = 
	let 
		--(strFuns, newPats,nfreeVar) = trace ("BSAFL: "++(show lop) ++ " " ++ (show numbers)) buildSecondaryAuxiliaryFunctionsList ruleNameAux lop numbers dict freeVar
		(strFuns, newPats,nfreeVar) = buildSecondaryAuxiliaryFunctionsList ruleNameAux lop numbers dict freeVar
		(strFuns_, newPats_,nnfreVar) = buildInternalSecondaryAuxiliaryFunctions tail_ ruleNameAux dict nfreeVar
	in (strFuns ++ strFuns_, [p] ++ newPats ++ newPats_,nnfreVar)


buildSecondaryAuxiliaryFunctionsList ruleNameAux [] [] dict freeVar = 
	("",[],freeVar) 
buildSecondaryAuxiliaryFunctionsList ruleNameAux [[]] [] dict freeVar = 
	("",[],freeVar) 
--buildSecondaryAuxiliaryFunctionsList ruleNameAux [[]] [n] dict freeVar = 
--	((buildSecondaryAuxiliaryFunction n ruleNameAux [] dict), [], freeVar)
buildSecondaryAuxiliaryFunctionsList ruleNameAux (_:ps) (-1:ns) dict freeVar = 
	buildSecondaryAuxiliaryFunctionsList ruleNameAux ps ns dict freeVar
buildSecondaryAuxiliaryFunctionsList ruleNameAux (p:ps) (n:ns) dict freeVar = 
	let
		--(strFuns, newPats,_,nfreeVar) = trace ((show (p:ps)) ++ " " ++ (show (n:ns))) buildSecondaryAuxiliaryFunctions ruleNameAux p n dict freeVar
		(strFuns, newPats,_,nfreeVar) = buildSecondaryAuxiliaryFunctions ruleNameAux p n dict freeVar
		(strFuns_, newPats_,nnfreVar) = buildSecondaryAuxiliaryFunctionsList ruleNameAux ps ns dict nfreeVar
	in (strFuns ++ strFuns_, newPats ++ newPats_,nnfreVar)

buildSecondaryAuxiliaryFunction number  ruleNameAux [] dict =
	ruleNameAux ++ (show number) ++" all [] =\n\t[(True,[all],[])]\n"
buildSecondaryAuxiliaryFunction number  ruleNameAux onlyPats dict =
	ruleNameAux ++ (show number) ++" [] acc " ++ (concat ["False " | _ <- [1..(length onlyPats)]]) ++ "=\n\t[(True,[acc],[])]\n"
	++ ruleNameAux ++ (show number) ++" [] acc " ++ (concat ["_ " | _ <- [1..(length onlyPats)]]) ++ "=\n\t[(False,[acc],[])]\n"
	++ (buildRuleStmtsPat number onlyPats ruleNameAux (length onlyPats) dict)
	++ ruleNameAux ++ (show number) ++ " (other:tail_) acc " ++ (concat [(" bool"++ (show i))| i <- [1..(length onlyPats)]]) ++ " =\n\t" 
	++ ruleNameAux ++ (show number) ++ " tail_ (acc ++ [other])" ++ (concat [(" bool"++ (show i))| i <- [1..(length onlyPats)]]) ++ "\n\n"	


buildRuleStmtsPat number [(pat,lop,intPats)] ruleNameAux total dict = 
	--let (intPatsStr,intInter,intLCP) = trace (show intPats)  buildIntLC intPats
	let (intPatsStr,intInter,intLCP) =  buildIntLC intPats
	in
	ruleNameAux ++ (show number) ++ " (stat@" ++ (buildBlockOrDeclStmt pat) ++ ":tail_) accsl "
	++  (concat ["False " | _ <- [1..(total -1)]]) ++  "True =\n"
	++ "\tlet\n\t\tlistItems = "  ++ ruleNameAux ++ (show number) ++ " tail_ [] " 
	++  (concat ["False " | _ <- [1..total]]) 
	++ (buildCallsIntPats ruleNameAux lop intPats dict) 
	++ "\n\tin "
	++ "[(True, (accsl:(" ++ intInter ++ "inter)),(stat:(" ++ intPatsStr ++ "pats))) | (True,inter,pats) <- listItems"
	++ intLCP
	++ "] ++ (" ++ ruleNameAux ++ (show number) ++ " tail_ (accsl ++ [stat]) " 
	++  (concat ["False " | _ <- [1..(total -1)]]) ++  "True)\n"
buildRuleStmtsPat number allpats@((pat,lop,intPats):pats) ruleNameAux total dict = 
	--let (intPatsStr,intInter,intLCP) = trace (show intPats) buildIntLC intPats
	let (intPatsStr,intInter,intLCP) =  buildIntLC intPats
	in
	ruleNameAux ++ (show number) ++ " (stat@" ++ (buildBlockOrDeclStmt pat) ++ ":tail_) accsl "
	++ (concat ["False " | _ <- [1..(total - (length allpats))]]) 
	++ (concat ["True " | _ <- [1..(length allpats)]]) ++ "=\n"
	++ "\tlet\n\t\tlistItems = "  ++ ruleNameAux ++ (show number) ++ " tail_ [] " 
	++ (concat ["False " | _ <- [0..(total - (length allpats))]]) 
	++ (concat ["True " | _ <- [2..(length allpats)]]) 
	++ (buildCallsIntPats ruleNameAux lop intPats dict) 
	++ "\n\tin "
	++ "[(True, (accsl:(" ++ intInter ++ "inter)),(stat:(" ++ intPatsStr ++ "pats))) | (True,inter,pats) <- listItems"
	++ intLCP
	++ "] ++ (" ++ ruleNameAux ++ (show number) ++ " tail_ (accsl ++ [stat]) " 
	++ (concat ["False " | _ <- [1..(total - (length allpats))]]) 
	++ (concat ["True " | _ <- [1..(length allpats)]]) ++ ")\n"
	++ (buildRuleStmtsPat number pats ruleNameAux total dict)
buildRuleStmtsPat _ [] _ _ _ = 
	""

buildCallsIntPats ruleNameAux [] [] _ =
	""
buildCallsIntPats ruleNameAux [[]] [] _ =
	""
buildCallsIntPats ruleNameAux (_:ps) (-1:ns) dict =
	(buildCallsIntPats ruleNameAux ps ns dict)
buildCallsIntPats ruleNameAux (p:ps) (n:ns) dict =
	"\n\t\tlistItems" ++ (show n) ++ " = "  ++ ruleNameAux 
	++ (show n) ++ " var_internal_" ++ (show n) ++ " [] " 
	++  (concat ["True "| _ <- [1..(length (filter (onlyPatterns dict) p))]])
	++ (buildCallsIntPats ruleNameAux ps ns dict)

-- (inter2 ++ inter)),(stat:(pats2 ++ pats))) | (True,inter,pats) <- listItems, (True,inter2,pats2) <- listIte
buildIntLC [] =
	("","","")
buildIntLC (-1:ns) =
	let (p,i,l) = buildIntLC ns
	in ((p),
		("[[]] ++ "++ i),
		"")
buildIntLC (n:ns) =
	let (p,i,l) = buildIntLC ns
	in (("pats" ++ (show n) ++ " ++ " ++ p),
		("inter" ++ (show n) ++ " ++ " ++ i),
		(", (True, inter" ++ (show n) ++ ", pats" ++ (show n) ++ ") <- listItems" ++ (show n) ++ l))

buildPatStr [pat] = 
	(buildBlockOrDeclStmt pat)
buildPatStr (pat:pats) = 
	(buildBlockOrDeclStmt pat) ++ "," ++ (buildPatStr pats)

searchAllPats [] freeVar =
	([],freeVar)
searchAllPats (pat:pats) freeVar =
	let 
		((npat,listNums,nfreeVar),found) = (searchPatInPat pat freeVar)
		(rest,nnfreeVar) = (searchAllPats pats nfreeVar)
	in ([(npat,found,listNums)] ++ rest,nnfreeVar)

searchPatInPat pat freeVar = 
	let (found,piecesPat) = searchPatInPat_ pat
	in 
		--case trace ("Found:" ++ (show (length found)) ++ "\n" ++ (show found)++ "\n" ++ (show pat) ++ "\n" ++ (show piecesPat)) (length found) of
		case (length found) of
			--0 -> ((piecesPat!!0 ++ "(CCompound _ [] _)" ++ piecesPat!!1,freeVar),found)
			1 -> ((pat,[],freeVar),found)
			_ -> ((buildNewPat piecesPat found freeVar True), (take ((length found) - 1 ) found))

searchPatInPat_ "" = 
	([],[])
searchPatInPat_ pat = 
	let 
		(patInside,restOfString,prevString) = searchPatGenCompound pat ""
		(patInsides,piecesPat) = (searchPatInPat_ restOfString)
	in ([patInside] ++ patInsides,[prevString] ++ piecesPat)
	--in (trace (prevString++"<----->"++ (readUntilClosingBracket 0 restOfString)) [patInside]) ++ (searchPatInPat_ restOfString)

-- TODO: transform to foldl or at least refactor in some way
buildNewPat [] _ freeVar _ =
	("",[],freeVar)
buildNewPat [p] _ freeVar True =
	(p,[],freeVar)
buildNewPat [p] _ freeVar False =
	((readUntilClosingBracket 0 p),[],freeVar)
buildNewPat (p:ps) (fp:fps) freeVar True =
	let 
		(newPat,listNum,nnfreeVar) = (buildNewPat ps fps nfreeVar False)
		(interPat,nnum,nfreeVar) = 
			case fp of 
				[] -> ("[]",-1,freeVar)
				_ -> ("var_internal_" ++ (show freeVar),freeVar,freeVar+1)
	in (p ++ "(CCompound _ " ++ interPat ++ " _)" ++ newPat,(nnum:listNum),nnfreeVar)
buildNewPat (p:ps) (fp:fps) freeVar False = 
	let 
		(newPat,listNum,nnfreeVar) = (buildNewPat ps fps nfreeVar False)
		(interPat,nnum,nfreeVar) = 
			case fp of 
				[] -> ("[]",-1,freeVar)
				_ -> ("var_internal_" ++ (show freeVar),freeVar,freeVar+1)
	in ((readUntilClosingBracket 0 p) ++ "(CCompound _ " ++ interPat ++ " _)" ++ newPat,(nnum:listNum),nnfreeVar)

-- TODO: transform to foldl
buildGenerateStr [gen] dict =
	case stripPrefix "(substitute" gen of 
		Just _ ->
			gen
		Nothing ->
			case stripPrefix "(" gen of 
				Just _ -> 
					"["++ (buildBlockOrDeclStmt ngen) ++ "]"
				Nothing -> 
					case [v | (_,"cstmt",vars) <- dict,v <- vars, v == gen] of 
						[] ->
							gen 
						_ -> 
							"[" ++ (buildBlockOrDeclStmt gen) ++ "]" 
	where 
		(found_,piecesGen) = searchPatInPat_ gen
		found = take ((length found_) - 1) found_
		gStrFound = [buildGenerateStr list dict | list <- found ]
		ngen = buildNewGen piecesGen gStrFound dict
buildGenerateStr (gen:gens) dict =
	case stripPrefix "(substitute" gen of 
		Just _ ->
			gen ++ " ++ " ++ (buildGenerateStr gens dict)
		Nothing ->
			case stripPrefix "(" gen of 
				Just _ -> 
					"[" ++ (buildBlockOrDeclStmt ngen) ++ "] ++ " ++ (buildGenerateStr gens dict)
				Nothing -> 
					case [v | (_,"cstmt",vars) <- dict,v <- vars, v == gen] of 
						[] ->
							gen ++ " ++ " ++ (buildGenerateStr gens dict)
						_ -> 
							"[" ++ (buildBlockOrDeclStmt gen) ++ "] ++ " ++ (buildGenerateStr gens dict)
	where 
		(found_,piecesGen) = searchPatInPat_ gen
		found = take ((length found_) - 1) found_
		gStrFound = [buildGenerateStr list dict| list <- found ]
		ngen = buildNewGen piecesGen gStrFound dict
buildGenerateStr [] _ = ""

--buildNewGen :: [String] -> [String] -> String
buildNewGen [] _ _= 
	""
buildNewGen [g] [] _ = 
	g
buildNewGen (g:gs) (ig:igs) dict = 
	case [v | (_,"cstmt",vars) <- dict,v <- vars, v == ig] of
		[] ->
			g ++ " (CCompound [] (" ++ ig ++ ") " ++ (buildNewGen gs igs dict)
		_ ->
			g ++ " (CCompound [] [" ++ (buildBlockOrDeclStmt ig) ++ "] " ++ (buildNewGen gs igs dict)


searchPatGenCond patGen "" = 
	([],"")
searchPatGenCond patGen string =  
	case stripPrefix ("CBlockStmt (CLabel (Ident \"" ++ patGen ++ "\" ") string of 
		Just restOfString -> 
			let (patGens,restOfString2,_) = searchPatGenCompound (readUntilClosingBracket 1 restOfString) ""
			in (patGens,restOfString2)
		Nothing ->
			case stripPrefix "CFDefExt (CFunDef [] (CDeclr (Just (Ident \"" string of 
				Just restOfString -> ([],"CFDefExt (CFunDef [] (CDeclr (Just (Ident \"" ++ restOfString)
				Nothing -> searchPatGenCond patGen (tail string)
		--	case stripPrefix " (CCompound [] [CBlockStmt (CExpr (Just " (readUntilClosingBracket 1 restOfString) of 
		--		Just restOfString2 ->
		--			("expr", readUntilClosingBracketStoring 0 restOfString2)
		--		Nothing -> 
		--			--case stripPrefix " (CCompound [] [CBlockStmt (CCompound [] ["  (readUntilClosingSqBracket 1 restOfString) of 
		--			--	Just restOfString3 ->
		--			--		("stmts", readUntilClosingBracketStoring 0 restOfString3)
		--			--	Nothing -> 
		--					case stripPrefix " (CCompound [] [CBlockStmt "  (readUntilClosingBracket 1 restOfString) of 
		--						Just restOfString4 ->
		--							("stmt", readUntilClosingBracketStoring 0 restOfString4)
		--						Nothing -> 
		--							("none", ("",restOfString))
		--Nothing ->
		--	searchPatGen patGen (tail string)

searchPatGenCompound :: String -> String -> ([String],String,String)
searchPatGenCompound "" acc =
	 ([],"",acc)
searchPatGenCompound str acc = 
	case stripPrefix "(CCompound [] "  str of 
		Just restOfString ->
			--("expr", readUntilClosingBracketStoring 0 restOfString2)
			let 
				(patGensTogether,restOfString2) = readUntilClosingSqBracketStoring 0 restOfString 
				cleanedPatGensTogether = removeFirstLast patGensTogether
				patGens = separatePatGen cleanedPatGensTogether
			in (patGens,restOfString2,acc)
		Nothing -> 
			searchPatGenCompound (tail str) (acc ++ [(head str)])

stripPrefixMultiple [] str =
	Nothing
stripPrefixMultiple (p:ps) str =
	case stripPrefix p str of  
		Just restOfString ->
			Just restOfString
		_ ->
			stripPrefixMultiple ps str	

separatePatGen "" =
	[]
separatePatGen str = 
	case stripPrefixMultiple ["CBlockStmt (","CBlockDecl ("] str of 
		Just restOfString ->
			let (pat,restOfString2) = 
				(readUntilClosingBracketStoring 1 restOfString)
			in let nstring = 
				 (drop 1 restOfString2)
			--in let nstring = 
			--		case trace (show restOfString2) (stripPrefix " (NodeInfo " restOfString2) of 
			--			Just restOfString3 ->
			--				(drop 2 (readUntilClosingBracket 1 restOfString3))
			--			Nothing ->
			--				"" 
			in (("(" ++ pat):(separatePatGen nstring))
		Nothing ->
				case stripPrefix "CBlockStmt var_" str of 
					Just restOfString ->
						let (pat,restOfString2) = 
							(readUntilCommaStoring 0 restOfString)
						in let nstring = restOfString2
						in let actualPat =
								case last pat of 
									',' -> (take ((length pat) - 1) pat)
									_ -> pat
						--in let nstring = 
						--		case trace (show restOfString2) (stripPrefix " (NodeInfo " restOfString2) of 
						--			Just restOfString3 ->
						--				(drop 2 (readUntilClosingBracket 1 restOfString3))
						--			Nothing ->
						--				"" 
						in (("var_" ++ actualPat):(separatePatGen nstring))
					Nothing ->
						[]

buildPatterns [] acc lastFree dict = 
	((reverse acc),dict,lastFree)
buildPatterns (pattern:patterns) acc currentFree dict = 
	let (patternStr,ndict,ncurrentFree) = 
		buildPattern pattern "" currentFree dict
	in buildPatterns patterns (patternStr:acc) ncurrentFree ndict


buildPattern "" acc currentFree dict = 
	(acc, dict, currentFree)
buildPattern string acc currentFree dict =
	buildPattern nstring nacc ncurrentFree ndict
	where 
	 (nstring, nacc, ncurrentFree, ndict) =
		case stripPrefixCCall string 
			[("(CExpr (Just (CCall (CVar (Ident \"cstmts\"",5,"cstmts"),
			 ("(CExpr (Just (CCall (CVar (Ident \"cstmt\"",5,"cstmt"),
			 ("(CCall (CVar (Ident \"cexpr\" ",3,"cexpr")] of
	     (Just restOfString,bra,typeCall) -> 
	     	let 
	     		(strread, strrest) = 
	     			case bra of 
	     				--2 -> 
	     				--	readUntilCommaWithBracketsStoring bra restOfString []
	     				_ ->
	     					readUntilClosingBracketStoring bra restOfString 
	     		varRuleName =  (extractVarName strread) 
	     		varName = (buildVarName varRuleName)
	     	in (strrest, acc ++ varName, currentFree + 1, (varRuleName,varName,typeCall):dict)
	     (Nothing,0,_) -> 
	     	case  stripPrefix "(NodeInfo " string of
	     		Just restOfString ->
	     			((readUntilClosingBracket 1 restOfString), acc ++ "_", currentFree, dict)
	     		Nothing -> 
	     			case  stripPrefix "(CIntConst " string of
	     				Just restOfString ->
	     					let 
	     						(strread, strrest) = 
	     							readUntilClosingBracketStoring 1 restOfString
	     						numberStr = 
	     							fst (readUntilSpace strread)
	     						varName = (buildVarName numberStr) 
	     					in (strrest, acc ++ "(CIntConst " ++ varName ++ " _)", currentFree + 1 , (numberStr,varName,"number"):dict)
	     				Nothing -> 
	     					(tail string, acc ++ [head string], currentFree, dict)
	 buildVarName varRuleName = "var_" ++ varRuleName ++ "_"  ++ (show currentFree)
 

buildGenerates :: [String] -> [String] -> [(String,String,String)] -> [(String,String,String)] -> ([String], [(String,String,String)])
buildGenerates [] acc accSubs dict = 
	((reverse acc), accSubs)
buildGenerates (generate:generates) acc accSubs dict = 
	let (generateStr,subs) = 
		buildGenerate generate "" [] dict
	in  buildGenerates generates (generateStr:acc) (accSubs ++ subs) dict


buildGenerate "" acc accSubs dict = 
	(acc,accSubs)
buildGenerate string acc accSubs dict =
	buildGenerate nstring nacc naccSubs dict
	where 
	 (nstring, nacc,naccSubs) =
		case stripPrefixCCall string 
			[("(CExpr (Just (CCall (CVar (Ident \"cstmts\"",5,"cstmts"),
			 ("(CExpr (Just (CCall (CVar (Ident \"cstmt\"",5,"cstmt"),
			 ("(CCall (CVar (Ident \"cexpr\" ",3,"cexpr"),
			 ("(CExpr (Just (CCall (CVar (Ident \"subs\"",5,"subs")] 
		of
		 	(Just restOfString,bra,"subs") -> 
			 	let 
			 		(strread_, strrest) = 
			 			readUntilClosingBracketStoring bra restOfString
			 		(_,strread) = readUntilSqOpen strread_
			 		arguments =  extractArguments strread dict []
			 	in (strrest, acc ++ "(substitute " ++ (arguments!!0) ++ " " ++ (arguments!!1) ++ " " ++ (arguments!!2) ++ ")",accSubs ++ [(arguments!!0,arguments!!1,arguments!!2)])
			(Just restOfString,bra,_) -> 
				let 
					(strread, strrest) = 
						readUntilClosingBracketStoring bra restOfString 
					varRuleName =  (extractVarName strread) 
					varName = 
						head ([varName_ | (varRuleName_,varName_,_) <- dict, varRuleName_ == varRuleName ])
				in (strrest, acc ++ varName,accSubs)
			(Nothing,_,_) -> 
				case stripPrefix "(NodeInfo " string of
					Just restOfString ->
						((readUntilClosingBracket 1 restOfString), acc ++ "undefNode",accSubs)
					Nothing -> 
						case  stripPrefix "(CIntConst " string of
							Just restOfString ->
								let 
									(strread, strrest) = 
										readUntilClosingBracketStoring 1 restOfString
									numberStr = 
										fst (readUntilSpace strread)
								in (strrest, acc ++ "(CIntConst (cInteger " ++ numberStr ++ " ) undefNode)",accSubs)
							Nothing -> 
								(tail string, acc ++ [head string],accSubs)

buildConditions [] acc dict = 
	(reverse acc)
buildConditions (condition:conditions) acc dict = 
	let conditionStr = 
		buildCondition condition [] dict
	in buildConditions conditions (conditionStr:acc) dict

buildCondition "" acc dict = 
	acc
buildCondition string acc dict =
	buildCondition nstring nacc dict
	where 
	 (nstring, nacc) =
		case stripPrefixCCall string [
			("(CExpr (Just (CCall (CVar (Ident \"no_defs\"",5,"no_defs"),
			("(CExpr (Just (CCall (CVar (Ident \"no_uses\"",5,"no_uses"),
			("(CExpr (Just (CCall (CVar (Ident \"is_cons\"",5,"is_cons")] of
	     (Just restOfString,bra,typeCall) -> 
	     	let 
	     		(strread_, strrest) = 
	     			case bra of 
	     				--2 -> 
	     				--	readUntilCommaWithBracketsStoring 0 restOfString []
	     				_ ->
	     					readUntilClosingBracketStoring bra restOfString 
	     		(_,strread) = readUntilSqOpen strread_
	     		arguments =  extractArguments strread dict []
	     		condition = buildConditionStr typeCall arguments dict
	     	in (strrest, acc ++ condition)
	     (Nothing,_,_) -> 
	     	(tail string, acc)
	     --(Nothing,_,_) -> 
	     --	(tail string, acc ++ [head string])


extractArguments "" _ acc =
	reverse acc 
extractArguments string dict acc = 
	let 
		(argument,restOfString) = (tokenizeCommas 0 string) 
		varRuleName = (extractVarName argument) 
		nacc = 
			case varRuleName of 
				"" -> 
					acc
				_ ->
					let 
						varName = 
							--trace ((show dict)++" " ++ (show varRuleName) ) head ([varName_ | (varRuleName_,varName_,_) <- dict, varRuleName_ == varRuleName ]) 
							head ([varName_ | (varRuleName_,varName_,_) <- dict, varRuleName_ == varRuleName ]) 
					in (varName:acc)
	in extractArguments restOfString dict nacc

stripPrefixCCall string [] = 
	(Nothing,0,"notype")
stripPrefixCCall string ((acc,bra,typeCall):tail_) =
	--case stripPrefix acc (trace ("LF: " ++ (show acc) ++ "\nSPCC: " ++ (show string)) string) of
	case stripPrefix acc string of
		Just restOfString ->
			(Just restOfString,bra,typeCall)
		Nothing ->
			stripPrefixCCall string tail_

buildConditionStr "no_uses" args dict =
	"(null (uses " ++ args!!0 ++ " " ++ (transformToStmts (args!!1) dict)  ++ "))"
buildConditionStr "no_defs" args dict =
	"(null (defs " ++ args!!0 ++ " " ++ (transformToStmts (args!!1) dict) ++ "))"	
buildConditionStr "is_cons" args _ =
	"(isConstant " ++ args!!0 ++ ")"
buildConditionStr _ _ _ =
	""

transformToStmts goal dict = 
	case head ([type_ | (_,varName_,type_) <- dict, varName_ == goal ]) of 
		"cstmt" ->
			"[" ++ goal ++ "]"
		"cexpr" ->
			"[(CBlockStmt (CExpr (Just " ++ goal ++ ") undefNode))]"
		_ ->
			goal


--buildConsDict :: [(String, [String])] -> String -> String
buildConsDict [] acc = 
	acc ++ "True"
buildConsDict [(nameRule,_,[nameVar])] acc  
	| head nameRule `elem` [head (show i) | i <- [0..9]] = 
		acc ++ (buildConsDictEntryConst nameVar nameRule)
	| otherwise = 
		acc ++ "True"
buildConsDict [(nameRule,_,nameVars)] acc = 
	acc ++ (buildConsDictEntry nameVars) 
buildConsDict ((nameRule,_,[nameVar]):dict) acc 
	| head nameRule `elem` [head (show i) | i <- [0..9]] = 
		buildConsDict dict (acc ++ (buildConsDictEntryConst nameVar nameRule) ++ " && ")
	| otherwise = 
		buildConsDict dict acc
buildConsDict ((nameRule,_,nameVars):dict) acc = 
	buildConsDict dict (acc ++ (buildConsDictEntry nameVars) ++ " && ")


buildConsDictEntry [v1,v2] =
	buildOneConsDictEntry v1 v2
buildConsDictEntry (v1:v2:rest) =
	(buildOneConsDictEntry v1 v2) ++ " && " ++ (buildConsDictEntry (v2:rest))


buildOneConsDictEntry v1 v2 = 
	"(exprEqual " ++ v1 ++ " " ++ v2 ++ ")"

buildConsDictEntryConst nameVar num = 
	"(getCInteger " ++ nameVar ++")" ++ " == " ++ num


joinDictEntries [] acc done =
	acc
joinDictEntries ((nameRule, nameVar,typeVar):dict) acc done =
 		joinDictEntries dict nacc ndone
 	where 
 	 (nacc, ndone) = 
	 	case  nameRule `elem` done of
	 		True ->  
	 			(acc, done)
	 		False ->
	 			case (head nameRule) `elem`  [head (show i) | i <- [0..9]] of 
	 				True -> 
	 					(((nameRule, typeVar, [nameVar]):acc), done)
	 				False -> 
	 					(((nameRule, typeVar, nameVar:[nameVar_ | (nameRule_, nameVar_,_) <- dict, nameRule_ == nameRule]):acc), nameRule:done)

readUntilClosingBracket n str = 
	readUntilClosing '(' ')' n str
readUntilClosingSqBracket n str = 
	readUntilClosing '[' ']' n str

readUntilClosing open close n (current:strtail) 
	| close == current && n <= 1 = 
		strtail
	| close == current = 
		readUntilClosing open close (n-1) strtail
	| open == current = 
		readUntilClosing open close (n+1) strtail
	| otherwise =
		readUntilClosing open close n strtail
readUntilClosing open close n [] =
	[]
--readUntilClosingBracket n list = 
--	trace ((show n) ++ " " ++ (show list)) ""

readUntilClosingBracketStoring n str =
	readUntilClosingStoring '(' ')' n str
readUntilClosingSqBracketStoring n str =
	readUntilClosingStoring '[' ']' n str
readUntilCommaStoring n str = 
	readUntilClosingStoring ',' ',' n str

--readUntilCommaWithBracketsStoring n str acc = 
--	let 
--		(nacc,restOfString) = readUntilClosingStoring '(' ')' n str
--	in 
--		case restOfString of
--			[] -> (acc ++ nacc,[])
--			(',':rest) ->  (acc ++ nacc, restOfString)
--			(']':rest) ->  (acc ++ "]" ++ nacc, restOfString)
--			_ -> readUntilCommaWithBracketsStoring 0 restOfString (acc++nacc)

readUntilClosingStoring open close n (current:strtail) 
	| current == close && n <= 1 = 
		([close],strtail) 
	| current == close =
		let (strread,strrest) = 
				readUntilClosingStoring open close (n-1) strtail
		in ((close:strread),strrest)
	| current == open =
		let (strread,strrest) = 
				readUntilClosingStoring open close (n+1) strtail
		in ((open:strread),strrest)
	| otherwise =
		let (strread,strrest) = 
			readUntilClosingStoring open close n strtail
		in ((current:strread),strrest)
readUntilClosingStoring open close n [] =
	([],[])

readUntilGivenChar char1 (char2:strtail)
	| char1 == char2 =
		("",strtail)
	| otherwise = 
		((char2:strread),strrest)
	where
		(strread,strrest) = readUntilGivenChar char1 strtail

readUntilQuotes = readUntilGivenChar '"'
readUntilSpace = readUntilGivenChar ' '
readUntilSqOpen = readUntilGivenChar '['


tokenizeCommas n (current:strtail) 
	| ((current == ')' || current == ']' || current == ',') && n == 0)  = 
		([],strtail) 
	| (current == ')' || current == ']') =
		let (strread,strrest) = 
				tokenizeCommas (n-1) strtail
		in ((current:strread),strrest)
	| (current == '(' || current == '[') =
		let (strread,strrest) = 
				tokenizeCommas (n+1) strtail
		in ((current:strread),strrest)
	| otherwise =
		let (strread,strrest) = 
			tokenizeCommas n strtail
		in ((current:strread),strrest)
tokenizeCommas n [] =
	([],[])

extractVarName "" = 
	""
extractVarName string = 
	case stripPrefix "[CVar (Ident \"" string of
		Just restOfString -> 
			fst (readUntilQuotes restOfString)
		Nothing -> 
			extractVarName (tail string)

removeFirstLast str = 
	drop 1 (take ((length str) - 1) str)


onlyPatterns dict pat = 
	case stripPrefix "(" pat of 
		Just _ -> 
			True
		Nothing -> 
			case [ v | (_,"cstmt",vars) <- dict,v <- vars, v == pat] of 
				[] -> False
				_ -> True

stringInterLists [current] dict prev state = 
	case stripPrefix "(" current of 
		Just _ -> 
			prev ++ ",[]"
		Nothing -> 
			case [ v | (_,"cstmts",vars) <- dict,v <- vars, v == current] of 
				[] -> 
					case stripPrefix "level" current of 
						Just levelNumber ->
							(head [lprev | (ln,lprev) <- state, ln == levelNumber]) ++ ",[]"
						Nothing ->
							prev ++ ",[]"
				_ -> 
					current
stringInterLists (current:tail_) dict prev state = 
	case stripPrefix "(" current of 
		Just _ -> 
			prev ++ "," ++ (stringInterLists tail_ dict "[]" state)
		Nothing -> 
			--case  trace ((show current) ++ " " ++ (show [ v | (_,"cstmts",vars) <- dict,v <- vars, v == current])) [ v | (_,"cstmts",vars) <- dict,v <- vars, v == current] of 
			case  [ v | (_,"cstmts",vars) <- dict,v <- vars, v == current] of 
				[] -> 
					--case trace ((show current) ++ " " ++ (show (stripPrefix "level" current)) ) (stripPrefix "level" current) of 
					case  (stripPrefix "level" current) of 
						Just levelNumber ->
							--case  trace ((show current) ++ " " ++ (show [lprev | (ln,lprev) <- state, ln == levelNumber]) ) [lprev | (ln,lprev) <- state, ln == levelNumber] of 
							case [lprev | (ln,lprev) <- state, ln == levelNumber] of 
								[] ->
									stringInterLists tail_ dict "[]" ((levelNumber,prev):state)
								[lprev] -> 
									prev ++ "," ++ (stringInterLists tail_ dict lprev (delete (levelNumber,lprev) state))
						Nothing ->
							prev ++ "," ++ (stringInterLists tail_ dict "[]" state)
				_ -> 
					stringInterLists tail_ dict current state
					
			

createStrListRuleNames [] = 
	""
createStrListRuleNames [rule] = 
	rule
createStrListRuleNames (rule:rules) = 
	rule ++ "," ++ (createStrListRuleNames rules)

buildBlockOrDeclStmt stmt = 
	case stripPrefix "(CDecl" stmt of 
		Just _ -> "(CBlockDecl "++ stmt ++ ")"
		Nothing -> "(CBlockStmt "++ stmt ++ ")"

buildConditionHaskell [] = 
	"True"
buildConditionHaskell [x] =
	x 
buildConditionHaskell (x:xs) = 
	x ++ " && " ++ (buildConditionHaskell xs)

buildCondSubs ([]) = 
	"True"
buildCondSubs ([(where_,from,to)]) = 
	"(geq (substitute " ++ where_ ++ " " ++ from ++ " " ++ to ++ ") " ++ where_ ++ " )" 
buildCondSubs ((where_,from,to):tail_) = 
	"(geq (substitute " ++ where_ ++ " " ++ from ++ " " ++ to ++ ") " ++ where_ ++ " ) && " 
	++ buildCondSubs (tail_) 
