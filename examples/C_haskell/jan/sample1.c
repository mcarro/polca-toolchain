
#define N 200


int v0[N];
int xs[N];
int Z;
int i0, i1;

void init()
{
	int i;
	for(i = 0; i < N; i++)
	{
		xs[i] = i;
	}
}


int main()
{
	init();
	// foldl (+) 0 (map (2*) xs)
	{
		// v0 = (map (2*) xs)
		for (i0 = 0; i0 < N; i0++) {
		  v0[i0] = 2 * xs[i0];
		}
		// foldl (+) 0 v0
		{
			Z = 0;
			for (i1 = 0; i1 < N; i1++) {
			  Z = Z + v0[i1];
			}
		}
	}
	return 0;
}
