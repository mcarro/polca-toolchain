/**************************************************************/
// Initial code with pragma annotations
//
// Function filterRGB creates 3 filters (RGB) all in the same function.
// It does not matter what is done inside for bodies. We just
// interpret a number of properties from map annotations. If these 
// properties are not true, then the code should not be annotated with a
// map. 
/**************************************************************/

void filterRGB(char img,char imgR,char imgG,char imgB)
{
	int i = 0;

	#pragma polca map red img imgR
	for(i = 0; i < SIZE; i++)
	{
		red(img,imgR,i);
	}

	#pragma polca map green img imgG
	for(i = 0; i < SIZE; i++)
	{
		green(img,imgG,i);
	}

	#pragma polca map blue img imgB
	for(i = 0; i < SIZE; i++)
	{
		blue(img,imgB,i);
	}

}

/**************************************************************/
// Annotation translation
//
// From previous annotations a set of low level annotation can be inferred.
// These annotations can be used directly by the transforamtion system 
// to apply or not some rules.
/**************************************************************/

void filterRGB(char img,char imgR,char imgG,char imgB)
{
	int i = 0;

	// imgR is read inside the loop in offset 0, i.e. img[i]
	#pragma polca reads img in {0}
	// imgR is written inside the loop in offset 0, i.e. imgR[i]
	#pragma polca writes imgR in {0}
	// Loop modifer only writes  i
	#pragma polca update_set i++ {i}
	//Following properties are not needed for this example
	#pragma iteration_independent
	#pragma same_length img imgR
	#pragma iteration_range 0 SIZE
	for(i = 0; i < SIZE; i++)
	// Loop body does not writes in i (i.e. loop iterator), 0 (i.e. loop initializer),
	// SIZE (i.e. loop maximum), and it only writes in arrays.
	#pragma polca no_writes i
	#pragma polca no_writes 0
	#pragma polca no_writes SIZE
	#pragma polca only_writes_arrays
	{
		red(img,imgR,i);	
	}

	#pragma polca reads img in {0}
	#pragma polca writes imgG in {0}
	#pragma polca update_set i++ {i}
	#pragma iteration_independent
	#pragma same_length img imgG
	#pragma iteration_range 0 SIZE
	for(i = 0; i < SIZE; i++)
	#pragma polca no_writes i
	#pragma polca no_writes 0
	#pragma polca no_writes SIZE
	#pragma polca only_writes_arrays
	{
		green(img,imgG,i);
	}

	#pragma polca reads img in {0}
	#pragma polca writes imgB in {0}
	#pragma polca update_set i++ {i}
	#pragma iteration_independent
	#pragma same_length img imgB
	#pragma iteration_range 0 SIZE
	for(i = 0; i < SIZE; i++)
	#pragma polca no_writes i
	#pragma polca no_writes 0
	#pragma polca no_writes SIZE
	#pragma polca only_writes_arrays
	{
		blue(img,imgB,i);
	}

}

/**************************************************************/
// Transforming the code using these properties
// 
// Next step is to transform previous code using the properties inferred
// from map annotations. We are going to apply fusion rule twice. First,
// fusing loop 1 and 2, and then loops 1-2 and 3.
//
// STML rule for fusion is the following:

for_loop_fusion
{
    pattern:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(init);bin_oper(cop(op),cexpr(i),cexpr(n));cexpr(mod))
        {
            cstmts(bodyFOR1);
        }
        for(cexpr(i) = cexpr(init);bin_oper(cop(op),cexpr(i),cexpr(n));cexpr(mod))
        {
            cstmts(bodyFOR2);
        }
        cstmts(fin);
    }
    condition:
    {
        no_writes(cstmts(bodyFOR1),cexpr(init));
        no_writes(cstmts(bodyFOR1),cexpr(n));  
        no_writes(cstmts(bodyFOR1),cexpr(i));  
        no_writes(cstmts(bodyFOR2),cexpr(init));
        no_writes(cstmts(bodyFOR2),cexpr(n));  
        no_writes(cstmts(bodyFOR2),cexpr(i)); 
        pure(cop(op));
        is_subseteq(writes(cexpr(mod)),{cexpr(i)}); 
        no_writes_except_arrays(cstmts(bodyFOR1), cstmts(bodyFOR2), cexpr(i));
        !anti_dep(cstmts(bodyFOR1), cstmts(bodyFOR2), cexpr(i));
        !flow_dep(cstmts(bodyFOR1), cstmts(bodyFOR2), cexpr(i));
        !out_dep(cstmts(bodyFOR1), cstmts(bodyFOR2), cexpr(i));
    }
    generate:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(init);bin_oper(cop(op),cexpr(i),cexpr(n));cexpr(mod))
        {
            cstmts(bodyFOR1);
            cstmts(bodyFOR2);
        }
        cstmts(fin);
    }
}


// Since conditions for fusion are met, we can fuse the first two loops.
// Conditions for fusing are the following:

// First loop's body does not writes in locations read by initialization expression 
no_writes({red(img,imgR,i);},0); // where 0 comes from for(i=0;...)
// Deduced from #pragma polca no_writes 0

// First loop's body does not writes in locations read by the maximum expression 
no_writes({red(img,imgR,i);},SIZE); // where SIZE comes from for(..;i < SIZE;...)
// Deduced from #pragma polca no_writes SIZE

// First loop's body does not writes in iterator
no_writes({red(img,imgR,i);},i); 
// Deduced from #pragma polca no_writes i

// Analogous for second loop's body conditions

// Comparation function is pure
pure(<) // for(...; i < SIZE; ...)
// Deduced from properties of default operators

// Modifier only writes iterator or nothing
is_subseteq(writes(i++),{i});
// Deduced from #pragma polca update_set i++ {i}

// First loop's body does not writes in any location read by second loop's body,
// except array indexed by i.
no_writes_except_arrays({red(img,imgR,i);}, {green(img,imgG,i);}, i);
// deduced from #pragma polca only_writes_arrays

// There is not anti-dependeces (i.e. read before write) between first loop's body and the second's one.
// This should hold for all arrays indexed by i in the loops's bodies.
!anti_dep({red(img,imgR,i);},{green(img,imgG,i);},i) 
////	All vectors are only read or written, so there is not anti-dependes 
// deduced from:
// 		#pragma polca reads img in {0}
// 		#pragma polca writes imgG in {0}
// 		-------------------------------
// 		#pragma polca reads img in {0}
// 		#pragma polca writes imgG in {0}

// There is not flow-dependeces (i.e. writes before read) between first loop's body and the second's one.
!flow_dep({red(img,imgR,i);},{green(img,imgG,i);},i)
////	All vectors are only read or written, so there is not flow-dependes
// deduced from:
// 		#pragma polca reads img in {0}
// 		#pragma polca writes imgG in {0}
// 		-------------------------------
// 		#pragma polca reads img in {0}
// 		#pragma polca writes imgG in {0}

// There is not out-dependeces (i.e. writes before write) between first loop's body and the second's one.
!out_dep({red(img,imgR,i);},{green(img,imgG,i);},i)
////	Vectors written are different in both loops so there is no out-dependeces
// deduced from:
// 		#pragma polca reads img in {0}
// 		#pragma polca writes imgG in {0}
// 		-------------------------------
// 		#pragma polca reads img in {0}
// 		#pragma polca writes imgG in {0}


// CODE GENERATION
//
// The resulting code is the one that generates block specifies. 
// Properties are melted in the new generated code.

/**************************************************************/

void filterRGB(char img, char imgR, char imgG, char imgB)
{
	int i = 0;

	#pragma polca reads img in {0}
	#pragma polca writes imgR in {0}
	#pragma polca writes imgG in {0}
	#pragma polca update_set i++ {i}
	#pragma iteration_independent
	#pragma same_length img imgR
	#pragma same_length img imgG
	#pragma iteration_range 0 SIZE
	for(i = 0; i < SIZE; i++)
	#pragma polca no_writes i
	#pragma polca no_writes 0
	#pragma polca no_writes SIZE
	#pragma polca only_writes_arrays
	{
		red(img,imgR,i);	
		green(img,imgG,i);
	}

	#pragma polca reads img in {0}
	#pragma polca writes imgB in {0}
	#pragma polca update_set i++ {i}
	#pragma iteration_independent
	#pragma same_length img imgB
	#pragma iteration_range 0 SIZE
	for(i = 0; i < SIZE; i++)
	#pragma polca no_writes i
	#pragma polca no_writes 0
	#pragma polca no_writes SIZE
	#pragma polca only_writes_arrays
	{
		blue(img,imgB,i);
	}

}

/**************************************************************/
// Transforming the code using these properties 
//
// Finally, we apply previous transformation again, so we get
// only one loop.
/**************************************************************/

void filterRGB(char img, char imgR, char imgG, char imgB)
{
	int i = 0;

	#pragma polca reads img in {0}
	#pragma polca writes imgR in {0}
	#pragma polca writes imgG in {0}
	#pragma polca writes imgB in {0}
	#pragma polca update_set i++ {i}
	#pragma iteration_independent
	#pragma same_length img imgR
	#pragma same_length img imgG
	#pragma same_length img imgB
	#pragma iteration_range 0 SIZE
	for(i = 0; i < SIZE; i++)
	#pragma polca no_writes i
	#pragma polca no_writes 0
	#pragma polca no_writes SIZE
	#pragma polca only_writes_arrays
	{
		red(img,imgR,i);	
		green(img,imgG,i);
		blue(img,imgB,i);
	}

}


/**************************************************************/
// Other uses: Parallelization

// From previous code, and using the following property:

#pragma iteration_independent 

// We can produce parallel code using, for instance, OpenMP.
/**************************************************************/


void filterRGB(char img,char imgR,char imgG,char imgB)
{
	int i = 0;

	// We indicate to omp that the loop is parallelizable
	#pragma omp parallel for
	for(i = 0; i < SIZE; i++)
	{
		red(img,imgR,i);	
		green(img,imgG,i);
		blue(img,imgB,i);
	}

}
