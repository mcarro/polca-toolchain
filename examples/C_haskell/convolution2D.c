/* 
 * Copyright (c) 2014, Recore Systems B.V., The Netherlands,
 * web: www.recoresystems.com, email: info@recoresystems.com
 *
 * Any reproduction in whole or in parts is prohibited
 * without the written consent of the copyright owner.
 *
 * All Rights Reserved.
 *
 */
// #include <stdio.h>
// #include <stdlib.h>
// #include <math.h>
// #include <assert.h>
// #include "pgm_import.h"
// #include "sift.h"

#include <stdint.h>



/* Function to perform 2D convolution */
void convolution2D(uint8_t width,
    uint8_t height,
    uint8_t filterLen,
    uint8_t fracLen,
    int32_t *pFilter,
    int32_t *pImage,
    int32_t *pGauss)
{
  uint8_t w, h, k, l;
  int16_t x, y, xstart, ystart;
  int32_t sum;
  uint8_t filterLenBy2 = filterLen >> 1;

  /* Loop over processed image height indices */
  for (h = 0; h < height; h++) 
  {
    /* Loop over processed image width indices */
    for (w = 0; w < width; w++) 
    {
      sum = 0; /* Initialize sum to 0 */
      /* Pixel row index to start with */
      ystart = h - filterLenBy2;
      /* Loop over filter length height-wise */
      for (l = 0; l < filterLen; l++) 
      {
        y = ystart;
        ystart++; /* Increment pixel row index */
        /* Pixel column index to start with */
        xstart = w - filterLenBy2;
        /* Loop over filter length width-wise */
        for (k = 0; k < filterLen; k++) 
        {
          x = xstart;
          xstart++; /* Increment pixel column index */
          /* Skip pixels that are outside the image boundary (Zero-padding) */
          // if ((x < 0) || (y < 0) || (x > width - 1) || (y > height - 1)) 
          //   continue;
          /* Multiply image pixel with filter coefficient and add to sum */
          if (!((x < 0) || (y < 0) || (x > width - 1) || (y > height - 1))) 
          {
            sum = sadd32(sum,
                fmul32(pImage[y * width + x],
                    pFilter[l * filterLen + k],
                    fracLen)
          );
          }
        }
      }
      /* Copy sum to output gaussian image location */
      pGauss[h * width + w] = sum;
    }
  }
}
