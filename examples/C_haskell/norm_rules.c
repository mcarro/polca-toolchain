//Rule used that moves the control declaration outside the loop statement
move_decl_outside_for
{
    pattern:
    {
        cstmts(ini);
        int i = 0;
        for(cexpr(i) = cexpr(init);cexpr(cond);cexpr(mod))
        {
            cstmts(body);
        }
        cstmts(end);
    }
    generate:
    {       
        cstmts(ini);
        cdecl(cint(),cexpr(i));
        for(cexpr(i) = cexpr(init);cexpr(cond);cexpr(mod))
        {
            cstmts(body);
        }
        cstmts(end);
    }
}