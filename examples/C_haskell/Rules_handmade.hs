--  Copyright (C) 2015 The IMDEA Software Institute 
-- 	            and the Technical University of Madrid

-- All rights reserved.
-- For additional copyright information see below.

-- This file is part of the polca-transformation-rules package

-- License: This work is licensed under the Creative Commons
-- Attribution-NonCommercial-NoDerivatives 4.0 International License. To
-- view a copy of this license, visit
-- http://creativecommons.org/licenses/by-nc-nd/4.0/ or send a letter to
-- Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
--
-- Main author: Salvador Tamarit
-- Please address questions, etc. to: <polca-project-madrid@software.imdea.org>



module Rules where 

import RulesLib

import Language.C
import Language.C.Data.Ident

import Debug.Trace


stmtRules = [applyRule1, applyRule2, rule_dec2inc_for_5, rule_change_order_6]

exprRules = [applyRulesExpr_]

--applyRulesExpr :: CTranslUnit -> CTranslUnit


applyRulesExpr_ :: CExpr -> [(String, CExpr,CExpr)]
-- a*a - b*b == (a - b) * (a + b)
applyRulesExpr_ 
	ori@(CBinary CSubOp
		(CBinary CMulOp op11 op12 _)
	    (CBinary CMulOp op21 op22 _)
		_) 
	| (exprEqual op11 op12) && (exprEqual op21 op22)
	= 
	[("sub_to_mul",ori,(CBinary CMulOp
		(CBinary CSubOp op11 op21 undefNode)
        (CBinary CAddOp op11 op21 undefNode)
        undefNode))]
applyRulesExpr_ x = 
	[] --trace ("\nExpr:" ++ (show (cast x)) ++ "\n") x



rule_dec2inc_for_5
	old@(CFor (Left (Just (CAssign CAssignOp var_i_0 (CBinary CSubOp var_n_1 (CConst (CIntConst var_1_2 _)) _) _))) (Just (CBinary CGeqOp var_i_3 (CConst (CIntConst var_0_4 _)) _)) (Just (CUnary CPostDecOp var_i_5 _)) (CCompound [] [CBlockStmt var_a_6,CBlockStmt var_b_7] _) _)
	| (getCInteger var_1_2) == 1 && (getCInteger var_0_4) == 0 && (exprEqual var_i_5 var_i_3) && (exprEqual var_i_3 var_i_0) && True =
	[("dec2inc_for",old,(CFor (Left (Just (CAssign CAssignOp var_i_5 (CConst (CIntConst (cInteger 0 ) undefNode)) undefNode))) (Just (CBinary CLeOp var_i_5 var_n_1 undefNode)) (Just (CUnary CPostIncOp var_i_5 undefNode)) (CCompound [] [CBlockStmt var_b_7,CBlockStmt var_a_6] undefNode) undefNode))]
rule_dec2inc_for_5 _ = []

--applyRule :: [(Name,[String])] -> CBlockItem -> CBlockItem
--applyRule :: CBlockItem -> [(CStat,CStat)]
--mapStride
applyRule2 x@( 
	ori_for@(CFor 
		initDecl@(Left (Just 
			(CAssign CAssignOp 
				iter@(CVar (Ident iter1 _ _) _) 
				offset@(CConst (CIntConst offset1 _)) 
				_ ) ))  
		cond@(Just
			(CBinary CLeOp
			 (CVar (Ident iter2 _ _) _)
			 (CBinary CSubOp
			    size
			    (CConst (CIntConst offset2 _) )
			    _ )
			_ ))
		inc@(Just
			(CUnary CPostIncOp
			 (CVar (Ident iter3 _ _) _)
			 _))
      	stmt@(CCompound []
			[CBlockStmt
				(CExpr
					(Just
						(CAssign CAssignOp
							(CIndex
								(CVar outs _ )
		                        exprO _ )
		                	(CCall
		                    	(CVar f _ )
		                        [(CVar ins _),
		                         exprI]
			                _)
			        _))
			    _)]
			_)
      	ninfo)) 
	| iter1 == iter2 && iter2 == iter3 && offset1 == offset2 &&
	  [ann | (name,ann) <- polcaAnn, (nameOfNode ninfo) == Just name, (head ann) == "mapStride"] /= [] = 
	  	[("mapStride", 
	  		ori_for,(CCompound []
	            [
	            CBlockDecl
	              (CDecl
	                 [CTypeSpec (CIntType undefNode)]
	                 [(Just
	                     (CDeclr
	                        (Just (mkIdent nopos ("ite"++(show currentIte)) (head newNameSupply)) )
	                        []
	                        Nothing
	                        []
	                        undefNode),
	                   Nothing, Nothing)]
	                 undefNode),
	             CBlockStmt 
					(CFor 
						(Left (Just 
							(CAssign CAssignOp 
								iteVar 
								(intConstant 0)
								undefNode) ))  
						(Just
							(CBinary CLeOp
							 iteVar
							 (CBinary CDivOp
							    size
							    (intConstant size_chunk)
							    undefNode )
							undefNode) )
						(Just
							(CUnary CPostIncOp
							 iteVar
							 undefNode))
					 	(CCompound [] 
					 		[CBlockStmt (forchunk 
					 			offset 
					 			(CBinary CSubOp (intConstant size_chunk) offset undefNode)
					 			newNode),
							CBlockStmt (CIf 
								(CBinary CNeqOp iteVar (intConstant 0) undefNode )
								(forchunk (intConstant 0) offset undefNode) 
								Nothing undefNode),
							CBlockStmt (CIf 
								(CBinary CNeqOp iteVar (CBinary CDivOp size (intConstant size_chunk) undefNode ) undefNode )
								(forchunk 
						 			(CBinary CSubOp (intConstant size_chunk) offset undefNode )
						 			(intConstant size_chunk)
						 			undefNode) 
								Nothing undefNode)
					 		]
					 		undefNode) 
					 	ninfo)
					] undefNode)
				)]
	| otherwise = 
		[]
	where
		--currentIte = getCurrentIte
		polcaAnn = getPolcaAnn 1
		--currentNode = getCurrentNode
		(newNode,currentIte) = getCurrentInfo ["mapStride", "boundary:false"]
		iteVar = (CVar (mkIdent nopos ("ite"++(show currentIte)) (head newNameSupply)) undefNode) 
		forchunk ini end node = 
				(CFor 
					(Left (Just 
						(CAssign CAssignOp 
							iter 
							ini
							undefNode) ))  
					(Just
						(CBinary CLeOp
						 iter
						 end
						undefNode) )
					(Just
						(CUnary CPostIncOp
						 iter
						 undefNode))
						internalbody 
						node)
		internalbody = 
			CCompound []
				[CBlockStmt
					(CExpr
						(Just
							(CAssign CAssignOp
								(CIndex
									(CVar outs undefNode)
			                        (replace exprO iter1 iteVar) undefNode )
			                	(CCall
			                    	(CVar f undefNode )
			                        [(CVar ins undefNode),
			                         (replace exprI iter1 iteVar)]
				                undefNode)
				        undefNode))
				    undefNode)]
				undefNode
applyRule2 _ = 
	[]
-- Power of a number
applyRule1 x@( 
	ori_for@(CFor 
		_ 
		(Just (CBinary CLeOp _ ntimes _ ))
		_
      	(CCompound []
			[CBlockStmt
				(CExpr
					(Just
						(CAssign CAssignOp
							varResult@(CVar(Ident name1 _ _) _)
		                	(CBinary CMulOp
		                    	(CVar(Ident name2 _ _) _)
		                        varMult
			                _)
			        _))
			    _)]
			_)
      	ninfo)) 
	| name1 == name2 = 
		[("power_number",ori_for,
			(CWhile
                 (CBinary CGrOp
                    ntimes (intConstant 0) undefNode)
                 (CCompound []
                    [CBlockStmt
                       (CIf
                          (CBinary CEqOp
                             (CBinary CRmdOp ntimes (intConstant 2) undefNode)
                             (intConstant 0)
                             undefNode)
                          (CCompound []
                             [CBlockStmt
                                (CExpr
                                   (Just
                                      (CAssign CAssignOp
                                         varResult
                                         (CBinary CMulOp
                                            varResult
                                            varResult
                                            undefNode)
                                        undefNode)
                                   )
                                undefNode),
                              CBlockStmt
                                (CExpr
                                   (Just
                                      (CAssign CAssignOp
                                         ntimes
                                         (CBinary CDivOp ntimes (intConstant 2) undefNode)
                                         undefNode))
                                   undefNode)]
                             undefNode)
                          (Just
                             (CCompound []
                                [CBlockStmt
                                   (CExpr
                                      (Just
                                         (CAssign CAssignOp
                                            varResult
                                            (CBinary CMulOp varResult (intConstant 3) undefNode)
                                            undefNode))
                                      undefNode),
                                 CBlockStmt
                                   (CExpr
                                      (Just
                                         (CAssign CAssignOp
                                            ntimes
                                            (CBinary CSubOp ntimes (intConstant 1) undefNode)
                                            undefNode))
                                      undefNode)]
                                undefNode))
                          undefNode)]
                    undefNode)
                 False
                 undefNode)
			)]
	| otherwise = 
		[]
applyRule1 _ = 
	[]

--applyRules_ :: CStat -> [(String,CStat,CStat)]
--applyRules_ ori@(CCompound ident bs nodeInfo) = 
--	if cond
--	then 
--		case pats of
--			[(CBlockStmt (CExpr (Just (CAssign CAddAssOp v0 v1 _)) _)),
--		 	 (CBlockStmt (CExpr (Just (CAssign CMulAssOp v2 v3 _)) _))] ->
--				[("swap", ori, (CCompound ident (b ++ m ++ [(CBlockStmt (CExpr (Just (CAssign CAddAssOp v0 (CBinary CAddOp v1 v3 undefNode) undefNode)) undefNode))] ++ a) nodeInfo))]
--			_ -> 
--				usualResult
--	else 
--		usualResult
--	where 
--		(cond, inter,pats) = joinable bs []
--		[b,m,a] = inter
--		--[b,m,a] = [concat (map (applyRule) interElem) | interElem <- inter]
--		usualResult = 
--			--(trace ("**********\n"++(prettyMyAST (CCompound [] bs undefNode))) (map (applyRule) bs))
--			--concat (map (applyRule) bs)
--			[]
--applyRules_ x = []



----joinable :: [CBlockItem] -> [CBlockItem] -> (Bool, [[CBlockItem]],[CBlockItem])
--joinable [] acc =
--	(False ,[acc],[])
--joinable (stat@(CBlockStmt (CExpr (Just (CAssign CAddAssOp _ _ _)) _)):tail_) accsl =
--	let (bool,inter,pats) = joinable tail_ []
--	in (bool && (length inter == 2),(accsl:inter),(stat:pats))
--joinable (stat@(CBlockStmt (CExpr (Just (CAssign CMulAssOp _ _ _)) _)):tail_) accsl =
--	let (_,inter,pats) = joinable tail_ []
--	in (length inter == 1,(accsl:inter),(stat:pats))
--joinable (other:tail_) acc =
--	joinable tail_ (acc ++ [other])

-- change_order
rule_change_order_6
	old@(CCompound ident bs nodeInfo) =
	if cond
	then
		case pats of
			[(CBlockStmt (CExpr (Just (CAssign CAddAssOp var_i_1 var_a_2 _)) _)),(CBlockStmt (CExpr (Just (CAssign CMulAssOp var_i_4 var_b_5 _)) _))] | (exprEqual var_i_4 var_i_1) && True->
				[("change_order", old, (CCompound ident (var_ini_0 ++ var_mid_3 ++ [(CBlockStmt (CExpr (Just (CAssign CAddAssOp var_i_4 (CBinary CAddOp var_a_2 var_b_5 undefNode) undefNode)) undefNode))] ++ var_fin_6) nodeInfo))]
			_ -> []
	else []
	where
		(cond, inter,pats) = rule_change_order_6_aux bs []
		[var_ini_0,var_mid_3,var_fin_6]= inter
rule_change_order_6 _ = []

rule_change_order_6_aux [] acc =
	(False,[acc],[])
rule_change_order_6_aux(stat@(CBlockStmt (CExpr (Just (CAssign CAddAssOp var_i_1 var_a_2 _)) _)):tail_) accsl =
	let (bool,inter,pats) = rule_change_order_6_aux tail_ []
	in (bool && length inter == 2, (accsl:inter),(stat:pats))
rule_change_order_6_aux(stat@(CBlockStmt (CExpr (Just (CAssign CMulAssOp var_i_4 var_b_5 _)) _)):tail_) accsl =
	let (_,inter,pats) = rule_change_order_6_aux tail_ []
	in (length inter == 1, (accsl:inter),(stat:pats))
rule_change_order_6_aux (other:tail_) acc =
	rule_change_order_6_aux tail_ (acc ++ [other])

-- change_order
rule_change_order_7
	old@(CCompound ident bs nodeInfo) =
	if cond
	then
		case pats of
			[(CBlockStmt (CExpr (Just (CAssign CAddAssOp var_i_1 var_a_2 _)) _)),(CBlockStmt (CExpr (Just (CAssign CMulAssOp var_i_4 var_b_5 _)) _))] | (exprEqual var_i_4 var_i_1) && True->
				[("change_order", old, (CCompound ident (var_ini_0 ++ var_mid_3 ++ [(CBlockStmt (CExpr (Just (CAssign CAddAssOp var_i_4 (CBinary CAddOp var_a_2 var_b_5 undefNode) undefNode)) undefNode))] ++ var_fin_6) nodeInfo))]
			_ -> []
	else []
	where
		(cond, inter,pats) = rule_change_order_7_aux bs [] True True
		[var_ini_0,var_mid_3,var_fin_6]= inter
rule_change_order_7 _ = []

rule_change_order_7_aux [] acc _ _ =
	(False,[acc],[])
rule_change_order_7_aux(stat@(CBlockStmt (CExpr (Just (CAssign CAddAssOp var_i_1 var_a_2 _)) _)):tail_) accsl True True =
	let (bool,inter,pats) = rule_change_order_7_aux tail_ [] False True
	in (bool && length inter == 2, (accsl:inter),(stat:pats))
rule_change_order_7_aux(stat@(CBlockStmt (CExpr (Just (CAssign CMulAssOp var_i_4 var_b_5 _)) _)):tail_) accsl False True =
	let (_,inter,pats) = rule_change_order_7_aux tail_ [] False False 
	in (length inter == 1, (accsl:inter),(stat:pats))
rule_change_order_7_aux (other:tail_) acc bool1 bool2 =
	rule_change_order_7_aux tail_ (acc ++ [other]) bool1 bool2

-- for_to_while
rule_for_to_while_8
	old@(CCompound ident bs nodeInfo) =
		concat [rule_for_to_while_8_aux2 (drop i bs) (take i bs) ident nodeInfo old | i <- [0..((length bs) - 1)]]
rule_for_to_while_8 _ = []

rule_for_to_while_8_aux2 currentBs restOfBs ident nodeInfo old = 
	if cond
	then
		case pats of
			[(CBlockStmt (CFor (Left (Just (CAssign CAssignOp var_i_1 var_initial_value_2 _))) (Just (CBinary CLeOp var_i_3 var_n_4 _)) (Just (CUnary CPostIncOp var_i_5 _)) (CCompound [] [CBlockStmt var_body_6] _) _))] | (exprEqual var_i_5 var_i_3) && (exprEqual var_i_3 var_i_1) && True->
				[("for_to_while", old, (CCompound ident (restOfBs ++ var_ini_0 ++ [(CBlockStmt (CExpr (Just (CAssign CAssignOp var_i_5 var_initial_value_2 undefNode)) undefNode))] ++ [(CBlockStmt (CWhile (CBinary CLeOp var_i_5 var_n_4 undefNode) (CCompound [] [CBlockStmt var_body_6,CBlockStmt (CExpr (Just (CUnary CPostIncOp var_i_5 undefNode)) undefNode)] undefNode) False undefNode))] ++ var_fin_7) nodeInfo))]
			_ -> []
	else []
	where
		(cond, inter,pats) = rule_for_to_while_8_aux currentBs [] True
		[var_ini_0,var_fin_7]= inter

rule_for_to_while_8_aux [] acc _ =
	(False,[acc],[])
rule_for_to_while_8_aux(stat@(CBlockStmt (CFor (Left (Just (CAssign CAssignOp var_i_1 var_initial_value_2 _))) (Just (CBinary CLeOp var_i_3 var_n_4 _)) (Just (CUnary CPostIncOp var_i_5 _)) (CCompound [] [CBlockStmt var_body_6] _) _)):tail_) accsl  True =
	let (_,inter,pats) = (rule_for_to_while_8_aux tail_ [] False)
	in ((length inter == 1), (accsl:inter),(stat:pats))
rule_for_to_while_8_aux (other:tail_) acc bool =
	rule_for_to_while_8_aux tail_ (acc ++ [other]) bool

