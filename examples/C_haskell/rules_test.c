test_generation
{
    pattern:
    {
        cstmts(ini);
        cstmt(a);
        cstmt(b);
        cstmt(c);
        cstmt(d);
        cstmt(e);
        cstmt(f);
        cstmt(g);
        cstmt(h);
        cstmts(fin);
    }
    generate:
    {
        cstmts(ini);
        gen_list:
        {
            cstmt(a);
            gen_list:
            {
                cstmt(b);
                if_then:
                {
                    no_defs(cstmt(c));
                    cstmt(c);
                }
                gen_list:
                {
                    cstmt(d);
                    cstmt(e);
                }
            }
            gen_list:
            {
                if_then_else:
                {
                	no_defs(cstmt(f));
                    cstmt(f);
                    cstmt(g);
                }
                cstmt(h);
            }
        }
        cstmts(fin);
    }
}

test_generation_simple
{
    pattern:
    {
        cstmts(ini);
        cstmt(a);
        cstmt(b);
        cstmt(c);
        cstmts(fin);
    }
    generate:
    {
    	cstmts(ini);
        gen_list:
        {
        	{cstmt(a);}
            gen_list:
        	{	
        		if_then:
        		{
        			no_defs(cstmt(b));
            		{cstmt(b);}
            	}
            	{cstmt(c);}
            }
        }
        cstmts(fin);
    }
}

test_generation_expr
{
    pattern:
    {
        cexpr(a) + cexpr(b);
    }
    generate:
    {
        cstmt(a);
        // gen_list:
        // {
        //     {cstmt(a);}
        //     {cstmt(b);}
        // }
    }
}

test_pat_creation
{
    pattern:
    {
        cstmts(ini);
        if(cexpr(cond))
        {
            cstmts(body1);
            cexpr(var) = cexpr(val);
            // cstmts(body2_2);
        }
        cstmts(mid);
        if(cexpr(cond))
        {
            cstmts(body_t_1);
            cexpr(var) = cexpr(val);
            cstmts(body_t_2);
            if(cexpr(cond))
            {
                // cstmts(body2_1);
                cexpr(var) = cexpr(val);
                cstmts(body_t_2_2);
            }
            cstmts(body_t_3);
        }
        else
        {
            cstmts(body_e_1);
            cexpr(var) = cexpr(val);
            cstmts(body_e_2);
        }
        cstmts(end);
    }
    generate:
    {
        // cstmts(ini);
        cstmts(mid);
        if(cexpr(cond))
        {
            cexpr(var) = cexpr(val);
            cstmts(body_t_1);
            cstmts(body_t_1);
            cstmts(body_t_1);
        }
        else
        {
            cexpr(var) = cexpr(val);
            cstmts(body_t_1);
            cstmts(body_t_1);
            cstmts(body_t_1);
        }
        cstmts(end);
    }
}