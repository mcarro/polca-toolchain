
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
// #include <stdlib.h>
#include <math.h>

#pragma polca type_size image
#pragma polca type_size redImage
#pragma polca type_size greenImage
#pragma polca type_size blueImage
size_t typeSize;

#pragma polca total_size image
#pragma polca total_size redImage
#pragma polca total_size greenImage
#pragma polca total_size blueImage
int imageSize;

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = r;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = g;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = b;
  	}
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Applying red color filter...\n");
  printf("Applying green color filter...\n");
  printf("Applying blue color filter...\n");

  // just to check
  // int num_proc = 4;
  int j;

  #pragma polca mpi kernel
  #pragma polca def rgbFilter
  #pragma polca input image
  #pragma polca output redImage
  #pragma polca output greenImage
  #pragma polca output blueImage
  #pragma polca chunked height
  #pragma polca previous_chunck_size prev_chunk_size_12
  #pragma polca current_chunk_size curr_chunk_size_13
  for (j = 0; j < num_proc; j++)
  {
    int prev_chunk_size_12 = height / num_proc;
    int curr_chunk_size_13 = j != (num_proc-1) ? prev_chunk_size_12 : prev_chunk_size_12 + height%num_proc;

    for(int i=j*prev_chunk_size_12;i<((j+1)*curr_chunk_size_13);i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
      kernelGreenFilter(image,i,rawWidth,greenImage);
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }
  }

}

int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../../input/test.ppm";
  char* inputImage;
  int width,height;

  #pragma polca io 
  #pragma polca init width
  #pragma polca init height
  #pragma polca init image
  // Internal pragmas
  // #pragma_polca send width
  // #pragma_polca send height
  LoadPPMImageArray(&inputImage,&width,&height,filename);


  typeSize = sizeof(char);
  #pragma polca init imageSize
  imageSize = width * height * 3;  
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);
  /* redcolorFilterImage(filename,"../output/img_redcolor.png"); */
  /* testFilterImage(filename,"../output/img_test.png"); */
  
  /* testFilterImage(inputImage,width,height,&blueImage); */

  #pragma polca io 
  {
    SavePPMImageArray(redImage,width,height,"../../output/test_red.ppm");
    SavePPMImageArray(greenImage,width,height,"../../output/test_green.ppm");
    SavePPMImageArray(blueImage,width,height,"../../output/test_blue.ppm");
  }

  return 0;
}
