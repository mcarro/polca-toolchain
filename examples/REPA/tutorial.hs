-- Taken (in part) from 
-- http://www.haskell.org/haskellwiki/Numeric_Haskell:_A_Repa_Tutorial
-- and
-- "Repa 3 Tutorial for Curious Haskells" (sic) by Eddeland and Söderlund

-- importing REPA
import qualified Data.Array.Repa as R

-- creating arrays
-- un array unidimensional de tamaño 100
cien = R.fromListUnboxed (R.ix1 100) [(0::Float)..99]

-- una matriz de 10 x 10
diezXdiez = R.fromListUnboxed (R.ix2 10 10) [(0::Float)..99]

-- basic operations on arrays
-- maps (returns a delayed - implicit - array)
doubles = R.map (2*) cien

-- we make it an explicit array by evaluating all its elements in
-- parallel:
the_doubles = R.computeP doubles :: IO (R.Array R.U R.DIM1 Float)

-- maps over matrices
halftone :: Float -> Float -> Float
halftone umbral x | x >= umbral = 99.0
halftone umbral x | x <  umbral =  0.0

diagonal = R.map (halftone 50) diezXdiez
show_diagonal = R.computeP diagonal :: IO (R.Array R.U R.DIM2 Float)

-- zipping
doubles' = R.zipWith (+) cien cien
nulos = R.zipWith (-) doubles doubles'
show_nulos = R.computeP nulos :: IO (R.Array R.U R.DIM1 Float)

-- reducing
-- parallel fold requires the operation to be associative (??) and have a
-- neutral element
suma = R.foldAllP (+) 0 cien :: IO Float

-- shape transforming operations: traverse
-- reverse using traverse
-- rev arr = [i <- arr @ (arr.size - i)] 
rev arr = R.traverse arr id indexfun 
    where len = R.size (R.extent arr)
          indexfun ixf (R.Z R.:.i) = ixf $ R.ix1 $ (len - 1 - i)

neic = rev cien
show_neic = R.computeP neic :: IO (R.Array R.U R.DIM1 Float)

-- matrix addition using traverse (called "generate" by Boyle et al. 2001)
-- matadd ma mb = [(i,j) <- ma(i,j) + mb(i.j)]

-- matrix transposition using traverse
-- transpose mat = [(i,j) <- mat(j,i)]
-- prove involutivity of transpose:
-- transpose (transpose mat)           = (transpose definition)
-- [(i,j) <- (transpose mat)(j,i)]     = (transpose definition)
-- [(i,j) <- [(i,j) <- mat(j,i)](j,i)] = (alpha-conversion)
-- [(i,j) <- [(a,b) <- mat(b,a)](j,i)] = (beta-conversion) (!)
-- [(i,j) <- mat(i,j)]                 = (eta-conversion) (!)
-- mat

-- matrix multiplication using traverse
-- matmul ma mb = [(i,j) <- transpose(row(i,ma)) · column(j,mb)]

-- dot product using traverse
-- dotprod v w = reduce (+) [i <- v_i * w_i]

-- unidimensional stencil using traverse
heatspread arr = R.traverse arr id 
                 (stencil3 (R.size R.extent arr))

stencil3 _ ixf (R.Z R.:.0) = ixf $ R.ix1 $ 0
stencil3 max ixf (R.Z R.:.n) | n == max-1 = ixf $ R.ix1 $ n 
stencil3 max ixf (R.Z R.:.i) | otherwise = 
  0.5 * (ixf $ R.ix1 $ i)    +
  0.25 * (ixf $ R.ix1 $ i-1) +
  0.25 * (ixf $ R.ix1 $ i+1)
  
  
  
  
  
  
  
  
  
-- The same function, now using REPA's native syntax for stencils:
