// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// Other functions: support, etc...
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

void readIO(int *v)
{
	*v = 0;
}


float f(int v)
{
	return v/2;
}

int main()
{
	return 0;
}

// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// From 
// https://www.dropbox.com/s/2kgcld31g3nl9ui/Automatic%20Parallelization%20-%20An%20Overview%20of%20Fundamental%20Compiler%20Techniques.pdf?dl=0
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

void foo0() 
{
	// A doubly nested loop
	int i, j;
	int n;
	int p[100], s[100], a[100], b[100];

	for (i = 0; i < n; i ++) {
		for (j = 0; j < n; i ++) {
			p[i] = p[i] + a[i] * b[i];
			s[i] = s[i] + a[i] + b[i]; 
		}
	}
}

void foo1(int n, int *p) 
{ 
	// Loop with pointers

	int i;
	int b[100];

	for (i = 0; i < n; i ++){ 
		int a = 0;
		b[i] = *p + (a ++);
	} 
}

void foo2()
{
	int i, n;
	int c,d;
	int b[100];

	for (i = 1; i < n; i ++) { 
		b[2 * i] = c;
		d = b[2 * i - 1];
	}
}

void foo3(int *k)
{

	// A program over which constant propagation can be performed.

	int i,j,n;
	*k=4;
	readIO(&j); 
	for(i=0;i<n;i++)
	{
		if (j>0){ 
			*k = 5;
			j = *k;
		}
	}
}

void foo3_constant_propagation(int *k)
{

	// A program over which constant propagation can be performed.
	
	int i,j,n;
	*k=4;
	readIO(&j); 
	if (j>0)
	{
		for(i=0;i<n;i++)
		{
			j = 5;			
		}
		if (n > 0)
			*k=5;
	}
}




void foo4()
{
	// A loop with a dependence on the
	// references to the c and d array.
	int i1, i2, n;
	int c[100][100], d[100][100];

	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = 1; i2 < n; i2 ++) {
			c[2 * i1][i2] = d[i1][i2];
			d[i1][i2] = c[2 * i1 - 2][i2 + 1];
		}
	}
}	

int foo6()
{
	// A loop with no data dependences.

	int i;
	int p, s,n;
	int a[100], b[100];
	for (i = 0; i < n; i ++) 
		a[2 * i] = a[2 * i - 1];
}

int foo61()
{
	//A loop where freezing the outer i1 loop  
	//exposes parallelism.

	int i1, i2, n, start1;
	int a[100][100], b[100], c[100][100];

	for (i1 = 1; i1 < 4; i1 ++) { 
		for (i2 = 1; i2 < 8; i2 ++) {
			a[i1][i2] = a[i1][i2] + b[i1];
			c[i1][i2] = a[i1 - 1][i2 + 1];
		}
	}
}

int foo62()
{
	// A loop with data dependences.
	// The loop can be fully parallelized by breaking it into multiple loops,
	// each of which has no loop-carried dependences.
	// To do this requires ensuring that all dependences are lexically forward.

	int i;
	int p, s,n;
	int a[100], b[100], c[100], d[100], e[100];
	for (i = 0; i < n; i ++) 
	{
		a[i] = b[i - 1] + s;
		b[i] = c[i - 1] + s;
		p = a[i - 1] + b[i] * d[i - 2] ;
		d[i] = b[i - 2];
	}
}

int foo62_reordered()
{
	// A loop with data dependences.
	// The loop can be fully parallelized by breaking it into multiple loops,
	// each of which has no loop-carried dependences.
	// To do this requires ensuring that all dependences are lexically forward.

	int i;
	int p, s,n;
	int a[100], b[100], c[100], d[100], e[100];
	for (i = 0; i < n; i ++) 
	{
		b[i] = c[i - 1] + s;
		a[i] = b[i - 1] + s;
		d[i] = b[i - 2];
		p = a[i - 1] + b[i] * d[i - 2] ;
	}
}

int foo62_separated()
{
	// A loop with data dependences.
	// The loop can be fully parallelized by breaking it into multiple loops,
	// each of which has no loop-carried dependences.
	// To do this requires ensuring that all dependences are lexically forward.

	int i;
	int p, s,n;
	int a[100], b[100], c[100], d[100], e[100];
	for (i = 0; i < n; i ++) 
		b[i] = c[i - 1] + s;
	for (i = 0; i < n; i ++) 
		a[i] = b[i - 1] + s;
	for (i = 0; i < n; i ++) 
		d[i] = b[i - 2];
	for (i = 0; i < n; i ++) 
		p = a[i - 1] + b[i] * d[i - 2];
}

int foo63()
{
	// A loop with cross-iteration dependences.
	// The loop of can be  synchronized with post/wait synchronization.

	int i;
	int p, s,n;
	int a[100], b[100], c[100], d[100], e[100];
	for (i = 0; i < n; i ++) 
	{
		a[i] = d[i];
		b[i] = c[i - 1];
		c[i] = b[i - 2] + a[i - 1];
	}
}

int foo64()
{
	// A loop with dependences to be synchronized.

	int i;
	int p, s,n;
	int a[100], b[100], c[100], d[100], e[100];
	for (i = 0; i < n; i ++) 
	{
		a[i] = d[i];
		b[i] = c[i - 1];
		c[i] = a[i - 2];
	}
}

int foo65()
{
	// A loop with a dependence causing scalar access in the last iteration.
	// In the loop, the last value for the array a is captured in the variable last. 
	// Standard dependence testing will assume that there are loop carried anti, output and flow dependences on last.

	int i;
	int p, s,n;
	float last;
	int b[100], c[100], d[100], e[100], t[100];
	for (i = 0; i < n; i ++) 
	{
		float a[n];
		t[i] = b[i];
		// a[i] = f(t[i]);
		a[i] = t[i];
		if (i == n - 1) 
			last = a[i];
	}
}

int foo65_peeled()
{
	// The loop with the last iteration peeled off, allowing the loop to be parallelized.

	int i;
	int p, s,n;
	float last;
	int b[100], c[100], d[100], e[100], t[100];
	{
		float a[n];
		for (i = 0; i < n; i ++) 
		{
			t[i] = b[i];
			// a[i] = f(t[i]);
			a[i] = t[i];
		}
		last = a[i];
	}
}



int foo7()
{
	// A loop with a (<, >) dependence.
	// This dependece precludes parallelization on both loops
	// and prevents interchanging the i1 and i2 loops for better cache locality.
	// Increasing the sink iteration of the i2 loop by two iterations would give a direction of (<, =), 
	// and by more than two iterations would give a “<" direction. 

	int i1, i2, n;
	int a[100][100];
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = 1; i2 < n; i2 ++) {
			a[i1][i2] = a[i1-1][i2 + 2];
		}
	}
}

int foo7_skewed_general(int k)
{
	// The loop skewed by k times the distance of dependences on the i1 loop.

	int i1, i2, n;
	int a[100][100];
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = k * i1 + 1; i2 < k * i1 + n; i2 ++) {
			a[i1][i2 - k*i1] = a[i1-1][i2 - k*i1 + 2];
		}
	}
}

int foo7_skewed_1()
{
	// The loop skewed by 1 times the distance of dependences on the i1 loop.
	// Still with a (<, >) dependence.

	int i1, i2, n;
	int a[100][100];
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = i1 + 1; i2 < i1 + n; i2 ++) {
			a[i1][i2 - i1] = a[i1-1][i2 - i1 + 2];
		}
	}
}

int foo7_skewed_2()
{
	// The loop skewed by 2 times the distance of dependences on the i1 loop.
	// With a (<, =) dependence.


	int i1, i2, n;
	int a[100][100];
	int k = 2;
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = k * i1 + 1; i2 < k * i1 + n; i2 ++) {
			a[i1][i2 - k*i1] = a[i1-1][i2 - k*i1 + 2];
		}
	}
}

int foo7_skewed_3()
{
	// The loop skewed by 3 times the distance of dependences on the i1 loop.
	// With a < dependence.

	int i1, i2, n;
	int a[100][100];
	int k = 3;
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = k * i1 + 1; i2 < k * i1 + n; i2 ++) {
			a[i1][i2 - k*i1] = a[i1-1][i2 - k*i1 + 2];
		}
	}
}


int foo8()
{
	// k is a candidate for induction variable substitution

	int i;
	int p, s,n, start;
	int a[100], b[100];

	int k = start;
	for (i = 0; i < n; i ++) 
	{
		a[k] = b[i];
		k++;
	}
}

int foo8_induction_var_subs()
{
	// The loop of after induction variable substitution.

	int i;
	int p, s,n, start;
	int a[100], b[100];

	int k = start;
	for (i = 0; i < n; i ++) 
	{
		a[k + i] = b[i];
	}

}


int foo9()
{
	// k is a candidate for induction variable substitution within a triangular loop nest.
	// In a triangular loop, the number of iterations the inner loop executes
	// depends on the value of the outer loop's loop variable.

	int i1, i2, n, start1;
	int a[100], b[100];

	int k = start1;
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = 1; i2 < i1; i2 ++) {
			k++;
			a[k] = b[i1];
		}
	}
}

int foo9_induction_var_subs()
{
	//  The loop after induction variable substitution.

	int i1, i2, n, start1;
	int a[100], b[100];

	int k = start1;
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = 1; i2 < i1; i2 ++) {
			a[i1 * n + i2 + k + 1] = b[i1];
		}
		if (i1>1)
		{
			k+=i1;
		}
	}
}


int foo10()
{

	// A loop with coupled induction variables 
	// in a triangular loop.

	int i1, i2, n, start1, start2;
	int a[100], b[100];

	int k1 = start1, k2 = start2;
	for (i1 = 1; i1 < n; i1 ++) { 
		for (i2 = 1; i2 < i1; i2 ++) {
			k1 ++;
			a[k2] = b[i1] + k1;
		}
		k2 = k2 + k1;
	}
}

int foo11()
{
	// A program that uses a temporary tmp to store a reused expression value.

	int i;
	int p, s,n, start;
	int a[100], b[100], c[100];

	int t1 = s;
	for (i = 0; i < n; i ++) 
	{
		int tmp = i*s;
		c[i] = a[i + tmp - 1];
		a[i + t1] = a [i + tmp - 1];
	}
}


int foo11_forward_substitution()
{
	// The program after forward substitution.
	
	int i;
	int p, s,n, start;
	int a[100], b[100], c[100];

	int t1 = s;
	for (i = 0; i < n; i ++) 
	{
		c[i] = a[i + i*s - 1];
		a[i + t1] = a [i + i*s - 1];
	}
}

int foo12()
{

	// A temporary t amenable to 
	// scalar expansion.

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];

	int t;

	for (i = 0; i < n; i ++) 
	{
		t = i * 2;
		a[i + t] = a[i + t - 1];
	}	
}

int foo12_scalar_expansion()
{

	// The loop after performing
	// scalar expansion.

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];


	int t[n];
	for (i = 0; i < n; i ++) 
	{
		t[i] = i * 2;
		a[i + t[i]] = a[i + t[i] - 1];
	}	
}

int foo12_privatization()
{

	// The loop after performing privatization.

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];


	for (i = 0; i < n; i ++) 
	{
		int t = i * 2;
		a[i + t] = a[i + t - 1];
	}	
}

int foo13()
{

	// A program that can benefit from node splitting because of dependences cycles.
	// There is a lexically forward flow dependence from S1 (a[i]) to S2 (a[i-1]), 
	// and a lexically backward anti-dependence from S2 (a[i+1]) to S1 (a[i]),
	// leading to a cycle.

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];

	int t;

	for (i = 0; i < n; i ++) {
		a[i] = t ;
		b[i] = a[i + 1] + a[i - 1];
	}

}

int foo13_node_splitting()
{

	// The program after splitting the
	// nodes and capturing the value of a[i+1].

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];
	int tmp[100];
	int t;

	for (i = 0; i < n; i ++){ 
		a[i] =  t;
		tmp[i] = a[i + 1];
		b[i] = tmp[i] + a[i - 1];
	}
}


int foo13_node_splitting_2()
{

	// Moving the read of a[i+1] to break the cycle

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];
	int tmp[100];
	int t;

	for (i = 0; i < n; i ++){ 
		tmp[i] = a[i + 1];
		a[i] =  t;
		b[i] = tmp[i] + a[i - 1];
	}
}

int foo131()
{
	// A loop containing a reduction.

	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];
	int tmp[100];
	int t;

	for (i = 0; i < n; i ++){ 
		a[i] = b[i];
		s = s + a[i];
	}
}

int foo14()
{
	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];
	int tmp[100];
	int t;

	for (i = 0; i < n; i ++){ 
		a[i] = a[i - 1] + a[i] + a[i + 1];
	}
}

int foo14_unrolling()
{
	int i;
	int p, s, n, e, start;
	int a[100], b[100], c[100];
	int tmp[100];
	int t;

	for (i = 0; i < n; i +=4){ 
		a[i]=a[i-1]+a[i]+a[i+1];
		a[i+1]=a[i]+a[i+1]+a[i+2];
		a[i+2]=a[i+1]+a[i+2]+a[i+3];
		a[i+3] = a[i+2]+a[i+3]+a[i+4];
	}

	int f = n%4;
	for (i = n - f ; i < n; i ++) 
	{
		a[i] = a[i - 1] + a[i] + a[i + 1];
	}
}


// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// From 
// https://www.dropbox.com/s/0lxv2fd948fdjoo/Dependence%20Testing%20%28lee%29.pdf?dl=0
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************


int bar1()
{

	// Iterations of the j loop must be executed sequentially. 
	// But iterations of the i loop can be executed in parallel. 

	int i, j;
	int n, m;
	int p[100], s[100], a[100], b[100][100];

	for (i = 0; i < n; i ++) {
		for (j = 1; j < m; i ++) {
			b[i][j] = p[i];
			a[i] = b[i][j-1]; 
		}
	}
}

int bar2()
{

	// The dependence flows within the same iteration 
	// The dependence is loop-independent

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		a[i] = b[i] + c[i];
		d[i] = a[i]; 
	}
}

int bar3()
{

	// There is a flow dependence,
	// The dependence flows between instances of the statements in different iterations
	// this is a loop-carried dependence

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		a[i] = b[i] + c[i];
		d[i] = a[i - 1]; 
	}
}

int bar4()
{

	// There is an anti dependence
	// The dependence flows between instances of the statements in different iterations
	// This is a loop-carried dependence

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		a[i] = b[i] + c[i];
		d[i] = a[i + 1]; 
	}
}

int bar5()
{

	// There is a flow dependence
	// This is a loop-carried dependence
	// Dependence distance is (1,-1) 

	int i, j;
	int n, m;
	int p[100], s[100], a[100][100], b[100][100];

	for (i = 1; i < n; i ++) {
		for (j = 1; j < n; i ++) {
			a[i][j] = a[i-1][j+1];
		}
	}
}

int bar6()
{

	// 2*i is even, whereas 2*i+1 is odd
	// There is no dependence


	int i, j;
	int n, m;
	int c[50], d[50], e[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		a[2*i] = b[i] + c[i];
		d[i] = a[2*i - 1]; 
		e[i] = a[2*i + 1]; 
	}
}

int bar7()
{

	// No dependence

	int i, j;
	int n, m;
	int p[100], s[100], a[100][100], b[100][100];

	for (i = 1; i < n; i ++) {
		for (j = 1; j < n; i ++) {
			a[i][2*j] = a[i-1][2*j+1];
		}
	}
}

int bar8()
{

	//there is no dependece due to loop bounds

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50];

	for (i = 10; i < 20; i ++) {
		a[i] = b[i] + c[i];
		// d[i] = a[i - 10];
		// Will introduce dependeces
		d[i] = a[i - 9]; 
	}
}

int bar9()
{

	// unknown loop bounds lead to false dependences

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50];

	// With n = 5 no dependece, with n = 20 there is dependece
	for (i = 0; i < n; i ++) {
		a[i] = a[i + 10];
	}
}

int bar10()
{

	// Triangular loops
	// generally requires addition of new constraints

	int i, j;
	int n, m;
	int p[100], s[100], a[100][100], b[100][100];

	for (i = 0; i < n; i ++) {
		for (j = 0; j < i - 1; i ++) {
			a[i][j] = a[j][i];
		}
	}
}

int bar11()
{

	// Dependences can be avoided by transformation

	int i, j;
	int n, m, x;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		x = a[i];
		b[i] = x;
	}
}

int bar11_trans()
{

	// Dependences can be avoided by transformation

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50], x[50];

	for (i = 0; i < n; i ++) {
		x[i] = a[i];
		b[i] = x[i];
	}
}

int bar12()
{

	// Dependences can be avoided by transformation

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50], x[50];

	j = n-1;
	for (i = 0; i < n; i ++) {
		a[i] = a[j];
		j = j-1;
	}
}

int bar12_trans()
{

	// Dependences can be avoided by transformation
	// there are no loop-carried dependences within the first half of the iteration space, 
	// and there are none within the second half of the iteration space, 
	// but there are loop-carried dependences from the first half to the second half of the iteration space. 
	// By using loop splitting to divide the loop into two loops, 
	// one running from 0 . . . n/2 − 1 and the other from n/2 . . . n − 1, 
	// two loops are formed that have no dependences and can therefore be made fully parallel.

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50], x[50];

	for (i = 0; i < n; i ++) {
		a[i] = a[n-i];
	}
}

int bar12_trans2()
{

	// Dependences can be avoided by transformation
	// there are no loop-carried dependences within the first half of the iteration space, 
	// and there are none within the second half of the iteration space, 
	// but there are loop-carried dependences from the first half to the second half of the iteration space. 
	// By using loop splitting to divide the loop into two loops, 
	// one running from 0 . . . n/2 − 1 and the other from n/2 . . . n − 1, 
	// two loops are formed that have no dependences and can therefore be made fully parallel.

	int i, j;
	int n, m;
	int c[50], d[50], a[50], b[50], x[50];

	for (i = 0; i < n/2; i ++) {
		a[i] = a[n-i];
	}

	for (i = n/2; i < n; i ++) {
		a[i] = a[n-i];
	}
}


int bar13()
{

	// The iterations of the j loop are sequential, 
	// but the i loop iterations are independent and 
	// can be executed in parallel

	int i, j;
	int n, m;
	int c[100], d[100], a[100][100], b[100][100];

	for (i = 1; i < n-1; i ++) {
		for (j = 1; j < m-1; i ++) {
			a[i][j] = b[i][j];
			c[j] = a[i][j-1];
		}
	}
}

int bar14()
{

	// The iterations of the j loop are parallel, 
	// but the i loop iterations must be executed sequentially

	int i, j;
	int n, m;
	int c[100], d[100], a[100][100], b[100][100];

	for (i = 1; i < n-1; i ++) {
		for (j = 1; j < m-1; i ++) {
			a[i][j] = b[i][j];
			c[j] = a[i-1][j];
		}
	}
}

int bar15()
{

	// The iterations of the j loop are parallel, 
	// but the i loop iterations must be executed sequentially

	int i, j;
	int n, m;
	int c[100], d[100], a[100][100], b[100][100];

	for (i = 1; i < n-1; i ++) {
		for (j = 1; j < m-1; i ++) {
			a[i][j] = b[i][j];
			c[j] = a[i-1][j-1];
		}
	}
}

// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// Other sources
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

int read(int x)
{
	return x;
}

int write(int *x)
{
	*x = *x + 1;
	return *x;
}

int noparam()
{
	return 0;
}


int baz1()
{
	// The dependence flows within the same iteration 
	// The dependence is loop-independent

	int i, j;
	int n, m;
	int scalar;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		a[i] = b[i] + c[i];
		d[i] = a[i] + scalar; 
	}
}

int baz2()
{
	// Add a function call that read

	int i, j;
	int n, m;
	int scalar;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		a[i] = b[i] + c[i];
		d[i] = a[i] + read(scalar); 
	}
}

int baz3()
{
	// Add a function call that write

	int i, j;
	int n, m;
	int scalar;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		write(&scalar);
		a[i] = b[i] + c[i];
		d[i] = a[i] + scalar;
	}
}

int baz4()
{
	// Add a call to a non-parametric function

	int i, j;
	int n, m;
	int scalar;
	int c[50], d[50], a[50], b[50];

	for (i = 0; i < n; i ++) {
		noparam();
		a[i] = b[i] + c[i];
		d[i] = a[i] + scalar;
	}
}

int baz5()
{
	// An example provided by CETUS

	int i;
	double t, s, a[100];
	for ( i=0; i<50; ++i )
	{
		t = a[i];
		a[i+50] = t +
		(a[i]+a[i+50])/2.0;
		s = s + 2*a[i];
	}
	return 0;
}


int G;

int baz6(){

	// An example provided by CETUS
	// Very Simple Parallelizable Loop Example

	G = 5;
	float a[10000], b[10000];
	int i;
	int f;

	f = 0;
	for (i=1; i<10000; i++) {
		int p;

		p=4;
		if(b[i]> f)
			p += b[i];
		a[i]= b[i] + p;
		b[i + 1]= a[i];
		f+= G ;
	}
	
   return 0;
}

