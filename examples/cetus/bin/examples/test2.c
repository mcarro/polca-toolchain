// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// Other functions: support, etc...
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

int main()
{
	return 0;
}


// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// From lecture 10 (http://www.cs.colostate.edu/~cs553/ClassNotes/lecture10-loop-dependencies.ppt.pdf)
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

// Abstraction
// < _ 1..n
// > _ -1..-n
// = _ 0

int foo()
{
	// No loop-carried dependences, so we can arbitrarily change order of
	// iteration execution
	int i ,j;
	int a[6][5];

	for(j = 0; j < 6; j++)
		for(i = 0; i < 5; i++)
			a[j][i] = a[j][i] + 1;
}

int foo2()
{
	// Loop-independent dependences
	// Dependences within the same loop iteration
	int i ,j;
	int a[100], b[100], c[100];

	for(i = 0; i < 100; i++)
	{
		a[i] = b[i] + 1;
		c[i] = a[i] * 2;
	}
}

int foo3()
{
	// Loop-carried dependences
	// Dependences that cross loop iterations
	int i ,j;
	int a[100], b[100], c[100];

	for(i = 0; i < 100; i++)
	{
		a[i] = b[i] + 1;
		c[i] = a[i - 1] * 2;
	}
}


int foo4()
{
	// a[(i+1)][(j-1)] -> a[i][j] is anti-dependece
	// when permute should be flow
	int i ,j, n = 100;
	int a[100][100];

	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			a[i][j] = a[i+1][j-1];
}

int foo5()
{
	// previous example permuted
	int i ,j, n = 100;
	int a[100][100];

	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			a[i][j] = a[i+1][j-1];
}

int foo6()
{
	// DistanceVector:(1,2)
	// We can permute the i and j loops
	int i ,j, n = 100;
	int a[100][100];

	
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			a[i][j] = a[i-1][j-2] + 1;
}

int foo7()
{
	// Kind of dependence: Flow  
	// Distance vector: (1, -1)
	// Permutation is not legal
	int i ,j, n = 100;
	int a[100][100];

	
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
		{
			a[i][j] = a[i-1][j+1] + 1;
		}
	}
}

int foo75()
{
	// Kind of dependence: Flow  
	// Distance vector: (0, 1)
	// Permutation is legal
	int i ,j, n = 100;
	int a[100][100];

	
	for(i = 0; i < n; i++)
	{
		for(j = 0; j < n; j++)
		{
			a[i][j] = a[i][j-1] + 1;
		}
	}
}

int foo8()
{
	// Distance vectors (0,1) for accesses to A
	// Distance vectors (1,0) for accesses to B  
	// The j loop carries dependence due to A
	// The i loop carries dependence due to B
	int i ,j, n = 100;
	int a[100][100], b[100][100];

	
	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
		{
			a[i][j] = b[i-1][j] + 1;
			b[i][j] = a[i][j-1]*2;
		}
}

int foo9()
{
	// Direction vector: (<,<)

	int i ,j, n = 100;
	int a[100][100], b[100][100];

	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)
			a[i][j] = a[i-1][j-1] + 1;
}

// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// From lecture 11 (http://www.cs.colostate.edu/~cs553/ClassNotes/lecture11-automating-intro.ppt.pdf)
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

int foo10()
{
	// pre-skewing

	int i ,j, n = 100;
	int a[100][100], b[100][100];

	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)	
			a[i][j] = a[i-1][j+1] + 1;
}

int foo11()
{
	// skewed

	int i ,j, n = 100;
	int a[100][100], b[100][100];

	for(i = 0; i < n; i++)
		for(j = i; j < n+i; j++)	
			a[i][j-i] = a[i-1][j-i+1] + 1;
}


// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// From lecture 14 (http://www.cs.colostate.edu/~cs553/ClassNotes/lecture14-direction-deps-frameworks-KnP.ppt.pdf)
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

int foo12()
{
	// pre-fusing

	int i ,j, n = 100, m = 100;
	int a[100], b[100], c[100];

	for(i = 0; i < n; i++)
		a[i] = a[i-1] + 1;

	for(i = 0; i < m; i+=1)
		c[i] = a[i]/2;
}

int foo13()
{
	// fused

	int i ,j, n = 100, m = 100;
	int a[100], b[100], c[100];

	for(i = 0; i < n; i++)
	{
		a[i] = a[i-1] + 1;
		c[i] = a[i]/2;
	}
}


// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************
// From http://symbolaris.com/course/Compilers11/24-cacheloop.pdf
// ***********************************************
// ***********************************************
// ***********************************************
// ***********************************************

int foo14()
{
	// pre-reversed

	int i ,j, n = 100;
	int a[100][100], b[100][100];

	for(i = 0; i < n; i++)
		for(j = 0; j < n; j++)	
			a[i][j] = a[i-1][j+1] + 1;
}

int foo15()
{
	// reversed

	int i ,j, n = 100;
	int a[100][100], b[100][100];

	for(i = 0; i < n; i++)
		for(j = n-1; j >= 0; j--)	
			a[i][j] = a[i-1][j+1] + 1;
}

int foo16()
{
	// BADLY reversed

	int i ,j, n = 100;
	int a[100][100], b[100][100];

	for(i = n - 1; i >= 0; i--)
		for(j = 0; j < n; j++)		
			a[i][j] = a[i-1][j+1] + 1;
}
