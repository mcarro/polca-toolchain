#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  int m,n,l,l1,l2,j,i,i1,c,i2,offset;

  m = 3;

  n = 1;
  for (i=0;i<m;i++) 
    n *= 2;

   for (c=m-1;c>=0;c--) {
     l1 = pow(2,c);
     l2 = l1 * 2;
      for (j=0;j<pow(2,c);j++) {
	for (i=j;i<n;i+=l2) {
	  i1 = i + l1;
	    printf("%d: %d , %d (%d,%d)\n",c,i,i1,l1,l2);
         }
      }
      printf("-------------\n");
   }

   printf("--------------------------------\n");
   printf("--------------------------------\n");

   for (c=m-1;c>=0;c--) {
     l1 = pow(2,c);
     l2 = pow(2,m-c-1);
      for (i=0;i<pow(2,m-c-1);i++){
	offset = i * 2 * l1;
        for (j=0;j<pow(2,c);j++) {
	  i1 = j + offset;
	  i2 = i1 + l1;
	  printf("%d: %d , %d (%d,%d, %d)\n",c,i1,i2,l2,l1,2 * l1);
         }
	
      }
      printf("-------------\n");
   }

   printf("--------------------------------\n");
   printf("--------------------------------\n");

   for (c=0;c<m;c++) {
     l1 = pow(2,c);
     l2 = pow(2,m-c-1);
      for (j=0;j<pow(2,c);j++) {
	offset = j * 2 * l2;
        for (i=0;i<pow(2,m-c-1);i++){
	  i1 = i + offset;
	  i2 = i1 + l2;
	  printf("%d: %d , %d (%d,%d, %d)\n",c,i1,i2,l1,l2,2*l2);
         }
	
      }
      printf("-------------\n");
   }

}
