#include <math.h>
#include <complex.h>
#include <stdio.h>

typedef double complex cplx;

void show(const char * s, cplx buf[], int n) {
  int i;
	printf("%s", s);
	for (i = 0; i < n; i++)
		if (!cimag(buf[i]))
			printf("%g ", creal(buf[i]));
		else
			printf("(%g, %g) ", creal(buf[i]), cimag(buf[i]));

	printf("\n");
}

void pease_fft(cplx *x, cplx *xt, int m)
{
  int n = pow(2,m);
  int i,c,r,j;
  float w;

  cplx y[n];// = (float*) malloc(n * sizeof(float));

  //xt = bitreversal(x);
  for(i=0;i<n;i++)
    xt[i] = x[i];

  //for(c=t;c>0;c--)
  for(c=0;c<m;c++)
  {
    //w = exp(-2*M_PI*sqrt(-1)/pow(2,t-c+1));
    //printf("%f <- [%f, %f, %d]\n",w,sqrt(-1),pow(2,t-c+1),t-c+1);
    printf("Stage: %d\n",c);
    for(r=0;r < pow(2,m-1);r++)
    {
      cplx w = cexp( (-I * 2 * M_PI * floor(r/pow(2,c+1)) ) / pow(2,m-c+1));
      //printf("(%g, %g)\n", creal(w), cimag(w));
      //float r1 = floor(r/pow(2,c-1) );
      //xt[r*2+1] = w * xt[2*r+1];
      y[r] = xt[2*r] + w * xt[2*r+1];
      y[r+n/2] = xt[2*r] - w * xt[2*r+1];
      //printf("%d <- %d + w * %d\n",r,2*r,2*r+1);
      //printf("%d <- %d + w * %d\n",r+n/2,2*r,2*r+1);
      printf("%d: %d , %d\n",c,2*r,2*r+1);
    }
    printf("\n");

    for(j=0;j<n;j++)
      xt[j] = y[j];

  }
  //for(int j=0;j<n<j++)
  //  y[j] = xt[t],
  
}

int main(int argc,char *argv[])
{

  int t=3;
  int n=pow(2,t);
  int j;

  //float *x = (float*) malloc(n * sizeof(float));
  //float *xt = (float*) malloc(n * sizeof(float));

  cplx x[n];// = {1, 2, 3, 4};
  cplx xt[n];

  for(j=0;j<n;j++)
    x[j] = j+1;

  //show("Data: ", x,n);

  pease_fft(x,xt,t);


  //show("FFT: ", xt,n);

}
