
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// MPI NOTE
// This code assumes the following MPI scheme for computation/data distribution:
// * Master rank (Rank==0):
//   + Performs I/O load/store of data
//   + Allocates data using global dimensions
//   + Performs computations using local dimensions of data
// * Rest of ranks
//   + Allocate data using local dimensions
//   + Perform computations using local dimensions of data


// Things to identify/annotate for MPI transformation:
//
// * Data and dimensions to be split
//   + Rank==0: has to keep track of global and local dimensions of data
//     since it computes over local dimensions but loads/stores using
//     global dimensions. This rank allocates data with global dimensions
//   + Rank!=0: just need local dimensions of data. Allocate data with local
//     dimensions
//   + In case of multiple dimensions need to decide over which dimensions to
//     peform the splitting
//     - Will have to inject code for transfers and create MPI tags
//     - Will have to change iteration spaces of loops to account for chunks
//
// * Blocks of code performing I/O to load/store data. Once identified, there
//   will be a common pattern to transform:
//
//     I/O(global_dimensions)
//
//   Is transformaed to:
//
//     if(Rank==0)
//       recv_from_ranks(global_dimensions)
//       I/O(global_dimensions)
//     else
//       send_to_master(local_dimensions)
///////////


// MPI TRANS
// These variables are defined as global as they can be used from
// several parts of the program. Thus, passing them as arguments makes 
// the code transformation more difficult
// generic to *any* MPI code
#include <mpi.h>

int numprocs;
int rank;
///////////


// MPI TRANS
// These variables are defined as global as they can be used from
// several parts of the program. Thus, passing them as arguments makes 
// the code transformation more difficult
// specific to this MPI code
int standard_chunk_width;
int standard_chunk_height;
int standard_chunk_size;
///////////

// MPI TRANS
// MPI tags required by MPI_Recv and MPI_Send to differenciate the data
// being sent/received
// specific to this MPI code
#define TAG_CHUNK     0
#define TAG_WIDTH     1
#define TAG_HEIGHT    2
///////////

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = r;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = g;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = b;
  	}
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  /* printf("Rank %d: Applying red color filter %d (%d-%d)...\n",rank,standard_chunk_height,rank*standard_chunk_height,height); */

  // MPI TRANS
  // Need to know over which dimension of data is iterating in order to
  // re-define the iteration space properly.
  // With annotations??
  // It is assumed that the partitioning of data has been done, and this function
  // receives parameters with local dimensions, so MPI ranks access local data with
  // local dimensions
  for (int i = 0; i < height; i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
    }

  printf("Rank %d: Applying green color filter...\n",rank);

  for (int i = 0; i < height; i++)
    {
      kernelGreenFilter(image,i,rawWidth,greenImage);
    }

  printf("Rank %d: Applying blue color filter...\n",rank);

  for (int i = 0; i < height; i++)
    {
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }
  ///////////

}

// MPI TRANS
// generic functions to *any* MPI code
void mpi_init(int argc, char *argv[])
{
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    printf("Starting rank %d\n",rank);
}

void mpi_finalize()
{
    MPI_Finalize();
}
////////////

// MPI TRANS
// specific functions to this MPI code

// This function send chunks from Master to rest of ranks
void send_chunks(int width, int height, char *pImage)
{
  int imageWC, imageHC;
  int sizeC;
  imageWC = width;
  imageHC = height/numprocs;

  for(int procId=1;procId<numprocs;procId++)
  {
    if(procId == numprocs-1) 
      imageHC += height%numprocs;

    // MPI Note
    // this way of computing size should come from annotations
    sizeC = imageWC * imageHC * 3;
    ////////////

    MPI_Send(&imageWC, 1, MPI_INT, procId, TAG_WIDTH, MPI_COMM_WORLD);
    MPI_Send(&imageHC, 1, MPI_INT, procId, TAG_HEIGHT, MPI_COMM_WORLD);
    MPI_Send(pImage+(procId*standard_chunk_size), sizeC, MPI_CHAR, procId, TAG_CHUNK, MPI_COMM_WORLD);
  }
}

// This functions receive chunks from rest of ranks to Master
void receive_chunks(int width, int height, char **pImage)
{
  int imageWC, imageHC;
  int sizeC;
  imageWC = width;
  imageHC = height/numprocs;

  for(int procId=1;procId<numprocs;procId++)
  {
    if(procId == numprocs-1) 
      imageHC += height%numprocs;

    // MPI Note
    // this way of computing size should come from annotations
    sizeC = imageWC * imageHC * 3;
    ////////////

    MPI_Recv((*pImage)+(procId*standard_chunk_size), sizeC, MPI_CHAR, procId, TAG_CHUNK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }
}

// This function sends a single chunk from rank to Master
void send_chunk(int width, int height, char *pImage)
{
  // MPI Note
  // this way of computing size should come from annotations
  int sizeC = width * height * 3;
  ////////////

  MPI_Send(pImage+(rank*standard_chunk_size), sizeC, MPI_CHAR, 0, TAG_CHUNK, MPI_COMM_WORLD);
}

// This function receives a single chunk from Master to rank
void receive_chunk(int *width, int *height, char **pImage)
{
  MPI_Recv(width, 1, MPI_INT, 0, TAG_WIDTH,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  MPI_Recv(height, 1, MPI_INT, 0, TAG_HEIGHT,MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  // MPI Note
  // this way of computing size should come from annotations
  int size = (*width) * (*height) * 3;
  *pImage = (char *) malloc( sizeof(char) * size);
  ///////////

  MPI_Recv(pImage[0], size, MPI_CHAR, 0, TAG_CHUNK,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}


void computeLocalSizes(int global_width,int global_height,int *width,int *height) {

  // MPI Note
  // compute dimensions (width,height) for all the chunks but the last
  // that's why variables are called "standard_chunk_*"
  standard_chunk_width = global_width;
  standard_chunk_height = global_height/numprocs;
  //////////

  // MPI Note
  // save local dimensions for rank 0
  *width =  standard_chunk_width;
  *height = standard_chunk_height;  
  //////////

  // MPI Note
  // this way of computing size should come from annotations
  standard_chunk_size = (*width) * (*height) * 3;
  //////////
  printf("MPI partitioning parameters: %d (w) %d (h) %d (s)\n",standard_chunk_width,standard_chunk_height,standard_chunk_size);
}

////////////

int main(int argc, char *argv[]) {

  // MPI TRANS
  // generic to *any* MPI code
  mpi_init(argc, argv);
  ////////////

  const char* filename = argc > 1 ? argv[1] : "../../../input/test.ppm";

  // MPI NOTE
  // these variables can be re-used for for each MPI rank local data
  // as they come from the input source code
  char* inputImage;
  int width,height;
  ////////////

  // MPI TRANS
  // at least the rank=0 needs to distinguish between global and local sizes
  // of data. They will be used for initial load and distribuition of data.
  // Once finished computation, global sizes are used to collect and dump 
  // result data
  int global_width,global_height;
  ////////////

  // MPI TRANS
  // If I/O for loading data is identified through annotations
  // the load and data initialization block can be also generic
  // to *any* MPI code
  if(rank==0){
    LoadPPMImageArray(&inputImage,&global_width,&global_height,filename);

    computeLocalSizes(global_width,global_height,&width,&height);

    send_chunks(global_width, global_height, inputImage);
  }else{
    receive_chunk(&width, &height, &inputImage);
  }
  ////////////

  // MPI TRANS
  // If block for allocating data is identified through annotations
  // the transformation could be performed. However, it might not be 
  // trivial to do it for *any* input code. Maybe we should consider 
  // requiring from the input program some code format for allocating
  // data? or maybe obtain information from annotations?
  size_t typeSize = sizeof(char);
  int imageSize = width * height * 3;  
  if(rank==0)
    imageSize = global_width * global_height * 3;  

  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);
  ////////////

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);

  // MPI TRANS
  // If I/O for storing data is identified through annotations
  // the store of data block of code can be also generic
  // to *any* MPI code. However, it might not be 
  // trivial to do it for *any* input code. Maybe we should consider 
  // requiring from the input program some code format for allocating
  // data? or maybe obtain information from annotations?
  if(rank==0){
    receive_chunks(global_width, global_height, &redImage);
    receive_chunks(global_width, global_height, &greenImage);
    receive_chunks(global_width, global_height, &blueImage);

    // MPI NOTE
    // change in I/O function to save image:
    // - width to global_width
    // - height to global_height
    SavePPMImageArray(redImage,global_width,global_height,"./test_red.ppm");
    SavePPMImageArray(greenImage,global_width,global_height,"./test_green.ppm");
    SavePPMImageArray(blueImage,global_width,global_height,"./test_blue.ppm");
    ////////////


  }else{
    send_chunk(width, height, redImage);
    send_chunk(width, height, greenImage);
    send_chunk(width, height, blueImage);
  }
  ////////////


  // MPI TRANS
  // generic to *any* MPI code
  mpi_finalize();
  ////////////

  return 0;
}

