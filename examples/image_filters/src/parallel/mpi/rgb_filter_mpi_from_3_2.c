
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// MPI TRANS
// These variables are defined as global as they can be used from
// several parts of the program. Thus, passing them as arguments makes 
// the code transformation more difficult
// generic to *any* MPI code
#include <mpi.h>

int num_proc;
int rank;
///////////

// MPI TRANS
// MPI tags required by MPI_Recv and MPI_Send to differenciate the data
// being sent/received
// specific to this MPI code
#define TAG_CHUNK     0
#define TAG_WIDTH     1
#define TAG_HEIGHT    2
///////////

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = r;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = g;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = b;
  	}
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Applying red color filter...\n");
  printf("Applying green color filter...\n");
  printf("Applying blue color filter...\n");

  // just to check
  //int num_proc = 4;

  int prev_chunk_size = height / num_proc;
  int curr_chunk_size = rank != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;
  if(rank==0){
    int procId;
    for (procId = 1; procId < num_proc; procId++)
    {
      int prev_chunk_size = height / num_proc;
      int curr_chunk_size = procId != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

      MPI_Send(image+(procId*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, procId, TAG_CHUNK, MPI_COMM_WORLD);
    }
  }else{
    MPI_Recv(image+(rank*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, 0, TAG_CHUNK,MPI_COMM_WORLD, MPI_STATUS_IGNORE);    
  }

  // peform RGB filter computation
  for(int i=rank*prev_chunk_size;i<((rank+1)*curr_chunk_size);i++)
  {
    kernelRedFilter(image,i,rawWidth,redImage);
    kernelGreenFilter(image,i,rawWidth,greenImage);
    kernelBlueFilter(image,i,rawWidth,blueImage);
  }

  // Start a send/recv round for redImage
  if(rank==0){
    int procId;
    for (procId= 1; procId < num_proc; procId++)
    {
      int prev_chunk_size = height / num_proc;
      int curr_chunk_size = procId != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

      MPI_Recv((*redImage)+(procId*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, procId, TAG_CHUNK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
  }else{
    MPI_Send((*redImage)+(rank*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, 0, TAG_CHUNK, MPI_COMM_WORLD);
  }

  // Start a send/recv round for greenImage
  if(rank==0){
    int procId;
    for (procId= 1; procId < num_proc; procId++)
    {
      int prev_chunk_size = height / num_proc;
      int curr_chunk_size = procId != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

      MPI_Recv((*greenImage)+(procId*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, procId, TAG_CHUNK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
  }else{
    MPI_Send((*greenImage)+(rank*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, 0, TAG_CHUNK, MPI_COMM_WORLD);
  }

  // Start a send/recv round for blueImage
  if(rank==0){
    int procId;
    for (procId= 1; procId < num_proc; procId++)
    {
      int prev_chunk_size = height / num_proc;
      int curr_chunk_size = procId != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

      MPI_Recv((*blueImage)+(procId*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, procId, TAG_CHUNK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
  }else{
    MPI_Send((*blueImage)+(rank*prev_chunk_size*width*3), curr_chunk_size*width*3, MPI_CHAR, 0, TAG_CHUNK, MPI_COMM_WORLD);
  }

}

// MPI TRANS
// generic functions to *any* MPI code
void mpi_init(int argc, char *argv[])
{
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&num_proc);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    printf("Starting rank %d\n",rank);
}

void mpi_finalize()
{
    MPI_Finalize();
}
////////////

int main(int argc, char *argv[]){

  // MPI TRANS
  // generic to *any* MPI code
  mpi_init(argc, argv);
  ////////////

  const char* filename = argc > 1 ? argv[1] : "../../../input/test.ppm";
  char* inputImage;
  int width,height;

  // MPI TRANS
  // If I/O for loading data is identified through annotations
  // the load and data initialization block can be also generic
  // to *any* MPI code
  if(rank==0){
    LoadPPMImageArray(&inputImage,&width,&height,filename);
    int procId;
    for(procId=1;procId<num_proc;procId++)
    {
      MPI_Send(&width, 1, MPI_INT, procId, TAG_WIDTH, MPI_COMM_WORLD);
      MPI_Send(&height, 1, MPI_INT, procId, TAG_HEIGHT, MPI_COMM_WORLD);
    }
  }else{
    MPI_Recv(&width, 1, MPI_INT, 0, TAG_WIDTH,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&height, 1, MPI_INT, 0, TAG_HEIGHT,MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    // Think about how these three lines are generated...
    size_t typeSize = sizeof(char);
    int imageSize = width * height * 3;
    inputImage = malloc( typeSize * imageSize);
  }


  size_t typeSize = sizeof(char);
  int imageSize = width * height * 3;
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);

  // MPI TRANS
  // If I/O for storing data is identified through annotations
  // the store of data block of code can be also generic
  // to *any* MPI code. However, it might not be 
  // trivial to do it for *any* input code. Maybe we should consider 
  // requiring from the input program some code format for allocating
  // data? or maybe obtain information from annotations?
  if(rank==0){
    SavePPMImageArray(redImage,width,height,"./test_red.ppm");
    SavePPMImageArray(greenImage,width,height,"./test_green.ppm");
    SavePPMImageArray(blueImage,width,height,"./test_blue.ppm");
  }

  // MPI TRANS
  // generic to *any* MPI code
  mpi_finalize();
  ////////////

  return 0;
}
