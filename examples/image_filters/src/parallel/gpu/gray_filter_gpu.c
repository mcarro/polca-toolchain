////////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <OpenCL/opencl.h>

#include "ppmImage.h"

////////////////////////////////////////////////////////////////////////////////

// Use a static data size for simplicity
//
#define DATA_SIZE (1024)

////////////////////////////////////////////////////////////////////////////////

// Simple compute kernel which computes the square of an input array 
//
const char *KernelSource = "\n" \
"__kernel void square(                                                       \n" \
"   __global char* input,                                              \n" \
"   __global char* output,                                             \n" \
"   const unsigned int count)                                           \n" \
"{                                                                      \n" \
"   int i = get_global_id(0);                                           \n" \
"   if( (i*3) < count){                                                       \n" \
"  	output[i*3+0] = (char)(.299 * input[i*3+0] + .587 * input[i*3+1] + .114 * input[i*3+2]);      " \
"  	output[i*3+1] = (char)(.299 * input[i*3+0] + .587 * input[i*3+1] + .114 * input[i*3+2]);      " \
"  	output[i*3+2] = (char)(.299 * input[i*3+0] + .587 * input[i*3+1] + .114 * input[i*3+2]);      " \
"   }" \
"}                                                                      \n" \
"\n";

////////////////////////////////////////////////////////////////////////////////

char *GetPlatformName (cl_platform_id id)
{
	size_t size = 0;
	clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetPlatformInfo (id, CL_PLATFORM_NAME, size, name, NULL);

	return name;
}

char *GetDeviceName (cl_device_id id)
{
	size_t size = 0;
	clGetDeviceInfo (id, CL_DEVICE_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetDeviceInfo (id, CL_DEVICE_NAME, size,name, NULL);

	return name;
}

void CheckError (cl_int error)
{
	if (error != CL_SUCCESS) {
	  printf("OpenCL call failed with error %d\n",error);
	  exit (1);
	}
}

char *LoadKernel (const char* name, long *sourceSize)
{

char *source = NULL;
FILE *fp = fopen(name, "r");
if (fp != NULL) {
    /* Go to the end of the file. */
    if (fseek(fp, 0L, SEEK_END) == 0) {
        /* Get the size of the file. */
        long bufsize = ftell(fp);
        if (bufsize == -1) { /* Error */ }

        /* Allocate our buffer to that size. */
        source = malloc(sizeof(char) * (bufsize + 1));
	*sourceSize = (bufsize + 1);

        /* Go back to the start of the file. */
        if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

        /* Read the entire file into memory. */
        size_t newLen = fread(source, sizeof(char), bufsize, fp);
        if (newLen == 0) {
            fputs("Error reading file", stderr);
        } else {
            source[newLen] = '\0'; /* Just to be safe. */
        }
    }
    fclose(fp);
}
 return source;

}

cl_program CreateProgram (long sourceSize,char *source,
	cl_context context)
{
  //http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clCreateProgramWithSource.html
  size_t *lengths = NULL;
  if(sourceSize!=0) {
    lengths = (size_t*)malloc(sizeof(size_t) * 1);
    lengths[0] = sourceSize;
  }
  const char* sources [1] = { source };

  cl_int error = 0;
  cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &error);
  CheckError (error);

  return program;
}


void BuildProgram(cl_program program,cl_device_id device_id)
{
  cl_int err;
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    CheckError(err);
    printf("OpenCL program built successfully!!\n");

    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        exit(1);
    }

}

void selectOpenCLDevice(cl_uint *platformIdCount,cl_platform_id **platformIds,cl_uint *deviceIdCount,cl_device_id **deviceIds)
{
    clGetPlatformIDs (0, NULL, platformIdCount);

    if ((*platformIdCount) == 0) {
      printf("No OpenCL platform found\n");
      exit(1);
    } else {
      printf("Found %d platform(s)\n",(*platformIdCount));
    }

    *platformIds = (cl_platform_id*)malloc(sizeof(cl_platform_id)*(*platformIdCount));
    clGetPlatformIDs ((*platformIdCount), *platformIds, NULL);

    for (cl_uint i = 0; i < (*platformIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetPlatformName((*platformIds)[i]));
    }

    // Just query for GPU devices
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, 0, NULL,deviceIdCount);

    if ((*deviceIdCount) == 0) {
      printf("No OpenCL devices found\n");
      exit(1);
    } else {
      printf("Found %d GPU device(s)\n",(*deviceIdCount));
    }

    *deviceIds = (cl_device_id*)malloc(sizeof(cl_device_id)*(*deviceIdCount));
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, *deviceIdCount,
		    *deviceIds, NULL);

    for (cl_uint i = 0; i < (*deviceIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetDeviceName((*deviceIds)[i]));
    }
}

void createOpenCLContext(cl_platform_id *platformIds,cl_uint deviceIdCount,cl_device_id *deviceIds,cl_context *context)
{
    cl_int error = CL_SUCCESS; 
    const cl_context_properties contextProperties [] =
      {
	CL_CONTEXT_PLATFORM,
	(cl_context_properties)platformIds[0],
	0,
	0
      };

    *context = clCreateContext (contextProperties, deviceIdCount,
					  deviceIds, NULL, NULL, &error);
    CheckError (error);

    printf("Context created successfully!!\n");
}

void createOpenCLKernel(cl_device_id *deviceIds,cl_context context,cl_program *program,cl_kernel *kernel)
{
    cl_int error = CL_SUCCESS; 
    /* // Create a program from source */
    /* long sourceSize; */
    /* char *KernelSource = LoadKernel ("kernels/image.cl",&sourceSize); */
    *program = CreateProgram (0,(char*)KernelSource,
		context);
    printf("OpenCL program created successfully!!\n");

    BuildProgram(*program,deviceIds[0]);

    // Create the compute kernel in the program we wish to run
    //
    *kernel = clCreateKernel(*program, "square", &error);
    if (!(*kernel) || error != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }
    printf("OpenCL kernel created successfully!!\n");
}


void createOpenCLQueue(cl_device_id *deviceIds,cl_context context,cl_command_queue *queue)
{
    cl_int error = CL_SUCCESS; 

    *queue = clCreateCommandQueue (context, deviceIds[0],
    						   0, &error);
    CheckError (error);

    printf("OpenCL command queue created successfully!!\n");
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    unsigned int correct;               // number of correct results returned
    
    // Fill our data set with random float values
    //
    char *data;
    int width,height;
    char *results;

    // Data type size of Device and host data
    size_t typeSize = sizeof(char);

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    Image image;
    LoadPPMImage(&image,"test.ppm");
    data   = image.pixel;
    width  = image.width;
    height = image.height;
    /////////////////////////////////////////////////////////////

    int i = 0;
    // 3 values (RGB) per pixel
    unsigned int count = width * height * 3;//DATA_SIZE;
    /* for(i = 0; i < count; i++) */
    /*     data[i] = rand() / (float)RAND_MAX; */
    
    results = (char*) malloc( typeSize * count);


    //////////////////////////////////////////////////////////////
    //  Definition of variables common to *any* OpenCL program
    /////////////////////////////////////////////////////////////
    cl_uint platformIdCount = 0;
    cl_platform_id *platformIds = NULL;
    cl_uint deviceIdCount = 0;
    cl_device_id *deviceIds = NULL;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;                   // compute kernel

    size_t local;                       // local domain size for our calculation

    cl_int error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem input;                       // device memory used for the input array
    unsigned int sizeInput;             // size of input array

    cl_mem output;                      // device memory used for the output array
    unsigned int sizeOutput;             // size of input array

    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    createOpenCLKernel(deviceIds,context,&program,&kernel);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    sizeInput = count;
    input = clCreateBuffer(context,  CL_MEM_READ_ONLY, typeSize * sizeInput, NULL, NULL);
    sizeOutput = count;
    output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, typeSize * sizeOutput, NULL, NULL);
    if (!input || !output)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }
    
    // Write our data set into the input array in device memory
    //
    error = clEnqueueWriteBuffer(queue, input, CL_TRUE, 0, typeSize * sizeInput, data, 0, NULL, NULL);
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }

    // Set the arguments to our compute kernel
    //
    error = 0;
    error  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
    error |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
    error |= clSetKernelArg(kernel, 2, sizeof(unsigned int), &sizeInput);
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }

    // Get the maximum work group size for executing the kernel on the device
    //
    error = clGetKernelWorkGroupInfo(kernel, deviceIds[0], CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to retrieve kernel work group info! %d\n", error);
        exit(1);
    }
    printf("OpenCL local domain: %d\n",local);

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = count;
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error = clEnqueueReadBuffer( queue, output, CL_TRUE, 0, typeSize * count, results, 0, NULL, NULL );
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
    /////////////////////////////////////////////////////////////

    
    // Validate our results
    //
    /* correct = 0; */
    /* for(i = 0; i < count; i++) */
    /* { */
    /*     if(results[i] == data[i]) */
    /*         correct++; */
    /* } */
    
    /* // Print a brief summary detailing the results */
    /* // */
    /* printf("Computed '%d/%d' correct values!\n", correct, count); */

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    Image resultImage;
    resultImage.pixel = results;
    resultImage.width = width;
    resultImage.height = height;
    SavePPMImage(resultImage,"output.ppm");
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(input);
    clReleaseMemObject(output);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

