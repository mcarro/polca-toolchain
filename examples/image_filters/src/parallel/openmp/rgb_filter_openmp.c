
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  char nPixelR = 0;
  	  char nPixelG = 0;
  	  char nPixelB = 0;

  	  nPixelR = r;
  	  nPixelG = 0;//g - 255;
  	  nPixelB = 0;//b - 255;

  	  (*outputImage)[i*rawWidth+j] = nPixelR;
  	  (*outputImage)[i*rawWidth+j+1] = nPixelG;
  	  (*outputImage)[i*rawWidth+j+2] = nPixelB;
  	}
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  char nPixelR = 0;
  	  char nPixelG = 0;
  	  char nPixelB = 0;

	  nPixelR = 0;//r - 255;
	  nPixelG = g;
	  nPixelB = 0;//b - 255;

  	  (*outputImage)[i*rawWidth+j] = nPixelR;
  	  (*outputImage)[i*rawWidth+j+1] = nPixelG;
  	  (*outputImage)[i*rawWidth+j+2] = nPixelB;
  	}
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  char nPixelR = 0;
  	  char nPixelG = 0;
  	  char nPixelB = 0;

	  nPixelR = 0;
	  nPixelG = 0;
	  nPixelB = b;

  	  (*outputImage)[i*rawWidth+j] = nPixelR;
  	  (*outputImage)[i*rawWidth+j+1] = nPixelG;
  	  (*outputImage)[i*rawWidth+j+2] = nPixelB;
  	}
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Applying red color filter...\n");
  //use image here
  char r,g,b;
#pragma omp parallel for
  for (int i = 0; i < height; i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
    }

  printf("Applying green color filter...\n");

#pragma omp parallel for
  for (int i = 0; i < height; i++)
    {
      kernelGreenFilter(image,i,rawWidth,greenImage);
    }

  printf("Applying blue color filter...\n");

#pragma omp parallel for
  for (int i = 0; i < height; i++)
    {
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }

}

void testFilterImage(char* image,int width,int height,char **outputImage)
{
  //unsigned error;
  //unsigned char* image;
  /* unsigned width, height; */

  unsigned rawWidth = width * 3;

  printf("Applying test filter...\n");
  /*use image here*/
  unsigned char r,g,b;
  for (int i = 0; i < height; i++)
    {
      for (int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j]   = r;
  	  (*outputImage)[i*rawWidth+j+1] = g;
  	  (*outputImage)[i*rawWidth+j+2] = b;
  	}
    }

}


int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../../../input/test.ppm";
  char* inputImage;
  int width,height;

  LoadPPMImageArray(&inputImage,&width,&height,filename);

  size_t typeSize = sizeof(char);
  int imageSize = width * height * 3;  
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);

  SavePPMImageArray(redImage,width,height,"./red_filtered.ppm");
  SavePPMImageArray(greenImage,width,height,"./green_filtered.ppm");
  SavePPMImageArray(blueImage,width,height,"./blue_filtered.ppm");

  return 0;
}

