
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = r;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = g;
  	  (*outputImage)[i*rawWidth+j+2] = 0;
  	}
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j] = 0;
  	  (*outputImage)[i*rawWidth+j+1] = 0;
  	  (*outputImage)[i*rawWidth+j+2] = b;
  	}
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Image: %d x %d\n",width,height);
  printf("Applying red color filter...\n");

  // just to check
  int num_proc = 4;

  for (int i = 0; i < num_proc; i++)
  {
    int prev_chunk_size = height / num_proc;
    int curr_chunk_size = i != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

    for(int var001=i*prev_chunk_size;var001<((i+1)*curr_chunk_size);var001++)
      kernelRedFilter(image,var001,rawWidth,redImage);
  }

  printf("Applying green color filter...\n");

  for (int i = 0; i < num_proc; i++)
  {
    int prev_chunk_size = height / num_proc;
    int curr_chunk_size = i != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

    for(int var001=i*prev_chunk_size;var001<((i+1)*curr_chunk_size);var001++)
      kernelGreenFilter(image,var001,rawWidth,greenImage);
  }

  printf("Applying blue color filter...\n");
  for (int i = 0; i < num_proc; i++)
  {
    int prev_chunk_size = height / num_proc;
    int curr_chunk_size = i != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

    for(int var001=i*prev_chunk_size;var001<((i+1)*curr_chunk_size);var001++)
      kernelBlueFilter(image,var001,rawWidth,blueImage);
  }

}

void testFilterImage(char* image,int width,int height,char **outputImage)
{
  //unsigned error;
  //unsigned char* image;
  /* unsigned width, height; */

  unsigned rawWidth = width * 3;

  printf("Applying test filter...\n");
  /*use image here*/
  unsigned char r,g,b;
  for (int i = 0; i < height; i++)
    {
      for (int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  (*outputImage)[i*rawWidth+j]   = r;
  	  (*outputImage)[i*rawWidth+j+1] = g;
  	  (*outputImage)[i*rawWidth+j+2] = b;
  	}
    }

}


int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../../input/test.ppm";
  char* inputImage;
  int width,height;

  LoadPPMImageArray(&inputImage,&width,&height,filename);

  size_t typeSize = sizeof(char);
  int imageSize = width * height * 3;  
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);

  SavePPMImageArray(redImage,width,height,"./test_red.ppm");
  SavePPMImageArray(greenImage,width,height,"./test_green.ppm");
  SavePPMImageArray(blueImage,width,height,"./test_blue.ppm");

  return 0;
}
