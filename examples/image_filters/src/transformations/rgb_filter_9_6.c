
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3){
       for(int k=0;k<3;k++) {
	 // Initial code:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?image[i*rawWidth+j+k]:k==1?0:0;
	 // First step:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?image[i*rawWidth+j+k]:0;
	 // Elimination step step:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?image[i*rawWidth+j+k]:0;

	 (*outputImage)[i*rawWidth+j+k] = k==0?image[i*rawWidth+j+k]:0;
       }

     }
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3){
       for(int k=0;k<3;k++) {

	 // Initial code:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?0: k==1?image[i*rawWidth+j+k]:0;
	 // First step:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?0: k!=1?0:image[i*rawWidth+j+k];
	 // Elimination step step:
	 // (*outputImage)[i*rawWidth+j+k] = k!=1?0:image[i*rawWidth+j+k];

	 (*outputImage)[i*rawWidth+j+k] = k!=1?0:image[i*rawWidth+j+k];
       }

  	}
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     for(int j = 0; j < rawWidth; j+=3){
       for(int k=0;k<3;k++) {
	 // Two representations of same functionality. From switch might be easier to eliminate
	 // redundant branches and then generate ternary expressions
	 /* char res; */
	 /* switch(k){ */
	 /* case 0: */
	 /*   res = 0; */
	 /*   break; */
	 /* case 1: */
	 /*   res = 0; */
	 /*   break; */
	 /* case 2: */
	 /*   res = image[i*rawWidth+j+k]; */
	 /* break; */
	 /* } */
	 /* (*outputImage)[i*rawWidth+j+k] = res; */


	 // Initial code:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?0: k==1?0:image[i*rawWidth+j+k];
	 // First step:
	 // (*outputImage)[i*rawWidth+j+k] = k==0?0: k==1?0:image[i*rawWidth+j+k];
	 // Elimination step step:
	 // (*outputImage)[i*rawWidth+j+k] = k!=1?0:image[i*rawWidth+j+k];

	 (*outputImage)[i*rawWidth+j+k] = k!=2?0:image[i*rawWidth+j+k];
       }

     }
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Applying red color filter...\n");

  for (int i = 0; i < height; i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
    }

  printf("Applying green color filter...\n");
  for (int i = 0; i < height; i++)
    {
      kernelGreenFilter(image,i,rawWidth,greenImage);
    }

  printf("Applying blue color filter...\n");
  for (int i = 0; i < height; i++)
    {
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }

}

int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../../input/test.ppm";
  char* inputImage;
  int width,height;

  LoadPPMImageArray(&inputImage,&width,&height,filename);

  size_t typeSize = sizeof(char);
  int imageSize = width * height * 3;  
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);

  SavePPMImageArray(redImage,width,height,"../../output/test_red.ppm");
  SavePPMImageArray(greenImage,width,height,"../../output/test_green.ppm");
  SavePPMImageArray(blueImage,width,height,"../../output/test_blue.ppm");

  return 0;
}
