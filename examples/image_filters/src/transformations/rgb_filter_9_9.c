
//#include "lodepng.h"
#include "ppmImage.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void kernelRedFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

  /* for(int j = 0; j < (rawWidth/3); j++){ */
       for(int k=0;k<(rawWidth/3)*3;k++) {
	 (*outputImage)[i*rawWidth+(0*3)+k] = (k%3)==0?image[i*rawWidth+(0*3)+k]:0;
       }

  /* } */
}

void kernelGreenFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     /* for(int j = 0; j < (rawWidth/3); j++){ */
       for(int k=0;k<(rawWidth/3)*3;k++) {
	 (*outputImage)[i*rawWidth+(0*3)+k] = (k%3)!=1?0:image[i*rawWidth+(0*3)+k];
       }

     /* } */
}

void kernelBlueFilter(char* image,int i,int rawWidth,char **outputImage){
  char r,g,b;

     /* for(int j = 0; j < (rawWidth/3); j++){ */
       for(int k=0;k<(rawWidth/3)*3;k++) {
	 (*outputImage)[i*rawWidth+(0*3)+k] = (k%3)!=2?0:image[i*rawWidth+(0*3)+k];
       }

     /* } */
}


void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  printf("Applying red color filter...\n");

  for (int i = 0; i < height; i++)
    {
      /* kernelRedFilter(image,i,rawWidth,redImage); */
      for(int k=0;k<(rawWidth/3)*3;k++) {
	(*redImage)[i*rawWidth+k] = (k%3)==0?image[i*rawWidth+k]:0;
      }
    }

  printf("Applying green color filter...\n");
  for (int i = 0; i < height; i++)
    {
      /* kernelGreenFilter(image,i,rawWidth,greenImage); */
      for(int k=0;k<(rawWidth/3)*3;k++) {
	(*greenImage)[i*rawWidth+k] = (k%3)!=1?0:image[i*rawWidth+k];
      }
    }

  printf("Applying blue color filter...\n");
  for (int i = 0; i < height; i++)
    {
      /* kernelBlueFilter(image,i,rawWidth,blueImage); */
      for(int k=0;k<(rawWidth/3)*3;k++) {
	(*blueImage)[i*rawWidth+k] = (k%3)!=2?0:image[i*rawWidth+k];
      }
    }

}

int main(int argc, char *argv[]){

  const char* filename = argc > 1 ? argv[1] : "../../input/test.ppm";
  char* inputImage;
  int width,height;

  LoadPPMImageArray(&inputImage,&width,&height,filename);

  size_t typeSize = sizeof(char);
  int imageSize = width * height * 3;  
  char *redImage = malloc( typeSize * imageSize);
  char *blueImage = malloc( typeSize * imageSize);
  char *greenImage = malloc( typeSize * imageSize);

  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);

  SavePPMImageArray(redImage,width,height,"../../output/test_red.ppm");
  SavePPMImageArray(greenImage,width,height,"../../output/test_green.ppm");
  SavePPMImageArray(blueImage,width,height,"../../output/test_blue.ppm");

  return 0;
}
