#include "lodepng.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
3 ways to decode a PNG from a file to RGBA pixel data (and 2 in-memory ways).
*/

/* unsigned char* decodeOneStep(const char* filename) */
/* { */
/*   unsigned error; */
/*   unsigned char* image; */
/*   unsigned width, height; */

/*   error = lodepng_decode32_file(&image, &width, &height, filename); */
/*   if(error) printf("error %u: %s\n", error, lodepng_error_text(error)); */

/*   /\*use image here*\/ */

/*   return image; */

/*   //free(image); */
/* } */

/* void encodeOneStep(const char* filename, const unsigned char* image, unsigned width, unsigned height) */
/* { */
/*   /\*Encode the image*\/ */
/*   unsigned error = lodepng_encode32_file(filename, image, width, height); */

/*   /\*if there's an error, display it*\/ */
/*   if(error) printf("error %u: %s\n", error, lodepng_error_text(error)); */
/* } */

void grayscaleFilterImage(const char* inputFileName,const char* outputFileName)
{
  unsigned error;
  unsigned char* image;
  unsigned width, height;

  printf("Loading image: %s ... \n",inputFileName);
  // Read in RGB format. For reading RGBA used lodepng_decode32_file() instead
  error = lodepng_decode24_file(&image, &width, &height, inputFileName);
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  printf("Image successfuly loaded: %d (width) x %d (height)\n",width,height);

  unsigned rawWidth = width * 3;

  printf("Applying grayscale filter...\n");
  /*use image here*/
  unsigned char r,g,b;
  for (int i = 0; i < height; i++)
    {
      for (int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];
	  
  	  unsigned char gray = (unsigned char)(.299 * r + .587 * g + .114 * b);

  	  image[i*rawWidth+j] = gray;
  	  image[i*rawWidth+j+1] = gray;
  	  image[i*rawWidth+j+2] = gray;
  	}
    }

  /*Encode the image*/
  error = lodepng_encode24_file(outputFileName, image, width, height);

  /*if there's an error, display it*/
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  printf("Output image written to %s\n",outputFileName);  

  free(image);
}

void testFilterImage(const char* inputFileName,const char* outputFileName)
{
  unsigned error;
  unsigned char* image;
  unsigned width, height;

  printf("Loading image: %s ... \n",inputFileName);
  // Read in RGB format. For reading RGBA used lodepng_decode32_file() instead
  error = lodepng_decode24_file(&image, &width, &height, inputFileName);
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));

  printf("Image successfully loaded: %d (width) x %d (height)\n",width,height);

  unsigned rawWidth = width * 3;

  /*use image here*/

  printf("Applying test filter...\n");
  unsigned char r,g,b;
  for (int i = 0; i < height; i++)
    {
      for (int j = 0; j < rawWidth; j+=3)
  	{
  	  r = image[i*rawWidth+j];
  	  g = image[i*rawWidth+j+1];
  	  b = image[i*rawWidth+j+2];

  	  image[i*rawWidth+j] = r;
  	  image[i*rawWidth+j+1] = g;
  	  image[i*rawWidth+j+2] = b;
  	}
    }


  /*Encode the image*/
  error = lodepng_encode24_file(outputFileName, image, width, height);

  /*if there's an error, display it*/
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));
  printf("Output image written to %s\n",outputFileName);

  free(image);
}


int main(int argc, char *argv[])
{
  const char* filename = argc > 1 ? argv[1] : "../input/img.png";
  unsigned char* image;

  grayscaleFilterImage(filename,"../output/img_grayscale.png");
  /* testFilterImage(filename,"../output/img_test.png"); */
  
  return 0;
}

