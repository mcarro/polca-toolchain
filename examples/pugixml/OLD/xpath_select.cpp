#define PUGIXML_HEADER_ONLY


#include "pugixml.hpp"
#include "pugixml.cpp"
#include "pugixml.hpp"

#include <string>
#include <iostream>


const char* node_types[] =
{
    "null", "document", "element", "pcdata", "cdata", "comment", "pi", "declaration"
};
//[code_traverse_walker_impl
struct simple_walker: pugi::xml_tree_walker
{
    virtual bool for_each(pugi::xml_node& node)
    {
        for (int i = 0; i < depth(); ++i) std::cout << "  "; // indentation

        std::cout << node_types[node.type()] << ": name='" << node.name() << "', value='" << node.value() << "'\n";

        return true; // continue traversal
    }
};
//]

struct replacement {
	char attri_value;
	std::string name_attri_value;
	std::string old_value;
	std::string new_value;
};


int main()
{
    pugi::xml_document doc;
    if (!doc.load_file("polca.xml")) return -1;

//[code_xpath_select
    //pugi::xpath_node_set args = doc.select_nodes("//call/*");
    //book[title/@lang = 'it']
//     This reads:

// -get all book elements
//   -that have at least one title
//     -which has an attribute lang
//       -with a value of "it"
    //pugi::xpath_node_set args = doc.select_nodes("//call[arg[@ord='2' and child::call[@function='slide' and child::arg[@ord='1' and child::var]]] and arg[@ord='3' and child::var] and arg[@ord='4' and child::call[@function='slide' and child::arg[@ord='1' and child::var]]] ]");
    //pugi::xpath_node_set args = doc.select_nodes("//call[arg[@ord='2' and child::call[@function='slide' and child::arg[@ord='1' and child::var]]] and arg[@ord='3' and child::var] and arg[@ord='4' and child::call[@function='slide' and child::arg[@ord='1' and child::var]]] and (arg[@ord='2']/call/arg[@ord='1'] = arg[@ord='1'])]");
    //It can be also selected using function select_single_node and then there is not need for iteration
    pugi::xpath_node_set args = doc.select_nodes("//call[arg[@ord='2']/call[@function='slide']/arg[@ord='1']/var[@name = ../../../../arg[@ord='3']/var/@name] and arg[@ord='4']/call[@function='slide']/arg[@ord='1']/var[@name = ../../../../arg[@ord='3']/var/@name]]");//

    std::cout << "pass\n";

    // //std::cout << args.begin() << " " << args.end() << "\n";

    for (pugi::xpath_node_set::const_iterator it = args.begin(); it != args.end(); ++it)
    {
        pugi::xpath_node node = *it;
        //std::cout << node.node().attribute("function").value() << "\n";
        node.node().print(std::cout);

     //    pugi::xml_document doc_res;

	    // //[code_modify_add
	    // // add node with some name
	    // pugi::xml_node node_res = doc_res.append_child("node");

	    // // add description node with text child
	    // pugi::xml_node descr = node_res.append_child("description");
	    // descr.append_child(pugi::node_pcdata).set_value("Simple node");

	    // // add param node before the description
	    // pugi::xml_node param = node_res.insert_child_before("param", descr);

	    // // add attributes to param node
	    // param.append_attribute("name") = "version";
	    // param.append_attribute("value") = 1.1;
	    // param.insert_attribute_after("type", param.attribute("name")) = "float";
	    // //]

	    // doc_res.print(std::cout);

	    replacement rep = {'a',"name","FunctionToApply","G"};

	   	std::cout << rep.old_value << "\n";

	    pugi::xml_document doc_schema;
    	if (!doc_schema.load_file("polca_res_schema.xml")) {std::cout << "Can't read xml document.\n";return -1;}

    	simple_walker walker;
    	doc_schema.traverse(walker);



        // pugi::xpath_node_set sec_var = doc.select_nodes("//call/arg[@ord='2']/call/arg[@ord='1']/var");
        // pugi::xpath_node_set thi_var = doc.select_nodes("//call/arg[@ord='3']/var");
        // pugi::xpath_node_set fou_var = doc.select_nodes("//call/arg[@ord='4']/call/arg[@ord='1']/var");
        // if(strcmp(sec_var.first().node().attribute("name").value(), thi_var.first().node().attribute("name").value())== 0 && strcmp(sec_var.first().node().attribute("name").value(), fou_var.first().node().attribute("name").value())== 0)
        // 	std::cout << "ok\n";
    }

	// pugi::xpath_node_set tools = doc.select_nodes("/Profile/Tools/Tool[@AllowRemote='true' and @DeriveCaptionFrom='lastparam']");

	// std::cout << "Tools:\n";

	// for (pugi::xpath_node_set::const_iterator it = tools.begin(); it != tools.end(); ++it)
	// {
	//     pugi::xpath_node node = *it;
	//     std::cout << node.node().attribute("Filename").value() << "\n";
	// }

	// pugi::xpath_node build_tool = doc.select_single_node("//Tool[contains(Description, 'build system')]");

	// if (build_tool)
	//     std::cout << "Build tool: " << build_tool.node().attribute("Filename").value() << "\n";
}

// vim:et