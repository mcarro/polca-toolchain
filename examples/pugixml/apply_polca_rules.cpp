#define PUGIXML_HEADER_ONLY


#include "pugixml.hpp"
#include "pugixml.cpp"
#include "pugixml.hpp"

#include <string>
#include <iostream>
#include <fstream>

//TODOS
//* Take several rules
//* Take the rules file and the specification file as parameters

struct rewriting {
	const pugi::char_t* pattern;
	const pugi::char_t* value;
    const pugi::char_t* attribute;
};

int main(int argc, char* argv[])
{

    // Check the number of parameters
    if (argc < 3) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " RULES_FILE SPECIFICATION_FILE" << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }



    //read the rules
    pugi::xml_document rules;
    if (!rules.load_file(argv[1])) {std::cout << "Can't read "<< argv[1]<<" document.\n";return -1;}

    pugi::xpath_node_set node_rules = rules.select_nodes("/rule");   

    for (pugi::xpath_node_set::const_iterator it_rule = node_rules.begin(); it_rule != node_rules.end(); ++it_rule)
    {
        pugi::xml_node rule_to_write = (*it_rule).node();

        //store and load file to handle a problem with xpath selection over partial nodes
        std::ofstream temp_rule_file ("temp_rule.xml");
        rule_to_write.print(temp_rule_file, "", pugi::format_declaration );
        temp_rule_file.close();
        pugi::xml_document rule;
        if (!rule.load_file("temp_rule.xml")) {std::cout << "Can't read temp_rule.xml document.\n";return -1;}

#ifdef DEBUG
        std::cout << "CURRENT RULE\n";
        rule.print(std::cout);
#endif 

        std::string rule_id = rule.first_child().attribute("id").value();

        //search and store the antecedent query
        pugi::xpath_node antecedent = rule.select_single_node("//antecedent");
        const pugi::char_t* antecedent_query = antecedent.node().child_value();
#ifdef DEBUG
        std::cout << "ANTECEDENT\n";
        std::cout << antecedent_query << "\n";
#endif

        //read the specification
        pugi::xml_document specification;
        if (!specification.load_file(argv[2])) {std::cout << "Can't read "<< argv[2] << " document.\n";return -1;}

        //search nodes matching the antecedent in the specification
        pugi::xpath_node_set matches = specification.select_nodes(antecedent_query);

        //search rewritings in the rule
        pugi::xpath_node_set rewritings = rule.select_nodes("//rewriting");
        int num_rewritings = rewritings.size();
        rewriting rews[num_rewritings]; 

#ifdef DEBUG
        std::cout << "REWRITINGS\n\n";
#endif
        int i = 0;
        //store each rewriting in the list
        for (pugi::xpath_node_set::const_iterator it = rewritings.begin(); it != rewritings.end(); ++it)
        {
            pugi::xpath_node node = *it;
            pugi::xml_node pattern_node = node.node().first_child();
            pugi::xml_node value_node = pattern_node.next_sibling();
            rews[i].pattern = pattern_node.child_value();
            rews[i].value = value_node.child_value();
            pugi::xml_node attribute_node = value_node.next_sibling();
            if(attribute_node)
                rews[i].attribute = attribute_node.child_value();
            else 
                rews[i].attribute = "";
#ifdef DEBUG
            if (rews[i].attribute != "")
                std::cout << "Rewriting " << i <<"\nPattern: " << rews[i].pattern << "\nValue: " << rews[i].value << "\nAttribute: " << rews[i].attribute << "\n\n";
            else
                std::cout << "Rewriting " << i <<"\nPattern: " << rews[i].pattern << "\nValue: " << rews[i].value << "\n\n";
#endif   
            i++;
        }

#ifdef DEBUG
        std::cout << "Total rewritings: " << num_rewritings << "\n";
        std::cout << "\nEND_REWRITINGS\n\n";
#endif

        //search and store the rule's schema. Should be written into a file and read each iteration in order to avoid problems when substitutions are done.
        pugi::xml_node original_schema = rule.select_single_node("//schema/*").node();
        std::ofstream schema_file ("schema.xml");
        original_schema.print(schema_file, "", pugi::format_declaration );
        schema_file.close();
#ifdef DEBUG
        std::cout << "SCHEMA\n";
        original_schema.print(std::cout);
#endif

        //loop over all the those nodes matching the antecedent
        for (pugi::xpath_node_set::const_iterator it = matches.begin(); it != matches.end(); ++it)
        {
            pugi::xml_node current_node = (*it).node();


            //PROBLEM: current_node is not pointing to the matched node, instead it points to the whole document
            //SOLVED: Create a temporal file create a new independent node reading this file
            //Additionally, we have needed some modification in the library (hpp and cpp). Search for Tamarit in order to find them.
            std::ofstream temp_file ("temp.xml");
            current_node.print(temp_file, "", pugi::format_declaration );
            temp_file.close();
            pugi::xml_document temp_current_node;
            if (!temp_current_node.load_file("temp.xml")) {std::cout << "Can't read temp.xml document.\n";return -1;}

#ifdef DEBUG
                std::cout << "MATCHING SPECIFICATION NODE\n";
                current_node.print(std::cout);
#endif

            //read the schema file
            pugi::xml_document schema;
            if (!schema.load_file("schema.xml")) {std::cout << "Can't read schema.xml document.\n";return -1;}

            //Loop over the rewriting rules in the rule
            for(int num_rew = 0; num_rew < num_rewritings; num_rew++)
            {
                //Search the node value (in the new document created to solve the problem with function)
                pugi::xml_node node_value = temp_current_node.select_single_node(rews[num_rew].value).node();
#ifdef DEBUG
                std::cout << "Perform rewriting " << num_rew << "\n";
                node_value.print(std::cout);
#endif
                //Loop over those nodes matching the pattern
                pugi::xpath_node_set nodes_pattern = schema.select_nodes(rews[num_rew].pattern);
                for (pugi::xpath_node_set::const_iterator it_pat = nodes_pattern.begin(); it_pat != nodes_pattern.end(); ++it_pat)
                {
                    pugi::xml_node node_pattern = (*it_pat).node();
#ifdef DEBUG
                    node_pattern.print(std::cout);
#endif
                    if(rews[num_rew].attribute == "")
                    {
                        //Substitute pattern node by value node
                        node_pattern.parent().insert_copy_after(node_value, node_pattern);
                        node_pattern.parent().remove_child(node_pattern);
                    }
                    else
                    {
                        node_pattern.append_copy(node_value.attribute(rews[num_rew].attribute));
                    }
                }
            }
            current_node.parent().insert_copy_before(schema.first_child(), current_node);
            current_node.parent().remove_child(current_node);
#ifdef DEBUG
            std::cout << "\nREWRITTEN SCHEMA\n";
            schema.print(std::cout);
            std::cout << "\nREWRITTEN SPECIFICATION\n";
            specification.print(std::cout);
#endif

            //we store the result in a file named polca_result_IdRule
            std::string save_file_name = "polca_result_" + rule_id + ".xml";
            specification.save_file(save_file_name.c_str());
        }
    }
}
