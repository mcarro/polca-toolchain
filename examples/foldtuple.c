#include <string.h>

void main() {
  int N = 100;
  int s,p,l[N];
  int i;

  /* var(l) /\ N = length l : (fold (+) 0 l, fold (*) 1 l) =>  (var(s), var(p)) */
  s = 0;
  for (i = 0; i < N; i++)
    s = s + l[i];
  p = 1;
  for (i = 0; i < N; i++)
    p = p * l[i];
  
  /*
  length l1 == length l2
                    (fold f1 b1 l1, fold f2 b2 l2)
  ------------------------------------------------------------------
  fold (\ (x1,x2) (a1,a2). (f1 x1 a1, f2 x2 a2)) (b1,b2) (zip l1 l2)
  */
  
  /* fold (\ (x1,x2) (a1,a2). ((+) x1 a1, (*) x2 a2)) (0,1) (zip l l) */
  
  s = 0;
  p = 1;
  for (i = 0; i < N; i++) {
    s += l[i];
    p *= l[i];
  }
}
