#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define N 11

int main(int argc, char *argv[])
{
  float a[N],b[N],c=2.0,*k;

  k = malloc(sizeof(float)*N);

  k[1] = c;
  *(k+2) = c+1;
  printf("%.2f - %.2f\n",k[1],*(k+2));

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   for(int i=0;i<N;i++)
   {
     c += a[i] * b[i];
   }

   printf("%.2f, ",c);
   printf("\n");
   
}
