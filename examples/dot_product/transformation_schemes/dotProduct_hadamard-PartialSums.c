#include <stdio.h>
#include <math.h>

#define N 10

#define P 5

int main(int argc, char *argv[])
{
   float a[N],b[N],d[N],c=0.0;

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   for(int i=0;i<N;i++)
   {
     d[i] = a[i] * b[i];
   }

   // The loop performing the sum is replaced by partial sums
   /* for(int i=0;i<N;i++) */
   /* { */
   /*   c += d[i]; */
   /* } */


   float partialSums[P];
   for(int i=0;i<P;i++)
   {
     partialSums[i] = 0.0;
     for(int j=i*(N/P);j<(i*(N/P))+(N/P);j++)
     {
       partialSums[i] += d[j];
     }
     if(i!=0)
       partialSums[0] += partialSums[i];
   }

   /* printf("%.2f == %.2f, ",c,partialSums[0]); */
   printf("%.2f, ",partialSums[0]);
   printf("\n");
   
}
