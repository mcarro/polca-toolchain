#include <stdio.h>
#include <math.h>

#define N 11

int main(int argc, char *argv[])
{
  float a[N],b[N],d[N],c=0.0;

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   // The single original loop is split in two loops
   /* for(int i=0;i<N;i++) */
   /* { */
   /*   c += a[i] * b[i]; */
   /* } */

   // Loop computing the Hadamard product
   for(int i=0;i<N;i++)
   {
     d[i] = a[i] * b[i];
   }

   // Loop computing the final sum
   for(int i=0;i<N;i++)
   {
     c += d[i];
   }

   printf("%.2f, ",c);
   printf("\n");
   
}
