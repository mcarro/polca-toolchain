#include <stdio.h>
#include <math.h>

#define N 10

#define P 5

int main(int argc, char *argv[])
{
   float a[N],b[N],d[N],c=0.0;

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   // The index split transformation results in splitting
   // this for loop
   /* for(int i=0;i<N;i++) */
   /* { */
   /*   d[i] = a[i] * b[i]; */
   /* } */


   for(int i=0;i<P;i++)
   {
     for(int j=i*(N/P);j<(i*(N/P))+(N/P);j++)
     {
       d[j] = a[j] * b[j];
     }
   }

   for(int i=0;i<N;i++)
   {
     c += d[i];
   }


   /* printf("%.2f == %.2f, ",c,partialSums[0]); */
   printf("%.2f, ",c);
   printf("\n");
   
}
