#include <stdio.h>
#include <math.h>

#define N 10

#define P 5

int main(int argc, char *argv[])
{
   float a[N],b[N],d[N],c=0.0;

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   // This is the initial code of this transformation
   // 1) Index split on the Hadamard product
   // 2) Partial sums on the final sum of the dot product

   /* for(int i=0;i<P;i++) */
   /* { */
   /*   for(int j=i*(N/P);j<(i*(N/P))+(N/P);j++) */
   /*   { */
   /*     d[j] = a[j] * b[j]; */
   /*   } */
   /* } */

   /* float partialSums[N/P]; */
   /* for(int i=0;i<P;i++) */
   /* { */
   /*   partialSums[i] = 0.0; */
   /*   for(int j=i*(N/P);j<(i*(N/P))+(N/P);j++) */
   /*   { */
   /*     partialSums[i] += d[j]; */
   /*   } */
   /*   if(i!=0) */
   /*     partialSums[0] += partialSums[i]; */
   /* } */


   // Further transformations could be done to the following code:
   // - Remove 'd' array and save memory
   float partialSums[N/P];
   for(int i=0;i<P;i++)
   {
     partialSums[i] = 0.0;
     for(int j=i*(N/P);j<(i*(N/P))+(N/P);j++)
     {
       d[j] = a[j] * b[j];
       partialSums[i] += d[j];
     }
     if(i!=0)
       partialSums[0] += partialSums[i];

   }


   /* printf("%.2f == %.2f, ",c,partialSums[0]); */
   printf("%.2f, ",partialSums[0]);
   printf("\n");
   
}
