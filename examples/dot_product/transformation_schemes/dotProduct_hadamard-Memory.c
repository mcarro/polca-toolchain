#include <stdio.h>
#include <math.h>

#define N 11

int main(int argc, char *argv[])
{
  // The auxiliar array 'd' is removed and used 'a' instead
  // This can be done thanks to dependencies in the loop
  // that computes the Hadamard product
  /* float a[N],b[N],d[N],c=0.0; */

   float a[N],b[N],c=0.0;

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   // Loop computing the Hadamard product
   for(int i=0;i<N;i++)
   {
     /* d[i] = a[i] * b[i]; */
     // Now Hadamard product result is stored in 'a'
     a[i] = a[i] * b[i];
   }

   // Loop computing the final sum
   for(int i=0;i<N;i++)
   {
     /* c += d[i]; */
     c += a[i];
   }

   printf("%.2f, ",c);
   printf("\n");
   
}
