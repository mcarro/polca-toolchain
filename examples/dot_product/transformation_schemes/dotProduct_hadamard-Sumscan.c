#include <stdio.h>
#include <math.h>

#define N 11

int main(int argc, char *argv[])
{
   float a[N],b[N],d[N],c=0.0;

   for(int i=0;i<N;i++)
   {
     a[i] = b[i] = (float)i+1;
   }

   for(int i=0;i<N;i++)
   {
     d[i] = a[i] * b[i];
   }

   // The loop performing the sum is replaced by sum-scan O(log(N))
   /* for(int i=0;i<N;i++) */
   /* { */
   /*   c += d[i]; */
   /* } */

   // number of iterations of outer loop is computed with log_2
   int steps = (int) ceil(log2((double) N)); 
   //printf("Computing sum-scan for %d steps...\n",steps);
   int offset = 1;
   for(int i=0;i<steps;i++)
   {
     for(int j=0;j<N;j=j+2*offset)
       if(j+offset<N)
         d[j] = d[j] + d[j+offset];

     offset *= 2;
   }


   //printf("%.2f == %.2f, ",c,d[0]);
   printf("%.2f, ",d[0]);
   printf("\n");
   
}
