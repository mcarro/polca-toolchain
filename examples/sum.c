#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

const int N = 10;
const int M = 2; /* Partition size */
#define F (N / M) /* Fragments */
/* assertion in main: N % M == 0 */

void sum_origin(int *v /* in */,
                int *s /* out */)
{
  /* Spec: 
   * sum :: [Int] -> Int
   * sum v = foldl (+) 0 v
   * 
   * Equivalent to
   * sum = foldl (+) 0
   *
   * Notes:
   *  N == length(v) in any call to sum v
   */
  int i;
  *s = 0;
  for (i=0; i<N; i++) {
    *s = *s + v[i];
  }
}

void sum_transformed_1(int *v, int *s)
{
  /*
   * Idea: v is conceptually split in vv with a "delinealization" such that vv!!i!!j == v!!(i*M+j)
   */

  /* Spec:
   * sum_transformed_1 :: [Int] -> Int -> Int
   * sum_transformed_1 v =
   *   let m = 25 -- M
   *       f = 1000 `div` m -- N `div` M
   *       s_0 = take m (repeat 0)
   *       vv = snd ((iterate (\ (v',vv') -> (drop m v', vv' ++ [take m v'])) (v,[]))!!f)
   *   in foldl (+) 0 (foldl (zipWith (+)) s_0 vv)
   */
  int i, j;
  int *s_in = (int *) malloc(M*sizeof(int));
  int *s_out = (int *) malloc(M*sizeof(int));

  /* This is s_0 = take m (repeat 0) */
  for (j=0; j<M; j++) {
    s_in[j] = 0;
  }

  /* This is the internal foldl */
  for (i=0; i<F; i++) {
    /* This is the zipWith */
    /* Q: is this a Maxeler kernel with input being s_in and v+(i*M), and output being s_out? */
    for (j=0; j<M; j++) {
      s_out[j] = s_in[j] + v[i*M+j];
    }
    memcpy(s_in,s_out,M*sizeof(int));
  }

  /* This is the outer foldl */
  *s = 0;
  for (j=0; j<M; j++) {
    *s = *s + s_in[j];
  }

  free(s_in);
  free(s_out);
}

int main() {
  assert(N%M == 0);

  int i;
  int s1;
  int s2;
  int *v = (int *) malloc(N*sizeof(int));

  printf("[");
  for (i = 0; i < N-1; i++) {
    v[i] = rand() % 100;
    printf("%i, ",v[i]);
  }
  v[i] = rand() % 100;
  printf("%i]\n",v[i]);

  sum_origin(v,&s1);
  sum_transformed_1(v,&s2);

  printf("sum_origin: %i\n",s1);
  printf("sum_transformed_1: %i\n",s2);

  free(v);
  return 0;
}
