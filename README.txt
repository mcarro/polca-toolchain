* Instructions to launch the tool that uses the ML oracle:

  - Simply run the following commands, from the path where this README file is:

    cd ./examples/C_haskell
    make tar
    cd ../..
    rm -rf polca_s2s.tgz polca_s2s/
    mv ./examples/C_haskell/polca_s2s.tgz .
    tar xzvf polca_s2s.tgz
    cd polca_s2s/
    make configure
    make rules FILE=rules_nbody.c
    make
    make interface
    make exe_ext
    time ./polca_s2s_ext "../static_analyser/s2s_ml_interface.py" ../static_analyser/train_set/hpcDwarfs/nBody/oracle_test/2arrays/nbody.c BLOCK_ABS
