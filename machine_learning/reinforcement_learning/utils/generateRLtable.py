#!/usr/bin/python

import re,os,sys

numActions = -1

statesDict = {}
transitionsDict = {}
valueTable = []

def processFile(filename):
    global numActions,statesDict,transitionsDict

    f = open(filename)


    for line in f:
        # Lines beginning with # are comments
        m1 = re.match("^[^#]\s*(.*\.c):\s*//\s*TEST_VECTOR:\s*\[(.*)\]\s+(\d+)\s+\->\s+(\[(.*)\])",line)

        m2 = re.match("\s*action\s+(\d+):\s*(.*)",line)

        if m1:
            if m1.group(2) in statesDict.keys():
                print("%s Warning: state %s: [%s] already present. Is it OK?" % (m1.group(1),m1.group(3),m1.group(2)))

            s1 = m1.group(4).replace("[[","[")
            s1 = s1.replace("]]","]")

            # print("%s - %s - %s" % (m1.group(2),m1.group(3),s1))

            l = s1.split("]")
            
            transitionList = []
            for elem in l:
                elem = elem.replace("[","")
                vals = elem.split(",")
                # print("%s - %d" % (vals,len(vals)))
                if ("*" not in vals) and not (len(vals)==1):
                    vals = [int(x) for x in vals if x != '']
                    transitionList.append(vals)


            # print("%s - %s" % (m1.group(3),transitionList))

            statesDict[m1.group(2)] = [int(m1.group(3))]
            transitionsDict[int(m1.group(3))] = transitionList

        elif m2:
            # print("action  %s - %s" % (m2.group(1),m2.group(2)))
            currentAction = int(m2.group(1))
            if currentAction > numActions:
                numActions = currentAction 


def generateValueTable():
    global statesDict,valueTable,transitionsDict,numActions

    sys.stdout.write("    actionT   = array([")

    keysList = sorted(transitionsDict)
    lastState = keysList[len(keysList)-1]
    firstState = keysList[0]

    for key in sorted(transitionsDict):
        # print("%d - %s" % (key,transitionsDict[key]))
        l = [key for i in range(0,numActions+1)]

        # print l

        for transition in transitionsDict[key]:
            if (l[transition[1]] != transition[0]) and (l[transition[1]] != key):
                print("ERROR: overwriting a previous written value. Previous: %d. Current: %d" % (l[transition[1],transition[0]]))
            else:
                l[transition[1]] = transition[0]

        if(key == lastState):
            sys.stdout.write("                       %s])\n" % l)
        elif key == firstState:
            sys.stdout.write("%s,\n" % l)
        else:
            sys.stdout.write("                       %s,\n" % l)
        # print("-------------------------------")

    print("-----------------------------")

    print("Num. States:  %d" % (len(keysList)))
    print("Num. Actions: %d" % (numActions+1))
        
    


if __name__ == "__main__":

    filename = "./tableData.txt"

    processFile(filename)
    generateValueTable()
