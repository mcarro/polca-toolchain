#!/usr/bin/python

__author__ = 'guillermo'

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import random
import matplotlib.colors as clrs
import matplotlib.cm as cm

from sklearn import cluster


from sklearn import cluster, datasets
iris = datasets.load_iris()
# X_iris = iris.data
# y_iris = iris.target

# data = [[1,0,0],
#         [0,1,0],
#         [0,0,1]]

data = []

numData = 100
for i in range(0,numData):
    data.append([random.random(),random.random(),random.random()])


X = np.array(data)

print(type(X))
print(X)

n_clusters = 3
k_means = cluster.KMeans(n_clusters=n_clusters)
k_means.fit(X)

# print(k_means.labels_)

values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
# face_compressed = np.choose(labels, values)
# face_compressed.shape = X_iris.shape

# print(values)
# print(labels)

# print(face_compressed)


fignum = 1
fig = plt.figure(fignum, figsize=(4, 3))
ax = Axes3D(fig, rect=[0, 0, .95, 1], elev=48, azim=134)
# est.fit(X)
# labels = est.labels_

cmap = cm.jet
norm = clrs.Normalize(vmin=0, vmax=n_clusters-1)

# colors = labels.astype(np.float)
colors = labels
# print(colors)
img = ax.scatter(X[:, 0], X[:, 1], X[:, 2],c=colors, edgecolor='k',cmap=cmap,norm=norm)
# print("Init facecolors: %s" % img.get_facecolors())

ax.set_xticks([0.0,0.25,0.5,0.75,1.0])
ax.set_yticks([0.0,0.25,0.5,0.75,1.0])
ax.set_zticks([0.0,0.25,0.5,0.75,1.0])

ax.w_xaxis.set_ticklabels([0.0,0.25,0.5,0.75,1.0])
ax.w_yaxis.set_ticklabels([0.0,0.25,0.5,0.75,1.0])
ax.w_zaxis.set_ticklabels([0.0,0.25,0.5,0.75,1.0])
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_title("Title")
ax.dist = 12
# fignum = fignum + 1

new_point = np.array([1.,1.,1.])
predicted_label = k_means.predict(new_point)
print(predicted_label)

img2 = ax.scatter(new_point[0], new_point[1], new_point[2],c=[float(predicted_label)], edgecolor='r',cmap=cmap,norm=norm)
print(img2._facecolors)

# print cmap(norm(0))
# print cmap(norm(1))
# print cmap(norm(2))
# print(cm._cmapnames)

fig.colorbar(img2, ticks=np.arange(0, n_clusters))

plt.show()

# print(y_iris[::10])


# data = [[1,0,0],
#         [0,1,0],
#         [0,0,1]]

# n_clusters = 3
# np.random.seed(0)
# 
# X = data.reshape((-1, 1))  # We need an (n_sample, n_feature) array
# k_means = cluster.KMeans(n_clusters=n_clusters, n_init=2)
# k_means.fit(X)
# values = k_means.cluster_centers_.squeeze()
# labels = k_means.labels_
