#ifndef TIMER_H
#define TIMER_H

#include <stdlib.h>

#ifdef WIN32
  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>
#else
  #ifndef __USE_BSD
    #define __USE_BSD
  #endif
  #include <sys/time.h>
#endif

#ifdef WIN32
double PCFreq = 0.0;
__int64 timerStart = 0;
#else
struct timeval timerStart;
#endif

void StartTimer()
{
#ifdef WIN32
  LARGE_INTEGER li;
  if(!QueryPerformanceFrequency(&li))
    printf("QueryPerformanceFrequency failed!\n");

  PCFreq = (double)li.QuadPart/1000.0;

  QueryPerformanceCounter(&li);
  timerStart = li.QuadPart;
#else
  gettimeofday(&timerStart, NULL);
#endif
}

// time elapsed in ms
void GetTimer()
{
#ifdef WIN32
  LARGE_INTEGER li;
  QueryPerformanceCounter(&li);
  return (double)(li.QuadPart-timerStart)/PCFreq;
#else
  struct timeval timerStop, timerElapsed;
  gettimeofday(&timerStop, NULL);
  timersub(&timerStop, &timerStart, &timerElapsed);

  double totalTime = timerElapsed.tv_sec*1000.0+timerElapsed.tv_usec/1000.0;

  printf("%0.3f ms\n", totalTime);

    /* return totalTime; */

#endif
}

#endif // TIMER_H
