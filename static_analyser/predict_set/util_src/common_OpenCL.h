
#ifndef _COMMON_OPENCL_H_
#define _COMMON_OPENCL_H_

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <OpenCL/opencl.h>
// For OpenCL
#define MAX_SOURCE_SIZE (0x100000)

//////////////////////////////////////////////////////////////
//  Definition of variables common to *any* OpenCL program
/////////////////////////////////////////////////////////////
cl_uint          platformIdCount;
cl_platform_id  *platformIds;
cl_uint          deviceIdCount;
cl_device_id    *deviceIds;
cl_context       context;
cl_command_queue queue;
/* cl_program program; */
/////////////////////////////////////////////////////////////

void initOpenCLVars();

char *GetPlatformName (cl_platform_id id);

char *GetDeviceName (cl_device_id id);

void CheckError (cl_int error);

char *LoadKernel (const char* name, long *sourceSize);;

cl_program CreateProgram (long sourceSize,char *source,cl_context context);

void BuildProgram(char *compileFlags,cl_program program,cl_device_id device_id);

void selectOpenCLDevice(cl_uint *platformIdCount,cl_platform_id **platformIds,cl_uint *deviceIdCount,cl_device_id **deviceIds);

void createOpenCLContext(cl_platform_id *platformIds,cl_uint deviceIdCount,cl_device_id *deviceIds,cl_context *context);

void createOpenCLKernel(char *kernelFunc,char *kernelName,char *compileFlags,cl_device_id *deviceIds,cl_context context,cl_kernel *kernel);

void createOpenCLQueue(cl_device_id *deviceIds,cl_context context,cl_command_queue *queue);

#endif
