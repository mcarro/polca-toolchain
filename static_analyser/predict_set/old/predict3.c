// TEST_VECTOR: [0, 1, 0, 0, 0, 0, 1, 0, 1]
// TEST_LABEL: 2 (MPI)

void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  /* printf("Applying red color filter...\n"); */
  /* printf("Applying green color filter...\n"); */
  /* printf("Applying blue color filter...\n"); */

  // just to check
  int num_proc;

#pragma stml loop_schedule
  for (int rank = 0; rank < num_proc; rank++)
  {
    int prev_chunk_size = height / num_proc;
    int curr_chunk_size = rank != (num_proc-1) ? prev_chunk_size : prev_chunk_size + height%num_proc;

    for(int i=rank*prev_chunk_size;i<((rank+1)*curr_chunk_size);i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
      kernelGreenFilter(image,i,rawWidth,greenImage);
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }
  }

}
