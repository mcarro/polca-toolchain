// TEST_VECTOR: [0, 0, 0, 0, 0, 0, 1, 1, 0]
// TEST_LABEL: 12 (FPGA/GPU/OpenMP)
void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  /* printf("Applying red color filter...\n"); */
  /* printf("Applying blue color filter...\n"); */
  /* printf("Applying green color filter...\n"); */

#pragma stml reads image in {0}
#pragma stml writes redImage in {0}
#pragma stml writes greenImage in {0}
#pragma stml writes blueImage in {0}
#pragma stml iteration_independent
  for(int k=0;k<(rawWidth/3)*3*height;k++) {
    (*redImage)[k] = (k%3)==0?image[k]:0;
      
    (*greenImage)[k] = (k%3)!=1?0:image[k];


    (*blueImage)[k] = (k%3)!=2?0:image[k];
  }


}
