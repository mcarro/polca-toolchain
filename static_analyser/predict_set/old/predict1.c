// TEST_VECTOR: [0, 1, 0, 0, 0, 0, 1, 1, 0]
// TEST_LABEL: 1 (OpenMP)

void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{

  unsigned rawWidth = width * 3;

  /* printf("Applying red color filter...\n"); */

  for (int i = 0; i < height; i++)
    {
      kernelRedFilter(image,i,rawWidth,redImage);
    }

  /* printf("Applying green color filter...\n"); */
  for (int i = 0; i < height; i++)
    {
      kernelGreenFilter(image,i,rawWidth,greenImage);
    }

  /* printf("Applying blue color filter...\n"); */
  for (int i = 0; i < height; i++)
    {
      kernelBlueFilter(image,i,rawWidth,blueImage);
    }

}
