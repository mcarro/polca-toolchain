// FUNC_ANALYZ: correlate2d
// FEAT_VECTOR: [3, 1, 1, 0, 1, 1, 1, 0, 0, 1, 2, 3, 2, 6, 0]
// TEST_VECTOR: [3, 1, 1, 0, 1, 1, 1, 0, 0, 1, 2, 3, 2, 6, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             3
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              1
//# anyIfStmt:                   1
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       2
//# numNon1Darray:               3
//# numAuxVarArrayIndex:         2
//# totalNumForLoops:            6
//# numNonNormalizedForLoops:    0


#include "traps.h"

#define         N       128

int image[N][N];
int template[N][N];
int output[N][N];

void correlate2d(int input_image[N][N], int template[N][N], int output_image[N][N]);
main()
{


  /* Read input image. */

  input_dsp(image, N*N, 1);


  correlate2d(image, template, output);

  /* Store binary image. */
  output_dsp(output, N*N, 1);
}


void correlate2d(int input_image[N][N], int template[N][N], int output_image[N][N])
{
  int i;
  int j;
  int c;
  int r;
  int sum;
  int ix;
  int jy;
  int normal_factor;
  int count;

  count = 0;
  for (i = 0; i < N - 1; i++) {
    for (j = 0; j < N - 1; j++) {
      template[i][j] = input_image[i][j];
      count++;
    }
  }

  normal_factor = 255;

  /* Convolve the input image with the kernel. */
  for (r = 0; r < N - 1; r++) {
    for (c = 0; c < N - 1; c++) {
      sum = 0;
      for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
	  ix = r+i;
	  jy = c+j;
	  if( (ix<N) && (jy<N) )
	    sum += input_image[ix][yi] * template[r][c];
        }
      }
      output_image[r][c] = abs(sum) % normal_factor;
    }
  }
}
