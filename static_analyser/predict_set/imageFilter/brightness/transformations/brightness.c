// FUNC_ANALYZ: brightness
// FEAT_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 2, 0]
// TEST_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 2, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             1
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               0
//# numLoopHoistedVarMods:       0
//# numNon1Darray:               2
//# numAuxVarArrayIndex:         0
//# totalNumForLoops:            2
//# numNonNormalizedForLoops:    0


// Added just for measure time
#include <sys/time.h>
#define PROFILING 1

#define         N               3000

int image[N][N];
int result[N][N];

int applyBrightness(int input,int value) {

  int aux;

  aux = input + value;

  if(aux > 255)
    aux = 255;
  if(aux < 0)
    aux = 0;

  return aux;

}

void brightness(int input_image[N][N], int value, int output_image[N][N])
{

  int c;
  int r;
  /* int l=0; */
  /* int k=0; */

  /* int one = 1; */
  int aux;


`  for (r = 0; r < N; r++) {
    for (c = 0; c < N; c++) {
      aux = applyBrightness(input_image[r][c],value);

      output_image[r][c] = aux;
    }
  }

}

void main() {

  /* Read the image */

  input_dsp(image, N*N, 1);

#if PROFILING == 1
  struct timeval tvalBefore, tvalAfter;
  long total;
  int SAMPLES = 30;
 for(int i=0;i<SAMPLES;i++){
	  gettimeofday(&tvalBefore,NULL);

#endif

  int brightnessValue = 127;
  brightness(image,brightnessValue,result);


#if PROFILING == 1
	  gettimeofday(&tvalAfter,NULL);
	  total = ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L+tvalAfter.tv_usec) - tvalBefore.tv_usec;
	  /* printf("it: %i, time: %ld microseconds\n",i,total); */
	  printf("%ld\n",total);
 } // End of loop for time samples

#endif

  /* Store the output */

  output_dsp(result,N*N,1);

}
