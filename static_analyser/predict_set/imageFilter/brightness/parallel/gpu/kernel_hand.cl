// Simple compute kernel
//


__kernel void brightness(
   __global int* input,
   const    int  brightnessValue,
   __global int* output)
{

    int i   = get_global_id(0);
    int lId = get_local_id(0);


    int aux; 

    aux = input[i] + brightnessValue;

    aux = ((aux > 255) * 255) +  ((aux < 0) * 0) + ( ( (aux <= 255) && (aux >= 0) ) * aux);

    output[i] =  aux;   
}