// FUNC_ANALYZ: brightness
// FEAT_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 2, 2, 2, 0]
// TEST_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 2, 2, 2, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             1
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       0
//# numNon1Darray:               2
//# numAuxVarArrayIndex:         2
//# totalNumForLoops:            2
//# numNonNormalizedForLoops:    0


int applyBrightness(int input,int value) {

  int aux;

  aux = input + value;

  if(aux > 255)
    aux = 255;
  if(aux < 0)
    aux = 0;

  return aux;

}

void brightness(int input_image[N][N], int output_image[N][N], int value)
{

  int c;
  int r;
  int l=0;
  int k=0;

  int one = 1;
  int aux;


  for (r = 0; r < N; r++) {
    for (c = 0; c < N; c++) {
      aux = applyBrightness(input[r][c],value);

      output_image[r+l][c+k] = aux / one;
    }
  }

}
