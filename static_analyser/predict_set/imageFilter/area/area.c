// FUNC_ANALYZ: area2d

int area2d(int input_image[N][N], int objValue)
{

  int c;
  int r;
  int area;

  area = 0;

  for (r = 0; r < N; r++) {
    for (c = 0; c < N; c++) {
      if(input_image[r][c] == objValue) area++;
    }
  }


  return area;
}
