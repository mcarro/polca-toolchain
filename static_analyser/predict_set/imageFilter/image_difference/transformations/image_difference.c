// FUNC_ANALYZ: convolve2d


// Added just for measure time
#include <sys/time.h>
#define PROFILING 1


#include "traps.h"

#define         K       3
#define         N       902

int image[N][N];
int template[N][N];
int output[N][N];
int filter[K][K];
void convolve2d(int input_image[N][N], int kernel[K][K], int output_image[N][N]);
main()
{
  int temp1;
  int temp2;
  int temp3;
  int i;
  int j;


  /* Read input image. */

  input_dsp(image, N*N, 1);

#if PROFILING == 1
  struct timeval tvalBefore, tvalAfter;
  long total;
  int SAMPLES = 30;
 for(int sample=0;sample<SAMPLES;sample++){
	  gettimeofday(&tvalBefore,NULL);
#endif


/* Set the values of the filter matrix to a Gaussian kernel.
   This is used as a low-pass filter which blurs the image so as to
   de-emphasize the response of some isolated points to the edge
   detection (Sobel) kernels. */

  filter[0][0] = 1;
  filter[0][1] = 2;
  filter[0][2] = 1;
  filter[1][0] = 2;
  filter[1][1] = 4;
  filter[1][2] = 2;
  filter[2][0] = 1;
  filter[2][1] = 2;
  filter[2][2] = 1;

  convolve2d(image, filter, template);


  for (i = 0; i < N - 1; i++) {
    for (j = 0; j < N - 1; j++) {
      output[i][j] = abs(image[i][j] - template[i][j]);
    }
  }

#if PROFILING == 1
	  gettimeofday(&tvalAfter,NULL);
	  total = ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L+tvalAfter.tv_usec) - tvalBefore.tv_usec;
	  /* printf("it: %i, time: %ld microseconds\n",sample,total); */
	  printf("%ld\n",total);
 } // End of loop for time samples

#endif


  /* Store binary image. */
  output_dsp(output, N*N, 1);
}


void convolve2d(int input_image[N][N], int kernel[K][K], int output_image[N][N])
{
  int i;
  int j;
  int c;
  int r;
  int normal_factor;
  int sum;
  int dead_rows;
  int dead_cols;

  /* Set the number of dead rows and columns. These represent the band of rows
     and columns around the edge of the image whose pixels must be formed from
     less than a full kernel-sized compliment of input image pixels. No output
     values for these dead rows and columns since  they would tend to have less
     than full amplitude values and would exhibit a "washed-out" look known as
     convolution edge effects. */

  dead_rows = K / 2;
  dead_cols = K / 2;

  /* Calculate the normalization factor of the kernel matrix. */

  normal_factor = 0;
  for (r = 0; r < K; r++) {
    for (c = 0; c < K; c++) {
      normal_factor += abs(kernel[r][c]);
    }
  }

  if (normal_factor == 0)
    normal_factor = 1;

  /* Convolve the input image with the kernel. */
  for (r = 0; r < N - K + 1; r++) {
    for (c = 0; c < N - K + 1; c++) {
      sum = 0;
      for (i = 0; i < K; i++) {
        for (j = 0; j < K; j++) {
          sum += input_image[r+i][c+j] * kernel[i][j];
        }
      }
      output_image[r+dead_rows][c+dead_cols] = (sum / normal_factor);
    }
  }
}
