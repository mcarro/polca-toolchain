// Simple compute kernel
//

#define         K       3
#define         N       902


__kernel void convolve2d(
   __global int* input,
   __global int* output)
{

    __local filter[K*K];
    __local block[K*K];

    int i,j;
    int normal_factor;
    int sum;
  int dead_rows;
  int dead_cols;

    int gId = get_global_id(0);
    int lId = get_local_id(0);

    filter[0*K+0] = 1;
    filter[0*K+1] = 2;  
    filter[0*K+2] = 1;
    filter[1*K+0] = 2;
    filter[1*K+1] = 4;
    filter[1*K+2] = 2;
    filter[2*K+0] = 1;
    filter[2*K+1] = 2;
    filter[2*K+2] = 1;


    block[lId] = input[gId];

    if(lId == 0){
  dead_rows = K / 2;
  dead_cols = K / 2;

        normal_factor = 0;
    	for(i=0;i<K;i++) {
	    for(j=0;j<K;j++) {
	        normal_factor += abs(filter[i*K+j]);
	    }
	}

        if (normal_factor == 0)
            normal_factor = 1;

        sum = 0;
        for (i = 0; i < K; i++) {
          for (j = 0; j < K; j++) {
            sum += block[i*K+j] * filter[i*K+j];
          }
        }
        output[gId] = (sum / normal_factor);

    }

   
}