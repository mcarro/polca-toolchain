// FUNC_ANALYZ: main
// FEAT_VECTOR: [3, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 4, 0, 6, 0]
// TEST_VECTOR: [3, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 4, 0, 6, 0]
// TEST_LABEL: -1 (NONE)

//# maxForStmtDepth:             3
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              1
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       1
//# numNon1Darray:               4
//# numAuxVarArrayIndex:         0
//# totalNumForLoops:            6
//# numNonNormalizedForLoops:    0


// Added just for measure time
#include <sys/time.h>
#define PROFILING 1

#define         N               128
#define         B               3
#define         S               N*3*3

float object[S][S];               /* 2D array that stores the 3D object */
float block[B][B];                /* 2D array that stores an object block. */
float rotX[B][B];                 /* 2D array that stores X rot matrix */
float rotY[B][B];                 /* 2D array that stores Y rot matrix */
float temp2d[B][B];               /* Temporary 2D array. */
float result[S][S];               /* Result array */

void rotate(float block[B][B]);
void loadObject(float *object,int width);
void storeBlock(int m,int n,float block[B][B]);

main()
{
  float xAngle;
  float yAngle;
  float dumm;

  int k;
  int l;
  int m;
  int n;


#if PROFILING == 1
  struct timeval tvalBefore, tvalAfter;
  long total;
  int SAMPLES = 30;
 for(int sample=0;sample<SAMPLES;sample++){
	  gettimeofday(&tvalBefore,NULL);

#endif


  /* Read the object */

  loadObject(object,S);


  /* Initialize the rotation matrices. */
  xAngle = 90.0;
  yAngle = 45.0;
  dumm = 0;
  for (m = 0; m < B; ++m) {
    for (n = 0; n < B; ++n) {
      rotX[m][n] = (((m*B+n)==0)*1) + (((m*B+n)==4)*cos(xAngle)) + (((m*B+n)==5)*-sin(xAngle)) + (((m*B+n)==7)*sin(xAngle)) + (((m*B+n)==8)*cos(xAngle));
      rotY[n][m] =  (((m*B+n)==4)*1) + (((m*B+n)==0)*cos(xAngle)) + (((m*B+n)==6)*-sin(xAngle)) + (((m*B+n)==2)*sin(xAngle)) + (((m*B+n)==8)*cos(xAngle));
    }
    dumm += yAngle;
  }


  for (m = 0; m < N; m++) {

    for(n = 0; n < N; n++) {

      /* Read next object block. */
      for (k = 0; k < B; k++) {
        for (l = 0; l < B; l++) {
          block[k][l] = object[B*m+k][B*n+l];
        }
      }


      /* Rotate */

      rotate(block);

      storeBlock(m,n,block);

    }

  }

#if PROFILING == 1
	  gettimeofday(&tvalAfter,NULL);
	  total = ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L+tvalAfter.tv_usec) - tvalBefore.tv_usec;
	  /* printf("it: %i, time: %ld microseconds\n",sample,total); */
	  printf("%ld\n",total);
 } // End of loop for time samples

#endif



}




/* This function rotates over X and Y the input object  block.
   The result is returned in the same matrix as the original block. */

void rotate(float block[B][B])
{
  int i;
  int j;
  int k;
  float sum;

  /* Multiply the input object block with the rotX matrix; store the result
     in the temporary matrix "temp2d". */

  for (i = 0; i < B; i++) {
    for (j = 0; j < B; j++) {
      sum = 0.0;
      for (k = 0; k < B; k++) {
        sum += block[i][k] * rotX[k][j];
      }
      temp2d[i][j] = sum;
    }
  }


  /* Multiply the rotY matrix by the temporary matrix; store the
     result back in the original matrix.  */

  for (i = 0; i < B; i++) {  
    for (j = 0; j < B; j++) {
      sum = 0.0;
      for (k = 0; k < B; k++) {
        sum += temp2d[i][k] * rotY[k][j] ;
      }
      block[i][j] = sum;
    }
  }

}

void loadObject(float *object,int width){
  int i;

  for(i=0;i<width*width;i++)
  {
    object[i] = 1.0;
  }

}

void storeBlock(int m,int n,float block[B][B]){

  int k,l;

  for (k = 0; k < B; k++) {
    for (l = 0; l < B; l++) {
      result[B*m+k][B*n+l] = block[k][l];
    }
  }

}
