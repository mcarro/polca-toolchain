#!/usr/bin/python

import os,re

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
# import itertools
from matplotlib import colors

sequences = []

def processTimes():
    f = open("times.txt")
    lines = f.readlines()

    numSamples = 0
    totalTime  = 0
    for line in lines:
        m1 = re.match("it: \d+, time: (\d+) microseconds",line)
        if m1:
            # print m1.group(1)
            totalTime  += int(m1.group(1))
            numSamples +=1

    f.close()

    return(totalTime/numSamples)

def autolabel(ax,rects,heightFactor):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., heightFactor*height,
                '%.2f' % height,
                ha='center', va='bottom')

def plotSequences(programNames,programTimes):

    
    colorNames = [name for name in colors.cnames.keys()]
    # print colorNames
    numBars = 2


    N = len(programNames)

    # ind = np.array(ind)

    ind = np.arange(N)
    # ind *= 2
    for pos in range(4,len(programNames)):
        ind[pos] = float(ind[pos]) + 1

    width = 0.35       # the width of the bars
    # fig = plt.figure(figsize=(5,6),facecolor='w') 
    # ax = fig.add_subplot(111)
    fig, ax = plt.subplots()
    labels = []
    count = 0

    

    maxYValue = -1
    programId = 0

    handValues = []
    autoValues = []

    for sublist in programTimes:
        for value in sublist[1:len(sublist)]:

            speedUp = float(sublist[0])/float(value)
            if (count%numBars) == 0:
                handValues.append(speedUp)
            else:
                autoValues.append(speedUp)

            if speedUp > maxYValue:
                maxYValue = speedUp

            count += 1
        programId += 1


    # print("%d / %d = %f" % (float(sublist[0]),float(value),float(sublist[0])/float(value)))
    
    rects1 = ax.bar(ind, handValues, width, color="c")

    rects2 = ax.bar(ind+width, autoValues, width, color="g")

    # autolabel(ax,rects1,1.1)
    # autolabel(ax,rects2,1.05)

    font0 = FontProperties()
    font0.set_size('medium')
    # ax.legend((rects1[0], rects2[0]), ('Hand', 'Auto'),loc='center left', bbox_to_anchor=(1, 0.5),prop=font0)
    # ax.legend((rects1[0], rects2[0]), ('Hand', 'Auto'),loc='upper right',prop=font0)
    ax.legend((rects1[0], rects2[0]), ('Hand', 'Auto'),loc='upper right', bbox_to_anchor=(1.05, 1.25),prop=font0)

    xCoord = 4.35
    yCoord = maxYValue + 0.2
    plt.annotate("",xy=(xCoord, 0.1), arrowprops=dict(arrowstyle='-'), xytext=(xCoord, yCoord-0.1))

    plt.annotate("Train Set",xy=(0, yCoord), arrowprops=dict(), xytext=((3-0)/2, yCoord+0.1))
    plt.annotate("",xy=(0, yCoord), arrowprops=dict(arrowstyle='-'), xytext=(4, yCoord))

    xCoord += 0.5

    plt.annotate("Predict Set",xy=(0, yCoord), arrowprops=dict(), xytext=(5.2, yCoord+0.1))
    plt.annotate("",xy=(xCoord, yCoord), arrowprops=dict(arrowstyle='-'), xytext=(7.7, yCoord))


    # add some text for labels, title and axes ticks
    ax.set_ylabel(r'SpeedUp')
    ax.set_ylim([0,maxYValue+0.7])
    ax.set_title('SpeedUp for all use cases')
    ax.set_xticks(ind)
    ax.set_xticklabels(programNames,rotation=55,x=width)
    ax.set_xlim([-width,ind[len(ind)-1] + 1])

    box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 1.235, box.height])
    ax.set_position([box.x0, box.y0, box.width, box.height * 1.07])
    # print ind

    fig.tight_layout()
    fig.savefig('./allSpeedUp.png',dpi=300)

    plt.show()


###########################################################

# programs = ["compress_gpu_2_14_0","compress_gpu_2_14_1","compress_gpu_6_14_0","compress_gpu_6_14_1"]
# programs = ["compress_gpu_1_10_1","compress_gpu_5_10_1","compress_gpu_2_14_1","compress_gpu_6_14_1"]
programs = []

programNames = ["compress"]
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes = [[1078,397,426]]

programNames.append("rgbFilter")
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes.append([41279,14230,14450])

programNames.append("edgeDetect")
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes.append([1814,986,1232])


programNames.append("threshold")
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes.append([59715,24964,25337])

programNames.append("rotate3D")
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes.append([9388,3271,3667])

programNames.append("imageDiff")
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes.append([87548,33777,41266])

programNames.append("brightness")
#Values provided as list for: [Original_code,Hand_Coded,Automatic_coded]
programTimes.append([73180,24605,24802])

# Execute all programs only once
# for program in programs:
#     cmd = "./%s.x > times.txt" % program
#     # print(cmd)
#     os.system(cmd)

#     if not (program in programNames):
#         programNames.append(program)
#         execTime = processTimes()
#         programTimes.append(execTime)
#         print("%s - %d" % (program,execTime))
#         # programTimesDict[program] = processTimes()



plotSequences(programNames,programTimes)

# cmd = "rm times.txt"
# print(cmd)
# os.system(cmd)
