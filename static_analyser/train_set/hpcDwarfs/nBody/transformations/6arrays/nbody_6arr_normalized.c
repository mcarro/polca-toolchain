
// Obtained from: https://github.com/harrism/mini-nbody

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"

#define SOFTENING 1e-9f

/* typedef struct { float x, y, z, vx, vy, vz; } Body; */

void randomizeBodies(float *x,float *y,float *z,float *vx,float *vy,float *vz, int n) {
  for (int i = 0; i < n; i++) {
    x[i]  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    y[i]  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    z[i]  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    vx[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    vy[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    vz[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
  }
}

void bodyForce(float *x,float *y,float *z,float *vx,float *vy,float *vz, float dt, int n) {
  for (int i = 0; i < n; i++) {
    float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

    for (int j = 0; j < n; j++) {

      float dx = x[j] - x[i];
      float dy = y[j] - y[i];
      float dz = z[j] - z[i];
      float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
      float invDist = 1.0f / sqrtf(distSqr);
      /* float invDist = 1.0f / distSqr; */

      float invDist3 = invDist * invDist * invDist;
      /* float invDist3 = distSqr * distSqr * distSqr; */

      /* if(i==24 && j==24) printf("%f  %f  %f\n",distSqr,invDist,invDist3); */
      /* if(i==8) printf("\n%d: %f + (%f,%f,%f) * %f\n",j,Fx,dx,dy,dz,distSqr); */
      /* if(i==8) printf("%d: %.15f %.15f %.15f %.15f\n",j,distSqr,invDist,invDist2,invDist3); */

      Fx += dx * invDist3; Fy += dy * invDist3; Fz += dz * invDist3;
      /* Fx = x[i]; Fy = y[i]; Fz = z[i]; */
    }
    /* if(i==24) printf("%d: %f + %f * %f\n",i,vx[i],dt,Fx); */
    vx[i] += dt*Fx; vy[i] += dt*Fy; vz[i] += dt*Fz;
  }
}

int main(const int argc, const char** argv) {
  
  int nBodies;
  int dataType;
  /* if (argc > 1) nBodies = atoi(argv[1]); */

  const float dt = 0.01f; // time step
  const int nIters = ITERS;  // simulation iterations

  /* int bytes = nBodies*sizeof(Body); */
  /* float *buf = (float*)malloc(bytes); */
  /* Body *p = (Body*)buf; */
  
  float *x, *y, *z, *vx, *vy, *vz;

  input_arr_arg("../../data/x_input.dat",  &x,&nBodies,&dataType);
  input_arr_arg("../../data/y_input.dat",  &y,&nBodies,&dataType);
  input_arr_arg("../../data/z_input.dat",  &z,&nBodies,&dataType);
  input_arr_arg("../../data/vx_input.dat",&vx,&nBodies,&dataType);
  input_arr_arg("../../data/vy_input.dat",&vy,&nBodies,&dataType);
  input_arr_arg("../../data/vz_input.dat",&vz,&nBodies,&dataType);

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif
  for (int iter = 1; iter <= nIters; iter++) {
    /* printf("%d: %f\n",nBodies,x[0]); */
    bodyForce(x,y,z,vx,vy,vz,dt, nBodies); // compute interbody forces

    for (int i = 0 ; i < nBodies; i++) { // integrate position
      x[i] += vx[i]*dt;
      y[i] += vy[i]*dt;
      z[i] += vz[i]*dt;
    }

  }
#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

 output_arr_arg("../../data/x_ref.dat",   x, nBodies, dataType);
 output_arr_arg("../../data/y_ref.dat",   y, nBodies, dataType);
 output_arr_arg("../../data/z_ref.dat",   z, nBodies, dataType);
 output_arr_arg("../../data/vx_ref.dat", vx, nBodies, dataType);
 output_arr_arg("../../data/vy_ref.dat", vy, nBodies, dataType);
 output_arr_arg("../../data/vz_ref.dat", vz, nBodies, dataType);

  free(x);
  free(y);
  free(z);
  free(vx);
  free(vy);
  free(vz);
}
