
// Obtained from: https://github.com/harrism/mini-nbody

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"

#define SOFTENING 1e-9f

/* typedef struct { float x, y, z, vx, vy, vz; } Body; */

void randomizeBodies(float *x,float *y,float *z,float *vx,float *vy,float *vz, int n) {
  for (int i = 0; i < n; i++) {
    x[i]  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    y[i]  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    z[i]  = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    vx[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    vy[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    vz[i] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
  }
}

void bodyForce(float *x,float *y,float *z,float *vx,float *vy,float *vz, float dt, int n) {
  for (int i = 0; i < n; i++) {
    float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

    for (int j = 0; j < n; j++) {
      float dx = x[j] - x[i];
      float dy = y[j] - y[i];
      float dz = z[j] - z[i];
      float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
      float invDist = 1.0f / sqrtf(distSqr);
      float invDist3 = invDist * invDist * invDist;

      Fx += dx * invDist3; Fy += dy * invDist3; Fz += dz * invDist3;
    }
    /* printf("%d: %f %f %f %f\n",n,dt,Fx,vx[i],vx[i] + dt*Fx); */
    vx[i] += dt*Fx; vy[i] += dt*Fy; vz[i] += dt*Fz;
    /* printf("%d: %f\n",n,vx[i]); */
    /* printf("%d:\n",i); */
  }
}

int main(const int argc, const char** argv) {
  
  int nBodies;
  int dataType;
  /* if (argc > 1) nBodies = atoi(argv[1]); */

  const float dt = 0.01f; // time step
  const int nIters = ITERS;  // simulation iterations

  /* int bytes = nBodies*sizeof(Body); */
  /* float *buf = (float*)malloc(bytes); */
  /* Body *p = (Body*)buf; */
  
  float *x, *y, *z, *vx, *vy, *vz;

  input_arr_arg("../../data/x_input.dat",  &x,&nBodies,&dataType);
  input_arr_arg("../../data/y_input.dat",  &y,&nBodies,&dataType);
  input_arr_arg("../../data/z_input.dat",  &z,&nBodies,&dataType);
  input_arr_arg("../../data/vx_input.dat",&vx,&nBodies,&dataType);
  input_arr_arg("../../data/vy_input.dat",&vy,&nBodies,&dataType);
  input_arr_arg("../../data/vz_input.dat",&vz,&nBodies,&dataType);

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif
  for (int iter = 1; iter <= nIters; iter++) {

    float *Fx = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *Fy = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *Fz = (float*) malloc(sizeof(float) *nBodies*nBodies);

    float *dx = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *dy = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *dz = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *distSqr = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *invDist = (float*) malloc(sizeof(float) *nBodies*nBodies);
    float *invDist3 = (float*) malloc(sizeof(float) *nBodies*nBodies);


    for (int t = 0; t < nBodies*nBodies; t++) {
      int i = t/nBodies;
      int j = t%nBodies;

      int iDFE = 5 * i + 1 * j + 0;
      int jDFE = 6 * i + 0 * j + 0;

      dx[t] = x[t-jDFE] - x[t-iDFE];
      dy[t] = y[t-jDFE] - y[t-iDFE];
      dz[t] = z[t-jDFE] - z[t-iDFE];
      distSqr[t] = dx[t-0]*dx[t-0] + dy[t-0]*dy[t-0] + dz[t-0]*dz[t-0] + SOFTENING;
      invDist[t] = 1.0f / sqrtf(distSqr[t-0]);
      invDist3[t] = invDist[t-0] * invDist[t-0] * invDist[t-0];

      Fx[t] = (j==0 ? 0.0f : Fx[t-1]) + dx[t-0] * invDist3[t-0];
      Fy[t] = (j==0 ? 0.0f : Fy[t-1]) + dy[t-0] * invDist3[t-0];
      Fz[t] = (j==0 ? 0.0f : Fz[t-1]) + dz[t-0] * invDist3[t-0];

      vx[i] = j==nBodies-1 ? vx[i] + dt*Fx[t-0] : vx[i];
      vy[i] = j==nBodies-1 ? vy[i] + dt*Fy[t-0] : vy[i];
      vz[i] = j==nBodies-1 ? vz[i] + dt*Fz[t-0] : vz[i];

    }

    for (int i = 0 ; i < nBodies; i++) { // integrate position
      x[i] += vx[i]*dt;
      y[i] += vy[i]*dt;
      z[i] += vz[i]*dt;
    }
    /* printf("%d: %f\n",nBodies,vx[0]); */
    /* printf("%d: %f\n",nBodies,x[0]); */
  }
#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

 output_arr_arg("../../data/x_out.dat",   x, nBodies, dataType);
 output_arr_arg("../../data/y_out.dat",   y, nBodies, dataType);
 output_arr_arg("../../data/z_out.dat",   z, nBodies, dataType);
 output_arr_arg("../../data/vx_out.dat", vx, nBodies, dataType);
 output_arr_arg("../../data/vy_out.dat", vy, nBodies, dataType);
 output_arr_arg("../../data/vz_out.dat", vz, nBodies, dataType);

  free(x);
  free(y);
  free(z);
  free(vx);
  free(vy);
  free(vz);

}
