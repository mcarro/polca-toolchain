
#include <stdio.h>

#define SIZE 6

void main() {

  int arr[SIZE];
  int i,j,t;

  for(i=0;i<SIZE;i++)
    arr[i] = i;

  for(i=0;i<SIZE;i++){
    for(j=0;j<SIZE;j++) {
      printf("%3d, ",arr[j]-arr[i]);
    }
    printf("\n");
  }

  printf("\n\n");

  for(t=0;t<SIZE*SIZE;t++){

      int i = t/SIZE;
      int j = t%SIZE;

      int aj = arr[t-(i*SIZE)];
      int ai = arr[t-(i*(SIZE-1)+j)];
      
      printf("%3d, ",aj - ai);

      if(j==(SIZE-1))
	printf("\n");
  }

}
