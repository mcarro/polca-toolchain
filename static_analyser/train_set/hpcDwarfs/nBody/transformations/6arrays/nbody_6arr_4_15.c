
// Obtained from: https://github.com/harrism/mini-nbody

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"

#define SOFTENING 1e-9f


void bodyForce(float *x,float *y,float *z,float *vx,float *vy,float *vz, float dt, int n) {
  for (int i = 0; i < n; i++) {
    float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

    for (int j = 0; j < n; j++) {
      float dx = x[j] - x[i];
      float dy = y[j] - y[i];
      float dz = z[j] - z[i];
      float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
      float invDist = 1.0f / sqrtf(distSqr);
      float invDist3 = invDist * invDist * invDist;

      Fx += dx * invDist3; Fy += dy * invDist3; Fz += dz * invDist3;
    }
    /* printf("%d: %f %f %f %f\n",n,dt,Fx,vx[i],vx[i] + dt*Fx); */
    vx[i] += dt*Fx; vy[i] += dt*Fy; vz[i] += dt*Fz;
    /* printf("%d: %f\n",n,vx[i]); */
    /* printf("%d:\n",i); */
  }
}

int main(const int argc, const char** argv) {
  
  int nBodies;
  int dataType;

  const float dt = 0.01f; // time step
  const int nIters = ITERS;  // simulation iterations
  
  float *x, *y, *z, *vx, *vy, *vz;

  input_arr_arg("../../data/x_input.dat",  &x,&nBodies,&dataType);
  input_arr_arg("../../data/y_input.dat",  &y,&nBodies,&dataType);
  input_arr_arg("../../data/z_input.dat",  &z,&nBodies,&dataType);
  input_arr_arg("../../data/vx_input.dat",&vx,&nBodies,&dataType);
  input_arr_arg("../../data/vy_input.dat",&vy,&nBodies,&dataType);
  input_arr_arg("../../data/vz_input.dat",&vz,&nBodies,&dataType);

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif
  for (int iter = 1; iter <= nIters; iter++) {

    float Fx,Fy,Fz;
    for (int t = 0; t < nBodies*nBodies; t++) {
      int i = t/nBodies;
      int j = t%nBodies;

      float dx = x[j] - x[i];
      float dy = y[j] - y[i];
      float dz = z[j] - z[i];
      float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
      float invDist = 1.0f / sqrtf(distSqr);
      float invDist3 = invDist * invDist * invDist;

      Fx = (j==0 ? 0.0f : Fx) + dx * invDist3;
      Fy = (j==0 ? 0.0f : Fy) + dy * invDist3;
      Fz = (j==0 ? 0.0f : Fz) + dz * invDist3;

      vx[i] = j==nBodies-1 ? vx[i] + dt*Fx : vx[i];
      vy[i] = j==nBodies-1 ? vy[i] + dt*Fy : vy[i];
      vz[i] = j==nBodies-1 ? vz[i] + dt*Fz : vz[i];

    }

    for (int i = 0 ; i < nBodies; i++) { // integrate position
      x[i] += vx[i]*dt;
      y[i] += vy[i]*dt;
      z[i] += vz[i]*dt;
    }

  }
#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

 output_arr_arg("../../data/x_out.dat",   x, nBodies, dataType);
 output_arr_arg("../../data/y_out.dat",   y, nBodies, dataType);
 output_arr_arg("../../data/z_out.dat",   z, nBodies, dataType);
 output_arr_arg("../../data/vx_out.dat", vx, nBodies, dataType);
 output_arr_arg("../../data/vy_out.dat", vy, nBodies, dataType);
 output_arr_arg("../../data/vz_out.dat", vz, nBodies, dataType);

  free(x);
  free(y);
  free(z);
  free(vx);
  free(vy);
  free(vz);
}
