#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "params.h"

int check_output(float *ref,float *out,int nElems) {

  int nErrors = 0;

  for(int i=0;i<nElems;i++){
    if(fabs(ref[i]-out[i]) > FP_TOLERANCE) {
      printf("\tError: %d - %.16f != %.16f\n",i,ref[i],out[i]);
      nErrors++;
    }
    /* else */
    /*   printf("\tOK: %d - %.16f != %.16f\n",i,ref[i],out[i]);       */
  }

  return nErrors;

}

void main() {

  int nBodies1,dataType1;
  int nBodies2,dataType2;
  float *ref,*out;
  int nErrors;

  int nFiles = 6;
  const char* const refFiles[] = { "../../data/x_ref.dat", 
				   "../../data/y_ref.dat",
				   "../../data/z_ref.dat",
				   "../../data/vx_ref.dat",
				   "../../data/vy_ref.dat",
				   "../../data/vz_ref.dat"};

  const char* const outFiles[] = { "../../data/x_out.dat",
				   "../../data/y_out.dat",
				   "../../data/z_out.dat",
				   "../../data/vx_out.dat",
				   "../../data/vy_out.dat",
				   "../../data/vz_out.dat"};

  for(int i=0;i<nFiles;i++) {
    printf("Comparing file %s\n",refFiles[i]);
    printf("     with file %s\n",outFiles[i]);
    input_arr_arg(refFiles[i],  &ref,&nBodies1,&dataType1);
    input_arr_arg(outFiles[i],  &out,&nBodies2,&dataType2);
    assert(nBodies1 == nBodies2 && dataType1 == dataType2);
    nErrors = check_output(ref,out,nBodies1);
    if(!nErrors) printf("test OK!\n");
    printf("------------------------------------------------\n");
  }

}
