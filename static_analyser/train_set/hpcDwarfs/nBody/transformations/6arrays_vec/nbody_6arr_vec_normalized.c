
// Obtained from: https://github.com/harrism/mini-nbody

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
// Check included file params.h for code constants
#include "params.h"

float dx[PROBLEM_SIZE][PROBLEM_SIZE];
float dy[PROBLEM_SIZE][PROBLEM_SIZE];
float dz[PROBLEM_SIZE][PROBLEM_SIZE];
float Fx[PROBLEM_SIZE];
float Fy[PROBLEM_SIZE];
float Fz[PROBLEM_SIZE];


// Initialize force for each body
void initForces(float Fx[PROBLEM_SIZE],
		float Fy[PROBLEM_SIZE],
		float Fz[PROBLEM_SIZE],
		int n) {

  for (int i = 0; i < n; i++) {
    Fx[i] = 0.0f;
    Fy[i] = 0.0f;
    Fz[i] = 0.0f;
  }


}

// compute distance from each body to rest of bodies
void computeDist(float x[PROBLEM_SIZE],
		 float y[PROBLEM_SIZE],
		 float z[PROBLEM_SIZE],
		 float dx[PROBLEM_SIZE][PROBLEM_SIZE],
		 float dy[PROBLEM_SIZE][PROBLEM_SIZE],
		 float dz[PROBLEM_SIZE][PROBLEM_SIZE],
		 int n) {

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {

      dx[i][j] = x[j] - x[i];
      dy[i][j] = y[j] - y[i];
      dz[i][j] = z[j] - z[i];
    }
  }


}

// update per-body force using distances to rest of bodies
void updateForces(float Fx[PROBLEM_SIZE],
		  float Fy[PROBLEM_SIZE],
		  float Fz[PROBLEM_SIZE],
		  float dx[PROBLEM_SIZE][PROBLEM_SIZE],
		  float dy[PROBLEM_SIZE][PROBLEM_SIZE],
		  float dz[PROBLEM_SIZE][PROBLEM_SIZE],
		  int n) {

  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {

      float distSqr = dx[i][j]*dx[i][j] + dy[i][j]*dy[i][j] + dz[i][j]*dz[i][j] + SOFTENING;
      float invDist = 1.0f / sqrtf(distSqr);

      float invDist3 = invDist * invDist * invDist;

      Fx[i] = Fx[i] + dx[i][j] * invDist3;
      Fy[i] = Fy[i] + dy[i][j] * invDist3;
      Fz[i] = Fz[i] + dz[i][j] * invDist3;
    }
  }


}

// update velocity of each body using computed forces
void updateVel(float vx[PROBLEM_SIZE],
	       float vy[PROBLEM_SIZE],
	       float vz[PROBLEM_SIZE],
	       float Fx[PROBLEM_SIZE],
	       float Fy[PROBLEM_SIZE],
	       float Fz[PROBLEM_SIZE],
	       float dt,
	       int n) {

  for (int i = 0; i < n; i++) {
    vx[i] += dt*Fx[i];
    vy[i] += dt*Fy[i];
    vz[i] += dt*Fz[i];
  }

}

// function in charge of:
// 1. init forces to 0.0
// 2. compute distances among bodies
// 3. update each body force
// 4. update each body velocity
void bodyForce(float x[PROBLEM_SIZE],
	       float y[PROBLEM_SIZE],
	       float z[PROBLEM_SIZE],
	       float vx[PROBLEM_SIZE],
	       float vy[PROBLEM_SIZE],
	       float vz[PROBLEM_SIZE],
	       float dt,
	       int n) {

  initForces(Fx,Fy,Fz,n); // init forces
  
  computeDist(x,y,z,dx,dy,dz,n);   // compute distances

  updateForces(Fx,Fy,Fz,dx,dy,dz,n);   //update forces

  updateVel(vx,vy,vz,Fx,Fy,Fz,dt,n);    // update velocity 

}

// compute each body new position using body velocity and time step
void integrate(float x[PROBLEM_SIZE],
	       float y[PROBLEM_SIZE],
	       float z[PROBLEM_SIZE],
	       float vx[PROBLEM_SIZE],
	       float vy[PROBLEM_SIZE],
	       float vz[PROBLEM_SIZE],
	       float dt,
	       int n) {
    for (int i = 0 ; i < n; i++) {
      x[i] += vx[i]*dt;
      y[i] += vy[i]*dt;
      z[i] += vz[i]*dt;
    }
}

int main(const int argc, const char** argv) {
  
  int nBodies;
  int dataType;

  const float dt = DT; // time step
  const int nIters = ITERS;  // simulation iterations
  
  float *x, *y, *z, *vx, *vy, *vz;

  input_arr_arg("../../data/x_input.dat",  &x,&nBodies,&dataType);
  input_arr_arg("../../data/y_input.dat",  &y,&nBodies,&dataType);
  input_arr_arg("../../data/z_input.dat",  &z,&nBodies,&dataType);
  input_arr_arg("../../data/vx_input.dat",&vx,&nBodies,&dataType);
  input_arr_arg("../../data/vy_input.dat",&vy,&nBodies,&dataType);
  input_arr_arg("../../data/vz_input.dat",&vz,&nBodies,&dataType);

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif
  for (int iter = 1; iter <= nIters; iter++) {
    bodyForce(x,y,z,vx,vy,vz,dt, nBodies); // compute interbody forces

    integrate(x,y,z,vx,vy,vz,dt, nBodies); // integrate position

  }
#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

 output_arr_arg("../../data/x_out.dat",   x, nBodies, dataType);
 output_arr_arg("../../data/y_out.dat",   y, nBodies, dataType);
 output_arr_arg("../../data/z_out.dat",   z, nBodies, dataType);
 output_arr_arg("../../data/vx_out.dat", vx, nBodies, dataType);
 output_arr_arg("../../data/vy_out.dat", vy, nBodies, dataType);
 output_arr_arg("../../data/vz_out.dat", vz, nBodies, dataType);

  free(x);
  free(y);
  free(z);
  free(vx);
  free(vy);
  free(vz);
}
