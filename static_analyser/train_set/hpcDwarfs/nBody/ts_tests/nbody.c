// RULE_APPLD: inlining
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"


typedef struct Triple Triple;

struct Triple {
    float x, y, z;
};

void bodyForce(struct Triple * pos, struct Triple * frc, int n)
    {
    int i, j;
    float d[3];
    float distSqr, invDist, invDist3;
#pragma polca map CALCFORCE pos frc
#pragma polca input pos
#pragma polca output frc
#pragma polca iteration_independent
    for (i = 0; i < n; i++)
        {
#pragma polca def CALCFORCE
#pragma polca input pos[i]
#pragma polca output frc[i]
    {
    frc[i].x = 0.0f;
    frc[i].y = 0.0f;
    frc[i].z = 0.0f;
#pragma polca fold ADDFORCE frc[i] pos frc[i]
    for (j = 0; j < n; j++)
        {
#pragma polca def ADDFORCE
#pragma polca input frc[i] pos[j]
#pragma polca output frc[i]
    {
    d[0] = pos[j].x - pos[i].x;
    d[1] = pos[j].y - pos[i].y;
    d[2] = pos[j].z - pos[i].z;
    distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + 1e-9f;
    invDist = 1.0f / sqrt(distSqr);
    invDist3 = invDist * invDist * invDist;
    frc[i].x = frc[i].x + d[0] * invDist3;
    frc[i].y = frc[i].y + d[1] * invDist3;
    frc[i].z = frc[i].z + d[2] * invDist3;
    }
    }
    }
    }
    }

void velocities(struct Triple * frc,
                struct Triple * vel,
                float dt,
                int n)
    {
    int i;
#pragma polca zipWith UPDV vel frc
#pragma polca input UPDV
#pragma polca input vel
#pragma polca output frc
#pragma polca iteration_independent
    for (i = 0; i < n; i++)
        {
#pragma polca def UPDV
#pragma polca input vel[i] frc[i]
#pragma polca output vel[i]
    {
    vel[i].x = vel[i].x + dt * frc[i].x;
    vel[i].y = vel[i].y + dt * frc[i].y;
    vel[i].z = vel[i].z + dt * frc[i].z;
    }
    }
    }

void integrate(struct Triple * pos,
               struct Triple * vel,
               float dt,
               int n)
    {
    int i;
#pragma polca zipWith UPDP pos vel
#pragma polca input UPDP
#pragma polca input pos
#pragma polca output vel
#pragma polca iteration_independent
    for (i = 0; i < n; i++)
        {
#pragma polca def UPDP
#pragma polca input pos[i] vel[i]
#pragma polca output pos[i]
    {
    pos[i].x = pos[i].x + vel[i].x * dt;
    pos[i].y = pos[i].y + vel[i].y * dt;
    pos[i].z = pos[i].z + vel[i].z * dt;
    }
    }
    }

int main(const int argc, const char * * argv)
    {
    int nBodies, nElems;
    int dataType;
    int i, iter;
    const float dt = DT;
    const int nIters = ITERS;
    float * pos, * vel;
    char fileName[80];
    sprintf(fileName, "%s/pos_input.dat", DATA_PATH);
    input_arr_arg(fileName, &pos, &nElems, &dataType);
    sprintf(fileName, "%s/vel_input.dat", DATA_PATH);
    input_arr_arg(fileName, &vel, &nElems, &dataType);
    nBodies = nElems / DIM;
    struct Triple pStruct[N];
    struct Triple vStruct[N];
    struct Triple fStruct[N];
    for (i = 0; i < nBodies; i++)
        {
    pStruct[i].x = pos[i * 3];
    pStruct[i].y = pos[i * 3 + 1];
    pStruct[i].z = pos[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
        {
    vStruct[i].x = vel[i * 3];
    vStruct[i].y = vel[i * 3 + 1];
    vStruct[i].z = vel[i * 3 + 2];
    }
#pragma polca itn UPD_FOR_VEL_POS (pStruct,fStruct) nIters (pStruct,fStruct)
    for (iter = 1; iter <= nIters; iter++)
        {
#pragma polca def UPD_FOR_VEL_POS
#pragma polca input (pStruct,fStruct)
#pragma polca output (pStruct,fStruct)
#pragma polca adapt openmp
#pragma polca def BLOCK_ABS
    {
    {
    int i, j;
    float d[3];
    float distSqr, invDist, invDist3;
#pragma polca map CALCFORCE pStruct fStruct
#pragma polca input pStruct
#pragma polca output fStruct
#pragma polca iteration_independent
    for (i = 0; i < nBodies; i++)
        {
#pragma polca def CALCFORCE
#pragma polca input pStruct[i]
#pragma polca output fStruct[i]
    {
    fStruct[i].x = 0.0f;
    fStruct[i].y = 0.0f;
    fStruct[i].z = 0.0f;
#pragma polca fold ADDFORCE fStruct[i] pStruct fStruct[i]
    for (j = 0; j < nBodies; j++)
        {
#pragma polca def ADDFORCE
#pragma polca input fStruct[i] pStruct[j]
#pragma polca output fStruct[i]
    {
    d[0] = pStruct[j].x - pStruct[i].x;
    d[1] = pStruct[j].y - pStruct[i].y;
    d[2] = pStruct[j].z - pStruct[i].z;
    distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + 1e-9f;
    invDist = 1.0f / sqrt(distSqr);
    invDist3 = invDist * invDist * invDist;
    fStruct[i].x = fStruct[i].x + d[0] * invDist3;
    fStruct[i].y = fStruct[i].y + d[1] * invDist3;
    fStruct[i].z = fStruct[i].z + d[2] * invDist3;
    }
    }
    }
    }
    }
    {
    int i;
#pragma polca zipWith UPDV vStruct fStruct
#pragma polca input UPDV
#pragma polca input vStruct
#pragma polca output fStruct
#pragma polca iteration_independent
    for (i = 0; i < nBodies; i++)
        {
#pragma polca def UPDV
#pragma polca input vStruct[i] fStruct[i]
#pragma polca output vStruct[i]
    {
    vStruct[i].x = vStruct[i].x + dt * fStruct[i].x;
    vStruct[i].y = vStruct[i].y + dt * fStruct[i].y;
    vStruct[i].z = vStruct[i].z + dt * fStruct[i].z;
    }
    }
    }
    {
    int i;
#pragma polca zipWith UPDP pStruct vStruct
#pragma polca input UPDP
#pragma polca input pStruct
#pragma polca output vStruct
#pragma polca iteration_independent
    for (i = 0; i < nBodies; i++)
        {
#pragma polca def UPDP
#pragma polca input pStruct[i] vStruct[i]
#pragma polca output pStruct[i]
    {
    pStruct[i].x = pStruct[i].x + vStruct[i].x * dt;
    pStruct[i].y = pStruct[i].y + vStruct[i].y * dt;
    pStruct[i].z = pStruct[i].z + vStruct[i].z * dt;
    }
    }
    }
    }
    }
    for (i = 0; i < nBodies; i++)
        {
    pos[i * 3] = pStruct[i].x;
    pos[i * 3 + 1] = pStruct[i].y;
    pos[i * 3 + 2] = pStruct[i].z;
    }
    for (i = 0; i < nBodies; i++)
        {
    vel[i * 3] = vStruct[i].x;
    vel[i * 3 + 1] = vStruct[i].y;
    vel[i * 3 + 2] = vStruct[i].z;
    }
    sprintf(fileName, "%s/pos_out.dat", DATA_PATH);
    output_arr_arg(fileName, pos, nBodies * DIM, dataType);
    sprintf(fileName, "%s/vel_out.dat", DATA_PATH);
    output_arr_arg(fileName, vel, nBodies * DIM, dataType);
    free(pos);
    free(vel);
    }

