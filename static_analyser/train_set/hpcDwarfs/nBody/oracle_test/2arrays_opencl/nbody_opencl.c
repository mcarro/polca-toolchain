#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"
#include "common_OpenCL.h"
const char kernel1Name[] = "nbody_kernel1.cl";
const char kernel1Func[] = "kernel1";
const char kernel2Name[] = "nbody_kernel2.cl";
const char kernel2Func[] = "kernel2";
const char compileFlags[] = "";

typedef struct Triple Triple;
struct Triple {
    float x, y, z;
};
void bodyForce(struct Triple * pos, struct Triple * frc, int n)
{
    int i, j;
    float d[3];
    float distSqr, invDist, invDist3;
    for (i = 0; i < n; i++)
    {
        {
            frc[i].x = 0.0f;
            frc[i].y = 0.0f;
            frc[i].z = 0.0f;
            for (j = 0; j < n; j++)
            {
                {
                    d[0] = pos[j].x - pos[i].x;
                    d[1] = pos[j].y - pos[i].y;
                    d[2] = pos[j].z - pos[i].z;
                    distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + 1e-9f;
                    invDist = 1.0f / sqrt(distSqr);
                    invDist3 = invDist * invDist * invDist;
                    frc[i].x = frc[i].x + d[0] * invDist3;
                    frc[i].y = frc[i].y + d[1] * invDist3;
                    frc[i].z = frc[i].z + d[2] * invDist3;
                }
            }
        }
    }
}
void velocities(struct Triple * frc,
                struct Triple * vel,
                float dt,
                int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        {
            vel[i].x = vel[i].x + dt * frc[i].x;
            vel[i].y = vel[i].y + dt * frc[i].y;
            vel[i].z = vel[i].z + dt * frc[i].z;
        }
    }
}
void integrate(struct Triple * pos,
               struct Triple * vel,
               float dt,
               int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        {
            pos[i].x = pos[i].x + vel[i].x * dt;
            pos[i].y = pos[i].y + vel[i].y * dt;
            pos[i].z = pos[i].z + vel[i].z * dt;
        }
    }
}
int main(const int argc, const char * * argv)
{
    int nBodies, nElems;
    int dataType;
    int i, iter;
    const float dt = DT;
    const int nIters = ITERS;
    float * pos, * vel;
    char fileName[80];
    sprintf(fileName, "%s/pos_input.dat", DATA_PATH);
    input_arr_arg(fileName, &pos, &nElems, &dataType);
    sprintf(fileName, "%s/vel_input.dat", DATA_PATH);
    input_arr_arg(fileName, &vel, &nElems, &dataType);
    nBodies = nElems / DIM;
    float pStruct[N * 3];
    float vStruct[N * 3];
    float fStruct[N * 3];
    for (i = 0; i < nBodies; i++)
    {
        pStruct[i * 3 + 0] = pos[i * 3];
        pStruct[i * 3 + 1] = pos[i * 3 + 1];
        pStruct[i * 3 + 2] = pos[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
    {
        vStruct[i * 3 + 0] = vel[i * 3];
        vStruct[i * 3 + 1] = vel[i * 3 + 1];
        vStruct[i * 3 + 2] = vel[i * 3 + 2];
    }
	size_t local;
	cl_kernel kernel1,kernel2;
	cl_int error = CL_SUCCESS;
	size_t global;
	cl_mem pStructDev;
	cl_mem fStructDev;
	unsigned int elemsInput;
	unsigned int sizeInput;
	cl_mem vStructDev;
	unsigned int elemsOutput;
	unsigned int sizeOutput;
	initOpenCLVars();
	selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);
	createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);
	createOpenCLQueue(deviceIds,context,&queue);
	createOpenCLKernel(kernel1Func,kernel1Name,compileFlags,deviceIds,context,&kernel1);
	createOpenCLKernel(kernel2Func,kernel2Name,compileFlags,deviceIds,context,&kernel2);
	elemsInput = N*3;
	sizeInput = sizeof(float) * elemsInput;
	pStructDev = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeInput, NULL, NULL);
	fStructDev = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeInput, NULL, NULL);
	elemsOutput = N*3;
	sizeOutput = sizeof(float) * elemsOutput;
	vStructDev = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeOutput, NULL, NULL);
	if (!pStructDev || !vStructDev || !fStructDev)
	{
		printf("Error: Failed to allocate device memory!\n");
		exit(1);
	}
	error  = 0;
	error  = clSetKernelArg(kernel1, 0, sizeof(cl_mem), &pStructDev);
	error |= clSetKernelArg(kernel1, 1, sizeof(cl_mem), &fStructDev);
	error |= clSetKernelArg(kernel1, 2, sizeof(cl_int), &nBodies);
#if DEBUG == 1
	if (error != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", error);
		exit(1);
	}
#endif
	error  = 0;
	error  = clSetKernelArg(kernel2, 0, sizeof(cl_mem), &pStructDev);
	error |= clSetKernelArg(kernel2, 1, sizeof(cl_mem), &vStructDev);
	error |= clSetKernelArg(kernel2, 2, sizeof(cl_mem), &fStructDev);
	error |= clSetKernelArg(kernel2, 3, sizeof(cl_float),  &dt);
	error |= clSetKernelArg(kernel2, 4, sizeof(cl_int), &nBodies);
#if DEBUG == 1
	if (error != CL_SUCCESS)
	{
		printf("Error: Failed to set kernel arguments! %d\n", error);
		exit(1);
	}
#endif
	error  = clEnqueueWriteBuffer(queue, pStructDev, CL_TRUE, 0, sizeInput, pStruct, 0, NULL, NULL);
	error  = clEnqueueWriteBuffer(queue, vStructDev, CL_TRUE, 0, sizeOutput, vStruct, 0, NULL, NULL);
#if DEBUG == 1
	if (error != CL_SUCCESS)
	{
		printf("Error: Failed to write to source array!\n");
		exit(1);
	}
#endif
    for (iter = 1; iter <= nIters; iter++)
    {
		local = 1;
		global = nBodies;
		error = clEnqueueNDRangeKernel(queue, kernel1, 1, NULL, &global, NULL, 0, NULL, NULL);
#if DEBUG == 1
		if (error)
		{
			printf("Error: Failed to execute kernel!\n");
			return EXIT_FAILURE;
		}
#endif
		error = clEnqueueNDRangeKernel(queue, kernel2, 1, NULL, &global, NULL, 0, NULL, NULL);
#if DEBUG == 1
		if (error)
		{
			printf("Error: Failed to execute kernel!\n");
			return EXIT_FAILURE;
		}
#endif
    }
	clFinish(queue);
	error =  clEnqueueReadBuffer( queue, pStructDev, CL_TRUE, 0, sizeOutput, pStruct, 0, NULL, NULL );
	error |= clEnqueueReadBuffer( queue, vStructDev, CL_TRUE, 0, sizeOutput, vStruct, 0, NULL, NULL );
#if DEBUG == 1
	if (error != CL_SUCCESS)
	{
		printf("Error: Failed to read output array! %d\n", error);
		exit(1);
	}
#endif
    for (i = 0; i < nBodies; i++)
    {
        pos[i * 3] = pStruct[i * 3 + 0];
        pos[i * 3 + 1] = pStruct[i * 3 + 1];
        pos[i * 3 + 2] = pStruct[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
    {
        vel[i * 3] = vStruct[i * 3 + 0];
        vel[i * 3 + 1] = vStruct[i * 3 + 1];
        vel[i * 3 + 2] = vStruct[i * 3 + 2];
    }
    sprintf(fileName, "%s/pos_out.dat", DATA_PATH);
    output_arr_arg(fileName, pos, nBodies * DIM, dataType);
    sprintf(fileName, "%s/vel_out.dat", DATA_PATH);
    output_arr_arg(fileName, vel, nBodies * DIM, dataType);
    free(pos);
    free(vel);
	clReleaseMemObject(pStructDev);
	clReleaseMemObject(vStructDev);
	clReleaseMemObject(fStructDev);
	clReleaseKernel(kernel1);
	clReleaseKernel(kernel2);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
}
