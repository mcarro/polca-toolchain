// FUNC_ANALYZ: main BLOCK_ABS
// FEAT_VECTOR: [-1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 3, 0]
// TEST_VECTOR: [-1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 3, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:            -1
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        0
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               0
//# numLoopHoistedVarMods:       0
//# numNon1Darray:               0
//# numAuxVarArrayIndex:         0
//# totalNumForLoops:            0
//# numNonNormalizedForLoops:    0
//# numStmtsRollUp:              0
//# numCompoundStmts:            1
//# anyTernaryOp:                0
//# anyUselessStmt:              0
//# numForPostambles:            0
//# numForPreambles:             0
//# numStructVarDecl:            3
//# numEmptyIf:                  0



// This is an adapted version of a code obtained from: https://github.com/harrism/mini-nbody

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"

typedef struct Triple Triple;

struct Triple { float x, y, z; };

// The original bodyForce traversed pos (the bodies' positions) and
// modified vel (the bodies' velocities), but this hardly fits any
// sensible definition for a map, so we refactorize the code so as to
// just obtain forces (anew) from positions...
void bodyForce(struct Triple *pos, struct Triple *frc, struct Triple *vel, float dt, int n) {
  int i,j;
  float d[3];
  float distSqr,invDist,invDist3;
  #pragma polca map CALCFORCE pos frc
  for(i = 0; i < n; i++) {
    #pragma polca def CALCFORCE
    #pragma polca input pos[i]
    #pragma polca output frc[i]
    {
      // we want the inner loop to perform a fold, so the neutral
      // element should be explicit... 
      // we initialize frc[i] to be the null vector   
      frc[i].x = 0.0f;
      frc[i].y = 0.0f;
      frc[i].z = 0.0f;

      #pragma polca fold ADDFORCE frc[i] pos frc[i]
      for(j = 0; j < n; j++) {
        #pragma polca def ADDFORCE
        #pragma polca input frc[i] pos[j]
        #pragma polca output frc[i] 
        {
	  if(i!=j){
            d[0] = pos[j].x - pos[i].x;
            d[1] = pos[j].y - pos[i].y;
            d[2] = pos[j].z - pos[i].z;
            distSqr = d[0]*d[0] + d[1]*d[1] + d[2]*d[2];
            invDist = 1.0f / sqrt(distSqr);
            invDist3 = invDist * invDist * invDist;

            frc[i].x = frc[i].x + d[0] * invDist3;
            frc[i].y = frc[i].y + d[1] * invDist3;
            frc[i].z = frc[i].z + d[2] * invDist3;
	  }
        } 
      } 
    }
  }
  int k;
#pragma polca zipWith UPDV vel frc
  for(k = 0; k < n; k++) {
#pragma polca def UPDV
#pragma polca input vel[k] frc[k]
#pragma polca output vel[k]
    {
    vel[k].x = vel[k].x + dt*frc[k].x;
    vel[k].y = vel[k].y + dt*frc[k].y;
    vel[k].z = vel[k].z + dt*frc[k].z;
    }
  }
  int l;
#pragma polca zipWith UPDP pos vel
    for(l = 0 ; l < n; l++) { // integrate position
#pragma polca def UPDP
#pragma polca input pos[l] vel[l]
#pragma polca output pos[l]
      {      
      pos[l].x = pos[l].x + vel[l].x*dt;
      pos[l].y = pos[l].y + vel[l].y*dt;
      pos[l].z = pos[l].z + vel[l].z*dt;
      }
    }

}  


int main(const int argc, const char** argv) {
  
  int nBodies,nElems;
  int dataType;
  int i,iter;

  const float dt   = DT;    // time step
  const int nIters = ITERS; // simulation iterations
  
  float *pos, *vel;

  char fileName[80];
  sprintf(fileName, "%s/pos_input.dat",DATA_PATH);
  input_arr_arg(fileName,&pos,&nElems,&dataType);
  sprintf(fileName, "%s/vel_input.dat",DATA_PATH);
  input_arr_arg(fileName,&vel,&nElems,&dataType);


  nBodies = nElems / DIM;
  struct Triple pStruct[N];
  struct Triple vStruct[N];
  struct Triple fStruct[N];
  //code for initializing linearized arrays into struct-based arrays
  for(i=0;i<nBodies;i++) {
	pStruct[i].x = pos[i*3];
	pStruct[i].y = pos[i*3+1];
	pStruct[i].z = pos[i*3+2];
  }
  for(i=0;i<nBodies;i++) {
	vStruct[i].x = vel[i*3];
	vStruct[i].y = vel[i*3+1];
	vStruct[i].z = vel[i*3+2];
  }

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif
#pragma polca itn UPD_FOR_VEL_POS (pStruct,fStruct) nIters (pStruct,fStruct)
  for(iter = 1; iter <= nIters; iter++) {
#pragma polca def UPD_FOR_VEL_POS
#pragma polca input (pStruct,fStruct)
#pragma polca output (pStruct,fStruct)
#pragma polca adapt opencl
#pragma polca def BLOCK_ABS
    {
      bodyForce(pStruct, fStruct, vStruct, dt, nBodies); // compute interbody forces
    }
  }
#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

  //code for copy back struct-based arrays into linearized arrays
  for(i=0;i<nBodies;i++) {
    pos[i*3]   = pStruct[i].x;
    pos[i*3+1] = pStruct[i].y;
    pos[i*3+2] = pStruct[i].z;
  }
  for(i=0;i<nBodies;i++) {
    vel[i*3]   = vStruct[i].x;
    vel[i*3+1] = vStruct[i].y;
    vel[i*3+2] = vStruct[i].z;
  }

  sprintf(fileName, "%s/pos_out.dat",DATA_PATH);
  output_arr_arg(fileName,pos,nBodies * DIM,dataType);
  sprintf(fileName, "%s/vel_out.dat",DATA_PATH);
  output_arr_arg(fileName,vel,nBodies * DIM,dataType);

  free(pos);
  free(vel);

}
