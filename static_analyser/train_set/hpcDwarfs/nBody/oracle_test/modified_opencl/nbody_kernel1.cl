__kernel void kernel1(
	__global float*   pStruct,
	__global float*   fStruct,
	const    int      nBodies)
    {
        int iv_i_0, iv_j_1;
        float iv_d_2[3];
        float iv_distSqr_3, iv_invDist_4, iv_invDist3_5;
        iv_i_0 = get_global_id(0);
        for (iv_j_1 = 0; iv_j_1 < nBodies; iv_j_1++)
        {
            fStruct[iv_i_0 * 3 + 0] = (iv_j_1 == 0) * 0.0f + (1 - (iv_j_1 == 0)) * fStruct[iv_i_0 * 3 + 0];
            fStruct[iv_i_0 * 3 + 1] = (iv_j_1 == 0) * 0.0f + (1 - (iv_j_1 == 0)) * fStruct[iv_i_0 * 3 + 1];
            fStruct[iv_i_0 * 3 + 2] = (iv_j_1 == 0) * 0.0f + (1 - (iv_j_1 == 0)) * fStruct[iv_i_0 * 3 + 2];
            iv_d_2[0] = (iv_i_0 != iv_j_1) * (pStruct[iv_j_1 * 3 + 0] - pStruct[iv_i_0 * 3 + 0]) + (1 - (iv_i_0 != iv_j_1)) * iv_d_2[0];
            iv_d_2[1] = (iv_i_0 != iv_j_1) * (pStruct[iv_j_1 * 3 + 1] - pStruct[iv_i_0 * 3 + 1]) + (1 - (iv_i_0 != iv_j_1)) * iv_d_2[1];
            iv_d_2[2] = (iv_i_0 != iv_j_1) * (pStruct[iv_j_1 * 3 + 2] - pStruct[iv_i_0 * 3 + 2]) + (1 - (iv_i_0 != iv_j_1)) * iv_d_2[2];
            iv_distSqr_3 = (iv_i_0 != iv_j_1) * (iv_d_2[0] * iv_d_2[0] + iv_d_2[1] * iv_d_2[1] + iv_d_2[2] * iv_d_2[2]) + (1 - (iv_i_0 != iv_j_1)) * iv_distSqr_3;
            iv_invDist_4 = (iv_i_0 != iv_j_1) * (1.0f / sqrt(iv_distSqr_3)) + (1 - (iv_i_0 != iv_j_1)) * iv_invDist_4;
            iv_invDist3_5 = (iv_i_0 != iv_j_1) * (iv_invDist_4 * iv_invDist_4 * iv_invDist_4) + (1 - (iv_i_0 != iv_j_1)) * iv_invDist3_5;
            fStruct[iv_i_0 * 3 + 0] = (iv_i_0 != iv_j_1) * (fStruct[iv_i_0 * 3 + 0] + iv_d_2[0] * iv_invDist3_5) + (1 - (iv_i_0 != iv_j_1)) * fStruct[iv_i_0 * 3 + 0];
            fStruct[iv_i_0 * 3 + 1] = (iv_i_0 != iv_j_1) * (fStruct[iv_i_0 * 3 + 1] + iv_d_2[1] * iv_invDist3_5) + (1 - (iv_i_0 != iv_j_1)) * fStruct[iv_i_0 * 3 + 1];
            fStruct[iv_i_0 * 3 + 2] = iv_i_0 != iv_j_1 ? fStruct[iv_i_0 * 3 + 2] + iv_d_2[2] * iv_invDist3_5 : fStruct[iv_i_0 * 3 + 2];
        }
    }