__kernel void kernel2(
	__global float* pStruct,
	__global float* vStruct,
	__global float* fStruct,
	const float dt,
	const int nBodies)
    {
        int iv_k_6;
        int iv_l_7;
        iv_l_7 = get_global_id(0);
        vStruct[iv_l_7 * 3 + 0] = vStruct[iv_l_7 * 3 + 0] + dt * fStruct[iv_l_7 * 3 + 0];
        vStruct[iv_l_7 * 3 + 1] = vStruct[iv_l_7 * 3 + 1] + dt * fStruct[iv_l_7 * 3 + 1];
        vStruct[iv_l_7 * 3 + 2] = vStruct[iv_l_7 * 3 + 2] + dt * fStruct[iv_l_7 * 3 + 2];
        {
            pStruct[iv_l_7 * 3 + 0] = pStruct[iv_l_7 * 3 + 0] + vStruct[iv_l_7 * 3 + 0] * dt;
            pStruct[iv_l_7 * 3 + 1] = pStruct[iv_l_7 * 3 + 1] + vStruct[iv_l_7 * 3 + 1] * dt;
            pStruct[iv_l_7 * 3 + 2] = pStruct[iv_l_7 * 3 + 2] + vStruct[iv_l_7 * 3 + 2] * dt;
        }
    }