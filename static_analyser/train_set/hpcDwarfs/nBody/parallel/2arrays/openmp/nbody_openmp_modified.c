// RULE_APPLD: remove_block
// FUNC_ANALYZ: main BLOCK_ABS
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"


typedef struct Triple Triple;

struct Triple {
  float x, y, z;
};

void bodyForce(struct Triple * pos,
               struct Triple * frc,
               struct Triple * vel,
               float dt,
               int n)
{
  int i, j;
  float d[3];
  float distSqr, invDist, invDist3;
#pragma polca iteration_independent
#pragma polca output frc
#pragma polca input pos
#pragma polca map CALCFORCE pos frc
  for (i = 0; i < n; i++)
    {
#pragma polca output frc[i]
#pragma polca input pos[i]
#pragma polca def CALCFORCE
      {
	frc[i].x = 0.0f;
	frc[i].y = 0.0f;
	frc[i].z = 0.0f;
#pragma polca fold ADDFORCE frc[i] pos frc[i]
	for (j = 0; j < n; j++)
	  {
#pragma polca output frc[i]
#pragma polca input frc[i] pos[j]
#pragma polca def ADDFORCE
	    {
	      if (i != j)
		{
		  d[0] = pos[j].x - pos[i].x;
		  d[1] = pos[j].y - pos[i].y;
		  d[2] = pos[j].z - pos[i].z;
		  distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2];
		  invDist = 1.0f / sqrt(distSqr);
		  invDist3 = invDist * invDist * invDist;
		  frc[i].x = frc[i].x + d[0] * invDist3;
		  frc[i].y = frc[i].y + d[1] * invDist3;
		  frc[i].z = frc[i].z + d[2] * invDist3;
		}
	    }
	  }
      }
    }
  int k;
#pragma polca iteration_independent
#pragma polca output frc
#pragma polca input vel
#pragma polca input UPDV
#pragma polca zipWith UPDV vel frc
  for (k = 0; k < n; k++)
    {
#pragma polca output vel[k]
#pragma polca input vel[k] frc[k]
#pragma polca def UPDV
      {
	vel[k].x = vel[k].x + dt * frc[k].x;
	vel[k].y = vel[k].y + dt * frc[k].y;
	vel[k].z = vel[k].z + dt * frc[k].z;
      }
    }
  int l;
#pragma polca iteration_independent
#pragma polca output vel
#pragma polca input pos
#pragma polca input UPDP
#pragma polca zipWith UPDP pos vel
  for (l = 0; l < n; l++)
    {
#pragma polca output pos[l]
#pragma polca input pos[l] vel[l]
#pragma polca def UPDP
      {
	pos[l].x = pos[l].x + vel[l].x * dt;
	pos[l].y = pos[l].y + vel[l].y * dt;
	pos[l].z = pos[l].z + vel[l].z * dt;
      }
    }
}

int main(const int argc, const char * * argv)
{
  int nBodies, nElems;
  int dataType;
  int i, iter;
  const float dt = DT;
  const int nIters = ITERS;
  float * pos, * vel;
  char fileName[80];
  sprintf(fileName, "%s/pos_input.dat", DATA_PATH);
  input_arr_arg(fileName, &pos, &nElems, &dataType);
  sprintf(fileName, "%s/vel_input.dat", DATA_PATH);
  input_arr_arg(fileName, &vel, &nElems, &dataType);
  nBodies = nElems / DIM;
  float pStruct[N * 3];
  float vStruct[N * 3];
  float fStruct[N * 3];
  for (i = 0; i < nBodies; i++)
    {
      pStruct[i * 3 + 0] = pos[i * 3];
      pStruct[i * 3 + 1] = pos[i * 3 + 1];
      pStruct[i * 3 + 2] = pos[i * 3 + 2];
    }
  for (i = 0; i < nBodies; i++)
    {
      vStruct[i * 3 + 0] = vel[i * 3];
      vStruct[i * 3 + 1] = vel[i * 3 + 1];
      vStruct[i * 3 + 2] = vel[i * 3 + 2];
    }
  for (iter = 1; iter <= nIters; iter++)
    {
#pragma polca def BLOCK_ABS
#pragma polca target omp
#pragma polca output (pStruct,fStruct)
#pragma polca input (pStruct,fStruct)
#pragma polca def UPD_FOR_VEL_POS
      {
	int iv_i_0, iv_j_1;
	float iv_d_2[3];
	float iv_distSqr_3, iv_invDist_4, iv_invDist3_5;
	iv_i_0 = 0 < nBodies ? nBodies : 0;
	int iv_k_6;
#pragma omp parallel for private(iv_j_1,iv_d_2,iv_distSqr_3,iv_invDist_4,iv_invDist3_5)
	for (iv_k_6 = 0; iv_k_6 < nBodies; iv_k_6++)
	  {
	    fStruct[iv_k_6 * 3 + 0] = 0.0f;
	    fStruct[iv_k_6 * 3 + 1] = 0.0f;
	    fStruct[iv_k_6 * 3 + 2] = 0.0f;
#pragma polca fold ADDFORCE frc[i] pos frc[i]
	    for (iv_j_1 = 0; iv_j_1 < nBodies; iv_j_1++)
	      {
		if (iv_k_6 != iv_j_1)
		  {
		    iv_d_2[0] = pStruct[iv_j_1 * 3 + 0] - pStruct[iv_k_6 * 3 + 0];
		    iv_d_2[1] = pStruct[iv_j_1 * 3 + 1] - pStruct[iv_k_6 * 3 + 1];
		    iv_d_2[2] = pStruct[iv_j_1 * 3 + 2] - pStruct[iv_k_6 * 3 + 2];
		    iv_distSqr_3 = iv_d_2[0] * iv_d_2[0] + iv_d_2[1] * iv_d_2[1] + iv_d_2[2] * iv_d_2[2];
		    iv_invDist_4 = 1.0f / sqrt(iv_distSqr_3);
		    iv_invDist3_5 = iv_invDist_4 * iv_invDist_4 * iv_invDist_4;
		    fStruct[iv_k_6 * 3 + 0] = fStruct[iv_k_6 * 3 + 0] + iv_d_2[0] * iv_invDist3_5;
		    fStruct[iv_k_6 * 3 + 1] = fStruct[iv_k_6 * 3 + 1] + iv_d_2[1] * iv_invDist3_5;
		    fStruct[iv_k_6 * 3 + 2] = fStruct[iv_k_6 * 3 + 2] + iv_d_2[2] * iv_invDist3_5;
		  }
	      }
	    vStruct[iv_k_6 * 3 + 0] = vStruct[iv_k_6 * 3 + 0] + dt * fStruct[iv_k_6 * 3 + 0];
	    vStruct[iv_k_6 * 3 + 1] = vStruct[iv_k_6 * 3 + 1] + dt * fStruct[iv_k_6 * 3 + 1];
	    vStruct[iv_k_6 * 3 + 2] = vStruct[iv_k_6 * 3 + 2] + dt * fStruct[iv_k_6 * 3 + 2];
	  }
	int iv_l_7;
#pragma polca iteration_independent
#pragma polca output vel
#pragma polca input pos
#pragma polca input UPDP
#pragma polca zipWith UPDP pos vel
#pragma omp parallel for
	for (iv_l_7 = 0; iv_l_7 < nBodies; iv_l_7++)
	  {
	    pStruct[iv_l_7 * 3 + 0] = pStruct[iv_l_7 * 3 + 0] + vStruct[iv_l_7 * 3 + 0] * dt;
	    pStruct[iv_l_7 * 3 + 1] = pStruct[iv_l_7 * 3 + 1] + vStruct[iv_l_7 * 3 + 1] * dt;
	    pStruct[iv_l_7 * 3 + 2] = pStruct[iv_l_7 * 3 + 2] + vStruct[iv_l_7 * 3 + 2] * dt;
	  }
      }
    }
  for (i = 0; i < nBodies; i++)
    {
      pos[i * 3] = pStruct[i * 3 + 0];
      pos[i * 3 + 1] = pStruct[i * 3 + 1];
      pos[i * 3 + 2] = pStruct[i * 3 + 2];
    }
  for (i = 0; i < nBodies; i++)
    {
      vel[i * 3] = vStruct[i * 3 + 0];
      vel[i * 3 + 1] = vStruct[i * 3 + 1];
      vel[i * 3 + 2] = vStruct[i * 3 + 2];
    }
  sprintf(fileName, "%s/pos_out.dat", DATA_PATH);
  output_arr_arg(fileName, pos, nBodies * DIM, dataType);
  sprintf(fileName, "%s/vel_out.dat", DATA_PATH);
  output_arr_arg(fileName, vel, nBodies * DIM, dataType);
  free(pos);
  free(vel);
}

