// FEAT_VECTOR: [4, 12, 1, 0, 0, 0, 1, 0, 0, 6, 3, 0, 0, 14, 0, 15, 24, 12, 0, 41, 41, 6, 0]
// TEST_VECTOR: [4, 12, 1, 0, 0, 0, 1, 0, 0, 6, 3, 0, 0, 14, 0, 15, 24, 12, 0, 41, 41, 6, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             4
//# anyFuncCall:                12
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               6
//# numLoopHoistedVarMods:       3
//# numNon1Darray:               0
//# numAuxVarArrayIndex:         0
//# totalNumForLoops:           14
//# numNonNormalizedForLoops:    0
//# numStmtsRollUp:             15
//# numCompoundStmts:           24
//# anyTernaryOp:               12
//# anyUselessStmt:              0
//# numForPostambles:           41
//# numForPreambles:            41
//# numStructVarDecl:            6
//# numEmptyIf:                  0



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"


typedef struct Triple Triple;

struct Triple {
    float x, y, z;
};

void bodyForce(struct Triple * pos, struct Triple * frc, int n)
{
    int i, j;
    float d[3];
    float distSqr, invDist, invDist3;
    for (i = 0; i < n; i++)
    {
        {
            frc[i].x = 0.0f;
            frc[i].y = 0.0f;
            frc[i].z = 0.0f;
            for (j = 0; j < n; j++)
            {
                {
                    d[0] = pos[j].x - pos[i].x;
                    d[1] = pos[j].y - pos[i].y;
                    d[2] = pos[j].z - pos[i].z;
                    distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + 1e-9f;
                    invDist = 1.0f / sqrt(distSqr);
                    invDist3 = invDist * invDist * invDist;
                    frc[i].x = frc[i].x + d[0] * invDist3;
                    frc[i].y = frc[i].y + d[1] * invDist3;
                    frc[i].z = frc[i].z + d[2] * invDist3;
                }
            }
        }
    }
}

void velocities(struct Triple * frc,
                struct Triple * vel,
                float dt,
                int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        {
            vel[i].x = vel[i].x + dt * frc[i].x;
            vel[i].y = vel[i].y + dt * frc[i].y;
            vel[i].z = vel[i].z + dt * frc[i].z;
        }
    }
}

void integrate(struct Triple * pos,
               struct Triple * vel,
               float dt,
               int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        {
            pos[i].x = pos[i].x + vel[i].x * dt;
            pos[i].y = pos[i].y + vel[i].y * dt;
            pos[i].z = pos[i].z + vel[i].z * dt;
        }
    }
}

void resize(void *arr,int nDim,int *dimSizes,int elemSize,void **newArr,int *newDimSizes) {

	int dim0Size = dimSizes[0] * elemSize;
	int paddedSize;

	// Device stream size must be a multiple of 16 Bytes
	paddedSize = dim0Size+(16-(dim0Size%16));

	int totalNElems = 1;
	int oldNElems = 1;
	for(int i=0;i<nDim;i++) {
		newDimSizes[i] = i==0 ? paddedSize/elemSize : dimSizes[i];
		totalNElems *= newDimSizes[i];
		oldNElems   *= dimSizes[i];
	}

	int totalSize = totalNElems * elemSize;
	int oldSize   = oldNElems * elemSize;

	printf("\nPtr. resize from %dB (%d) to %dB (%d)\n\n",oldSize,oldNElems,totalSize,totalNElems);

//	*newNElems = paddedSize/elemSize;
	*(newArr) = (void*) malloc(totalSize);

//	  printf("\nInside: %d\n\n",*newNElems);

	memcpy(*newArr,arr,(size_t)oldSize);

}

int main(const int argc, const char * * argv)
{
    int nBodies, nElems;
    int dataType;
    int i, iter;
    const float dt = DT;
    const int nIters = ITERS;
    float * pos, * vel;
    char fileName[80];
    sprintf(fileName, "%s/pos_input.dat", DATA_PATH);
    input_arr_arg(fileName, &pos, &nElems, &dataType);
    sprintf(fileName, "%s/vel_input.dat", DATA_PATH);
    input_arr_arg(fileName, &vel, &nElems, &dataType);
    nBodies = nElems / DIM;
    float pStruct[N * 3];
    float vStruct[N * 3];
    float fStruct[N * 3];

    for (i = 0; i < nBodies; i++)
    {
        pStruct[i * 3] = pos[i * 3];
        pStruct[i * 3 + 1] = pos[i * 3 + 1];
        pStruct[i * 3 + 2] = pos[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
    {
        vStruct[i * 3] = vel[i * 3];
        vStruct[i * 3 + 1] = vel[i * 3 + 1];
        vStruct[i * 3 + 2] = vel[i * 3 + 2];
    }

    int    nDim = 2;
    int    posDim[2],velDim[2];

    float *posInput,*posOutput;
    float *velInput,*velOutput;
    int    posDevDim[2],velDevDim[2];
    int    posDevElems,velDevElems;

    posDim[0] = nBodies;
    posDim[1] = DIM;

    velDim[0] = nBodies;
    velDim[1] = DIM;

    resize(pStruct,nDim,posDim,sizeof(float),&posInput,posDevDim);
    resize(vStruct,nDim,velDim,sizeof(float),&velInput,velDevDim);

    posDevElems = posDevDim[0] * posDevDim[1];
    velDevElems = velDevDim[0] * velDevDim[1];
    posOutput = (float*)malloc(posDevElems*sizeof(float));
    velOutput = (float*)malloc(velDevElems*sizeof(float));

    for (iter = 1; iter <= nIters; iter++)
    {
        {
            int iv_i_0, iv_j_1;
            float iv_d_2[3];
            float iv_distSqr_3, iv_invDist_4, iv_invDist3_5;
            iv_i_0 = 0 < nBodies ? nBodies : 0;
            int iv_i_6;
	    NBody(posDevElems,posInput,velInput,velOutput);


            int iv_i_7;
            for (iv_i_7 = 0; iv_i_7 < nBodies; iv_i_7++)
            {
                {
                    pStruct[iv_i_7 * 3] = pStruct[iv_i_7 * 3] + vStruct[iv_i_7 * 3] * dt;
                    pStruct[iv_i_7 * 3 + 1] = pStruct[iv_i_7 * 3 + 1] + vStruct[iv_i_7 * 3 + 1] * dt;
                    pStruct[iv_i_7 * 3 + 2] = pStruct[iv_i_7 * 3 + 2] + vStruct[iv_i_7 * 3 + 2] * dt;
                }
            }
        }
    }
    for (i = 0; i < nBodies; i++)
    {
        pos[i * 3] = pStruct[i * 3];
        pos[i * 3 + 1] = pStruct[i * 3 + 1];
        pos[i * 3 + 2] = pStruct[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
    {
        vel[i * 3] = vStruct[i * 3];
        vel[i * 3 + 1] = vStruct[i * 3 + 1];
        vel[i * 3 + 2] = vStruct[i * 3 + 2];
    }
    sprintf(fileName, "%s/pos_out.dat", DATA_PATH);
    output_arr_arg(fileName, pos, nBodies * DIM, dataType);
    sprintf(fileName, "%s/vel_out.dat", DATA_PATH);
    output_arr_arg(fileName, vel, nBodies * DIM, dataType);
    free(pos);
    free(vel);
}

