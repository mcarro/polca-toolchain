

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"


typedef struct Triple Triple;

struct Triple {
    float x, y, z;
};

void bodyForce(struct Triple * pos, struct Triple * frc, int n)
{
    int i, j;
    float d[3];
    float distSqr, invDist, invDist3;
    for (i = 0; i < n; i++)
    {
        {
            frc[i].x = 0.0f;
            frc[i].y = 0.0f;
            frc[i].z = 0.0f;
            for (j = 0; j < n; j++)
            {
                {
                    d[0] = pos[j].x - pos[i].x;
                    d[1] = pos[j].y - pos[i].y;
                    d[2] = pos[j].z - pos[i].z;
                    distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + 1e-9f;
                    invDist = 1.0f / sqrt(distSqr);
                    invDist3 = invDist * invDist * invDist;
                    frc[i].x = frc[i].x + d[0] * invDist3;
                    frc[i].y = frc[i].y + d[1] * invDist3;
                    frc[i].z = frc[i].z + d[2] * invDist3;
                }
            }
        }
    }
}

void velocities(struct Triple * frc,
                struct Triple * vel,
                float dt,
                int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        {
            vel[i].x = vel[i].x + dt * frc[i].x;
            vel[i].y = vel[i].y + dt * frc[i].y;
            vel[i].z = vel[i].z + dt * frc[i].z;
        }
    }
}

void integrate(struct Triple * pos,
               struct Triple * vel,
               float dt,
               int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        {
            pos[i].x = pos[i].x + vel[i].x * dt;
            pos[i].y = pos[i].y + vel[i].y * dt;
            pos[i].z = pos[i].z + vel[i].z * dt;
        }
    }
}

int main(const int argc, const char * * argv)
{
    int nBodies, nElems;
    int dataType;
    int i, iter;
    const float dt = DT;
    const int nIters = ITERS;
    float * pos, * vel;
    char fileName[80];
    sprintf(fileName, "%s/pos_input.dat", DATA_PATH);
    input_arr_arg(fileName, &pos, &nElems, &dataType);
    sprintf(fileName, "%s/vel_input.dat", DATA_PATH);
    input_arr_arg(fileName, &vel, &nElems, &dataType);
    nBodies = nElems / DIM;
    float pStruct[N * 3];
    float vStruct[N * 3];
    float fStruct[N * 3];
    for (i = 0; i < nBodies; i++)
    {
        pStruct[i * 3] = pos[i * 3];
        pStruct[i * 3 + 1] = pos[i * 3 + 1];
        pStruct[i * 3 + 2] = pos[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
    {
        vStruct[i * 3] = vel[i * 3];
        vStruct[i * 3 + 1] = vel[i * 3 + 1];
        vStruct[i * 3 + 2] = vel[i * 3 + 2];
    }
    for (iter = 1; iter <= nIters; iter++)
    {
        {
            int iv_i_0, iv_j_1;
            float iv_d_2[3];
            float iv_distSqr_3, iv_invDist_4, iv_invDist3_5;
            iv_i_0 = 0 < nBodies ? nBodies : 0;
            int iv_i_6;
            for (iv_i_6 = 0; iv_i_6 < nBodies; iv_i_6++)
            {
                int iv_iv_i_8_15;
                int iv_iv_i_13_14;
                for (iv_j_1 = 0; iv_j_1 < nBodies; iv_j_1++)
                {
                    int iv_iv_iv_i_9_11_16;
                    int iv_iv_iv_i_10_12_17;
                    for (iv_iv_iv_i_9_11_16 = 0; iv_iv_iv_i_9_11_16 < 3; iv_iv_iv_i_9_11_16++)
                    {
                        for (iv_iv_i_13_14 = 0; iv_iv_i_13_14 < 3; iv_iv_i_13_14++)
                        {
                            iv_iv_i_8_15 = iv_iv_i_13_14 == 0 && iv_iv_iv_i_9_11_16 == 0 ? 0 < 3 ? 3 : 0 : iv_iv_i_8_15;
                            fStruct[iv_i_6 * 3 + iv_iv_iv_i_9_11_16] = iv_iv_i_13_14 == 0 && iv_j_1 == 0 ? 0.0f : fStruct[iv_i_6 * 3 + iv_iv_iv_i_9_11_16];
                            iv_d_2[iv_iv_iv_i_9_11_16] = iv_iv_i_13_14 == 0 ? pStruct[iv_j_1 * 3 + iv_iv_iv_i_9_11_16] - pStruct[iv_i_6 * 3 + iv_iv_iv_i_9_11_16] : iv_d_2[iv_iv_iv_i_9_11_16];
                            iv_distSqr_3 = iv_iv_i_13_14 == 0 && iv_iv_iv_i_9_11_16 == 2 ? iv_d_2[0] * iv_d_2[0] + iv_d_2[1] * iv_d_2[1] + iv_d_2[2] * iv_d_2[2] + 1e-9f : iv_distSqr_3;
                            iv_invDist_4 = iv_iv_i_13_14 == 0 && iv_iv_iv_i_9_11_16 == 2 ? 1.0f / sqrt(iv_distSqr_3) : iv_invDist_4;
                            iv_invDist3_5 = iv_iv_i_13_14 == 0 && iv_iv_iv_i_9_11_16 == 2 ? iv_invDist_4 * iv_invDist_4 * iv_invDist_4 : iv_invDist3_5;
                            iv_iv_iv_i_10_12_17 = iv_iv_i_13_14 == 0 && iv_iv_iv_i_9_11_16 == 2 ? 0 < 3 ? 3 : 0 : iv_iv_iv_i_10_12_17;
                            fStruct[iv_i_6 * 3 + iv_iv_i_13_14] = iv_iv_iv_i_9_11_16 == 2 ? fStruct[iv_i_6 * 3 + iv_iv_i_13_14] + iv_d_2[iv_iv_i_13_14] * iv_invDist3_5 : fStruct[iv_i_6 * 3 + iv_iv_i_13_14];
                            vStruct[iv_i_6 * 3 + iv_iv_i_13_14] = iv_iv_iv_i_9_11_16 == 2 && iv_j_1 == nBodies - 1 ? vStruct[iv_i_6 * 3 + iv_iv_i_13_14] + dt * fStruct[iv_i_6 * 3 + iv_iv_i_13_14] : vStruct[iv_i_6 * 3 + iv_iv_i_13_14];
                        }
                    }
                }
            }
            int iv_i_7;
            for (iv_i_7 = 0; iv_i_7 < nBodies; iv_i_7++)
            {
                {
                    pStruct[iv_i_7 * 3] = pStruct[iv_i_7 * 3] + vStruct[iv_i_7 * 3] * dt;
                    pStruct[iv_i_7 * 3 + 1] = pStruct[iv_i_7 * 3 + 1] + vStruct[iv_i_7 * 3 + 1] * dt;
                    pStruct[iv_i_7 * 3 + 2] = pStruct[iv_i_7 * 3 + 2] + vStruct[iv_i_7 * 3 + 2] * dt;
                }
            }
        }
    }
    for (i = 0; i < nBodies; i++)
    {
        pos[i * 3] = pStruct[i * 3];
        pos[i * 3 + 1] = pStruct[i * 3 + 1];
        pos[i * 3 + 2] = pStruct[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
    {
        vel[i * 3] = vStruct[i * 3];
        vel[i * 3 + 1] = vStruct[i * 3 + 1];
        vel[i * 3 + 2] = vStruct[i * 3 + 2];
    }
    sprintf(fileName, "%s/pos_out.dat", DATA_PATH);
    output_arr_arg(fileName, pos, nBodies * DIM, dataType);
    sprintf(fileName, "%s/vel_out.dat", DATA_PATH);
    output_arr_arg(fileName, vel, nBodies * DIM, dataType);
    free(pos);
    free(vel);
}

