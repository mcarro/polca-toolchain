
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <MaxSLiCInterface.h>
#include "Maxfiles.h"

#include "timer.h"
#include "params.h"



void resize(void *arr,int nDim,int *dimSizes,int elemSize,void **newArr,int *newDimSizes) {

	int dim0Size = dimSizes[0] * elemSize;
	int paddedSize;

	// Device stream size must be a multiple of 16 Bytes
	paddedSize = dim0Size+(16-(dim0Size%16));

	int totalNElems = 1;
	int oldNElems = 1;
	for(int i=0;i<nDim;i++) {
		newDimSizes[i] = i==0 ? paddedSize/elemSize : dimSizes[i];
		totalNElems *= newDimSizes[i];
		oldNElems   *= dimSizes[i];
	}

	int totalSize = totalNElems * elemSize;
	int oldSize   = oldNElems * elemSize;

	printf("\nPtr. resize from %dB (%d) to %dB (%d)\n\n",oldSize,oldNElems,totalSize,totalNElems);

//	*newNElems = paddedSize/elemSize;
	*(newArr) = (void*) malloc(totalSize);

//	  printf("\nInside: %d\n\n",*newNElems);

	memcpy(*newArr,arr,(size_t)oldSize);

}

void bodyForce(float *pos,float *vel, float dt, int n) {

//    float distSqr;
//    float invDist;
//    float invDist3;
//    float d[3];
//    float F[3];
//
//	for (int ijkw = 0; ijkw < n*n*3*3; ijkw++) {
//	  int i   = ijkw / (n*3*3);
//	  int jkw = ijkw % (n*3*3);
//	  int j   = jkw / (3*3);
//	  int kw  = jkw % (3*3);
//	  int k   = kw / 3;
//	  int w   = kw % 3;
//
////    	  if(w==0) printf("%2d,%2d,%2d,%2d: %.7f - %.7f\n",i,j,k,w,pos[j*DIM+k],pos[i*DIM+k]);
//	  d[k] = w==0 ? (pos[j*DIM+k]-pos[i*DIM+k]) : d[k];
//
//	  distSqr  = w==0 && k==2 ? d[0]*d[0] + d[1]*d[1] + d[2]*d[2] + SOFTENING : distSqr;
//	  invDist  = w==0 && k==2 ? 1.0f / distSqr : invDist;
//	  invDist3 = w==0 && k==2 ? invDist * invDist * invDist : invDist3;
//
//	  F[w] = k==2 ? (j==0 ? 0.0f: F[w]) + d[w] * invDist3 : F[w];
//	  vel[i*DIM+w] = k==2 && j==(n-1) ? vel[i*DIM+w] + dt*F[w] : vel[i*DIM+w];
//
//	}

    float *distSqr = (float*) malloc(sizeof(float) *n*n*3*3);
    float *invDist = (float*) malloc(sizeof(float) *n*n*3*3);
    float *invDist3 = (float*) malloc(sizeof(float) *n*n*3*3);
    float *d = (float*) malloc(sizeof(float) *n*n*3*3);
    float *F = (float*) malloc(sizeof(float) *n*n*3*3);

    for (int ijkw = 0; ijkw < n*n*3*3; ijkw++) {
      int i   = ijkw / (n*3*3);
      int jkw = ijkw % (n*3*3);
      int j   = jkw / (3*3);
      int kw  = jkw % (3*3);
      int k   = kw / 3;
      int w   = kw % 3;

      int nj = ijkw - (((n * DIM * DIM)) * i +((DIM * DIM) - 3) * j +(DIM - 1) * k);
      int ni = ijkw - (((n * DIM * DIM) - 3) * i +((DIM * DIM)) * j +(DIM - 1) * k);

      d[ijkw] = w==0 ? pos[nj]   - pos[ni] : d[ijkw];

      // System eqs.
      distSqr[ijkw]  = w==0 && k==2 ? d[ijkw-6]*d[ijkw-6] + d[ijkw-3]*d[ijkw-3] + d[ijkw-0]*d[ijkw-0] + SOFTENING : distSqr[ijkw];
      // System eqs.
      invDist[ijkw]  = w==0 && k==2 ? 1.0f / distSqr[ijkw] : invDist[ijkw];
      // System eqs.
      invDist3[ijkw] = w==0 && k==2 ? invDist[ijkw] * invDist[ijkw] * invDist[ijkw] : invDist3[ijkw];
      // System eqs.
      F[ijkw]    = k==2 ? (j==0 ? 0.0f : F[ijkw-9]) + d[ijkw-(-2*w+6)] * invDist3[ijkw-w] : F[ijkw];

      vel[i*3+w] = k==2 && j==n-1 ? vel[i*3+w]   + dt*F[ijkw] : vel[i*3+w];

    }

}

void NBodyCPU(float *pos,float *vel, float dt,int nBodies,int nIters) {
	  for (int iter = 1; iter <= nIters; iter++) {
	    /* printf("%d: %f\n",nBodies,x[0]); */
	    bodyForce(pos,vel,dt, nBodies); // compute interbody forces

	    for (int i = 0 ; i < nBodies*DIM; i+=DIM) { // integrate position
	      pos[i]   += vel[i]*dt;
	      pos[i+1] += vel[i+1]*dt;
	      pos[i+2] += vel[i+2]*dt;
	    }

	  }
}

int checkRes(float *pos,float *posDev,int nBodies) {
	int result = 0;

    for (int i = 0 ; i < nBodies*DIM; i++) { // integrate position
      if(pos[i] != posDev[i]) {
    	  result++;
    	  printf("%d: %f - %f\n",i,pos[i],posDev[i]);
      }
    }

    return result;

}

int main()
{
	  int nBodies,nElems;
	  int dataType;
	  /* if (argc > 1) nBodies = atoi(argv[1]); */

	  const float dt   = DT;    // time step
	  const int nIters = ITERS; // simulation iterations

	  float *pos,*vel;
	  int    nDim = 2;
	  int    posDim[2],velDim[2];

	  float *posInput,*posOutput;
	  float *velInput,*velOutput;
	  int    posDevDim[2],velDevDim[2];
	  int    posDevElems,velDevElems;

	  input_arr_arg("./data/pos_input.dat",  &pos,&nElems,&dataType);
	  input_arr_arg("./data/vel_input.dat",  &vel,&nElems,&dataType);

	  nBodies = nElems / DIM;
	  posDim[0] = nBodies;
	  posDim[1] = DIM;

	  velDim[0] = nBodies;
	  velDim[1] = DIM;

	  resize(pos,nDim,posDim,sizeof(float),&posInput,posDevDim);
	  resize(vel,nDim,velDim,sizeof(float),&velInput,velDevDim);

	  posDevElems = posDevDim[0] * posDevDim[1];
	  velDevElems = velDevDim[0] * velDevDim[1];
	  posOutput = (float*)malloc(posDevElems*sizeof(float));
	  velOutput = (float*)malloc(velDevElems*sizeof(float));

#if PROFILING == 1
	  for(int sample=0;sample<SAMPLES;sample++)
#endif
		  NBodyCPU(pos,vel,dt,nBodies,nIters);

	#if PROFILING == 1
	 for(int sample=0;sample<SAMPLES;sample++){
	    StartTimer();
	#endif
	  for (int iter = 1; iter <= nIters; iter++) {

			NBody(posDevElems,posInput,velInput,velOutput);

		    for (int i = 0 ; i < nBodies*DIM; i+=DIM) { // integrate position
		      posInput[i]   += velOutput[i]*dt;
		      posInput[i+1] += velOutput[i+1]*dt;
		      posInput[i+2] += velOutput[i+2]*dt;
		    }

		    for (int i = 0 ; i < nBodies*DIM; i++)
			      velInput[i]   = velOutput[i];

	  }

	#if PROFILING == 1
	  GetTimer();
	 } // End of loop for time samples
	#endif

	  output_arr_arg("./data/pos_out.dat",   posOutput, nBodies*DIM, dataType);
	  output_arr_arg("./data/vel_out.dat",   velOutput, nBodies*DIM, dataType);

	int status = checkRes(vel, velOutput, nBodies);
	if (status)
		printf("Test failed.\n");
	else
		printf("Test passed OK!\n");

	free(pos);
	free(vel);


	return status;
}
