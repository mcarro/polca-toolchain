////////////////////////////////////////////////////////////////////////////////

/* #include <fcntl.h> */
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* #include <string.h> */
#include <math.h>
/* #include <unistd.h> */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */

/* #include <OpenCL/opencl.h> */
/* // For OpenCL */
/* #define MAX_SOURCE_SIZE (0x100000) */

#include "common_OpenCL.h"

const char kernel1Name[] = "./kernel1_auto.cl";
const char kernel1Func[] = "kernel1";

const char kernel2Name[] = "./kernel2_auto.cl";
const char kernel2Func[] = "kernel2";

const char compileFlags[] = "";

#include "timer.h"
#include "params.h"

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    /* unsigned int correct;               // number of correct results returned */
    
  int nBodies,nElems;
  int dataType;
  int i;
  /* if (argc > 1) nBodies = atoi(argv[1]); */

  const float dt = DT; // time step
  const int nIters = ITERS;  // simulation iterations

  /* int bytes = nBodies*sizeof(Body); */
  /* float *buf = (float*)malloc(bytes); */
  /* Body *p = (Body*)buf; */
  
  float *pos,*vel;

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////

  input_arr_arg("../../../data/pos_input.dat",  &pos,&nElems,&dataType);
  input_arr_arg("../../../data/vel_input.dat",  &vel,&nElems,&dataType);
  nBodies = nElems / DIM;

    /////////////////////////////////////////////////////////////

  float pStruct[N * 3];
  float vStruct[N * 3];
  float fStruct[N * 3];
  for (i = 0; i < nBodies; i++)
    {
      pStruct[i * 3 + 0] = pos[i * 3];
      pStruct[i * 3 + 1] = pos[i * 3 + 1];
      pStruct[i * 3 + 2] = pos[i * 3 + 2];
    }
  for (i = 0; i < nBodies; i++)
    {
      vStruct[i * 3 + 0] = vel[i * 3];
      vStruct[i * 3 + 1] = vel[i * 3 + 1];
      vStruct[i * 3 + 2] = vel[i * 3 + 2];
    }


    //////////////////////////////////////////////////////////////
    //  Definition of OpenCL related variables specific to this program
    /////////////////////////////////////////////////////////////
    size_t         local;                       // local domain size for our calculation

    cl_kernel      kernel1,kernel2;

    cl_int         error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem pStructDev;                       // device memory used for the input array
    cl_mem fStructDev;
    unsigned int elemsInput;
    unsigned int sizeInput;             // size of input array

    cl_mem vStructDev;                      // device memory used for the output array
    unsigned int elemsOutput;
    unsigned int sizeOutput;             // size of input array

    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    initOpenCLVars();

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    createOpenCLKernel(kernel1Func,kernel1Name,compileFlags,deviceIds,context,&kernel1);

    createOpenCLKernel(kernel2Func,kernel2Name,compileFlags,deviceIds,context,&kernel2);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    elemsInput = N*3; 
    sizeInput = sizeof(float) * elemsInput;
    pStructDev = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeInput, NULL, NULL);
    fStructDev = clCreateBuffer(context,  CL_MEM_READ_WRITE, sizeInput, NULL, NULL);

    elemsOutput = N*3; 
    sizeOutput = sizeof(float) * elemsOutput;
    vStructDev = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeOutput, NULL, NULL);
    if (!pStructDev || !vStructDev || !fStructDev)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }

     // Set the arguments to our compute kernel 1
    //
    error  = 0;
    error  = clSetKernelArg(kernel1, 0, sizeof(cl_mem), &pStructDev);
    error |= clSetKernelArg(kernel1, 1, sizeof(cl_mem), &fStructDev);
    error |= clSetKernelArg(kernel1, 2, sizeof(cl_int), &nBodies);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

    // Set the arguments to our compute kernel 2
    //
    error  = 0;
    error  = clSetKernelArg(kernel2, 0, sizeof(cl_mem), &pStructDev);
    error |= clSetKernelArg(kernel2, 1, sizeof(cl_mem), &vStructDev);
    error |= clSetKernelArg(kernel2, 2, sizeof(cl_mem), &fStructDev);
    error |= clSetKernelArg(kernel2, 3, sizeof(cl_float),  &dt);
    error |= clSetKernelArg(kernel2, 4, sizeof(cl_int), &nBodies);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif

    // Write our data set into the input array in device memory
    //
    error  = clEnqueueWriteBuffer(queue, pStructDev, CL_TRUE, 0, sizeInput, pStruct, 0, NULL, NULL);

    error  = clEnqueueWriteBuffer(queue, vStructDev, CL_TRUE, 0, sizeOutput, vStruct, 0, NULL, NULL);

#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

  for (int iter = 1; iter <= nIters; iter++) {

    local = 1;
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = nBodies;
    error = clEnqueueNDRangeKernel(queue, kernel1, 1, NULL, &global, NULL, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////
    // We don't need to use clFinish between kernel calls when kernels are enqueued in the same
    // OpenCL command queue (huge performance benefit)
    /* clFinish(queue); */
    /////////////////////////////////////////////////////////////

    error = clEnqueueNDRangeKernel(queue, kernel2, 1, NULL, &global, NULL, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////



 } // end for nIters of nBody use case

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error =  clEnqueueReadBuffer( queue, pStructDev, CL_TRUE, 0, sizeOutput, pStruct, 0, NULL, NULL );
    error |= clEnqueueReadBuffer( queue, vStructDev, CL_TRUE, 0, sizeOutput, vStruct, 0, NULL, NULL );


#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif
    /////////////////////////////////////////////////////////////


#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

  for (i = 0; i < nBodies; i++)
    {
      pos[i * 3] = pStruct[i * 3 + 0];
      pos[i * 3 + 1] = pStruct[i * 3 + 1];
      pos[i * 3 + 2] = pStruct[i * 3 + 2];
    }
  for (i = 0; i < nBodies; i++)
    {
      vel[i * 3] = vStruct[i * 3 + 0];
      vel[i * 3 + 1] = vStruct[i * 3 + 1];
      vel[i * 3 + 2] = vStruct[i * 3 + 2];
    }

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
 output_arr_arg("../../../data/pos_out.dat",   pos, nElems, dataType);
 output_arr_arg("../../../data/vel_out.dat",   vel, nElems, dataType);
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(pStructDev);
    clReleaseMemObject(vStructDev);
    clReleaseMemObject(fStructDev);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    /* clReleaseProgram(program); */
    clReleaseKernel(kernel1);
    clReleaseKernel(kernel2);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

