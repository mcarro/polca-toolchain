// Simple compute kernel
//

#define SOFTENING 1e-9f

#define P 256

__kernel void bodyForce(
   __global float*   x,
   __global float*   y,
   __global float*   z,
   __global float*   vx,
   __global float*   vy,
   __global float*   vz,
   const    float    dt,
   const    int      nBodies)
{

    __local float      x_shr[P];
    __local float      y_shr[P];
    __local float      z_shr[P];

    __private float    x_prv,y_prv,z_prv;

    int gId   = get_global_id(0);
    int lId   = get_local_id(0);

    if(gId < nBodies) {
      x_prv = x[gId];
      y_prv = y[gId];
      z_prv = z[gId];

      float Fx = 0.0f; float Fy = 0.0f; float Fz = 0.0f;

      for (int p = 0; p < nBodies/P; p++) {
        x_shr[lId] = x[p*P+lId];
        y_shr[lId] = y[p*P+lId];
        z_shr[lId] = z[p*P+lId];

        barrier(CLK_LOCAL_MEM_FENCE);

        for (int j = 0; j < P; j++) {
          float dx = x_shr[j] - x_prv;
          float dy = y_shr[j] - y_prv;
          float dz = z_shr[j] - z_prv;
          float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
          float invDist = 1.0f / sqrt(distSqr);
          // float invDist = 1.0f / distSqr;
          float invDist3 = invDist * invDist * invDist;

          Fx += dx * invDist3; Fy += dy * invDist3; Fz += dz * invDist3;
          // Fx += dx * invDist; Fy += dy * invDist3; Fz += dz * invDist3;
	}
      }

      vx[gId] += dt*Fx; vy[gId] += dt*Fy; vz[gId] += dt*Fz;
   }

}