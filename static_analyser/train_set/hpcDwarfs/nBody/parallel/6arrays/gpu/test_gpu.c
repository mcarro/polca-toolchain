////////////////////////////////////////////////////////////////////////////////

/* #include <fcntl.h> */
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* #include <string.h> */
#include <math.h>
/* #include <unistd.h> */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */

/* #include <OpenCL/opencl.h> */
/* // For OpenCL */
/* #define MAX_SOURCE_SIZE (0x100000) */

#include "common_OpenCL.h"

const char kernel1Name[] = "./test.cl";
const char kernel1Func[] = "test";

/* const char compileFlags[] = "-cl-opt-disable"; */
const char compileFlags[] = "";

#include "timer.h"
#include "params.h"

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    /* unsigned int correct;               // number of correct results returned */
    
  int nBodies = 1024;
  int dataType;
  /* if (argc > 1) nBodies = atoi(argv[1]); */

  const float dt = 0.01f; // time step
  const int nIters = ITERS;  // simulation iterations

    /////////////////////////////////////////////////////////////

  float *x, *vx;
  x  = (float*) malloc(sizeof(float) * nBodies);
  vx = (float*) malloc(sizeof(float) * nBodies);

  for(int k=0;k<nBodies;k++){
    x[k] = 2.0f * (rand() / (float)RAND_MAX) - 1.0f;
    /* x[k] = 1 / (10*(k+1)); */
    printf("%d: %.16f\n",k,x[k]);
  }

    //////////////////////////////////////////////////////////////
    //  Definition of OpenCL related variables specific to this program
    /////////////////////////////////////////////////////////////
    size_t         local;                       // local domain size for our calculation

    cl_kernel      kernel1;

    cl_int         error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation

    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem input;               // device memory used for the input array
    unsigned int elemsInput;
    unsigned int sizeInput;              // size of input array

    cl_mem output;                      // device memory used for the output array
    unsigned int elemsOutput;
    unsigned int sizeOutput;             // size of input array
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    initOpenCLVars();

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    createOpenCLKernel(kernel1Func,kernel1Name,compileFlags,deviceIds,context,&kernel1);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    elemsInput = nBodies; 
    sizeInput = sizeof(float) * elemsInput;
    input = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    elemsOutput = nBodies; 
    sizeOutput = sizeof(float) * elemsOutput;
    output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    if (!input || !output)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }

     // Set the arguments to our compute kernel 1
    //
    error  = 0;
    error  = clSetKernelArg(kernel1, 0, sizeof(cl_mem), &input);
    error  = clSetKernelArg(kernel1, 1, sizeof(cl_mem), &output);
    error  = clSetKernelArg(kernel1, 2, sizeof(cl_float), &dt);
    error  = clSetKernelArg(kernel1, 3, sizeof(cl_int), &nBodies);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif

    error  = clEnqueueWriteBuffer(queue, input, CL_TRUE, 0, sizeInput, x, 0, NULL, NULL);

  for (int iter = 1; iter <= nIters; iter++) {
    local = 1;
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = elemsInput;
    error = clEnqueueNDRangeKernel(queue, kernel1, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////
    // We don't need to use clFinish between kernel calls when kernels are enqueued in the same
    // OpenCL command queue (huge performance benefit)
    /* clFinish(queue); */
    /////////////////////////////////////////////////////////////


 } // end for nIters of nBody use case

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //

    error |= clEnqueueReadBuffer( queue, output, CL_TRUE, 0, sizeOutput, vx, 0, NULL, NULL );

    float *cpuOutput = (float *) malloc(sizeof(float) * nBodies);
    for(int i=0;i<nBodies;i++){
        float distSqr = x[i];

    	for(int j=0;j<1;j++){

          float invDist = 1.0/distSqr; //1.0f / sqrt(distSqr*distSqr);

          distSqr = invDist * invDist;

	  printf("%d, %d: %f %f\n",nBodies,i,invDist,distSqr);
	}
	cpuOutput[i] = distSqr;
    }
    printf("-------------------------\n");

    for(int k=0;k<nBodies;k++){
      if(abs(cpuOutput[k]-vx[k]) >= FP_TOLERANCE)
	printf("Error: %d - %.16f != %.16f\n",k,cpuOutput[k],vx[k]);
    }
      

#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif
    /////////////////////////////////////////////////////////////


#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif    

    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(input);
    clReleaseMemObject(output);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    /* clReleaseProgram(program); */
    clReleaseKernel(kernel1);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

