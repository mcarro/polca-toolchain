// Simple compute kernel
//

#define SOFTENING 1e-9f

__kernel void test(
   __global float*   input,
   __global float*   output,
   const    float    dt,
   const    int      nBodies)
{
	
    int gId   = get_global_id(0);

    if(gId < nBodies) {

        float distSqr = input[gId];

    	for(int j=0;j<1;j++){

          float invDist = 1.0/distSqr;//1.0f / sqrt(distSqr*distSqr);

          distSqr = invDist * invDist;
	}

	printf("%d, %d: %.16f\n",nBodies,gId,distSqr);

	output[gId] = distSqr;

   }

}