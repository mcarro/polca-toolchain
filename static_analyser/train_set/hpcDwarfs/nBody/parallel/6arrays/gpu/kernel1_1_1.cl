// Simple compute kernel
//

#define SOFTENING 1e-9f

__kernel void bodyForce(
   __global float*   x,
   __global float*   y,
   __global float*   z,
   __global float*   vx,
   __global float*   vy,
   __global float*   vz,
   const    float    dt,
   const    int      nBodies)
{

    int gId   = get_global_id(0);

    if(gId < nBodies) {
      float Fx = 0.0; float Fy = 0.0f; float Fz = 0.0f;

      for (int j = 0; j < nBodies; j++) {
        float dx = x[j] - x[gId];
        float dy = y[j] - y[gId];
        float dz = z[j] - z[gId];
        float distSqr = dx*dx + dy*dy + dz*dz + SOFTENING;
        float invDist = 1.0f / sqrt(distSqr);
        // float invDist = 1.0 / distSqr;

        float invDist3 = invDist * invDist * invDist;
        // float invDist3 = distSqr * distSqr * distSqr;

        // if(gId==8) printf("\n%d: %f + (%f,%f,%f) * %f\n",j,Fx,dx,dy,dz,distSqr);
        // if(gId==8) printf("%d: %.15f %f %f %f\n",j,distSqr,invDist,invDist2,invDist3);

        Fx += dx * invDist3; Fy += dy * invDist3; Fz += dz * invDist3;
        // Fx = x[gId]; Fy = y[gId]; Fz = z[gId];
      }

      // if(gId==24) printf("\n%d: %f + %f * %f\n",gId,vx[gId],dt,Fx);
      vx[gId] = vx[gId] + dt*Fx; 
      vy[gId] = vy[gId] + dt*Fy;
      vz[gId] = vz[gId] + dt*Fz;
   }

}

// 999999988484154753734934528.000000
// 999999988484154753734934528.000000