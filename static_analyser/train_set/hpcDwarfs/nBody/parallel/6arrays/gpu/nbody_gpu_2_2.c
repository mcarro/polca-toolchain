////////////////////////////////////////////////////////////////////////////////

/* #include <fcntl.h> */
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* #include <string.h> */
#include <math.h>
/* #include <unistd.h> */
/* #include <sys/types.h> */
/* #include <sys/stat.h> */

/* #include <OpenCL/opencl.h> */
/* // For OpenCL */
/* #define MAX_SOURCE_SIZE (0x100000) */

#include "common_OpenCL.h"

const char kernel1Name[] = "./kernel1_2_2.cl";
const char kernel1Func[] = "bodyForce";

const char kernel2Name[] = "./kernel2_2_2.cl";
const char kernel2Func[] = "integrate";

const char compileFlags[] = "";

#include "timer.h"
#include "params.h"

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    /* unsigned int correct;               // number of correct results returned */
    
  int nBodies;
  int dataType;
  /* if (argc > 1) nBodies = atoi(argv[1]); */

  const float dt = 0.01f; // time step
  const int nIters = ITERS;  // simulation iterations

  /* int bytes = nBodies*sizeof(Body); */
  /* float *buf = (float*)malloc(bytes); */
  /* Body *p = (Body*)buf; */
  
  float *x, *y, *z, *vx, *vy, *vz;

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////

  input_arr_arg("../../../data/x_input.dat",  &x,&nBodies,&dataType);
  input_arr_arg("../../../data/y_input.dat",  &y,&nBodies,&dataType);
  input_arr_arg("../../../data/z_input.dat",  &z,&nBodies,&dataType);
  input_arr_arg("../../../data/vx_input.dat",&vx,&nBodies,&dataType);
  input_arr_arg("../../../data/vy_input.dat",&vy,&nBodies,&dataType);
  input_arr_arg("../../../data/vz_input.dat",&vz,&nBodies,&dataType);

    /////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////
    //  Definition of OpenCL related variables specific to this program
    /////////////////////////////////////////////////////////////
    size_t         local;                       // local domain size for our calculation

    cl_kernel      kernel1,kernel2;

    cl_int         error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem xDev,yDev,zDev;                       // device memory used for the input array
    unsigned int elemsInput;
    unsigned int sizeInput;             // size of input array

    cl_mem vxDev,vyDev,vzDev;                      // device memory used for the output array
    unsigned int elemsOutput;
    unsigned int sizeOutput;             // size of input array

    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    initOpenCLVars();

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    createOpenCLKernel(kernel1Func,kernel1Name,compileFlags,deviceIds,context,&kernel1);

    createOpenCLKernel(kernel2Func,kernel2Name,compileFlags,deviceIds,context,&kernel2);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    elemsInput = nBodies; 
    sizeInput = sizeof(float) * elemsInput;
    xDev = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    yDev = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    zDev = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    elemsOutput = nBodies; 
    sizeOutput = sizeof(float) * elemsOutput;
    vxDev = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    vyDev = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    vzDev = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    if (!xDev || !yDev || !zDev || !vxDev || !vyDev || !vzDev)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }
    

     // Set the arguments to our compute kernel 1
    //
    error  = 0;
    error  = clSetKernelArg(kernel1, 0, sizeof(cl_mem), &xDev);
    error |= clSetKernelArg(kernel1, 1, sizeof(cl_mem), &yDev);
    error |= clSetKernelArg(kernel1, 2, sizeof(cl_mem), &zDev);
    error |= clSetKernelArg(kernel1, 3, sizeof(cl_mem), &vxDev);
    error |= clSetKernelArg(kernel1, 4, sizeof(cl_mem), &vyDev);
    error |= clSetKernelArg(kernel1, 5, sizeof(cl_mem), &vzDev);
    error |= clSetKernelArg(kernel1, 6, sizeof(cl_float),  &dt);
    error |= clSetKernelArg(kernel1, 7, sizeof(cl_int), &nBodies);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

    // Set the arguments to our compute kernel 2
    //
    error  = 0;
    error  = clSetKernelArg(kernel2, 0, sizeof(cl_mem), &xDev);
    error |= clSetKernelArg(kernel2, 1, sizeof(cl_mem), &yDev);
    error |= clSetKernelArg(kernel2, 2, sizeof(cl_mem), &zDev);
    error |= clSetKernelArg(kernel2, 3, sizeof(cl_mem), &vxDev);
    error |= clSetKernelArg(kernel2, 4, sizeof(cl_mem), &vyDev);
    error |= clSetKernelArg(kernel2, 5, sizeof(cl_mem), &vzDev);
    error |= clSetKernelArg(kernel2, 6, sizeof(cl_float),  &dt);
    error |= clSetKernelArg(kernel2, 7, sizeof(cl_int), &nBodies);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

#if PROFILING == 1
 for(int sample=0;sample<SAMPLES;sample++){
    StartTimer();
#endif

    // Write our data set into the input array in device memory
    //
    error  = clEnqueueWriteBuffer(queue, xDev, CL_TRUE, 0, sizeInput, x, 0, NULL, NULL);
    error |= clEnqueueWriteBuffer(queue, yDev, CL_TRUE, 0, sizeInput, y, 0, NULL, NULL);
    error |= clEnqueueWriteBuffer(queue, zDev, CL_TRUE, 0, sizeInput, z, 0, NULL, NULL);
    error |= clEnqueueWriteBuffer(queue, vxDev, CL_TRUE, 0, sizeInput, vx, 0, NULL, NULL);
    error |= clEnqueueWriteBuffer(queue, vyDev, CL_TRUE, 0, sizeInput, vy, 0, NULL, NULL);
    error |= clEnqueueWriteBuffer(queue, vzDev, CL_TRUE, 0, sizeInput, vz, 0, NULL, NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

  for (int iter = 1; iter <= nIters; iter++) {

    
    local = 256;
    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = elemsInput;
    error = clEnqueueNDRangeKernel(queue, kernel1, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////
    // We don't need to use clFinish between kernel calls when kernels are enqueued in the same
    // OpenCL command queue (huge performance benefit)
    /* clFinish(queue); */
    /////////////////////////////////////////////////////////////

    local = 256;
    global = elemsInput;
    error = clEnqueueNDRangeKernel(queue, kernel2, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////



 } // end for nIters of nBody use case

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error =  clEnqueueReadBuffer( queue, xDev, CL_TRUE, 0, sizeOutput, x, 0, NULL, NULL );
    error |= clEnqueueReadBuffer( queue, yDev, CL_TRUE, 0, sizeOutput, y, 0, NULL, NULL );
    error |= clEnqueueReadBuffer( queue, zDev, CL_TRUE, 0, sizeOutput, z, 0, NULL, NULL );

    error |= clEnqueueReadBuffer( queue, vxDev, CL_TRUE, 0, sizeOutput, vx, 0, NULL, NULL );
    error |= clEnqueueReadBuffer( queue, vyDev, CL_TRUE, 0, sizeOutput, vy, 0, NULL, NULL );
    error |= clEnqueueReadBuffer( queue, vzDev, CL_TRUE, 0, sizeOutput, vz, 0, NULL, NULL );

#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif
    /////////////////////////////////////////////////////////////


#if PROFILING == 1
  GetTimer();
 } // End of loop for time samples
#endif

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
 output_arr_arg("../../../data/x_out.dat",   x, nBodies, dataType);
 output_arr_arg("../../../data/y_out.dat",   y, nBodies, dataType);
 output_arr_arg("../../../data/z_out.dat",   z, nBodies, dataType);
 output_arr_arg("../../../data/vx_out.dat", vx, nBodies, dataType);
 output_arr_arg("../../../data/vy_out.dat", vy, nBodies, dataType);
 output_arr_arg("../../../data/vz_out.dat", vz, nBodies, dataType);
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(xDev);
    clReleaseMemObject(yDev);
    clReleaseMemObject(zDev);
    clReleaseMemObject(vxDev);
    clReleaseMemObject(vyDev);
    clReleaseMemObject(vzDev);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    /* clReleaseProgram(program); */
    clReleaseKernel(kernel1);
    clReleaseKernel(kernel2);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

