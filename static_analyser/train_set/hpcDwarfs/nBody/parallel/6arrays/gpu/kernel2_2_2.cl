// Simple compute kernel
//

__kernel void integrate(
   __global float*   x,
   __global float*   y,
   __global float*   z,
   __global float*   vx,
   __global float*   vy,
   __global float*   vz,
   const    float    dt,
   const    int      nBodies)
{

    int gId   = get_global_id(0);

    if(gId < nBodies) {
      x[gId] = x[gId] + vx[gId]*dt;
      y[gId] = y[gId] + vy[gId]*dt;
      z[gId] = z[gId] + vz[gId]*dt;
   }

}