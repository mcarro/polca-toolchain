#include "common_OpenCL.h"


void initOpenCLVars() {
  platformIdCount     = 0;
  platformIds         = NULL;
  deviceIdCount       = 0;
  deviceIds           = NULL;
}

char *GetPlatformName (cl_platform_id id)
{
	size_t size = 0;
	clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetPlatformInfo (id, CL_PLATFORM_NAME, size, name, NULL);

	return name;
}

char *GetDeviceName (cl_device_id id)
{
	size_t size = 0;
	clGetDeviceInfo (id, CL_DEVICE_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetDeviceInfo (id, CL_DEVICE_NAME, size,name, NULL);

	return name;
}

void CheckError (cl_int error)
{
	if (error != CL_SUCCESS) {
	  printf("OpenCL call failed with error %d\n",error);
	  exit (1);
	}
}

char *LoadKernel (const char* name, long *sourceSize)
{

	FILE *fp;		
	/* const char fileName[] = "./kernel.cl";		 */
	size_t source_size;		
	char *source_str;		
			
	/* Load kernel source file */		
	fp = fopen(name, "rb");		
	if (!fp) {		
	  fprintf(stderr, "Failed to load kernel %s\n",name);	
		exit(1);	
	}		
	source_str = (char *)malloc(MAX_SOURCE_SIZE);		
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);		
	fclose(fp);

	return source_str;
}

cl_program CreateProgram (long sourceSize,char *source,
	cl_context context)
{
  //http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clCreateProgramWithSource.html
  size_t *lengths = NULL;
  if(sourceSize!=0) {
    lengths = (size_t*)malloc(sizeof(size_t) * 1);
    lengths[0] = sourceSize;
  }
  const char* sources [1] = { source };

  cl_int error = 0;
  cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &error);
  CheckError (error);

  return program;
}


void BuildProgram(char *compileFlags,cl_program program,cl_device_id device_id)
{
  cl_int err;
  size_t log_size;
  char *build_log;
  err = clBuildProgram(program, 0, NULL, compileFlags, NULL, NULL);

  if (err != CL_SUCCESS) {
        err = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
        build_log = (char* )malloc((log_size+1));

        // Second call to get the log
        err = clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, build_log, NULL);
        build_log[log_size] = '\0';
        printf("--- Build log ---\n ");
        fprintf(stderr, "%s\n", build_log);
        free(build_log);
        printf("--- End Build log ---\n ");
	exit(1);
  }
  CheckError(err);
  printf("OpenCL program built successfully!!\n");

    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        exit(1);
    }

}

void selectOpenCLDevice(cl_uint *platformIdCount,cl_platform_id **platformIds,cl_uint *deviceIdCount,cl_device_id **deviceIds)
{
    clGetPlatformIDs (0, NULL, platformIdCount);

    if ((*platformIdCount) == 0) {
      printf("No OpenCL platform found\n");
      exit(1);
    } else {
      printf("Found %d platform(s)\n",(*platformIdCount));
    }

    *platformIds = (cl_platform_id*)malloc(sizeof(cl_platform_id)*(*platformIdCount));
    clGetPlatformIDs ((*platformIdCount), *platformIds, NULL);

    for (cl_uint i = 0; i < (*platformIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetPlatformName((*platformIds)[i]));
    }

    // Just query for GPU devices
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, 0, NULL,deviceIdCount);

    if ((*deviceIdCount) == 0) {
      printf("No OpenCL devices found\n");
      exit(1);
    } else {
      printf("Found %d GPU device(s)\n",(*deviceIdCount));
    }

    *deviceIds = (cl_device_id*)malloc(sizeof(cl_device_id)*(*deviceIdCount));
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, *deviceIdCount,
		    *deviceIds, NULL);

    for (cl_uint i = 0; i < (*deviceIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetDeviceName((*deviceIds)[i]));

      size_t param_value_size;
      size_t param_value_size_ret;

      cl_uint compute_units;
      param_value_size = sizeof(compute_units);
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_MAX_COMPUTE_UNITS,
 	param_value_size,
	&compute_units,
	&param_value_size_ret);

      size_t group_size;
      param_value_size = sizeof(group_size);
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_MAX_WORK_GROUP_SIZE,
 	param_value_size,
	&group_size,
	&param_value_size_ret);

      cl_ulong cache_size;
      param_value_size = sizeof(cache_size);
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
 	param_value_size,
	&cache_size,
	&param_value_size_ret);

      cl_ulong gMem_size;
      param_value_size = sizeof(gMem_size);
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_GLOBAL_MEM_SIZE,
 	param_value_size,
	&gMem_size,
	&param_value_size_ret);

      cl_ulong lMem_size;
      param_value_size = sizeof(lMem_size);
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_LOCAL_MEM_SIZE,
 	param_value_size,
	&lMem_size,
	&param_value_size_ret);

      printf("\t CL_DEVICE_MAX_COMPUTE_UNITS     : %d\n",(int)compute_units);
      printf("\t CL_DEVICE_MAX_WORK_GROUP_SIZE   : %d\n",(int)group_size);
      printf("\t CL_DEVICE_GLOBAL_MEM_CACHE_SIZE : %lu\n",cache_size);
      printf("\t CL_DEVICE_LOCAL_MEM_SIZE        : %lu KB\n",((cl_ulong)lMem_size/1024));
      printf("\t CL_DEVICE_GLOBAL_MEM_SIZE       : %lu MB\n",((cl_ulong)gMem_size/(1024*1024)));


    }


}

void createOpenCLContext(cl_platform_id *platformIds,cl_uint deviceIdCount,cl_device_id *deviceIds,cl_context *context)
{
    cl_int error = CL_SUCCESS; 
    const cl_context_properties contextProperties [] =
      {
	CL_CONTEXT_PLATFORM,
	(cl_context_properties)platformIds[0],
	0,
	0
      };

    *context = clCreateContext (contextProperties, deviceIdCount,
					  deviceIds, NULL, NULL, &error);
    CheckError (error);

    printf("Context created successfully!!\n");
}

void createOpenCLKernel(char *kernelFunc,char *kernelName,char *compileFlags,cl_device_id *deviceIds,cl_context context,cl_kernel *kernel)
{
    cl_program program;

    cl_int error = CL_SUCCESS; 
    /* // Create a program from source */
    long sourceSize;
    char *KernelSource = LoadKernel (kernelName,&sourceSize);
    program = CreateProgram (0,(char*)KernelSource,context);
    printf("OpenCL program %s created successfully!!\n",kernelName);

    BuildProgram(compileFlags,program,deviceIds[0]);

    // Create the compute kernel in the program we wish to run
    //
    *kernel = clCreateKernel(program, kernelFunc, &error);
    if (!(*kernel) || error != CL_SUCCESS)
    {
      printf("Error: Failed to create compute kernel: %s\n",kernelName);
        exit(1);
    }
    printf("OpenCL kernel %s created successfully!!\n",kernelFunc);

    clReleaseProgram(program);

}


void createOpenCLQueue(cl_device_id *deviceIds,cl_context context,cl_command_queue *queue)
{
    cl_int error = CL_SUCCESS; 

    *queue = clCreateCommandQueue (context, deviceIds[0],
    						   0, &error);
    CheckError (error);

    printf("OpenCL command queue created successfully!!\n");
}

////////////////////////////////////////////////////////////////////////////////
