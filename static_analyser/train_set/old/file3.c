// TEST_VECTOR: [0, 1, 0, 0, 0, 0, 1, 1, 0]
// TEST_LABEL: 1 (OpenMP)

#define N 5

int glob_var = 0;

int foo(int i)
{
    return 2 * i;
}

int main()
{
  int i,tot=0;
  int n = N;
  int a[N];
  int b[N];
  int local_var = globar_var;

#pragma stml reads a in {0}
#pragma stml writes c in {0}
#pragma stml iteration_independent
  for(i=0;i<N;i++)
    c[i] = local_var * foo(i) * a[i];

    
}
