// TEST_VECTOR: [0, 1, 0, 0, 0, 0, 1, 0, 1]
// TEST_LABEL: 2 (MPI)

#define N 5

int glob_var = 0;

int foo(int i)
{
    return 2 * i;
}

int main()
{
  int i,tot=0;
  int n = N;
  int a[N];
  int b[N];
  int local_var = global_var;

  int j;
  int num_proc;
#pragma stml loop_schedule
  for(i = 0;i < num_proc;i++)
  {
    int prev_chunk_size = N / num_proc;
    int curr_chunk_size = i != (num_proc-1) ? prev_chunk_size : prev_chunk_size + N%num_proc;

    for(j=i*prev_chunk_size;j<((i+1)*curr_chunk_size);j++)
    {
      c[j] = local_var * foo(j) * a[j];
    }
  }

    
}
