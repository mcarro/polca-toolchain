// Simple compute kernel
//


__kernel void threshold(
   __global int* input,
   const    int  thresholdValue,
   __global int* output)
{

    int i   = get_global_id(0);
    int lId = get_local_id(0);


    output[i] = input[i] % thresholdValue;   
}