// FUNC_ANALYZ: threshold
// FEAT_VECTOR: [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 4, 2, 0]
// TEST_VECTOR: [1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 4, 2, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             1
//# anyFuncCall:                 0
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        1
//# usesGlobalVars:              0
//# anyIfStmt:                   1
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       0
//# numNon1Darray:               0
//# numAuxVarArrayIndex:         4
//# totalNumForLoops:            2
//# numNonNormalizedForLoops:    0


/* This function convolves the input image by the kernel and stores the result
   in the output image. */

void threshold(int input_image[N][N], int threshold, int output_image[N][N])
{

  int c;
  int r;
  int k;
  int l;


  /* Convolve the input image with the kernel. */
  for (r = 0; r < N; r++) {
    k = r;
    for (c = 0; c < N; c++) {
          l = c;
	  if((k > N) || (l>N))
	    continue;
          output_image[k*N+l]= input_image[k*N+l] % threshold;
    }
  }
}
