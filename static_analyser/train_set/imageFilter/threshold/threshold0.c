// FUNC_ANALYZ: threshold
// FEAT_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 2, 2, 2, 0]
// TEST_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 2, 2, 2, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             1
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       0
//# numNon1Darray:               2
//# numAuxVarArrayIndex:         2
//# totalNumForLoops:            2
//# numNonNormalizedForLoops:    0

#define         N               128

int image[N][N];
int output[N][N];

int applyThreshold(int value, int threshold) {
  return value % threshold;
}

/* This function convolves the input image by the kernel and stores the result
   in the output image. */

void threshold(int input_image[N][N], int threshold, int output_image[N][N])
{

  int c;
  int r;
  int k=0;
  int l=0;
  int one = 1;

  int aux;

  /* Convolve the input image with the kernel. */
  for (r = 0; r < N; r++) {
    for (c = 0; c < N; c++) {
      aux = applyThreshold(input_image[r][c],threshold); 
      output_image[r+k][c+l]= aux * one;
    }
  }
}

void main() {

  /* Read the image */

  input_dsp(image, N*N, 1);

  int thresholdValue = 127;
  threshold(image,thresholdValue,output);


  /* Store the output */

  output_dsp(output,N*N,1);

}
