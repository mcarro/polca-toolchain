// FEAT_VECTOR: [3, 0, 0, 1, 0, 1, 1, 0, 0, 4, 1, 0, 2, 4, 0]
// TEST_VECTOR: [3, 0, 0, 1, 0, 1, 1, 0, 0, 4, 1, 0, 2, 4, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             3
//# anyFuncCall:                 0
//# anyArrayWriteShifted:        0
//# numIrregularForLoops:        1
//# usesGlobalVars:              0
//# anyIfStmt:                   1
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               4
//# numLoopHoistedVarMods:       1
//# numNon1Darray:               0
//# numAuxVarArrayIndex:         2
//# totalNumForLoops:            4
//# numNonNormalizedForLoops:    0


// Remove loop internal counters (i.e. xstart++;)
void Convolution2D(int size, int nx, float *dataIn, float *dataOut,double *filter)
{
	int filterWidth = 3;
	int filterLen = filterWidth;
	int height,width;
	height = width = nx;
	int w, h, k, l;
	int x, y, xstart, ystart;
	int sum;
	int filterLenBy2 = filterLen >> 1;
	/* Loop over processed image height indices */
	for (h = 0; h < height; h++)
	{
	  /* Loop over processed image width indices */
	  for (w = 0; w < width; w++)
	  {
		sum = 0; /* Initialize sum to 0 */
		/* Pixel row index to start with */
		ystart = h - filterLenBy2;
		/* Loop over filter length height-wise */
		for (l = 0; l < filterLen; l++)
		{
		  y = ystart+l;
		  /* ystart++; /\* Increment pixel row index *\/ */
		  /* Pixel column index to start with */
		  xstart = w - filterLenBy2;
		  /* Loop over filter length width-wise */
		  for (k = 0; k < filterLen; k++)
		  {
			x = xstart+k;
			/* xstart++; /\* Increment pixel column index *\/ */
			/* Skip pixels that are outside the image boundary (Zero-padding) */
			if ((x < 0) || (y < 0) || (x > width - 1) || (y > height - 1))
			  continue;
			/* Multiply image pixel with filter coefficient and add to sum */
			sum = sum + dataIn[y * width + x] * filter[l * filterLen + k];
		  }
		}
		/* Copy sum to output gaussian image location */
		dataOut[h * width + w] = sum;
	  }
	}
}
