// FUNC_ANALYZ: convolve2d
// FEAT_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 2, 2, 0]
// TEST_VECTOR: [1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 2, 2, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             1
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 0
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       0
//# numNon1Darray:               1
//# numAuxVarArrayIndex:         2
//# totalNumForLoops:            2
//# numNonNormalizedForLoops:    0


  /* Set the number of dead rows and columns. These represent the band of rows
     and columns around the edge of the image whose pixels must be formed from
     less than a full kernel-sized compliment of input image pixels. No output
     values for these dead rows and columns since  they would tend to have less
     than full amplitude values and would exhibit a "washed-out" look known as
     convolution edge effects. */
  /* dead_rows = K / 2; */
  /* dead_cols = K / 2; */
  /* /\* Calculate the normalization factor of the kernel matrix. *\/ */
  /* normal_factor = 0; */
  /* for (r = 0; r < K; r++) { */
  /*   for (c = 0; c < K; c++) { */
  /*     normal_factor += abs(kernel[r][c]); */
  /*   } */
  /* } */

  /* normal_factor = normal_factor == 0 ? 1 : normal_factor; */


/* This function convolves the input image by the kernel and stores the result
   in the output image. */
void convolve2d(int input_image[N][N], int kernel[K][K], int output_image[N][N])
{
  int i;
  int j;
  int c;
  int r;
  int normal_factor;
  int sum;
  int dead_rows;
  int dead_cols;

  int block[k][k];

  /* Convolve the input image with the kernel. */
  for (r = 0; r < N - K + 1; r++) {
    for (c = 0; c < N - K + 1; c++) {

      extractBlock(r,c,input_image,block);

      sum = applyMask(block,kernel);

      output_image[r+dead_rows][c+dead_cols] = (sum / normal_factor);
    }
  }
}

void extractBlock(int r,int c,int input_image[N][N],int block[K][K]) {

  for (i = 0; i < K; i++) {
    for (j = 0; j < K; j++) {
      block[i][j] = input_image[r+i][c+j];
    }
  }


}

int applyMask(int block[K][K], int kernel[K][K]) {
  int sum = 0;

  for (i = 0; i < K; i++) {
    for (j = 0; j < K; j++) {
      sum += block[i][j] * kernel[i][j];
    }
  }

  return sum;

}
