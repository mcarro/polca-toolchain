// FEAT_VECTOR: [2, 1, 1, 0, 0, 1, 1, 1, 0, 1, 2, 0, 2, 5, 0]
// TEST_VECTOR: [2, 1, 1, 0, 0, 1, 1, 1, 0, 1, 2, 0, 2, 5, 0]
// TEST_LABEL: 12 (FPGA/GPU/OpenMP)

//# maxForStmtDepth:             2
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        1
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   1
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 1
//# anyLoop_Schedule:            0
//# numLoopInvVar:               1
//# numLoopHoistedVarMods:       2
//# numNon1Darray:               0
//# numAuxVarArrayIndex:         2
//# totalNumForLoops:            5
//# numNonNormalizedForLoops:    0


/* This function convolves the input image by the kernel and stores the result
   in the output image. */
void convolve2d(int input_image[N*N], int kernel[K*K], int output_image[N*N])
{
  int i;
  int j;
  int c;
  int r;
  int normal_factor;
  int sum;
  int dead_rows;
  int dead_cols;
  /* Set the number of dead rows and columns. These represent the band of rows
     and columns around the edge of the image whose pixels must be formed from
     less than a full kernel-sized compliment of input image pixels. No output
     values for these dead rows and columns since  they would tend to have less
     than full amplitude values and would exhibit a "washed-out" look known as
     convolution edge effects. */
  dead_rows = K / 2;
  dead_cols = K / 2;
  /* Calculate the normalization factor of the kernel matrix. */
  normal_factor = 0;
  for (r = 0; r < K; r++) {
    for (c = 0; c < K; c++) {
      normal_factor += abs(kernel[r*K+c]);
    }
  }
  if (normal_factor == 0)
    normal_factor = 1;
  /* Convolve the input image with the kernel. */
#pragma stml write output_image in {(dead_rows,dead_cols)}
#pragma stml read input_image in {(0,0)}
#pragma stml iteration_independent
  for (z = 0; z < (N - K + 1)*(N - K + 1); z++) {
  /* for (r = 0; r < N - K + 1; r++) { */
  /*   for (c = 0; c < N - K + 1; c++) { */
      sum = 0;
      for (i = 0; i < K; i++) {
        for (j = 0; j < K; j++) {
          /* sum += input_image[r+i][c+j] * kernel[i][j]; */
          sum += input_image[((z / (N - K + 1))+i)*(N - K + 1) + ((z % (N - K + 1))+j)] * kernel[i*K+j];
        }
      }
      /* output_image[r+dead_rows][c+dead_cols] = (sum / normal_factor); */
      output_image[((z / (N - K + 1))+dead_rows)*(N - K + 1) + ((z % (N - K + 1))+dead_cols)] = (sum / normal_factor);
    /* } */
  }
}
