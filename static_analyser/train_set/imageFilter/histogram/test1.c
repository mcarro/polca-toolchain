/* This program enhances a 256-gray-level, 128x128 pixel image by applying
   global histogram equalization.

   This program is based on the routines and algorithms found in the book
   "C Language Algorithms for Disgital Signal Processing" by P. M. Embree
   and B. Kimble.

   Copyright (c) 1992 -- Mazen A.R. Saghir -- University of Toronto */
/* Modified to use arrays - SMP */

/* #include "traps.h" */
/* #include "stdio.h" */

#define         N       8
#define         L       49

#define         P       4

int image[N][N];
int histogram[L];
int histogram2D[P][L];
int gray_level_mapping[L];

int main()
{
  float cdf,b2;
  float pixels;
  int i,j,ii,b3;

  /* Compute the image's histogram */

   for (i = 0; i < N; i++) {
     for (j = 0; j < N; ++j) {
       image[i][j] = i*j;
     }
   }

  for (i = 0; i < L; i++)
    histogram[i] = 0;

   for (i = 0; i < N; i++) {
     for (j = 0; j < N; ++j) {
       histogram[image[i][j]] += 1;
     }
   }

   for (i = 0; i < L; i++) {
     printf("%d, ",histogram[i]);
   }
   printf("\n");


   //---------------------------------------
   // Loop scheduling
  for (i = 0; i < L; i++)
    histogram[i] = 0;

  for(int p=0;p<P;p++){
    for (i = p*N/P; i < (p+1)*N/P; i++) {
     for (j = 0; j < N; ++j) {
       histogram[image[i][j]] += 1;
     }
   }
  }

   for (i = 0; i < L; i++) {
     printf("%d, ",histogram[i]);
   }
   printf("\n");

   //---------------------------------------
   // Adding a histogram for each Computing element
   // Adding a for loop for reduction
  for(int p=0;p<P;p++)
    for (i = 0; i < L; i++)
      histogram2D[p][i] = 0;


  for(int p=0;p<P;p++){
    for (i = p*N/P; i < (p+1)*N/P; i++) {
     for (j = 0; j < N; ++j) {
       histogram2D[p][image[i][j]] += 1;
     }
   }
  }

  for(int p=1;p<P;p++)
    for (i = 0; i < L; i++)
      histogram2D[0][i] += histogram2D[p][i];

   for (i = 0; i < L; i++) {
     printf("%d, ",histogram2D[0][i]);
   }
   printf("\n");


   //---------------------------------------
   // Loop scheduling over histogram reduction for loop
  for(int p=0;p<P;p++)
    for (i = 0; i < L; i++)
      histogram2D[p][i] = 0;


  for(int p=0;p<P;p++){
    for (i = p*N/P; i < (p+1)*N/P; i++) {
     for (j = 0; j < N; ++j) {
       histogram2D[p][image[i][j]] += 1;
     }
   }
  }

  for(int p=1;p<P;p++)
    for(int k=0;k<P;k++)
      for (i = k*L/P; i < (k+1)*L/P; i++)
	histogram2D[0][i] += histogram2D[p][i];


   for (i = 0; i < L; i++) {
     printf("%d, ",histogram2D[0][i]);
   }
   printf("\n");


   return 0;
}


