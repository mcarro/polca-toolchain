
// ft1=9, ft2=4, ft3=2, ft4=0, ft5=5, ft6=2, ft7=0, ft8=4, ft9=0, ft10=0, ft11=2, ft12=0, ft13=7, ft14=0, ft15=0, ft16=10, ft17=0, ft18=0, ft24=9, ft25=1.29, ft19=0, ft39=0, ft20=2, ft33=0, ft21=6, ft35=0, ft22=3, ft23=0, ft34=0, ft36=0, ft37=0, ft38=0, ft40=0, ft41=3, ft42=0, ft43=0, ft44=0, ft45=0, ft46=3, ft48=3, ft47=8, ft49=0, ft51=14, ft50=17, ft52=5, ft53=2, ft54=0, ft55=0, ft26=0.71, ft27=1.80, ft28=3, ft29=0, ft30=4, ft31=0, ft32=3

#define         L       49
#define         N       8

int histogram[L];
int image[N][N];
int main()
{
  float cdf,b2;
  float pixels;
  int i,j,ii,b3;

   for (i = 0; i < N; i++) {
     for (j = 0; j < N; ++j) {
       histogram[image[i][j]] += 1;
     }
   }
}


// -------------------------------------

// ft1=12, ft2=6, ft3=3, ft4=0, ft5=7, ft6=3, ft7=0, ft8=6, ft9=0, ft10=0, ft11=3, ft12=0, ft13=10, ft14=0, ft15=0, ft16=14, ft17=0, ft18=0, ft24=16, ft25=1.60, ft19=0, ft39=0, ft20=3, ft33=0, ft21=12, ft35=0, ft22=8, ft23=0, ft34=0, ft36=0, ft37=0, ft38=0, ft40=0, ft41=8, ft42=0, ft43=0, ft44=0, ft45=0, ft46=3, ft48=4, ft47=13, ft49=0, ft51=18, ft50=33, ft52=10, ft53=2, ft54=0, ft55=0, ft26=0.70, ft27=1.86, ft28=4, ft29=0, ft30=6, ft31=0, ft32=4

#define         L       49
#define         N       8
#define         P       4
int histogram[L];
int image[N][N];
int main()
{
  float cdf,b2;
  float pixels;
  int i,j,ii,b3,p;

  for(p=0;p<P;p++){
    for (i = p*N/P; i < (p+1)*N/P; i++) {
     for (j = 0; j < N; ++j) {
       histogram[image[i][j]] += 1;
     }
   }
  }
}
