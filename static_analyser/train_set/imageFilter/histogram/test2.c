/* This program enhances a 256-gray-level, 128x128 pixel image by applying
   global histogram equalization.

   This program is based on the routines and algorithms found in the book
   "C Language Algorithms for Disgital Signal Processing" by P. M. Embree
   and B. Kimble.

   Copyright (c) 1992 -- Mazen A.R. Saghir -- University of Toronto */
/* Modified to use arrays - SMP */

/* #include "traps.h" */
/* #include "stdio.h" */

#define         N       8
#define         L       49

#define         P       4

int image[N][N];
int histogram[L];
int gray_level_mapping_orig[L];
int gray_level_mapping[L];

void check_output(int *orig,int *new,int size)
{
  int error = 0;
  for(int i=0;i<size;i++){
    if(orig[i]!=new[i])
      error = 1;
  }
  
  if(error)
    printf("ERROR\n");
  else
    printf("OK!!!\n");

}

int main()
{
  float cdf,b2;
  float pixels;
  int i,j,ii,b3;

  /* Compute the image's histogram */

   for (i = 0; i < N; i++) {
     for (j = 0; j < N; ++j) {
       image[i][j] = i*j;
     }
   }

  for (i = 0; i < L; i++)
    histogram[i] = 0;

   for (i = 0; i < N; i++) {
     for (j = 0; j < N; ++j) {
       histogram[image[i][j]] += 1;
     }
   }

  /* Compute the mapping from the old to the new gray levels */

  cdf = 0.0;
  pixels = (float) (N * N);
  for (i = 0; i < L; i++) {
    cdf += ((float)(histogram[i])) / pixels;
    gray_level_mapping_orig[i] = (int)(255.0 * cdf);
  }

  check_output(gray_level_mapping_orig,gray_level_mapping_orig,L);

  //---------------------------------------
  // Scalar acc. variable to vector

  float cdf_arr[L];

  for (i = 0; i < L; i++) 
    cdf_arr[i] = 0.0;

  pixels = (float) (N * N);
  for (i = 0; i < L; i++) {
    for(int k=0;k<i+1;k++)
      cdf_arr[i] += ((float)(histogram[k])) / pixels;
    gray_level_mapping[i] = (int)(255.0 * cdf_arr[i]);
  }

  check_output(gray_level_mapping_orig,gray_level_mapping,L);

  //---------------------------------------
  // Expand x += y into x = x + y

  for (i = 0; i < L; i++) 
    cdf_arr[i] = 0.0;

  pixels = (float) (N * N);
  for (i = 0; i < L; i++) {
    for(int k=0;k<i+1;k++)
      cdf_arr[i] = cdf_arr[i] + ((float)(histogram[k])) / pixels;
    gray_level_mapping[i] = (int)(255.0 * cdf_arr[i]);
  }

  check_output(gray_level_mapping_orig,gray_level_mapping,L);


  //---------------------------------------
  // Save initialization for loop

  /* for (i = 0; i < L; i++)  */
  /*   cdf_arr[i] = 0.0; */

  pixels = (float) (N * N);
  for (i = 0; i < L; i++) {
    for(int k=0;k<i+1;k++)
      cdf_arr[i] = k==0 ? 0 + ((float)(histogram[k])) / pixels : (float)cdf_arr[i]+(((float)(histogram[k])) / pixels);
    gray_level_mapping[i] = (int)(255.0 * cdf_arr[i]);
  }

  check_output(gray_level_mapping_orig,gray_level_mapping,L);

  //---------------------------------------
  // Remove addition by 0

  /* for (i = 0; i < L; i++)  */
  /*   cdf_arr[i] = 0.0; */

  pixels = (float) (N * N);
  for (i = 0; i < L; i++) {
    for(int k=0;k<i+1;k++)
      cdf_arr[i] = k==0 ? ((float)(histogram[k])) / pixels : (float)cdf_arr[i]+(((float)(histogram[k])) / pixels);
    gray_level_mapping[i] = (int)(255.0 * cdf_arr[i]);
  }

  check_output(gray_level_mapping_orig,gray_level_mapping,L);

  //---------------------------------------
  // Foor loop scheduling

  /* for (i = 0; i < L; i++)  */
  /*   cdf_arr[i] = 0.0; */

  pixels = (float) (N * N);

  for(int p=1;p<P;p++) {
    for (i = p*L/P; i < (p+1)*L/P; i++) {
      for(int k=0;k<i+1;k++)
	cdf_arr[i] = k==0 ? ((float)(histogram[k])) / pixels : (float)cdf_arr[i]+(((float)(histogram[k])) / pixels);
      gray_level_mapping[i] = (int)(255.0 * cdf_arr[i]);
    }
  }

  check_output(gray_level_mapping_orig,gray_level_mapping,L);


   return 0;
}


