////////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <OpenCL/opencl.h>
#include "ppmImage.h"
#include <sys/time.h>

////////////////////////////////////////////////////////////////////////////////
// OpenCL kernel given as string
//
/* const char *KernelSource = "\n"					\ */
/* "__kernel void rgbImageFilter_gpu(                                                       \n" \ */
/* "   __global char* image,                                              \n" \ */
/* "   __global char* redImage,                                         \n" \ */
/* "   __global char* greenImage,                                         \n" \ */
/* "   __global char* blueImage)                                         \n" \ */
/* "{                                                                      \n" \ */
/* "       int j = get_global_id(0);                                           \n" \ */
/* "       char r,g,b;                                                    \n" \ */
/* "" \ */
/* "  	r = image[j*3+0];                                     \n" \ */
/* "  	g = image[j*3+1];                                     \n" \ */
/* "  	b = image[j*3+2];                                     \n" \ */
/* "" \ */
/* "  	redImage[j*3+0] = r;      " \ */
/* "  	redImage[j*3+1] = 0;      " \ */
/* "  	redImage[j*3+2] = 0;      " \ */
/* "" \ */
/* "  	greenImage[j*3+0] = 0;      " \ */
/* "  	greenImage[j*3+1] = g;      " \ */
/* "  	greenImage[j*3+2] = 0;      " \ */
/* "" \ */
/* "  	blueImage[j*3+0] = 0;      " \ */
/* "  	blueImage[j*3+1] = 0;      " \ */
/* "  	blueImage[j*3+2] = b;      " \ */
/* "}                                                                      \n" \ */
/* "\n"; */

// This kernel is less efficient since it issues 9 global mem. read operations instead of 3,
// but it comes from mechanical code transform (see file ../transformations/rgb_filter_7_7.c)

const char *KernelSource = "\n"					\
"__kernel void rgbImageFilter_gpu(                                                       \n" \
"   __global char* image,                                              \n" \
"   __global char* redImage,                                         \n" \
"   __global char* greenImage,                                         \n" \
"   __global char* blueImage)                                         \n" \
"{                                                                      \n" \
"       int j = get_global_id(0);                                           \n" \
"" \
"       char r,g,b;                                                    \n" \
"       char r1,g1,b1;                                                    \n" \
"       char r2,g2,b2;                                                    \n" \
"" \
"  	r = image[j*3+0];                                     \n" \
"  	g = image[j*3+1];                                     \n" \
"  	b = image[j*3+2];                                     \n" \
"" \
"  	redImage[j*3+0] = r;      " \
"  	redImage[j*3+1] = 0;      " \
"  	redImage[j*3+2] = 0;      " \
"" \
"  	r1 = image[j*3+0];                                     \n" \
"  	g1 = image[j*3+1];                                     \n" \
"  	b1 = image[j*3+2];                                     \n" \
"" \
"  	greenImage[j*3+0] = 0;      " \
"  	greenImage[j*3+1] = g1;      " \
"  	greenImage[j*3+2] = 0;      " \
"" \
"  	r2 = image[j*3+0];                                     \n" \
"  	g2 = image[j*3+1];                                     \n" \
"  	b2 = image[j*3+2];                                     \n" \
"" \
"  	blueImage[j*3+0] = 0;      " \
"  	blueImage[j*3+1] = 0;      " \
"  	blueImage[j*3+2] = b2;      " \
"}                                                                      \n" \
"\n";

////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
//  Definition of variables common to *any* OpenCL program
//  Defined as global for ease the transformation process and
//  avoidi having to modify functions to pass these variables
//  as arguments
/////////////////////////////////////////////////////////////
cl_uint platformIdCount = 0;
cl_platform_id *platformIds = NULL;
cl_uint deviceIdCount = 0;
cl_device_id *deviceIds = NULL;
cl_context context;
cl_command_queue queue;
cl_program program;
cl_kernel kernel;                   // compute kernel

size_t local;                       // local domain size for our calculation

cl_int error = CL_SUCCESS; 
/////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
//  Definition of variables specific to this OpenCL program
//  Defined as global for ease the transformation process and
//  avoidi having to modify functions to pass these variables
//  as arguments
/////////////////////////////////////////////////////////////
size_t global;                      // global domain size for our calculation
/////////////////////////////////////////////////////////////

char *GetPlatformName (cl_platform_id id)
{
	size_t size = 0;
	clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetPlatformInfo (id, CL_PLATFORM_NAME, size, name, NULL);

	return name;
}

char *GetDeviceName (cl_device_id id)
{
	size_t size = 0;
	clGetDeviceInfo (id, CL_DEVICE_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetDeviceInfo (id, CL_DEVICE_NAME, size,name, NULL);

	return name;
}

void CheckError (cl_int error)
{
	if (error != CL_SUCCESS) {
	  printf("OpenCL call failed with error %d\n",error);
	  exit (1);
	}
}

char *LoadKernel (const char* name, long *sourceSize)
{

char *source = NULL;
FILE *fp = fopen(name, "r");
if (fp != NULL) {
    /* Go to the end of the file. */
    if (fseek(fp, 0L, SEEK_END) == 0) {
        /* Get the size of the file. */
        long bufsize = ftell(fp);
        if (bufsize == -1) { /* Error */ }

        /* Allocate our buffer to that size. */
        source = malloc(sizeof(char) * (bufsize + 1));
	*sourceSize = (bufsize + 1);

        /* Go back to the start of the file. */
        if (fseek(fp, 0L, SEEK_SET) != 0) { /* Error */ }

        /* Read the entire file into memory. */
        size_t newLen = fread(source, sizeof(char), bufsize, fp);
        if (newLen == 0) {
            fputs("Error reading file", stderr);
        } else {
            source[newLen] = '\0'; /* Just to be safe. */
        }
    }
    fclose(fp);
}
 return source;

}

cl_program CreateProgram (long sourceSize,char *source,
	cl_context context)
{
  //http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clCreateProgramWithSource.html
  size_t *lengths = NULL;
  if(sourceSize!=0) {
    lengths = (size_t*)malloc(sizeof(size_t) * 1);
    lengths[0] = sourceSize;
  }
  const char* sources [1] = { source };

  cl_int error = 0;
  cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &error);
  CheckError (error);

  return program;
}


void BuildProgram(cl_program program,cl_device_id device_id)
{
  cl_int err;
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    CheckError(err);
    //printf("OpenCL program built successfully!!\n");

    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        exit(1);
    }

}

void selectOpenCLDevice(cl_uint *platformIdCount,cl_platform_id **platformIds,cl_uint *deviceIdCount,cl_device_id **deviceIds)
{
    clGetPlatformIDs (0, NULL, platformIdCount);

    if ((*platformIdCount) == 0) {
      printf("No OpenCL platform found\n");
      exit(1);
    } else {
      printf("Found %d platform(s)\n",(*platformIdCount));
    }

    *platformIds = (cl_platform_id*)malloc(sizeof(cl_platform_id)*(*platformIdCount));
    clGetPlatformIDs ((*platformIdCount), *platformIds, NULL);

    for (cl_uint i = 0; i < (*platformIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetPlatformName((*platformIds)[i]));
    }

    // Just query for GPU devices
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, 0, NULL,deviceIdCount);

    if ((*deviceIdCount) == 0) {
      printf("No OpenCL devices found\n");
      exit(1);
    } else {
      printf("Found %d GPU device(s)\n",(*deviceIdCount));
    }

    *deviceIds = (cl_device_id*)malloc(sizeof(cl_device_id)*(*deviceIdCount));
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, *deviceIdCount,
		    *deviceIds, NULL);

    for (cl_uint i = 0; i < (*deviceIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetDeviceName((*deviceIds)[i]));
    }
}

void createOpenCLContext(cl_platform_id *platformIds,cl_uint deviceIdCount,cl_device_id *deviceIds,cl_context *context)
{
    cl_int error = CL_SUCCESS; 
    const cl_context_properties contextProperties [] =
      {
	CL_CONTEXT_PLATFORM,
	(cl_context_properties)platformIds[0],
	0,
	0
      };

    *context = clCreateContext (contextProperties, deviceIdCount,
					  deviceIds, NULL, NULL, &error);
    CheckError (error);

    printf("Context created successfully!!\n");
}

void createOpenCLKernel(cl_device_id *deviceIds,cl_context context,char *KernelSourceCode,cl_program *program,cl_kernel *kernel)
{
    cl_int error = CL_SUCCESS; 
    /* // Create a program from source */
    /* long sourceSize; */
    /* char *KernelSource = LoadKernel ("kernels/image.cl",&sourceSize); */
    *program = CreateProgram (0,(char*)KernelSourceCode,
		context);
    //printf("OpenCL program created successfully!!\n");

    BuildProgram(*program,deviceIds[0]);

    // Create the compute kernel in the program we wish to run
    //
    *kernel = clCreateKernel(*program, "rgbImageFilter_gpu", &error);
    if (!(*kernel) || error != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }
    //printf("OpenCL kernel created successfully!!\n");
}


void createOpenCLQueue(cl_device_id *deviceIds,cl_context context,cl_command_queue *queue)
{
    cl_int error = CL_SUCCESS; 

    *queue = clCreateCommandQueue (context, deviceIds[0],
    						   0, &error);
    CheckError (error);

    printf("OpenCL command queue created successfully!!\n");
}

////////////////////////////////////////////////////////////////////////////////

void rgbImageFilter(char* image,int width,int height,char **redImage,char **greenImage,char **blueImage)
{
    // This definition of rawWidth and the value comes directly from file rgb_filter_7_7.c (func. rgbImageFilter)
    unsigned rawWidth = width * 3;

    // Data type size of Device and host data
    size_t typeSize = sizeof(char);    
    // 3 values (RGB) per pixel
    unsigned int imageSize = width * height * 3;

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem gpu_inputImage;                       // device memory used for the input array
    unsigned int sizeInput;             // size of input array

    cl_mem gpu_redImage;                      // device memory used for the output array
    cl_mem gpu_greenImage;                      // device memory used for the output array
    cl_mem gpu_blueImage;                      // device memory used for the output array
    unsigned int sizeOutput;             // size of output array

    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    createOpenCLKernel(deviceIds,context,(char*)KernelSource,&program,&kernel);

    // Create the input and output arrays in device memory for our calculation
    //
    sizeInput = imageSize;
    gpu_inputImage = clCreateBuffer(context,  CL_MEM_READ_ONLY, typeSize * sizeInput, NULL, NULL);

    sizeOutput = imageSize;
    gpu_redImage = clCreateBuffer(context, CL_MEM_WRITE_ONLY, typeSize * sizeOutput, NULL, NULL);
    gpu_greenImage = clCreateBuffer(context, CL_MEM_WRITE_ONLY, typeSize * sizeOutput, NULL, NULL);
    gpu_blueImage = clCreateBuffer(context, CL_MEM_WRITE_ONLY, typeSize * sizeOutput, NULL, NULL);

    if (!gpu_inputImage || !gpu_redImage || !gpu_greenImage || !gpu_blueImage)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }
    
    // Write our data set into the input array in device memory
    //
    struct timeval tvalBefore, tvalAfter;
    gettimeofday(&tvalBefore,NULL);
    error = clEnqueueWriteBuffer(queue, gpu_inputImage, CL_TRUE, 0, typeSize * sizeInput, image, 0, NULL, NULL);
    gettimeofday(&tvalAfter,NULL);
    printf("\t write time: %ld microseconds\n",((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);

    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }

    // Set the arguments to our compute kernel
    //
    error = 0;
    error  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &gpu_inputImage);
    error |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &gpu_redImage);
    error |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &gpu_greenImage);
    error |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &gpu_blueImage);
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }

    // Get the maximum work group size for executing the kernel on the device
    //
    error = clGetKernelWorkGroupInfo(kernel, deviceIds[0], CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to retrieve kernel work group info! %d\n", error);
        exit(1);
    }
    /* printf("OpenCL local domain: %d\n",local); */
    /////////////////////////////////////////////////////////////


    /* printf("Applying red color filter...\n"); */
    /* printf("Applying green color filter...\n"); */
    /* printf("Applying blue color filter...\n"); */

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    // This value of "global" comes directly from file rgb_filter_7_7.c (limit of for loop in func. rgbImageFilter)
    global = height * (rawWidth/3);

    gettimeofday(&tvalBefore,NULL);
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
    gettimeofday(&tvalAfter,NULL);
    printf("\t comp. time: %ld microseconds\n",((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);

    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    gettimeofday(&tvalBefore,NULL);
    error = clEnqueueReadBuffer( queue, gpu_redImage, CL_TRUE, 0, typeSize * sizeOutput, (*redImage), 0, NULL, NULL );
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }

    error = clEnqueueReadBuffer( queue, gpu_greenImage, CL_TRUE, 0, sizeOutput * typeSize, (*greenImage), 0, NULL, NULL );
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }

    error = clEnqueueReadBuffer( queue, gpu_blueImage, CL_TRUE, 0, sizeOutput * typeSize, (*blueImage), 0, NULL, NULL );
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
    gettimeofday(&tvalAfter,NULL);
    printf("\t read time: %ld microseconds\n",((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);

    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(gpu_inputImage);
    clReleaseMemObject(gpu_redImage);
    clReleaseMemObject(gpu_greenImage);
    clReleaseMemObject(gpu_blueImage);
    /////////////////////////////////////////////////////////////



}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
    const char* filename = argc > 1 ? argv[1] : "../../../input/test.ppm";
    
    // Fill our data set with random float values
    //
    char *inputImage;
    int width,height;

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    LoadPPMImageArray(&inputImage,&width,&height,filename);
    /////////////////////////////////////////////////////////////

    // Data type size of Device and host data
    size_t typeSize = sizeof(char);    
    // 3 values (RGB) per pixel
    unsigned int imageSize = width * height * 3;
    char *redImage   = (char*) malloc( typeSize * imageSize);
    char *greenImage = (char*) malloc( typeSize * imageSize);
    char *blueImage  = (char*) malloc( typeSize * imageSize);


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    /////////////////////////////////////////////////////////////

 struct timeval tvalBefore, tvalAfter;
 for(int i=0;i<30;i++){
	  gettimeofday(&tvalBefore,NULL);
	  rgbImageFilter(inputImage,width,height,&redImage,&greenImage,&blueImage);
	  gettimeofday(&tvalAfter,NULL);
	  printf("it: %i, time: %ld microseconds\n",i,((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);
 }

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    SavePPMImageArray(redImage,width,height,"test_red.ppm");
    SavePPMImageArray(greenImage,width,height,"test_green.ppm");
    SavePPMImageArray(blueImage,width,height,"test_blue.ppm");
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

