////////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <OpenCL/opencl.h>
// For OpenCL
#define MAX_SOURCE_SIZE (0x100000)
const char kernelName[] = "./kernel_6_14_1.cl";
const char kernelFunc[] = "block_dct";

const char compileFlags[] = "";

#define DEBUG 0

#include "traps.h"

// Added just for measure time
#include <sys/time.h>

////////////////////////////////////////////////////////////////////////////////

// Use a static data size for simplicity
//

#define         N               128
#define         B               8
#define         P               1024

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    unsigned int correct;               // number of correct results returned
    
    // Fill our data set with random float values
    //
    int image[N*N];                /* 2D array that stores the original image. */
    float cos1[B*B];               /* 2D array that stores cosine coefficients. */
    float cos2[B*B];               /* 2D array that is the transpose of cos1. */
    float temp2d[B*B];             /* Temporary 2D array. */
    int result[16*16*4];          /* Result array */

    // Data type size of Device and host data
    size_t intSize = sizeof(int);
    size_t floatSize = sizeof(float);

    float factor1;
    float factor2;
    float temp_cos;

    int k;
    int l;
    int m;
    int n;

  /* Initialize the cosine matrices. "cos2" is the transpose of "cos1" */
  factor1 = 2.0 * atan(1.0) / B;
  factor2 = 0.0;
  for (m = 0; m < B; ++m) {
    for (n = 0; n < B; ++n) {
      temp_cos = cos(factor2 * (2*n + 1)) / B;
      cos1[m*B+n] = temp_cos;
      cos2[n*B+m] = temp_cos;
    }
    factor2 += factor1;
  }

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    /* Read the image */

  input_dsp_arg("../../data/input.dsp", image, 16384, 1);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  Definition of variables common to *any* OpenCL program
    /////////////////////////////////////////////////////////////
    cl_uint platformIdCount = 0;
    cl_platform_id *platformIds = NULL;
    cl_uint deviceIdCount = 0;
    cl_device_id *deviceIds = NULL;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;                   // compute kernel

    size_t local;                       // local domain size for our calculation

    cl_int error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem input;                       // device memory used for the input array
    unsigned int elemsInput;
    unsigned int sizeInput;             // size of input array

    cl_mem output;                      // device memory used for the output array
    unsigned int elemsOutput;
    unsigned int sizeOutput;             // size of input array

    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    // Deprecated
    /* createOpenCLKernel(deviceIds,context,&program,&kernel); */
    createOpenCLKernel(kernelFunc,kernelName,compileFlags,deviceIds,context,&kernel);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    elemsInput = N*N; 
    sizeInput = intSize * elemsInput;
    input = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    elemsOutput = 16*16*4; 
    sizeOutput = intSize * elemsOutput;
    output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    if (!input || !output)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }
    

    // Set the arguments to our compute kernel
    //
    error = 0;
    error  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
    error |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
    error |= clSetKernelArg(kernel, 2, sizeof(cl_mem), NULL);
    error |= clSetKernelArg(kernel, 3, sizeof(cl_mem), NULL);
    error |= clSetKernelArg(kernel, 4, sizeof(cl_mem), NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

 struct timeval tvalBefore, tvalAfter;
 int SAMPLES = 30;
 for(int i=0;i<SAMPLES;i++){
	  gettimeofday(&tvalBefore,NULL);
    // Write our data set into the input array in device memory
    //
    error = clEnqueueWriteBuffer(queue, input, CL_TRUE, 0, sizeInput, image, 0, NULL, NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

    // Get the maximum work group size for executing the kernel on the device
    //
    /* error = clGetKernelWorkGroupInfo(kernel, deviceIds[0], CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL); */
    /* if (error != CL_SUCCESS) */
    /* { */
    /*     printf("Error: Failed to retrieve kernel work group info! %d\n", error); */
    /*     exit(1); */
    /* } */
    local = B * B;
    //printf("OpenCL local domain: %d\n",local);

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = elemsInput;
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error = clEnqueueReadBuffer( queue, output, CL_TRUE, 0, sizeOutput, result, 0, NULL, NULL );
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif
    /////////////////////////////////////////////////////////////

	  gettimeofday(&tvalAfter,NULL);
	  printf("it: %i, time: %ld microseconds\n",i,((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);
} // End of loop for time samples

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    output_dsp_arg("../../data/output.dsp",result,1024,1);
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(input);
    clReleaseMemObject(output);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    /* clReleaseProgram(program); */
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

