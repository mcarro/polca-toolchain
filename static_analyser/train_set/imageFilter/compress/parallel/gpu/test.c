#include "stdio.h"

#define I 8
#define J 8
#define K 4

void printArr(void *arr){

  int i,j,k;

  int *linArr = (int*) arr;

  for(i = 0; i < I; i++) {
    for(j = 0; j < J; j++) {
      for(k = 0; k < K; k++) {
	printf("%d\n",linArr[i*(J*K)+j*K+k]);
      }
    }
  }

  for(i = 0; i < J*K; i++) {
    printf("%d\n",linArr[i]);
  }


}

int main()
{
  int arr[I][J][K];

  int i,j,k;

  int value = 0;

  for(i = 0; i < I; i++) {
    for(j = 0; j < J; j++) {
	printf("%2d, ",i*J+j);
    }
    printf("\n");
  }

  printf("\n");

  for(i = 0; i < I*J; i++) {
    if( ((i-1)/J) != ((i)/J) )
      printf("\n");

    int row = i/J;
    int col = i%J;

    int lRow = row%K;
    int lCol = col%K;

    int bRow = row / K;
    int bCol = col / K;
    /* printf("(%2d,%d), ",i,lRow*K+lCol); */

    /* printf("(%2d,|%d,%d|), ",i,lRow,lCol); */

    /* printf("(%d,|%d,%d|), ",i,bRow,bCol); */
    printf("(%2d,%2d), ",i,(bRow*K+bCol)*(K*K)+(lRow*K+lCol));
  }

  printf("\n");

  return 0;
}
