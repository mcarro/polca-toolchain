////////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <OpenCL/opencl.h>
// For OpenCL
#define MAX_SOURCE_SIZE (0x100000)
const char kernelName[] = "./kernel_6_14_0.cl";

#define DEBUG 0

#include "traps.h"

// Added just for measure time
#include <sys/time.h>

////////////////////////////////////////////////////////////////////////////////

// Use a static data size for simplicity
//

#define         N               128
#define         B               8
#define         P               1024

////////////////////////////////////////////////////////////////////////////////

// Simple compute kernel which computes the square of an input array 
//
/* const char *KernelSource = "\n" \ */
/* "__kernel void square(                                                       \n" \ */
/* "   __global int* input,                                              \n" \ */
/* "   __global int* output,                                             \n" \ */
/* "   const unsigned int count)                                           \n" \ */
/* "{                                                                      \n" \ */
/* "   int i = get_global_id(0);                                           \n" \ */
/* "   if( i < count){                                                       \n" \ */
/* "  	output[i] = 0;      " \ */
/* "   }" \ */
/* "}                                                                      \n" \ */
/* "\n"; */

////////////////////////////////////////////////////////////////////////////////

char *GetPlatformName (cl_platform_id id)
{
	size_t size = 0;
	clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetPlatformInfo (id, CL_PLATFORM_NAME, size, name, NULL);

	return name;
}

char *GetDeviceName (cl_device_id id)
{
	size_t size = 0;
	clGetDeviceInfo (id, CL_DEVICE_NAME, 0, NULL, &size);

	/* std::string result; */
	/* result.resize (size); */
	char *name = (char*)malloc(sizeof(char)*size);
	clGetDeviceInfo (id, CL_DEVICE_NAME, size,name, NULL);

	return name;
}

void CheckError (cl_int error)
{
	if (error != CL_SUCCESS) {
	  printf("OpenCL call failed with error %d\n",error);
	  exit (1);
	}
}

char *LoadKernel (const char* name, long *sourceSize)
{

	FILE *fp;		
	/* const char fileName[] = "./kernel.cl";		 */
	size_t source_size;		
	char *source_str;		
			
	/* Load kernel source file */		
	fp = fopen(name, "rb");		
	if (!fp) {		
		fprintf(stderr, "Failed to load kernel.\n");	
		exit(1);	
	}		
	source_str = (char *)malloc(MAX_SOURCE_SIZE);		
	source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);		
	fclose(fp);

	return source_str;

/* char *source = NULL; */
/* FILE *fp = fopen(name, "r"); */
/* if (fp != NULL) { */
/*     /\* Go to the end of the file. *\/ */
/*     if (fseek(fp, 0L, SEEK_END) == 0) { */
/*         /\* Get the size of the file. *\/ */
/*         long bufsize = ftell(fp); */
/*         if (bufsize == -1) { /\* Error *\/ } */

/*         /\* Allocate our buffer to that size. *\/ */
/*         source = malloc(sizeof(char) * (bufsize + 1)); */
/* 	*sourceSize = (bufsize + 1); */

/*         /\* Go back to the start of the file. *\/ */
/*         if (fseek(fp, 0L, SEEK_SET) != 0) { /\* Error *\/ } */

/*         /\* Read the entire file into memory. *\/ */
/*         size_t newLen = fread(source, sizeof(char), bufsize, fp); */
/*         if (newLen == 0) { */
/*             fputs("Error reading file", stderr); */
/*         } else { */
/*             source[newLen] = '\0'; /\* Just to be safe. *\/ */
/*         } */
/*     } */
/*     fclose(fp); */
/* } */
/*  return source; */

}

cl_program CreateProgram (long sourceSize,char *source,
	cl_context context)
{
  //http://www.khronos.org/registry/cl/sdk/1.1/docs/man/xhtml/clCreateProgramWithSource.html
  size_t *lengths = NULL;
  if(sourceSize!=0) {
    lengths = (size_t*)malloc(sizeof(size_t) * 1);
    lengths[0] = sourceSize;
  }
  const char* sources [1] = { source };

  cl_int error = 0;
  cl_program program = clCreateProgramWithSource (context, 1, sources, lengths, &error);
  CheckError (error);

  return program;
}


void BuildProgram(cl_program program,cl_device_id device_id)
{
  cl_int err;
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
    CheckError(err);
    printf("OpenCL program built successfully!!\n");

    if (err != CL_SUCCESS)
    {
        size_t len;
        char buffer[2048];

        printf("Error: Failed to build program executable!\n");
        clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        printf("%s\n", buffer);
        exit(1);
    }

}

void selectOpenCLDevice(cl_uint *platformIdCount,cl_platform_id **platformIds,cl_uint *deviceIdCount,cl_device_id **deviceIds)
{
    clGetPlatformIDs (0, NULL, platformIdCount);

    if ((*platformIdCount) == 0) {
      printf("No OpenCL platform found\n");
      exit(1);
    } else {
      printf("Found %d platform(s)\n",(*platformIdCount));
    }

    *platformIds = (cl_platform_id*)malloc(sizeof(cl_platform_id)*(*platformIdCount));
    clGetPlatformIDs ((*platformIdCount), *platformIds, NULL);

    for (cl_uint i = 0; i < (*platformIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetPlatformName((*platformIds)[i]));
    }

    // Just query for GPU devices
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, 0, NULL,deviceIdCount);

    if ((*deviceIdCount) == 0) {
      printf("No OpenCL devices found\n");
      exit(1);
    } else {
      printf("Found %d GPU device(s)\n",(*deviceIdCount));
    }

    *deviceIds = (cl_device_id*)malloc(sizeof(cl_device_id)*(*deviceIdCount));
    clGetDeviceIDs ((*platformIds)[0], CL_DEVICE_TYPE_GPU, *deviceIdCount,
		    *deviceIds, NULL);

    for (cl_uint i = 0; i < (*deviceIdCount); ++i) {
      printf("\t (%d) : %s\n",i+1,GetDeviceName((*deviceIds)[i]));

      size_t param_value_size;
      size_t param_value_size_ret;

      size_t group_size;
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_MAX_WORK_GROUP_SIZE,
 	param_value_size,
	&group_size,
	&param_value_size_ret);

      cl_ulong cache_size;
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
 	param_value_size,
	&cache_size,
	&param_value_size_ret);

      cl_ulong gMem_size;
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_GLOBAL_MEM_SIZE,
 	param_value_size,
	&gMem_size,
	&param_value_size_ret);

      cl_ulong lMem_size;
      clGetDeviceInfo((*deviceIds)[i],
 	CL_DEVICE_LOCAL_MEM_SIZE,
 	param_value_size,
	&lMem_size,
	&param_value_size_ret);

      printf("\t CL_DEVICE_MAX_WORK_GROUP_SIZE   : %d\n",group_size);
      printf("\t CL_DEVICE_GLOBAL_MEM_CACHE_SIZE : %lu\n",cache_size);
      printf("\t CL_DEVICE_LOCAL_MEM_SIZE        : %lu KB\n",((cl_ulong)lMem_size/1024));
      printf("\t CL_DEVICE_GLOBAL_MEM_SIZE       : %lu MB\n",((cl_ulong)gMem_size/(1024*1024)));


    }


}

void createOpenCLContext(cl_platform_id *platformIds,cl_uint deviceIdCount,cl_device_id *deviceIds,cl_context *context)
{
    cl_int error = CL_SUCCESS; 
    const cl_context_properties contextProperties [] =
      {
	CL_CONTEXT_PLATFORM,
	(cl_context_properties)platformIds[0],
	0,
	0
      };

    *context = clCreateContext (contextProperties, deviceIdCount,
					  deviceIds, NULL, NULL, &error);
    CheckError (error);

    printf("Context created successfully!!\n");
}

void createOpenCLKernel(cl_device_id *deviceIds,cl_context context,cl_program *program,cl_kernel *kernel)
{
    cl_int error = CL_SUCCESS; 
    /* // Create a program from source */
    long sourceSize;
    char *KernelSource = LoadKernel (kernelName,&sourceSize);
    *program = CreateProgram (0,(char*)KernelSource,
		context);
    printf("OpenCL program created successfully!!\n");

    BuildProgram(*program,deviceIds[0]);

    // Create the compute kernel in the program we wish to run
    //
    *kernel = clCreateKernel(*program, "block_dct", &error);
    if (!(*kernel) || error != CL_SUCCESS)
    {
        printf("Error: Failed to create compute kernel!\n");
        exit(1);
    }
    printf("OpenCL kernel created successfully!!\n");
}


void createOpenCLQueue(cl_device_id *deviceIds,cl_context context,cl_command_queue *queue)
{
    cl_int error = CL_SUCCESS; 

    *queue = clCreateCommandQueue (context, deviceIds[0],
    						   0, &error);
    CheckError (error);

    printf("OpenCL command queue created successfully!!\n");
}

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    unsigned int correct;               // number of correct results returned
    
    // Fill our data set with random float values
    //
    int image[N*N];                /* 2D array that stores the original image. */
    float cos1[B*B];               /* 2D array that stores cosine coefficients. */
    float cos2[B*B];               /* 2D array that is the transpose of cos1. */
    float temp2d[B*B];             /* Temporary 2D array. */
    int result[16*16*4];          /* Result array */

    // Data type size of Device and host data
    size_t intSize = sizeof(int);
    size_t floatSize = sizeof(float);

    float factor1;
    float factor2;
    float temp_cos;

    int k;
    int l;
    int m;
    int n;

  /* Initialize the cosine matrices. "cos2" is the transpose of "cos1" */
  factor1 = 2.0 * atan(1.0) / B;
  factor2 = 0.0;
  for (m = 0; m < B; ++m) {
    for (n = 0; n < B; ++n) {
      temp_cos = cos(factor2 * (2*n + 1)) / B;
      cos1[m*B+n] = temp_cos;
      cos2[n*B+m] = temp_cos;
    }
    factor2 += factor1;
  }

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    /* Read the image */

  input_dsp_arg("../../data/input.dsp", image, 16384, 1);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  Definition of variables common to *any* OpenCL program
    /////////////////////////////////////////////////////////////
    cl_uint platformIdCount = 0;
    cl_platform_id *platformIds = NULL;
    cl_uint deviceIdCount = 0;
    cl_device_id *deviceIds = NULL;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;                   // compute kernel

    size_t local;                       // local domain size for our calculation

    cl_int error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem input;                       // device memory used for the input array
    unsigned int elemsInput;
    unsigned int sizeInput;             // size of input array

    cl_mem output;                      // device memory used for the output array
    unsigned int elemsOutput;
    unsigned int sizeOutput;             // size of input array

    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);

    createOpenCLKernel(deviceIds,context,&program,&kernel);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    elemsInput = N*N; 
    sizeInput = intSize * elemsInput;
    input = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    elemsOutput = 16*16*4; 
    sizeOutput = intSize * elemsOutput;
    output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    if (!input || !output)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }
    

    // Set the arguments to our compute kernel
    //
    error = 0;
    error  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input);
    error |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
    /* error |= clSetKernelArg(kernel, 2, sizeof(unsigned int), &elemsOutput); */
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

 struct timeval tvalBefore, tvalAfter;
 int SAMPLES = 30;
 for(int i=0;i<SAMPLES;i++){
	  gettimeofday(&tvalBefore,NULL);

    // Write our data set into the input array in device memory
    //
    error = clEnqueueWriteBuffer(queue, input, CL_TRUE, 0, sizeInput, image, 0, NULL, NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

    // Get the maximum work group size for executing the kernel on the device
    //
    /* error = clGetKernelWorkGroupInfo(kernel, deviceIds[0], CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL); */
    /* if (error != CL_SUCCESS) */
    /* { */
    /*     printf("Error: Failed to retrieve kernel work group info! %d\n", error); */
    /*     exit(1); */
    /* } */
    local = B * B;
    //printf("OpenCL local domain: %d\n",local);

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = elemsInput;
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);

#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error = clEnqueueReadBuffer( queue, output, CL_TRUE, 0, sizeOutput, result, 0, NULL, NULL );
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif
    /////////////////////////////////////////////////////////////
	  gettimeofday(&tvalAfter,NULL);
	  printf("it: %i, time: %ld microseconds\n",i,((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);
 } // End of loop for time samples

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    output_dsp_arg("../../data/output.dsp",result,1024,1);
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(input);
    clReleaseMemObject(output);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

