// Simple compute kernel
//

#define         N               128
#define         B               8

#define         ROUND(f)        (((f)<0.0) ? (int)((f)-0.5) : (int)((f)+0.5))

int pack2in8(int a, int b)
{
  /* limit to signed 4 bits */

  a &= 0x0f;
  b &= 0x0f;
  return((a << 4) | (b));
}

int pack4in32(int a,int b,int c,int d)
{
  /* limit to signed 8 bits */

  a &= 0xff;
  b &= 0xff;
  c &= 0xff;
  d &= 0xff;

  return((a << 24) | (b << 16) | (c << 8) | (d));
}

__kernel void block_dct(
   __global int* input,
   __global int* output,
   __local  float* cos1,
   __local  float* cos2,
   __local  float* temp2d,
   __local  int* block)
{
    float factor1;
    float factor2;
    float temp_cos;
//    float cos1[B*B];               /* 2D array that stores cosine coefficients. */
//    float cos2[B*B];               /* 2D array that is the transpose of cos1. */

//    float temp2d[B*B];

//    int block[B*B];

    int k;

    int i   = get_global_id(0);
    int lId = get_local_id(0);
    int gRow = i / N;
    int gCol = i % N;

    int lRow = gRow % B;
    int lCol = gCol % B;
    
    int bRow = gRow / B;
    int bCol = gCol / B;

    /* Initialize the cosine matrices. "cos2" is the transpose of "cos1" */
    factor1 = 2.0 * atan(1.0) / B;
    factor2 = factor1 * lRow;

    temp_cos = cos(factor2 * (2*lCol + 1)) / B;
    cos1[lRow*B+lCol] = temp_cos;
    cos2[lCol*B+lRow] = temp_cos;


    block[lRow*B+lCol] = input[(bRow*B+bCol)*(B*B)+(lRow*B+lCol)];

    // compute dct
    float sum = 0.0;
    for (k = 0; k < B; k++) {
    	sum += block[lRow*B+k] * cos2[k*B+lCol];
    }
    temp2d[lRow*B+lCol] = sum;

    sum = 0.0;
    for (k = 0; k < B; k++) {  /* advances cos1 col */
        sum += cos1[lRow*B+k] * temp2d[k*B+lCol];
    }
    /* round the result */
    block[lRow*B+lCol] = ROUND(sum);

    input[(bRow*B+bCol)*(B*B)+(lRow*B+lCol)] = block[lRow*B+lCol];

    /* Select coefficients, scale, and pack */
    if( lId == 0){
      int outInd = i / (B*B);
      output[outInd+0] = pack4in32(block[0*B+0],block[0*B+1],
				  pack2in8(block[0*B+2] / 3, block[0*B+3] / 3),
				  pack2in8(block[0*B+4] / 2, block[0*B+5] / 2));

      output[outInd+1] = pack4in32(pack2in8(block[0*B+6],block[0*B+7]),
				  block[1*B+0],block[1*B+1],
				  pack2in8(block[1*B+2] / 2,block[1*B+3]));

      output[outInd+2] = pack4in32(pack2in8(block[1*B+4], block[1*B+5]),
				  pack2in8(block[2*B+0] / 3, block[2*B+1] / 2),
				  pack2in8(block[2*B+2] / 2, block[2*B+3]),
				  pack2in8(block[3*B+0] / 3, block[3*B+1]));

      output[outInd+3] = pack4in32(pack2in8(block[3*B+2], block[3*B+3]),
				  pack2in8(block[4*B+0] / 2, block[4*B+1]),
				  pack2in8(block[5*B+0] / 2, block[5*B+1]),
				  pack2in8(block[6*B+0] , block[7*B+0]));

    }
   
}