#!/usr/bin/python

import os,re

import numpy as np
import matplotlib.pyplot as plt
# import itertools
from matplotlib import colors

sequences = []

def processTimes():
    f = open("times.txt")
    lines = f.readlines()

    numSamples = 0
    totalTime  = 0
    for line in lines:
        m1 = re.match("it: \d+, time: (\d+) microseconds",line)
        if m1:
            # print m1.group(1)
            totalTime  += int(m1.group(1))
            numSamples +=1

    f.close()

    return(totalTime/numSamples)

def plotSequences(programNames,programTimes):

    # colorNames = [name for name in colors.cnames.keys()]

    # Check for color names either in:
    # - http://matplotlib.org/examples/color/named_colors.html
    # - colors.cnames.keys()
    colores = ['tomato','b','g','c','y']
    colorNames = [name for name in colores]

    N = len(programNames)
    ind = np.arange(N)  # the x locations for the groups
    # ind = 1
    width = 0.35       # the width of the bars

    fig, ax = plt.subplots()
    count = 0
    labels = []
    for value in programTimes:
        if programNames[count] == "original":
            currentColor = "y"
        else:
            currentColor = colorNames[count-1]

        rects1 = ax.bar(ind[count], value, width, color=currentColor)

        speedUp = float(programTimes[0])/float(value)
        height = rects1[0].get_height()
        print("%s - %d / %d (%.2f)" % (programNames[count],programTimes[0],value,speedUp))
        ax.text(rects1[0].get_x() + rects1[0].get_width()/2., 1.05*height,
                '%.2fx' % speedUp,
                ha='center', va='bottom')
        if count == 0:
            labels.append("original")
        else:
            labels.append("Seq%d" % (count))            

        count += 1

    # add some text for labels, title and axes ticks
    ax.set_ylabel(r'Exec. Time ($\mu$s)')
    ax.set_title('Exec. Times (original vs. final versions)')
    ax.set_xticks(ind + (width/2))
    # labels = [name.replace("compress_gpu_","") for name in programNames]
    ax.set_xticklabels(labels)
    ax.set_xlim([-width,ind[len(ind)-1] + (width*2)])

    fig.tight_layout()
    fig.savefig('./finalExecTimes.png',dpi=300)

    plt.show()


###########################################################

# programs = ["compress_gpu_2_14_0","compress_gpu_2_14_1","compress_gpu_6_14_0","compress_gpu_6_14_1"]
programs = ["compress_gpu_1_10_1","compress_gpu_5_10_1","compress_gpu_2_14_1","compress_gpu_6_14_1"]


programNames = ["original"]
programTimes = [1078]

# programNames.append("compress_gpu_2_14_0")
# programTimes.append(1637)

programNames.append("compress_gpu_1_10_1")
programTimes.append(837)

programNames.append("compress_gpu_2_14_1")
programTimes.append(762)

programNames.append("compress_gpu_5_10_1")
programTimes.append(652)

# programNames.append("compress_gpu_6_14_0")
# programTimes.append(3837)

programNames.append("compress_gpu_6_14_1")
programTimes.append(426)

# programTimesDict["original"] = 1078

# Execute all programs only once
for program in programs:
    cmd = "./%s.x > times.txt" % program
    # print(cmd)
    os.system(cmd)

    if not (program in programNames):
        programNames.append(program)
        execTime = processTimes()
        programTimes.append(execTime)
        print("%s - %d" % (program,execTime))
        # programTimesDict[program] = processTimes()


# timesList = []
# for sequence in sequences:
#     sequenceTimesList = []
#     for pair in sequence:
#         sequenceTimesList.append((programTimesDict[pair[0]],pair[1]))
#         # print(sequenceTimesList[len(sequenceTimesList)-1])
#     timesList.append(sequenceTimesList)



plotSequences(programNames,programTimes)

cmd = "rm times.txt"
print(cmd)
os.system(cmd)
