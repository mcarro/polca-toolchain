OpenCL versions coming from transformation sequences. For each version there is a host and kernel files, i.e. for sequence X, transformation step Y and version Z, file names would be:
       - compress_gpu_X_Y_Z.c
       - kernel_X_Y_Z.cl

From sequence 1:
  - 1_10_0: straight forward translation from C code to OpenCL
  - 1_10_1: all shared arrays to local memory

From sequence 5:

  - 5_10_0: straight forward translation from C code to OpenCL
  - 5_10_1: all shared arrays to local memory


From sequence 2:
  - 2_14_0: straight forward translation from C code to OpenCL
  - 2_14_1: all shared arrays to local memory

From sequence 6:

  - 6_14_0: straight forward translation from C code to OpenCL
  - 6_14_1: all shared arrays to local memory

From sequence 3:

From sequence 4:
