collapse_2_for_loops
{
    pattern:
    {
        // #pragma polca def a
        for(cexpr(i) = cexpr(initial_value); cexpr(i) < cexpr(limit1); cexpr(i)++)
       {
           cstmts(prelude);
            for(cexpr(j) = cexpr(initial_value); cexpr(j) < cexpr(limit2); cexpr(j)++)
            {
                cstmts(body);
            }
            cstmts(postlude);
       }
    }
    condition:
    {
        no_writes(cexpr(i),cstmts(body));
        no_writes(cexpr(j),cstmts(body));
        no_writes(cexpr(i),cstmts(prelude));
        no_writes(cexpr(i),cstmts(postlude));
    }
    generate:
    {
        // #pragma polca same_properties a
        for(cexpr(j) = cexpr(initial_value); cexpr(j) < (cexpr(limit1) * cexpr(limit2)); cexpr(j)++)
        {
            if_then_else:
            {
                no_empty(cstmts(prelude));
                if(cexpr(j) % cexpr(limit2) == 0) 
                {
                    cstmts(prelude);
                }
                ;
            }
            subs(
                subs(cstmts(body),cexpr(j),cexpr(j)%cexpr(limit2)),
                cexpr(i),
                cexpr(j)/cexpr(limit2)
                ); 
            if_then_else:
            {
                no_empty(cstmts(postlude));
                if(cexpr(j) % cexpr(limit2) == 0) 
                {
                    cstmts(postlude);
                }
                ;
            }      
        }
    }
}

inlining
{
    pattern:
    {
        cstmts(ini);
        // #pragma stml has_function_calls true 
        cstmt(with_call);
        cstmts(fin);
    }
    condition:
    {
        has_calls(cstmt(with_call));
    }
    generate:
    {
        cstmts(ini);
        inline_stmt(cstmt(with_call));
        cstmts(fin);
    }
}

remove_block
{
    pattern:
    {
        cstmts(ini);
        {
            cstmts(block);
        }
        cstmts(fin);
    }
    generate:
    {
        cstmts(ini);
        fresh(cstmts(block));
        cstmts(fin);
    }
}

remove_useless_statement
{
    pattern:
    {
        cstmts(ini);
        cstmt(mid);
        cstmts(end);
    }
    condition:
    {
        is_expr(cstmt(mid));
        pure(cstmt(mid));
    }
    generate:
    {
        cstmts(ini);
        cstmts(end);
    }    
}

flatten_float_array
{
    pattern:
    {
        cstmts(ini);
        cdecl(cfloat(),cexpr(v)[cexpr(l1)][cexpr(l2)]);
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        cdecl(cfloat(),cexpr(v)[cexpr(l1)*cexpr(l2)]);
        change_access(cstmts(end), cexpr(v), flatten(cexpr(l2)));
    }   
}

flatten_int_array
{
    pattern:
    {
        cstmts(ini);
        cdecl(cint(),cexpr(v)[cexpr(l1)][cexpr(l2)]);
        cstmts(end);
    }
    generate:
    {
        cstmts(ini);
        cdecl(cint(),cexpr(v)[cexpr(l1)*cexpr(l2)]);
        change_access(cstmts(end), cexpr(v), flatten(cexpr(l2)));
    }   
}

remove_aux_2D_array
{
    pattern:
    {
        cstmts(ini);
        for (cexpr(k) = 0; cexpr(k) < cexpr(l1); cexpr(k)++) {
            for (cexpr(l) = 0; cexpr(l) < cexpr(l2); cexpr(l)++) {
                cexpr(v)[cexpr(k)][cexpr(l)] = 
                    cexpr(nv)[cexpr(m1)+cexpr(k)][cexpr(m2)+cexpr(l)];
            }
        }
        cstmts(end);
    }
    condition:
    {
        no_writes(cexpr(nv), cstmts(end));
    }
    generate:
    {
        cstmts(ini);
        change_array(cstmts(end), cexpr(v), cexpr(nv), cexpr(m1), cexpr(m2));
    }   
}

// Fusion of two for-loops into a single one. This rule only should be applied ...
for_loop_fusion
{
    pattern:
    {
        cstmts(ini);
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
        {
            cstmts(bodyFOR1);
        }
        cstmts(mid);
        for(cexpr(j) = cexpr(init);cexpr(j) < cexpr(n);cexpr(modj))
        {
            cstmts(bodyFOR2);
        }
        cstmts(fin);
    }
    condition:
    {
        no_reads(cexpr(i),cstmts(mid));
        // This only shuold be a condtion when i/= j
        // no_rw(cexpr(i),cstmts(bodyFOR2));
        no_writes(cexpr(i),cstmts(bodyFOR2));
        no_writes(cexpr(j),cstmts(bodyFOR1));
    }
    generate:
    {
        cstmts(ini);
        cstmts(mid);
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(modi))
        {
            cstmts(bodyFOR1);
            subs(cstmts(bodyFOR2), cexpr(j), cexpr(i));
        }
        cstmts(fin);
    }
}


// Fission of two for-loops into a single one. AKA loop distribution or loop splitting
for_loop_fission
{
    pattern:
    {
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(body1);
            cstmt(mid);
            cstmts(body2);
        }
    }
    condition:
    {
        no_writes(cexpr(i),cstmts(body1));
        no_writes(cexpr(i),cstmt(mid));
        no_writes(cexpr(i),cstmts(body2));
        no_reads_in_written(cstmt(mid),cstmts(body2));
        no_reads_in_written(cstmts(body1),cstmts(body2));
        no_empty(cstmts(body2));
    }
    generate:
    {   
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(body1);
            cstmt(mid);
        }
        for(cexpr(i) = cexpr(init);cexpr(i) < cexpr(n);cexpr(i)++)
        {
            cstmts(body2);
        }
    }
}
