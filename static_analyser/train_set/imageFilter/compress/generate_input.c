#include <stdio.h>
#include <stdlib.h>

#define SIZE 16384

void output_dsp (fileName, words)
char *fileName;
int  words;
{
  FILE	*output_fp=NULL;

  int i;

  if (output_fp==NULL && (output_fp = fopen (fileName, "w")) == NULL) {
    printf ("** Error: cannot open output.dsp.\n");
    exit(1);
  }

  if (words <= 0) {
    printf ("** Error: trying to write a negative or zero number of words.\n");
    exit (1);
  }

  for (i = 0; i < words; i++)
    fprintf (output_fp, "%d\n", 255);

  fclose(output_fp);

}

int main(int argc, char* argv[]){

  char fileName[80];

  sprintf(fileName, "./data/input_%d.dsp",SIZE);

  output_dsp(fileName,SIZE);

  return 0;

}
