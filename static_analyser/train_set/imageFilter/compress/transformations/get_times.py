#!/usr/bin/python

import os,re

import numpy as np
import matplotlib.pyplot as plt
#import itertools
from matplotlib import colors

sequences = []

def processTimes():
    f = open("times.txt")
    lines = f.readlines()

    numSamples = 0
    totalTime  = 0
    for line in lines:
        m1 = re.match("it: \d+, time: (\d+) microseconds",line)
        if m1:
            # print m1.group(1)
            totalTime  += int(m1.group(1))
            numSamples +=1

    f.close()

    return(totalTime/numSamples)

def plotSequences(timesList):

    fig = plt.figure()

    ax1 = fig.add_subplot(111)

    ax1.set_title("Exec. Times of Trans. Sequences")    
    ax1.set_xlabel('Trans. Step')
    ax1.set_ylabel(r'Exec. Time ($\mu$s)')

    # colorNames = (name for name in colors.cnames.keys())

    # Check for color names either in:
    # - http://matplotlib.org/examples/color/named_colors.html
    # - colors.cnames.keys()
    colores = ['tomato','b','g','c','y']
    colorNames = (name for name in colores)

    # maxLength = 1e-20
    currentSeq = 1
    for sequence in timesList:
        # maxLength = len(sequence)

        y = []
        x = []
        count = 0
        for pair in sequence:
            y.append(pair[0])
            if pair[1] == "OpenCL":
                plt.annotate("To %s" % pair[1],xy=(count, pair[0]), arrowprops=dict(arrowstyle='->'), xytext=(11, 1350))

            x.append(count)
            count += 1


        # x = range(1,maxLength+1)
        print(x)
        print(y)
        print("------------------------------")

        seqName = "Seq%d" % currentSeq
        col = next(colorNames)
        ax1.plot(x,y, c=col, label=seqName)
        ax1.set_xlim([0,16])
        ax1.set_ylim([900,1850])

        # Shrink current axis by a %
        box = ax1.get_position()
        # ax1.set_position([box.x0, box.y0, box.width * 0.945, box.height])
        ax1.set_position([box.x0, box.y0, box.width  * 1.023, box.height])

        # Put a legend to the right of the current axis
        # ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        ax1.legend(loc='lower right')

        # leg = ax1.legend(loc="upper left")

        currentSeq += 1

    # fig.tight_layout()
    # fig.savefig('./sequencesExecTimes.png')

    plt.savefig('./sequencesExecTimes.png',dpi=300)

    plt.show()

    plt.close(fig)    # close the figure


###########################################################

# programs = ["compress","compress_1_1","compress_1_2","compress_1_3","compress_1_4","compress_1_5","compress_1_6","compress_1_7","compress_1_8","compress_1_9","compress_1_10","compress_1_11","compress_2_1","compress_2_2","compress_2_3","compress_2_4","compress_2_5","compress_2_6","compress_2_7","compress_2_8","compress_2_9","compress_2_10","compress_2_11","compress_2_12","compress_2_13","compress_2_14","compress_2_15","compress_5_1","compress_5_2","compress_5_3","compress_5_4","compress_5_5","compress_5_6","compress_5_7","compress_5_8","compress_5_9","compress_5_10"]

# programs = ["compress_5_1","compress_5_2","compress_5_3","compress_5_4","compress_5_5","compress_5_6","compress_5_7","compress_5_8","compress_5_9","compress_5_10"]

# programs = ["compress_1_1","compress_1_2","compress_1_3","compress_1_4","compress_1_5","compress_1_6","compress_1_7","compress_1_8","compress_1_9","compress_1_10"]

# programs=["compress_6_1", "compress_6_2", "compress_6_3", "compress_6_4", "compress_6_5", "compress_6_6", "compress_6_7", "compress_6_8", "compress_6_9", "compress_6_10", "compress_6_11", "compress_6_12", "compress_6_13", "compress_6_14"]

programs=[]

sequences.append([("compress",""),("compress_1_1",""),("compress_1_2",""),("compress_1_3",""),("compress_1_4",""),("compress_1_5",""),("compress_1_6",""),("compress_1_7",""),("compress_1_8",""),("compress_1_9",""),("compress_1_10","OpenCL")])

sequences.append([("compress",""),("compress_2_1",""),("compress_2_2",""),("compress_2_3",""),("compress_2_4",""),("compress_2_5",""),("compress_2_6",""),("compress_2_7",""),("compress_2_8",""),("compress_2_9",""),("compress_2_10",""),("compress_2_11",""),("compress_2_12",""),("compress_2_13",""),("compress_2_14","OpenCL")])

sequences.append([("compress",""),("compress_5_1",""),("compress_5_2",""),("compress_5_3",""),("compress_5_4",""),("compress_5_5",""),("compress_5_6",""),("compress_5_7",""),("compress_5_8",""),("compress_5_9",""),("compress_5_10","OpenCL")])

sequences.append([("compress",""),("compress_6_1",""),("compress_6_2",""),("compress_6_3",""),("compress_6_4",""),("compress_6_5",""),("compress_6_6",""),("compress_6_7",""),("compress_6_8",""),("compress_6_9",""),("compress_6_10",""),("compress_6_11",""),("compress_6_12",""),("compress_6_13",""),("compress_6_14","OpenCL")])

programTimesDict = {}

programTimesDict["compress"]=1078

programTimesDict["compress_1_1"]=1065
programTimesDict["compress_1_2"]=1123
programTimesDict["compress_1_3"]=1239
programTimesDict["compress_1_4"]=1179
programTimesDict["compress_1_5"]=1248
programTimesDict["compress_1_6"]=1243
programTimesDict["compress_1_7"]=1293
programTimesDict["compress_1_8"]=1306
programTimesDict["compress_1_9"]=1196
programTimesDict["compress_1_10"]=1266

programTimesDict["compress_2_1"]=1036
programTimesDict["compress_2_2"]=1280
programTimesDict["compress_2_3"]=1623
programTimesDict["compress_2_4"]=1660
programTimesDict["compress_2_5"]=1587
programTimesDict["compress_2_6"]=1630
programTimesDict["compress_2_7"]=1728
programTimesDict["compress_2_8"]=1620
programTimesDict["compress_2_9"]=1555
programTimesDict["compress_2_10"]=1659
programTimesDict["compress_2_11"]=1609
programTimesDict["compress_2_12"]=1592
programTimesDict["compress_2_13"]=1636
programTimesDict["compress_2_14"]=1639


programTimesDict["compress_5_1"]=997
programTimesDict["compress_5_2"]=1059
programTimesDict["compress_5_3"]=1067
programTimesDict["compress_5_4"]=1199
programTimesDict["compress_5_5"]=1232
programTimesDict["compress_5_6"]=1242
programTimesDict["compress_5_7"]=1380
programTimesDict["compress_5_8"]=1199
programTimesDict["compress_5_9"]=1257
programTimesDict["compress_5_10"]=1205

programTimesDict["compress_6_1"]=1024
programTimesDict["compress_6_2"]=1074
programTimesDict["compress_6_3"]=1081
programTimesDict["compress_6_4"]=1686
programTimesDict["compress_6_5"]=1596
programTimesDict["compress_6_6"]=1633
programTimesDict["compress_6_7"]=1494
programTimesDict["compress_6_8"]=1798
programTimesDict["compress_6_9"]=1512
programTimesDict["compress_6_10"]=1624
programTimesDict["compress_6_11"]=1529
programTimesDict["compress_6_12"]=1545
programTimesDict["compress_6_13"]=1599
programTimesDict["compress_6_14"]=1490

# Execute all programs only once
for program in programs:
    cmd = "./%s.x > times.txt" % program
    print(cmd)
    os.system(cmd)

    programTimesDict[program] = processTimes()


timesList = []
for sequence in sequences:
    sequenceTimesList = []
    for pair in sequence:
        sequenceTimesList.append((programTimesDict[pair[0]],pair[1]))
        # print(sequenceTimesList[len(sequenceTimesList)-1])
    timesList.append(sequenceTimesList)

plotSequences(timesList)

cmd = "rm times.txt"
print(cmd)
os.system(cmd)
