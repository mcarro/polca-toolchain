////////////////////////////////////////////////////////////////////////////////

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <OpenCL/opencl.h>
// For OpenCL
#define MAX_SOURCE_SIZE (0x100000)
const char kernelName[] = "./kernel_auto.cl";
const char kernelFunc[] = "convolve2d";

const char compileFlags[] = "";


#define PROFILING 1
#define DEBUG 1

#include "traps.h"

// Added just for measure time
#include <sys/time.h>

////////////////////////////////////////////////////////////////////////////////

// Use a static data size for simplicity
//

#define         K       3
#define         N       128
#define         T       127

////////////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  //int err;                            // error code returned from api calls
 
    unsigned int correct;               // number of correct results returned
    
    // Fill our data set with random float values
    //
    int image_buffer1[N*N];
    int image_buffer2[N*N];
    int image_buffer3[N*N];
    int filter[K*K];

    // Data type size of Device and host data
    size_t intSize = sizeof(int);
    size_t floatSize = sizeof(float);


    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    /* Read the image */

  input_dsp_arg("../../data/input.dsp", image_buffer1, N*N, 1);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  Definition of variables common to *any* OpenCL program
    /////////////////////////////////////////////////////////////
    cl_uint platformIdCount = 0;
    cl_platform_id *platformIds = NULL;
    cl_uint deviceIdCount = 0;
    cl_device_id *deviceIds = NULL;
    cl_context context;
    cl_command_queue queue;
    cl_program program;
    cl_kernel kernel;                   // compute kernel

    size_t local;                       // local domain size for our calculation

    cl_int error = CL_SUCCESS; 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  Definition of variables specific to this OpenCL program
    /////////////////////////////////////////////////////////////
    size_t global;                      // global domain size for our calculation
    
    // For each input/output buffer we need to define"
    // - The buffer (cl_mem)
    // - The size of the buffer
    cl_mem input1;                       // device memory used for the input array
    cl_mem input2;                       // device memory used for the input array
    cl_mem input3;                       // device memory used for the input array
    unsigned int elemsInput;
    unsigned int sizeInput;             // size of input array

    cl_mem output;                      // device memory used for the output array
    unsigned int elemsOutput;
    unsigned int sizeOutput;             // size of input array

    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////    

    selectOpenCLDevice(&platformIdCount,&platformIds,&deviceIdCount,&deviceIds);

    createOpenCLContext(platformIds,deviceIdCount,deviceIds,&context);

    createOpenCLQueue(deviceIds,context,&queue);
    
    // Deprecated
    /* createOpenCLKernel(deviceIds,context,&program,&kernel); */
    createOpenCLKernel(kernelFunc,kernelName,compileFlags,deviceIds,context,&kernel);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////

    // Create the input and output arrays in device memory for our calculation
    //
    elemsInput = N*N; 
    sizeInput = intSize * elemsInput;
    input1 = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    input2 = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    input3 = clCreateBuffer(context,  CL_MEM_READ_ONLY, sizeInput, NULL, NULL);
    elemsOutput = N*N; 
    sizeOutput = intSize * elemsOutput;
    output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeOutput, NULL, NULL);
    if (!input1 || !input2 || !input3 || !output)
    {
        printf("Error: Failed to allocate device memory!\n");
        exit(1);
    }
    

    // Set the arguments to our compute kernel
    //
    error = 0;
    error  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input1);
    error |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &output);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", error);
        exit(1);
    }
#endif

#if PROFILING == 1
 struct timeval tvalBefore, tvalAfter;
 int SAMPLES = 30;
 for(int i=0;i<SAMPLES;i++){
	  gettimeofday(&tvalBefore,NULL);
#endif

    // Write our data set into the input array in device memory
    //
    error = clEnqueueWriteBuffer(queue, input1, CL_TRUE, 0, sizeInput, image_buffer1, 0, NULL, NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

    local = K * K;
    //printf("OpenCL local domain: %d\n",local);

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = (N - K + 1) * (N - K + 1);
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error = clEnqueueReadBuffer( queue, output, CL_TRUE, 0, sizeOutput, image_buffer3, 0, NULL, NULL );
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif




    // Write our data set into the input array in device memory
    //
    error = clEnqueueWriteBuffer(queue, input1, CL_TRUE, 0, sizeInput, image_buffer3, 0, NULL, NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

    local = K * K;
    //printf("OpenCL local domain: %d\n",local);

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = (N - K + 1) * (N - K + 1);
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error = clEnqueueReadBuffer( queue, output, CL_TRUE, 0, sizeOutput, image_buffer1, 0, NULL, NULL );
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif




    // Write our data set into the input array in device memory
    //
    error = clEnqueueWriteBuffer(queue, input1, CL_TRUE, 0, sizeInput, image_buffer3, 0, NULL, NULL);
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to write to source array!\n");
        exit(1);
    }
#endif

    local = K * K;
    //printf("OpenCL local domain: %d\n",local);

    // Execute the kernel over the entire range of our 1d input data set
    // using the maximum number of work group items for this device
    //
    global = (N - K + 1) * (N - K + 1);
    error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
#if DEBUG == 1
    if (error)
    {
        printf("Error: Failed to execute kernel!\n");
        return EXIT_FAILURE;
    }
#endif
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    // Wait for the command commands to get serviced before reading back results
    //
    clFinish(queue);
    /////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    // Read back the results from the device to verify the output
    //
    error = clEnqueueReadBuffer( queue, output, CL_TRUE, 0, sizeOutput, image_buffer2, 0, NULL, NULL );
#if DEBUG == 1
    if (error != CL_SUCCESS)
    {
        printf("Error: Failed to read output array! %d\n", error);
        exit(1);
    }
#endif

#if PROFILING == 1
    /////////////////////////////////////////////////////////////

	  gettimeofday(&tvalAfter,NULL);
	  /* printf("it: %i, time: %ld microseconds\n",i,((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L */
	  			          /* +tvalAfter.tv_usec) - tvalBefore.tv_usec); */
	  printf("%ld\n",((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L
	  			          +tvalAfter.tv_usec) - tvalBefore.tv_usec);
} // End of loop for time samples
#endif

    //////////////////////////////////////////////////////////////
    //  I/O code specific for this program
    /////////////////////////////////////////////////////////////
    output_dsp_arg("../../data/output1.dsp",image_buffer1, N*N, 1);
    output_dsp_arg("../../data/output2.dsp",image_buffer2, N*N, 1);
    output_dsp_arg("../../data/output3.dsp",image_buffer3, N*N, 1);
    output_dsp_arg("../../data/filter.dsp",filter, K*K, 1);
    /////////////////////////////////////////////////////////////
    


    //////////////////////////////////////////////////////////////
    //  Code for releasing OpenCL 
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL specific calls for this program
    /////////////////////////////////////////////////////////////
    clReleaseMemObject(input1);
    clReleaseMemObject(input2);
    clReleaseMemObject(input3);
    clReleaseMemObject(output);
    /////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////
    //  OpenCL calls common to *any* program
    /////////////////////////////////////////////////////////////
    /* clReleaseProgram(program); */
    clReleaseKernel(kernel);
    clReleaseCommandQueue(queue);
    clReleaseContext(context);
    /////////////////////////////////////////////////////////////

    return 0;
}

