#!/usr/bin/python

# Copyright (c) 2013-2016, The IMDEA Software Institute and
# Copyright (c) 2013-2016, Universidad Politecnica de Madrid

# See LICENSE.txt and AUTHORS.txt for licensing and authorship


import sys,re,os

nameRules = []

abst2State   = {}
abst2SeqList = {}
seqList2State = {}

trainTransitionTable = {}

initStates = []
finalStates = []

def convertAbsList2Key(abstraction):

    return "".join(str(x) for x in eval(abstraction))


# return a string of the current sequence list from the file name that is being
# processed. The string is calculated to use it as hash key for a dictionary. E.g.
#
# For the file name:   nbody_001_003_002.c
# Recives the list:    [001,003,002]
# returns the string:  001_003_002
def seqList2Str(seqList):

    return '_'.join(seqList)


# IMPORTANT: this function relies on the list of files that is done by python
#            as depth-first. If is done breadth-first it won't work correctly
def updateTrainTable(useCase):
    global initStates,nameRules,abst2State,trainTransitionTable,finalStates


    name = useCase[0]
    path = useCase[1]

    # initState = 0
    # initStates.append(initState)

    # use only files *.c and avoid *.c~ (emacs) and .#*.c (emacs)
    # namePattern = "^%s(_(.)*_(.)*)*\.c$" % name
    # namePattern = "^%s_(\d+)_(\d+)\.c$" % name
    namePattern = "^%s(_\d+)*\.c$" % name
    for dirpath, dirnames, filenames in os.walk(path):

        ruleAppldId = -1
        abstraction = None
        stateId = -1
        prevStateId = -1
        abstInitState = -1
        currTransfStep = -1
        currSeqId = []
        prevSeqId = []

        for file in filenames:
            m0 = re.match(namePattern,file)
            if m0:
                r = re.compile("(_\d+)")
                currSeqId = r.findall(file)
                # clean numbers to remove "_"
                currSeqId = [x.replace("_","") for x in currSeqId]

                # currSeqId = m0
                # print("File %s" % (file))
                # print("\t %s" % (currSeqId))
                if(len(prevSeqId) == len(currSeqId) and len(currSeqId) != 0):
                    # print("\t%s -> final State" % prevSeqId)
                    finalStates.append(getStateId(abstraction))
                elif (len(prevSeqId) >= len(currSeqId) and len(currSeqId) != 0):
                    # print("\t%s -> final State" % prevSeqId)
                    finalStates.append(getStateId(abstraction))

                filename = "%s/%s" % (dirpath,file)
                f = open(filename)

                for line in f:

                    m1 = re.match("// TEST_VECTOR: \[(.*)\]",line)
                    m6 = re.match("// RULE_APPLD:\s+(.*)",line)

                    if m6:
                        ruleAppldId = getRuleId(m6.group(1))
                        # print("\t%s" % m6.group(0))

                    if m1:
                        abstraction = convertAbsList2Key(m1.group(1))
                        stateId = setStateId(currSeqId,abstraction)

                # use a string to hash dictionary 'seqList2State'
                seqList2State[seqList2Str(currSeqId)] = stateId

                prevStateId = seqList2State[seqList2Str(currSeqId[0:len(currSeqId)-1])]
                print("File %s -> %2d" % (file,stateId))
                print("\t[%s] -> (%2d , %2d) -> %2d" % (currSeqId,prevStateId,ruleAppldId,stateId))
                trainTransitionTable[prevStateId][ruleAppldId] = stateId

                prevStateId = stateId
                if ruleAppldId == -1: # We are in an init state for RL training
                    initStates.append(stateId)
                prevSeqId = currSeqId
                # print("###################")
                f.close()

        # add the last abstraction as final state
        finalStates.append(getStateId(abstraction))


def setStateId(sequenceId,abstraction):
    global abst2State,abst2SeqList

    # absKey = convertAbsList2Key(abstraction)
    # print absKey
    if abstraction in abst2State.keys():
        if abstraction in abst2SeqList.keys():
            seqList = abst2SeqList[abstraction]
            if sequenceId in seqList:
                print("\t*******************************************")
                print("\tWARNING: Different codes have the same abstraction and are mapped to the same state Id!! ( %s )" % (abstraction))
                print("\t*******************************************")
            else:
                abst2SeqList[abstraction].append(sequenceId)
        else:
            print("\t*******************************************")
            print("\tERROR: Abstraction present in abst2State and not present in abst2SeqList!! ( %s )" % (abstraction))
            print("\t*******************************************")
        # return abst2State[abstraction]

    else:
        stateId = len(abst2State)
        initTransitionTableRow(stateId)

        abst2State[abstraction] = stateId
        abst2SeqList[abstraction] = [sequenceId]

    # for abs in abst2SeqList:
    #     print("\t%d -> %s" % (abst2State[abs],abst2SeqList[abs]))

    return abst2State[abstraction]


def getStateId(abstraction):
    global abst2State


    return abst2State[abstraction]


def getRuleId(ruleName):
    global nameRules

    ruleId = -1
    for rule in nameRules:
        ruleId += 1
        if rule == ruleName:
            return ruleId

    return ruleId

def readRulesList(rulesFile):
    global nameRules

    # Rules for nBody for POLCA demo
    nameRules = ["remove_ternary", "remove_empty_if", "normalize_iteration_step", "collapse_2_for_loops", "inlining", "remove_block", "remove_useless_statement", "subs_struct_by_fields", "roll_up_array_init", "roll_up_array", "move_inside_for_pre", "move_inside_for_post", "move_enclosing_if_inside_for", "if_2_assign_ternary", "for_loop_fusion", "contiguous_same_if", "divide_if"]
    # All rules
    # nameRules = ["remove_identity", "reduce_to_0", "undo_distributive", "sub_to_mult", "normalize_iteration_step", "loop_reversal_d2i", "loop_reversal_i2d", "loop_interchange", "loop_interchange_pragma", "for_chunk", "unrolling", "move_inside_for_pre", "move_inside_for_post", "move_enclosing_if_inside_for", "collapse_2_for_loops", "for_loop_fission", "for_loop_fusion_mapmap", "for_loop_fusion", "for_wo_block_2_for_w_block", "remove_empty_for", "for_to_while", "while_to_for", "if_wo_block_2_if_w_block", "if_wo_else_2_if_w_else", "split_addition_assign", "join_addition_assign", "mult_ternary_2_ternary", "sum_ternary_2_ternary", "assign_ternary_2_if_else", "if_else_2_assign_ternary", "if_2_assign_ternary", "empty_else", "remove_ternary", "remove_block", "remove_empty_if", "remove_useless_statement", "strength_reduction", "useless_assign", "replace_var_equal", "contiguous_same_if", "just_one_iteration_removal", "join_assignments", "propagate_assignment", "loop_inv_code_motion", "inlining", "inlining_assignment", "common_subexp_elimination", "introduce_aux_array", "flatten_float_array", "flatten_int_array", "subs_struct_by_fields", "roll_up_init", "roll_up", "roll_up_array_init", "roll_up_array", "feat_move_inside_for_pre", "feat_move_inside_for_post"]
    # nameRules = ["remove_identity", "reduce_to_0", "undo_distributive", "sub_to_mult", "normalize_iteration_step", "loop_reversal_d2i", "loop_reversal_i2d", "loop_interchange", "loop_interchange_pragma", "for_chunk", "unrolling", "move_inside_for_pre", "move_inside_for_post", "collapse_2_for_loops", "for_loop_fission", "for_loop_fusion_mapmap", "for_loop_fusion", "for_wo_block_2_for_w_block", "remove_empty_for", "for_to_while", "while_to_for", "if_wo_block_2_if_w_block", "if_wo_else_2_if_w_else", "split_addition_assign", "join_addition_assign", "mult_ternary_2_ternary", "sum_ternary_2_ternary", "assign_ternary_2_if_else", "if_else_2_assign_ternary", "if_2_assign_ternary", "empty_else", "remove_ternary", "remove_block", "remove_empty_if", "remove_useless_statement", "strength_reduction", "useless_assign", "replace_var_equal", "contiguous_same_if", "just_one_iteration_removal", "join_assignments", "propagate_assignment", "loop_inv_code_motion", "inlining", "inlining_assignment", "common_subexp_elimination", "introduce_aux_array", "flatten_float_array", "flatten_int_array", "subs_struct_by_fields", "roll_up_init", "roll_up", "roll_up_array_init", "roll_up_array"]

def initTransitionTableRow(state):
    global trainTransitionTable,nameRules

    trainTransitionTable[state] = []

    for rule in nameRules:
        trainTransitionTable[state].append(state)

def printTransitionTable():
    global trainTransitionTable

    for stateId in trainTransitionTable:
        print(trainTransitionTable[stateId])

def printTrainDataToFile(trainDataFile):
    global trainTransitionTable,initStates,finalStates,nameRules


    print("Printing train data to file %s ..." % (trainDataFile))
    f = open(trainDataFile,'w')

    f.write("Rule Names\n")
    f.write("%s\n\n" % (nameRules))

    f.write("Abst.-State mappings\n")
    f.write("%s\n\n" % (abst2State))

    f.write("Init. States\n")
    f.write("%s\n\n" % (initStates))

    f.write("Final States\n")
    f.write("%s\n\n" % (finalStates))

    f.write("Transition Table\n")
    for stateId in trainTransitionTable:
        f.write('%s\n' % (trainTransitionTable[stateId]))

    f.close()

if __name__ == "__main__":


    if len(sys.argv) < 2:
        print("ERROR: usage -> %s <target_platform>" % (sys.argv[0]))
        exit(0)
    else:
        targetPlatform = sys.argv[1]

    trainDataPath = '../machine_learning/reinforcement_learning/utils/'
    trainDataFile = 'trainingData.txt'


    # pathList = [["threshold0","./train_set/imageFilter/threshold/s2s_test"]
    #             ]
    # pathList = [["nbody","./train_set/hpcDwarfs/nBody/s2s_transformations/2arrays"]]

    # pathList = [["nbody","./train_set/hpcDwarfs/nBody/oracle_test"]
    #             ]

    # pathList = [["nbody","./train_set/hpcDwarfs/nBody/s2s_transformations/merged"]]
    pathList = [["nbody","./train_set/hpcDwarfs/nBody/test_nn"]]

    print("\n#####################################################\n")

    rulesFile = ''
    readRulesList(rulesFile)

    # for rule in nameRules:
    #     print rule

    for useCase in pathList:

        updateTrainTable(useCase)

    print("Init. States: %s" % (initStates))
    print("Final States: %s" % (finalStates))

    printTransitionTable()

    defaultTarget = "maxj"
    if targetPlatform == "none":
        targetPlatform = defaultTarget

    replaceStr = "_%s.txt" % (targetPlatform)
    trainDataFile = trainDataFile.replace(".txt",replaceStr)

    printTrainDataToFile(trainDataPath+trainDataFile)