#!/usr/bin/python

import os,re

Before = 0
After  = 1 

Current = 0
Prev    = 1

# codeToLinesDict = {}


# codeToLinesDict["// FUNC_ANALYZ: main"] = [("/\* This program uses the discrete cosine transform to compress a 128 x 128(.)*",Before,Current)]

# codeToLinesDict["#define PROFILING 0"] = [("#include <sys/time.h>(.)*",After,Current)]

# codeToLinesDict["#if PROFILING == 1"] = \
# [("\s*struct timeval(.)*",Before,Current),\
# ("\s*gettimeofday\(&tvalAfter,NULL\);(.)*",Before,Current),\
#  ("\n\s*gettimeofday\(&tvalBefore,NULL\);(.)*",Before,Prev)]

# codeToLinesDict["#endif"] = \
# [("\s*for\(int i=0;i<SAMPLES;i\+\+\)\{\n\s*gettimeofday\(&tvalBefore,NULL\);(.)*",After,Prev),\
# ("\s*\} // End of loop for time samples(.)*",After,Current),\
#  ("\s*total = (.)*\n*\s*/\* Read the image \*/(.)*",After,Prev),\
#  ("\n\s*gettimeofday\(&tvalBefore,NULL\);(.)*",After,Prev)]


def injectCode(filename):

    filenameAux = filename+"_aux"

    f = open(filename)
    fw = open(filenameAux,"w")

    prevLine = ""
    for line in f:
        written = 0
        # print line
        for key in codeToLinesDict:
            for elem in codeToLinesDict[key]:
                
                if elem[2] == Current:
                    m1 = re.match(elem[0],line)
                else:
                    m1 = re.match(elem[0],prevLine+line)                    

                if m1:
                    print("Line: %s" % (m1.group(0)))
                    if elem[1] == Before:
                        fw.write("\n%s\n" % key)
                        fw.write(line)
                    else:
                        fw.write(line)
                        fw.write("\n%s\n" % key)

                    written = 1

        prevLine = line

        if not written:
            fw.write(line)                    

    f.close()
    fw.close()


    os.system("mv %s %s" % (filenameAux,filename))

def prepareInjectDict(param):
    global codeToLinesDict

    codeToLinesDict = {}

    if param == 1:
        codeToLinesDict["// FUNC_ANALYZ: main"] = [("/\* This program uses the discrete cosine transform to compress a 128 x 128(.)*",Before,Current)]

    elif param == 0:
        codeToLinesDict["#define PROFILING 0"] = [("#include <sys/time.h>(.)*",After,Current)]

        codeToLinesDict["#if PROFILING == 1"] = \
[("\s*struct timeval(.)*",Before,Current),\
("\s*gettimeofday\(&tvalAfter,NULL\);(.)*",Before,Current),\
 ("\n\s*gettimeofday\(&tvalBefore,NULL\);(.)*",Before,Prev)]

        codeToLinesDict["#endif"] = \
[("\s*for\(int i=0;i<SAMPLES;i\+\+\)\{\n\s*gettimeofday\(&tvalBefore,NULL\);(.)*",After,Prev),\
("\s*\} // End of loop for time samples(.)*",After,Current),\
 ("\s*total = (.)*\n*\s*/\* Read the image \*/(.)*",After,Prev),\
 ("\n\s*gettimeofday\(&tvalBefore,NULL\);(.)*",After,Prev)]


def browsePath(testsPath):

    # First pass to inject preprocessor directives to disable profiling code
    # Second pass to inject annotation to state function to obtain code features
    for i in range(2):
        prepareInjectDict(i)

        for dirpath, dirnames, filenames in os.walk(testsPath):
            for file in filenames:
                # use only files *.c and avoid *.c~ (emacs) and .#*.c (emacs)
                if re.match("^[^.].*\.c$",file):
                    filename = "%s/%s" % (dirpath,file)

                    print("Updating file: %s" % filename)
                    injectCode(filename)

if __name__ == "__main__":


    pathList = ["../predict_set/imageFilter/compress/test"]

    for path in pathList:
        browsePath(path)

  #   line = """	  total = ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L+tvalAfter.tv_usec) - tvalBefore.tv_usec;

  # /* Read the image */"""

  #   regex = "\s*total = (.)*\n*\s*/\* Read the image \*/(.)*"


  #   m1 = re.match(regex,line)
  #   if m1:
  #       print("Line: %s" % (m1.group(0)))
  #   else:
  #       print("KKKKKKK")

  #   line = """	  total = ((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L+tvalAfter.tv_usec) - tvalBefore.tv_usec;
  # /* Read the image */"""

  #   m1 = re.match(regex,line)
  #   if m1:
  #       print("Line: %s" % (m1.group(0)))
  #   else:
  #       print("KKKKKKK")
