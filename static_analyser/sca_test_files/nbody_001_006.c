// RULE_APPLD: remove_block
// FUNC_ANALYZ: main BLOCK_ABS
// FEAT_VECTOR: [1, 1, 0, 0, 0, 0, 1, 1, 0, 3, 3, 0, 0, 4, 0, 0, 9, 0, 0, 3, 6, 3, 0]
// TEST_VECTOR: [1, 1, 0, 0, 0, 0, 1, 1, 0, 3, 15, 0, 0, 4, 0, 0, 9, 0, 0, 3, 6, 3, 0]
// TEST_LABEL: 0 (CPU)

//# maxForStmtDepth:             1
//# anyFuncCall:                 1
//# anyArrayWriteShifted:        0
//# numIrregularForLoops:        0
//# usesGlobalVars:              0
//# anyIfStmt:                   0
//# allForLoopWithStaticLimit:   1
//# anySIMDloop:                 1
//# anyLoop_Schedule:            0
//# numLoopInvVar:               3
//# numLoopHoistedVarMods:      15
//# numNon1Darray:               0
//# numAuxVarArrayIndex:         0
//# totalNumForLoops:            4
//# numNonNormalizedForLoops:    0
//# numStmtsRollUp:              0
//# numCompoundStmts:            9
//# anyTernaryOp:                0
//# anyUselessStmt:              0
//# numForPostambles:            3
//# numForPreambles:             6
//# numStructVarDecl:            3
//# numEmptyIf:                  0



#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "params.h"


typedef struct Triple Triple;

struct Triple {
    float x, y, z;
};

void bodyForce(struct Triple * pos, struct Triple * frc, int n)
    {
    int i, j;
    float d[3];
    float distSqr, invDist, invDist3;
#pragma polca iteration_independent
#pragma polca output frc
#pragma polca input pos
#pragma polca map CALCFORCE pos frc
    for (i = 0; i < n; i++)
        {
#pragma polca output frc[i]
#pragma polca input pos[i]
#pragma polca def CALCFORCE
    {
    frc[i].x = 0.0f;
    frc[i].y = 0.0f;
    frc[i].z = 0.0f;
    for (j = 0; j < n; j++)
        {
#pragma polca output frc[i]
#pragma polca input pos[i] frc[i] pos[j]
#pragma polca def ADDFORCE
    {
    d[0] = pos[j].x - pos[i].x;
    d[1] = pos[j].y - pos[i].y;
    d[2] = pos[j].z - pos[i].z;
    distSqr = d[0] * d[0] + d[1] * d[1] + d[2] * d[2] + SOFTENING;
    invDist = 1.0f / sqrtf(distSqr);
    invDist3 = invDist * invDist * invDist;
    frc[i].x = frc[i].x + d[0] * invDist3;
    frc[i].y = frc[i].y + d[1] * invDist3;
    frc[i].z = frc[i].z + d[2] * invDist3;
    }
    }
    }
    }
    }

void velocities(struct Triple * frc,
                struct Triple * vel,
                float dt,
                int n)
    {
    int i;
    for (i = 0; i < n; i++)
        {
#pragma polca output vel[i]
#pragma polca input vel[i] frc[i]
#pragma polca def UPDV
    {
    vel[i].x = vel[i].x + dt * frc[i].x;
    vel[i].y = vel[i].y + dt * frc[i].y;
    vel[i].z = vel[i].z + dt * frc[i].z;
    }
    }
    }

void integrate(struct Triple * pos,
               struct Triple * vel,
               float dt,
               int n)
    {
    int i;
    for (i = 0; i < n; i++)
        {
#pragma polca output pos[i]
#pragma polca input pos[i] vel[i]
#pragma polca def UPDP
    {
    pos[i].x = pos[i].x + vel[i].x * dt;
    pos[i].y = pos[i].y + vel[i].y * dt;
    pos[i].z = pos[i].z + vel[i].z * dt;
    }
    }
    }

int main(const int argc, const char * * argv)
    {
    int nBodies, nElems;
    int dataType;
    int i, iter;
    const float dt = DT;
    const int nIters = ITERS;
    float * pos, * vel;
    input_arr_arg("../../data/pos_input.dat",
                  &pos,
                  &nElems,
                  &dataType);
    input_arr_arg("../../data/vel_input.dat",
                  &vel,
                  &nElems,
                  &dataType);
    nBodies = nElems / DIM;
    struct Triple pStruct[N];
    struct Triple vStruct[N];
    struct Triple fStruct[N];
    for (i = 0; i < nBodies; i++)
        {
    pStruct[i].x = pos[i * 3];
    pStruct[i].y = pos[i * 3 + 1];
    pStruct[i].z = pos[i * 3 + 2];
    }
    for (i = 0; i < nBodies; i++)
        {
    vStruct[i].x = vel[i * 3];
    vStruct[i].y = vel[i * 3 + 1];
    vStruct[i].z = vel[i * 3 + 2];
    }
    for (iter = 1; iter <= nIters; iter++)
        {
#pragma polca def BLOCK_ABS
    {
    int iv_i_0, iv_j_1;
    float iv_d_2[3];
    float iv_distSqr_3, iv_invDist_4, iv_invDist3_5;
#pragma polca iteration_independent
#pragma polca output frc
#pragma polca input pos
#pragma polca map CALCFORCE pos frc
    for (iv_i_0 = 0; iv_i_0 < nBodies; iv_i_0++)
        {
#pragma polca output frc[i]
#pragma polca input pos[i]
#pragma polca def CALCFORCE
    {
    fStruct[iv_i_0].x = 0.0f;
    fStruct[iv_i_0].y = 0.0f;
    fStruct[iv_i_0].z = 0.0f;
    for (iv_j_1 = 0; iv_j_1 < nBodies; iv_j_1++)
        {
#pragma polca output frc[i]
#pragma polca input pos[i] frc[i] pos[j]
#pragma polca def ADDFORCE
    {
    iv_d_2[0] = pStruct[iv_j_1].x - pStruct[iv_i_0].x;
    iv_d_2[1] = pStruct[iv_j_1].y - pStruct[iv_i_0].y;
    iv_d_2[2] = pStruct[iv_j_1].z - pStruct[iv_i_0].z;
    iv_distSqr_3 = iv_d_2[0] * iv_d_2[0] + iv_d_2[1] * iv_d_2[1] + iv_d_2[2] * iv_d_2[2] + SOFTENING;
    iv_invDist_4 = 1.0f / sqrtf(iv_distSqr_3);
    iv_invDist3_5 = iv_invDist_4 * iv_invDist_4 * iv_invDist_4;
    fStruct[iv_i_0].x = fStruct[iv_i_0].x + iv_d_2[0] * iv_invDist3_5;
    fStruct[iv_i_0].y = fStruct[iv_i_0].y + iv_d_2[1] * iv_invDist3_5;
    fStruct[iv_i_0].z = fStruct[iv_i_0].z + iv_d_2[2] * iv_invDist3_5;
    }
    }
    }
    }
    int iv_i_6;
    for (iv_i_6 = 0; iv_i_6 < nBodies; iv_i_6++)
        {
#pragma polca output vel[i]
#pragma polca input vel[i] frc[i]
#pragma polca def UPDV
    {
    vStruct[iv_i_6].x = vStruct[iv_i_6].x + dt * fStruct[iv_i_6].x;
    vStruct[iv_i_6].y = vStruct[iv_i_6].y + dt * fStruct[iv_i_6].y;
    vStruct[iv_i_6].z = vStruct[iv_i_6].z + dt * fStruct[iv_i_6].z;
    }
    }
    int iv_i_7;
    for (iv_i_7 = 0; iv_i_7 < nBodies; iv_i_7++)
        {
#pragma polca output pos[i]
#pragma polca input pos[i] vel[i]
#pragma polca def UPDP
    {
    pStruct[iv_i_7].x = pStruct[iv_i_7].x + vStruct[iv_i_7].x * dt;
    pStruct[iv_i_7].y = pStruct[iv_i_7].y + vStruct[iv_i_7].y * dt;
    pStruct[iv_i_7].z = pStruct[iv_i_7].z + vStruct[iv_i_7].z * dt;
    }
    }
    }
    }
    for (i = 0; i < nBodies; i++)
        {
    pos[i * 3] = pStruct[i].x;
    pos[i * 3 + 1] = pStruct[i].y;
    pos[i * 3 + 2] = pStruct[i].z;
    }
    for (i = 0; i < nBodies; i++)
        {
    vel[i * 3] = vStruct[i].x;
    vel[i * 3 + 1] = vStruct[i].y;
    vel[i * 3 + 2] = vStruct[i].z;
    }
    output_arr_arg("../../data/pos_out.dat",
                   pos,
                   nBodies * DIM,
                   dataType);
    output_arr_arg("../../data/vel_out.dat",
                   vel,
                   nBodies * DIM,
                   dataType);
    free(pos);
    free(vel);
    }

