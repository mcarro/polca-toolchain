#!/usr/bin/python

# Copyright (c) 2013-2016, The IMDEA Software Institute and
# Copyright (c) 2013-2016, Universidad Politecnica de Madrid

# See LICENSE.txt and AUTHORS.txt for licensing and authorship


import json,sys,os
import static_analyzer as sca
import warnings
warnings.filterwarnings("ignore")

sys.path.extend(['../machine_learning/reinforcement_learning'])
import oracle


COUNT_NEWCODE_TYPE = 'countNewCode'
COUNT_CHANGES_TYPE = 'countChanges'

rule2IdchangeDict = {}

featExtractorCmd = 'polca_features'

# def prepareCode(codeStr):
#
#     modeCode = ""
#     if "{" in codeStr:
#         modCode = "void main()%s" % codeStr
#     else:
#         modCode = "void main(){\n%s;\n}" % codeStr
#
#     return modCode


def callFeaturesCmd(filename,featRule,blockName):

    cwd = os.getcwd()
    # print(cwd)

    cmdPath = ''
    if 'static_analyser' in cwd:
        cmdPath = '../polca_s2s/'
    else:
        cmdPath = './'

    # filename = 'train_set/hpcDwarfs/nBody/s2s_transformations/2arrays/nbody_001_013.c'

    try:
        cmd = '%s %s "%s" %s' % (cmdPath+featExtractorCmd,filename,featRule,blockName)
        # print cmd

        os.chdir('../polca_s2s')
        jsonStr = os.popen(cmd).read()


        codeChanges = json.loads(jsonStr)
        os.chdir(cwd)
    except ValueError as err:
        sys.stderr.write("S2S error: {0}\n".format(err))
        os.chdir(cwd)
        return None
        # raise ValueError(err)


    return codeChanges

def countNewCode(codeChanges,rule):

    # ruleList = []
    # rule2IdchangeDict = {}
    val = 0
    for i in range(len(codeChanges['changes'])):
        if rule == codeChanges['changes'][i]['ruleName']:
            # sys.stderr.write(str(codeChanges['changes'][i]["idChange"])+"\n")
            # # print codeChanges['changes'][i]["line"]
            # sys.stderr.write(str(codeChanges['changes'][i]['ruleName'])+"\n")
            # # sys.stderr.write(str(codeChanges['changes'][i]['oldCode'])+"\n")
            # sys.stderr.write(str(codeChanges['changes'][i]['newCode'])+"\n")
            # sys.stderr.write("#####################################################"+"\n\n")
            #
            # # Convert from unicode  to regular string
            # ruleName = str(codeChanges['changes'][i]['ruleName'])
            # idChange = int(codeChanges['changes'][i]["idChange"])
            # ruleList.append(ruleName)
            # rule2IdchangeDict[ruleName] = idChange
            newCodeStr = str(codeChanges['changes'][i]['newCode'])
            newCodeStr = newCodeStr.replace('{','')
            newCodeStr = newCodeStr.replace('}','')
            val += int(newCodeStr.replace(';',''))


    return val

def countChanges(codeChanges,rule):

    # ruleList = []
    # rule2IdchangeDict = {}
    val = 0
    for i in range(len(codeChanges['changes'])):
        if rule == codeChanges['changes'][i]['ruleName']:
            # sys.stderr.write(str(codeChanges['changes'][i]["idChange"])+"\n")
            # # print codeChanges['changes'][i]["line"]
            # sys.stderr.write(str(codeChanges['changes'][i]['ruleName'])+"\n")
            # sys.stderr.write(str(codeChanges['changes'][i]['oldCode'])+"\n")
            # sys.stderr.write(str(codeChanges['changes'][i]['newCode'])+"\n")
            # sys.stderr.write("#####################################################"+"\n\n")
            #
            # # Convert from unicode  to regular string
            # ruleName = str(codeChanges['changes'][i]['ruleName'])
            # idChange = int(codeChanges['changes'][i]["idChange"])
            # ruleList.append(ruleName)
            # rule2IdchangeDict[ruleName] = idChange
            val += 1

    return val


def measureFeature(codeChanges,rule,featureType):

    count = -1

    if featureType == COUNT_NEWCODE_TYPE:
        count = countNewCode(codeChanges,rule)
    elif featureType == COUNT_CHANGES_TYPE:
        count = countChanges(codeChanges,rule)
    else:
        sys.stderr.write("ERROR: wrong feature type to measure %s" % (featureType))

    return count

def measureFeatures(filename,featuresList,blockName):


    featRule = ""
    for rule in featuresList:
        featRule = featRule + rule[0] + " "
    # Remove the last 'space' character
    featRule = featRule[:-1]

    codeChanges = callFeaturesCmd(filename,featRule,blockName)


    # print json.dumps(codeChanges, sort_keys=True,indent=3, separators=(',', ': '))

    retList = []

    if codeChanges != None:
        for rule in featuresList:
            retList.append([rule[0],measureFeature(codeChanges,rule[0],rule[1])])
    else:
        for rule in featuresList:
            retList.append([rule[0],0])

    return retList

    # ruleList = []
    # rule2IdchangeDict = {}
    # for i in range(len(codeChanges['changes'])):
    #     sys.stderr.write(str(codeChanges['changes'][i]["idChange"])+"\n")
    #     # print codeChanges['changes'][i]["line"]
    #     sys.stderr.write(str(codeChanges['changes'][i]['ruleName'])+"\n")
    #     sys.stderr.write(str(codeChanges['changes'][i]['oldCode'])+"\n")
    #     sys.stderr.write(str(codeChanges['changes'][i]['newCode'])+"\n")
    #     sys.stderr.write("#####################################################"+"\n\n")
    #
    #     # Convert from unicode  to regular string
    #     ruleName = str(codeChanges['changes'][i]['ruleName'])
    #     idChange = int(codeChanges['changes'][i]["idChange"])
    #     ruleList.append(ruleName)
    #     rule2IdchangeDict[ruleName] = idChange


if __name__ == "__main__":


    if len(sys.argv) > 2:
        filename  = sys.argv[1]
        featRule = sys.argv[2]
        blockName = sys.argv[3]
    else:
        print("Usage: %s filename [rule1<, rule2, ...>] blockName" % (sys.argv[0]))
        exit(0)

    codeChanges = callFeaturesCmd(filename,featRule,blockName)

    ruleList = []
    rule2IdchangeDict = {}
    for i in range(len(codeChanges['changes'])):
        sys.stderr.write(str(codeChanges['changes'][i]["idChange"])+"\n")
        # print codeChanges['changes'][i]["line"]
        sys.stderr.write(str(codeChanges['changes'][i]['ruleName'])+"\n")
        sys.stderr.write(str(codeChanges['changes'][i]['oldCode'])+"\n")
        sys.stderr.write(str(codeChanges['changes'][i]['newCode'])+"\n")
        sys.stderr.write("#####################################################"+"\n\n")

        # Convert from unicode  to regular string
        ruleName = str(codeChanges['changes'][i]['ruleName'])
        idChange = int(codeChanges['changes'][i]["idChange"])
        ruleList.append(ruleName)
        rule2IdchangeDict[ruleName] = idChange
