#ifndef _POLCAASTVISITORMPI_H_
#define _POLCAASTVISITORMPI_H_

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

#include "PolcaPragma.h"
#include "SymbolTable.h"
#include "PolcaPragmaTable.h"
#include "PolcaASTVisitor.cpp"

using namespace std;
using namespace clang;
using namespace sema;


class PolcaASTVisitorMpi : public PolcaASTVisitor {


public:
  explicit PolcaASTVisitorMpi(CompilerInstance *CI
		  ,ASTContext *context
		  , SourceManager *sourceManager
		  ,PolcaPragmaTable *pragmaTable
		  , SymbolTable *symbolTable
		  , PolcaRules* polcaRules
		  , llvm::StringRef inFile):PolcaASTVisitor(CI,context,sourceManager,pragmaTable,symbolTable,polcaRules,inFile)
  {}

  void initializeRewriters()
  {
	  Rewriter *rewriter;

	  RewritingElements*  re = new RewritingElements();

	  rewriter = new Rewriter();
	  rewriter->setSourceMgr(m_compilerInstance->getSourceManager(), m_compilerInstance->getLangOpts());
	  re->m_rewritersVector.push_back(rewriter);

	  m_rewritingElementsVector.push_back(re);
  }

void rewriteStatement(Stmt *statement)
  {
    std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
    std::vector<PolcaPragma*>::iterator it;

    for(it = v.begin(); it != v.end(); ++it) {
        PolcaPragma *pp = *it;
        // if (m_compilerInstance->getSourceManager().getSpellingLineNumber(statement->getLocStart()) == 61){
        //   llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
        //   llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";
        //   llvm::outs() << "Source Location statement: "<< statement->getLocStart().printToString(m_compilerInstance->getSourceManager())<<"\n";
        //   llvm::outs() << "Source line statement: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(statement->getLocStart())  <<"\n";
        //   llvm::outs() << pp->getPragmaId() <<"\n";
        //   llvm::outs() << pp->getPragmaDirective() <<"\n";
        // }
        if(isa<ForStmt>(statement))
        {
          ForStmt *forStmt = cast<ForStmt>(statement);

          if(m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())+1 == m_compilerInstance->getSourceManager().getSpellingLineNumber(forStmt->getForLoc()) && pp->getPragmaId().compare("G") == 0)
          {
            // llvm::outs() << "\nSTMT_FOUND\n";
            // llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
            // llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation()) <<"\n";
            // llvm::outs() << "Source Location for: "<< forStmt->getForLoc().printToString(m_compilerInstance->getSourceManager())<<"\n";
            // llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(forStmt->getForLoc()) <<"\n";
            // llvm::outs() << pp->getPragmaId() <<"\n";


            DeclarationNameInfo initForVar = cast<DeclRefExpr>(cast<BinaryOperator>(forStmt->getInit())->getLHS())->getNameInfo();
            std::string varNameFor = initForVar.getAsString();

            SourceRange rangeFor;

            rangeFor.setBegin(forStmt->getLocStart());
            rangeFor.setEnd(forStmt->getLocEnd());

            std::string newString = "\
MPI_Status status;\n\
  if (rank < size - 1) \n\
    MPI_Send(&heatmap[local_n_elem], 1, MPI_FLOAT, rank + 1, 0,\n\
       MPI_COMM_WORLD );\n\
  if (rank > 0)\n\
    MPI_Recv( &heatmap[0], 1, MPI_FLOAT, rank - 1, 0, MPI_COMM_WORLD,\n\
        &status );\n\
  \n\
  if (rank > 0)\n\
    MPI_Send( &heatmap[1], 1, MPI_FLOAT, rank - 1, 1, MPI_COMM_WORLD );\n\
  if (rank < size - 1)\n\
    MPI_Recv( &heatmap[local_n_elem+1], 1, MPI_FLOAT, rank + 1, 1,\n\
        MPI_COMM_WORLD, &status );\n\
  \n\
  for("+varNameFor+"=1; "+varNameFor+"<local_n_elem+1; "+varNameFor+"++)\n\
  #pragma polca def G heatmap["+varNameFor+"-1]:i heatmap["+varNameFor+"]:i heatmap["+varNameFor+"+1]:i heatmap_tmp["+varNameFor+"]:o \n\
  {\n\
    heatmap_tmp["+varNameFor+"] = g(heatmap["+varNameFor+"-1], heatmap["+varNameFor+"], heatmap["+varNameFor+"+1]);\n\
  }\n";


            m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);

            // m_rewriter->RemoveText(rangeFor);
          }
        }

      if(isa<CompoundStmt>(statement))  
      {
        CompoundStmt *compStmt = cast<CompoundStmt>(statement);
        if(m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())+1 == m_compilerInstance->getSourceManager().getSpellingLineNumber(compStmt->getLocStart()) && pp->getPragmaId().compare("B") == 0)
        {
            SourceRange rangeComp;

            rangeComp.setBegin(compStmt->getLocStart());
            rangeComp.setEnd(compStmt->getLocEnd());

            std::string newString = "\
  if (0/local_n_elem == rank)\n\
    heatmap_tmp[0%local_n_elem + 1] = 0.0;\n\
  if ((n_elem-1)/local_n_elem == rank)\n\
    heatmap_tmp[(n_elem-1)%local_n_elem + 1] = 0.0;\n";

            m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangeComp,newString);
        }
      }


      //if(isa<CompoundStmt>(statement))  
      //{
        //CompoundStmt *compStmt = cast<CompoundStmt>(statement);
        if(m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())+1 == m_compilerInstance->getSourceManager().getSpellingLineNumber(statement->getLocStart()) && pp->getPragmaDirective().compare("MemCopy") == 0)
        {
            //llvm::outs() << "\nMem FOUND\n";

            // statement->dump(llvm::outs(),*m_sourceManager);

            SourceRange range;

            range.setBegin(statement->getLocStart());
            range.setEnd(statement->getLocEnd());

            std::string newString = "\
  memcpy((void*)heatmap, (void*)heatmap_tmp, (local_n_elem+2) * sizeof(float));\n";

            m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(range,newString);
        }

    if(isa<CallExpr>(statement))
    {
      Expr *calleeExpr = cast<CallExpr>(statement)->getCallee();
      if(isa<ImplicitCastExpr>(calleeExpr))
      {
        Expr *subExpr = cast<ImplicitCastExpr>(calleeExpr)->getSubExpr();
        if(isa<DeclRefExpr>(subExpr))
        {
          DeclRefExpr *refCallee = cast<DeclRefExpr>(subExpr);
          if (refCallee->getNameInfo().getAsString().compare("init") == 0)
          {
            SourceRange range;

            range.setBegin(statement->getLocStart());
            range.setEnd(statement->getLocEnd());

            std::string newString = "\
MPI_Init( &argc, &argv );\n\
  \n\
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );\n\
  MPI_Comm_size( MPI_COMM_WORLD, &size );\n\
  init(&heatmap,&heatmap_tmp)";

            m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(range,newString);
          }
        }
      }
    }

    }
  }

  bool VisitStmt(Stmt *statement)
  {

//	  if(m_firstTraverseVisit)
//	  {
//		  printf("\n\n\n");
//		  printf("/*******************************************************/\n");
//		  printf("/                 Traversing input AST                  /\n");
//		  printf("/*******************************************************/\n");
//		  printf("\n");
//
//		  m_firstTraverseVisit = false;
//
//		  initializeRewriters();
//	  }


      PolcaASTVisitor::VisitStmt(statement);

      rewriteStatement(statement);

	  return true;
  }


void rewriteDecl(Decl* D)
  {

      unsigned lineSrcLocation = m_sourceManager->getSpellingLineNumber(D->getSourceRange().getBegin());
      if(isa<FunctionDecl>(D))
      {
          FunctionDecl *funcDecl = cast<FunctionDecl>(D);

          std::vector<PolcaPragma*>::iterator it;
          std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
       for(it = v.begin(); it != v.end(); ++it) {
        PolcaPragma *pp = *it;
        // llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
        // llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";
        // llvm::outs() << "Source Location for: "<< forStmt->getForLoc().printToString(m_compilerInstance->getSourceManager())<<"\n";
        // llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(forStmt->getForLoc())  <<"\n";
        // llvm::outs() << pp->getPragmaId() <<"\n";
        if(m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())+1 == m_compilerInstance->getSourceManager().getSpellingLineNumber(funcDecl->getLocStart()) && pp->getPragmaId() == "init" )
        {
          llvm::outs() << "\nDECL_FOUND\n";
          llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
          llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation()) <<"\n";
          llvm::outs() << "Source Location decl: "<< funcDecl->getLocStart().printToString(m_compilerInstance->getSourceManager())<<"\n";
          llvm::outs() << "Source line decl: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(funcDecl->getLocStart())  <<"\n";
          llvm::outs() << pp->getPragmaId() <<"\n";

          
          Stmt* Body = funcDecl->getBody();

          SourceRange rangeBody;

          rangeBody.setBegin(Body->getLocStart());
          rangeBody.setEnd(Body->getLocEnd());

          std:string newString = "{\n\
\tn_elem = 21;\n\
\tn_iter = 10;\n\
\tlocal_n_elem = n_elem/size;\n\
\n\
\t*heatmap = malloc(sizeof(float) * local_n_elem+2);\n\
\t*heatmap_tmp = malloc(sizeof(float) * local_n_elem+2);\n\
\n\
\tint i;\n\
\tfor(i=0; i<local_n_elem+2; i++) {\n\
\t\t(*heatmap)[i] = 0.0;\n\
\t\t(*heatmap_tmp)[i] = 0.0;\n\
\t}\n\
\t\n\
\tif((n_elem/2)/local_n_elem == rank)\n\
\t\t(*heatmap)[(n_elem/2)%local_n_elem + 1] = 100;\n\
}\n";

          m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangeBody,newString);

          //Body->dump(llvm::outs(),*m_sourceManager);

        }
      }
    }


    if(isa<VarDecl>(D))
      {
        VarDecl *varDecl = cast<VarDecl>(D);
        // llvm::outs() << "Source Location decl: "<< varDecl->getLocStart().printToString(m_compilerInstance->getSourceManager())<<"\n";
        // llvm::outs() << "Source line decl: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(varDecl->getLocStart())  <<"\n";
        // llvm::outs() << "Name decl: "<< varDecl->getNameAsString()  <<"\n";


        std::string strDef;
        llvm::raw_string_ostream rawDef(strDef);
        varDecl->print(rawDef, PrintingPolicy(Context->getLangOpts()),0);
        rawDef.str();


        SourceRange rangeDecl;
        rangeDecl.setBegin(varDecl->getLocStart());
        rangeDecl.setEnd(varDecl->getLocEnd());

        if(varDecl->getNameAsString().compare("n_iter") == 0)
        {
          std::string newString = "#include <mpi.h>\n\nint rank;\nint size;\nint local_n_elem;\n" + strDef ;
          m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangeDecl,newString);
        }

      }

      // if(isa<FunctionDecl>(D))
      // {
      //   FunctionDecl *funcDecl = cast<FunctionDecl>(D);
      //   if(funcDecl->getNameAsString().compare("main") == 0)
      //   {
      //     funcDecl->getBody()->dump(llvm::outs(),*m_sourceManager);


      //     SourceRange rangeBody;

      //     rangeBody.setBegin(funcDecl->getBody()->getLocStart());
      //     rangeBody.setEnd(funcDecl->getBody()->getLocEnd());

      //     std::string strForBody;
      //     llvm::raw_string_ostream rawForBody(strForBody);
      //     funcDecl->getBody()->printPretty(rawForBody, 0, PrintingPolicy(Context->getLangOpts()),0);
      //     rawForBody.str();

      //     llvm::outs() << strForBody << "\n";

      //   }
      // }

  }

  bool VisitDecl(Decl* D)
  {
    rewriteDecl(D);
	  PolcaASTVisitor::VisitDecl(D);

	  return true;
  }

//  std::vector<Rewriter*> getRewriters(){
//	  return m_rewritersVector;
//  }

  void createOutputStreams()
  {
	  raw_ostream *OS;
	  char buffer[255];

	  //sprintf(buffer, "./output/mpi/rule%d/heatmapMPI", 4);
    sprintf(buffer, "./output/mpi/rule_%s/heatmapMPI", m_polcaRulesNames[3]);
	  OS = m_compilerInstance->createDefaultOutputFile(false, buffer, "c");
	  m_rewritingElementsVector[0]->m_outStreamsVector.push_back(OS);

  }

void removeComments()
{
    std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
    std::vector<PolcaPragma*>::iterator itpp;

    for(itpp = v.begin(); itpp != v.end(); ++itpp) {
      PolcaPragma *pp = *itpp;
      //pp->getPragmaSrcLocation()
      llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
      //llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";

      SourceLocation newSL2 = m_compilerInstance->getSourceManager().translateLineCol(m_compilerInstance->getSourceManager().getFileID(pp->getPragmaSrcLocation()), m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation()), 1);

      llvm::outs() << "Start source Location pragma: "<< newSL2.printToString(m_compilerInstance->getSourceManager())<<"\n";

      SourceLocation newSL3 = m_compilerInstance->getSourceManager().translateLineCol(m_compilerInstance->getSourceManager().getFileID(pp->getPragmaSrcLocation()), m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation()) , 100);

      llvm::outs() << "End line source Location pragma: "<< newSL3.printToString(m_compilerInstance->getSourceManager())<<"\n";

      SourceRange rangePP;
      rangePP.setBegin(newSL2);
      rangePP.setEnd(newSL3);


      //for(int i=0;i<m_polcaRules->v.size();i++)
      //{
        m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangePP, "");
      //}

      //llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";

    }
}


  void dumpOutputStreams()
  {
  	createOutputStreams();

  	int i = 0;
  	const RewriteBuffer *RewriteBuf;

    //removeComments();


//  	std::vector<Rewriter*> rewriters = getRewriters();
  	std::vector<Rewriter*>::iterator it;

	for(it=m_rewritingElementsVector[0]->m_rewritersVector.begin();it!=m_rewritingElementsVector[0]->m_rewritersVector.end();it++)
  	{
  		Rewriter *rw = *(it);
  		RewriteBuf = rw->getRewriteBufferFor(m_compilerInstance->getSourceManager().getMainFileID());
  		raw_ostream *OS = m_rewritingElementsVector[0]->m_outStreamsVector[i];
  	    *(OS) << string(RewriteBuf->begin(), RewriteBuf->end());

  	    i++;
  	}
  }

//private:
//  CompilerInstance 			*m_compilerInstance;
//  ASTContext       			*Context;
//  SourceManager    			*m_sourceManager;
//  std::vector<Rewriter*>	 m_rewritersVector;
//  llvm::StringRef 			 m_inputFile;
//  PolcaPragmaTable			*m_pragmaTable;
//  SymbolTable				*m_symbolTable;
//
//  int 						 m_traversalId;
//  bool						 m_firstTraverseVisit;

};

////////////////////////////////////////////////////////////////

#endif
