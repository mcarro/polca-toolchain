#ifndef _POLCAASTVISITOROPENMP_H_
#define _POLCAASTVISITOROPENMP_H_

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

#include "PolcaPragma.h"
#include "SymbolTable.h"
#include "PolcaPragmaTable.h"
#include "PolcaASTVisitor.cpp"

using namespace std;
using namespace clang;
using namespace sema;


class PolcaASTVisitorOpenMP : public PolcaASTVisitor {


public:
  explicit PolcaASTVisitorOpenMP(CompilerInstance *CI
		  ,ASTContext *context
		  , SourceManager *sourceManager
		  ,PolcaPragmaTable *pragmaTable
		  , SymbolTable *symbolTable
		  , PolcaRules* polcaRules
		  , llvm::StringRef inFile):PolcaASTVisitor(CI,context,sourceManager,pragmaTable,symbolTable,polcaRules,inFile)
  {}

  void initializeRewriters()
  {
	  Rewriter *rewriter;
	  RewritingElements*  re;

    int size = m_polcaRules->v.size() - 2;

	  for(int i=0;i<size;i++)
	  {

		  re = new RewritingElements();

		  rewriter = new Rewriter();
		  rewriter->setSourceMgr(m_compilerInstance->getSourceManager(), m_compilerInstance->getLangOpts());
		  re->m_rewritersVector.push_back(rewriter);

		  m_rewritingElementsVector.push_back(re);


	  }
  }

  void rewriteStatement(Stmt *statement)
  {
      if(isa<ForStmt>(statement))
      {

        ForStmt *forStmt = cast<ForStmt>(statement);

         std::string strForBody;
          llvm::raw_string_ostream rawForBody(strForBody);
          forStmt->getBody()->printPretty(rawForBody, 0, PrintingPolicy(Context->getLangOpts()),2);
          rawForBody.str();


          std::string strFor;
          llvm::raw_string_ostream rawFor(strFor);


          SourceRange rangeFor;
          rangeFor.setBegin(forStmt->getLocStart());
          rangeFor.setEnd(forStmt->getLocEnd());

          std::string newString;

          std::vector<PolcaPragma*>::iterator it;
         std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
       for(it = v.begin(); it != v.end(); ++it) {
        PolcaPragma *pp = *it;
        // llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
        // llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";
        // llvm::outs() << "Source Location for: "<< forStmt->getForLoc().printToString(m_compilerInstance->getSourceManager())<<"\n";
        // llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(forStmt->getForLoc())  <<"\n";
        // llvm::outs() << pp->getPragmaId() <<"\n";


        //******************************************************
        // zipWith3
        //******************************************************

        if(m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())+1 == m_compilerInstance->getSourceManager().getSpellingLineNumber(forStmt->getForLoc()) && pp->getPragmaId().compare("G") == 0)
        {
          forStmt->printPretty(rawFor, 0, PrintingPolicy(Context->getLangOpts()),1);
          rawFor.str();

          DeclarationNameInfo initForVar = cast<DeclRefExpr>(cast<BinaryOperator>(forStmt->getInit())->getLHS())->getNameInfo();

          //VarDecl *varFor = forStmt->getConditionVariable();

          //initFor-> dump(llvm::outs(),*m_sourceManager);

          //initFor-> dump(llvm::outs(),*m_sourceManager);

          //llvm::outs() << initForVar.getAsString() << "\n";

          std::string varNameFor = initForVar.getAsString();

          //varFor->dump(llvm::outs());

          newString = "\
n_chunks = 3;\n\
\tint chunk;\n\
\tn_elem_chunk = n_elem / n_chunks;\n\
\tfor(chunk = 0; chunk<n_chunks; chunk++){\n\
\t\tint start = chunk == 0?1:0;\n\
\t\tint end = chunk == n_chunks-1?1:0;\n\
\t\tfor("+varNameFor+"=start; "+varNameFor+"<n_elem_chunk-end; "+varNameFor+"++)\n\
\t\t#pragma polca def G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o\n\
\t\t{\n\
\t\t\tint ni = chunk*n_elem_chunk+"+varNameFor+";\n\
\t\t\theatmap_tmp[ni] = g(heatmap[ni - 1], heatmap[ni], heatmap[ni + 1]);\n\
\t\t}\n\
\t}";
          m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);
          newString = "\
n_chunks = 3;\n\
\tint chunk;\n\
\tn_elem_chunk = n_elem / n_chunks;\n\
\t#pragma omp parallel for private("+varNameFor+")\n\
\tfor(chunk = 0; chunk<n_chunks; chunk++){\n\
\t\tint start = chunk == 0?1:0;\n\
\t\tint end = chunk == n_chunks-1?1:0;\n\
\t\tfor("+varNameFor+"=start; "+varNameFor+"<n_elem_chunk-end; "+varNameFor+"++)\n\
\t\t#pragma polca def G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o\n\
\t\t{\n\
\t\t\tint ni = chunk*n_elem_chunk+"+varNameFor+";\n\
\t\t\theatmap_tmp[ni] = g(heatmap[ni - 1], heatmap[ni], heatmap[ni + 1]);\n\
\t\t}\n\
\t}";
          m_rewritingElementsVector[1]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);
//           newString = "\
// int start = chunk == 0?1:0;\n\
// \tint end = chunk == n_chunks-1?1:0;\n\
// \tfor(i=start; i<n_elem_chunk-end; i++)\n\
// \t#pragma polca def G\n\
// \t{\n\
// \t\tint ni = chunk*n_elem_chunk+i;\n\
// \t\theatmap_tmp[ni] = g(heatmap[ni - 1], heatmap[ni], heatmap[ni + 1]);\n\
// \t}\n";
          // m_rewritingElementsVector[2]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);
          // m_rewritingElementsVector[3]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);

          // m_rewriter->RemoveText(rangeFor);
        }


        //******************************************************
        // itn
        //******************************************************

        if(m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())+1 == m_compilerInstance->getSourceManager().getSpellingLineNumber(forStmt->getForLoc()) && pp->getPragmaId().compare("HEATSPREAD") == 0)
        {

          forStmt->printPretty(rawFor, 0, PrintingPolicy(Context->getLangOpts()),2);
          rawFor.str();

          m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangeFor,strFor);
          m_rewritingElementsVector[1]->m_rewritersVector[0]->ReplaceText(rangeFor,strFor);
          // newString = "n_chunks = 10;\n\tn_elem_chunk = n_elem / n_chunks;\n\tfor(int chunk = 0; chunk<n_chunks; chunk++){\n"+ strFor + "\t}\n";
          // m_rewritingElementsVector[2]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);
          // newString = "n_chunks = 10;\n\tn_elem_chunk = n_elem / n_chunks;\n\t#pragma omp parallel for\n\tfor(int chunk = 0; chunk<n_chunks; chunk++){\n"+ strFor + "\t\t#pragma omp barrier\n\t}\n";
          // m_rewritingElementsVector[3]->m_rewritersVector[0]->ReplaceText(rangeFor,newString);

          // m_rewriter->RemoveText(rangeFor);
        }


      }
    }
  }

  bool VisitStmt(Stmt *statement)
  {

//	  if(m_firstTraverseVisit)
//	  {
//		  printf("\n\n\n");
//		  printf("/*******************************************************/\n");
//		  printf("/                 Traversing input AST                  /\n");
//		  printf("/*******************************************************/\n");
//		  printf("\n");
//
//		  m_firstTraverseVisit = false;
//
//		  initializeRewriters();
//	  }


      PolcaASTVisitor::VisitStmt(statement);

      rewriteStatement(statement);

	  return true;
  }

  void rewriteDecl(Decl* D)
  {
      unsigned lineSrcLocation = m_sourceManager->getSpellingLineNumber(D->getSourceRange().getBegin());


      if(isa<VarDecl>(D))
      {
        VarDecl *varDecl = cast<VarDecl>(D);
        // llvm::outs() << "Source Location decl: "<< varDecl->getLocStart().printToString(m_compilerInstance->getSourceManager())<<"\n";
        // llvm::outs() << "Source line decl: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(varDecl->getLocStart())  <<"\n";
        // llvm::outs() << "Name decl: "<< varDecl->getNameAsString()  <<"\n";


        std::string strDef;
        llvm::raw_string_ostream rawDef(strDef);
        varDecl->print(rawDef, PrintingPolicy(Context->getLangOpts()),0);
        rawDef.str();


        SourceRange rangeDecl;
        rangeDecl.setBegin(varDecl->getLocStart());
        rangeDecl.setEnd(varDecl->getLocEnd());

        if(varDecl->getNameAsString().compare("n_iter") == 0)
        {
          for(int i=0;i<m_polcaRules->v.size() - 2;i++)
          {
            std::string newString = "#include <omp.h>;\n\nint n_chunks;\nint n_elem_chunk;\n" + strDef ;
             m_rewritingElementsVector[i]->m_rewritersVector[0]->ReplaceText(rangeDecl,newString);
          }
        }

      }

      if(isa<FunctionDecl>(D))
      {
        FunctionDecl *funcDecl = cast<FunctionDecl>(D);
        if(funcDecl->getNameAsString().compare("heatspread") == 0)
        {
          ArrayRef<ParmVarDecl*> parameters = funcDecl->parameters();

          ArrayRef<ParmVarDecl*>::iterator it;

          for(it=parameters.begin();it!=parameters.end();it++)
          {
            ParmVarDecl *par = *(it);
            llvm::outs() << par->getNameAsString() << "\n";
          }

          //funcDecl -> dump(llvm::outs());

          // funcDecl->getBody()->dump(llvm::outs(),*m_sourceManager);


        SourceRange rangeDecl;
        rangeDecl.setBegin(funcDecl->getLocStart());
        rangeDecl.setEnd(funcDecl->getLocEnd());

        std::string strBody;
        llvm::raw_string_ostream rawBody(strBody);
        funcDecl->getBody()->printPretty(rawBody, 0, PrintingPolicy(Context->getLangOpts()),0);
        rawBody.str();


        std::string newString = "void heatspread(float *heatmap,float *heatmap_tmp,int chunk)" + strBody;

        llvm::outs() << newString << "\n";

        //m_rewritingElementsVector[2]->m_rewritersVector[0]->ReplaceText(rangeDecl,newString);
        //m_rewritingElementsVector[3]->m_rewritersVector[0]->ReplaceText(rangeDecl,newString);
          // llvm::outs() << strForBody << "\n";

        }
      }
  }

  bool VisitDecl(Decl* D)
  {
    rewriteDecl(D);

	  PolcaASTVisitor::VisitDecl(D);

	  return true;
  }

//  std::vector<Rewriter*> getRewriters(){
//	  return m_rewritersVector;
//  }

  void createOutputStreams()
  {
	  raw_ostream *OS;
	  char buffer[255];

	  for(int i=0;i<m_polcaRules->v.size() - 2;i++)
	  {
		  //sprintf(buffer, "./output/openmp/rule%d/heatmapOpenMP", (i+1));
      sprintf(buffer, "./output/openmp/rule_%s/heatmapOpenMP", m_polcaRulesNames[i]);
		  OS = m_compilerInstance->createDefaultOutputFile(false, buffer, "c");
		  m_rewritingElementsVector[i]->m_outStreamsVector.push_back(OS);
	  }

  }

void removeComments()
{
    std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
    std::vector<PolcaPragma*>::iterator itpp;

    for(itpp = v.begin(); itpp != v.end(); ++itpp) {
      PolcaPragma *pp = *itpp;
      //pp->getPragmaSrcLocation()
      //llvm::outs() << "Source Location pragma: "<< pp->getPragmaSrcLocation().printToString(m_compilerInstance->getSourceManager())<<"\n";
      //llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";

      SourceLocation newSL2 = m_compilerInstance->getSourceManager().translateLineCol(m_compilerInstance->getSourceManager().getFileID(pp->getPragmaSrcLocation()), m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation()), 1);

      //llvm::outs() << "Start source Location pragma: "<< newSL2.printToString(m_compilerInstance->getSourceManager())<<"\n";

      SourceLocation newSL3 = m_compilerInstance->getSourceManager().translateLineCol(m_compilerInstance->getSourceManager().getFileID(pp->getPragmaSrcLocation()), m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation()) , 100);

      //llvm::outs() << "End line source Location pragma: "<< newSL3.printToString(m_compilerInstance->getSourceManager())<<"\n";

      SourceRange rangePP;
      rangePP.setBegin(newSL2);
      rangePP.setEnd(newSL3);


      //for(int i=0;i<m_polcaRules->v.size();i++)
      //{
        m_rewritingElementsVector[0]->m_rewritersVector[0]->ReplaceText(rangePP, "");
      //}

      //llvm::outs() << "Source line pragma: "<< m_compilerInstance->getSourceManager().getSpellingLineNumber(pp->getPragmaSrcLocation())  <<"\n";

    }
}


  void dumpOutputStreams()
  {
  	createOutputStreams();

//  	int i = 0;
  	const RewriteBuffer *RewriteBuf;

    //removeComments();

  	std::vector<Rewriter*>::iterator it;

    //int total_rules = m_polcaRules->v.size();
    int total_rules = 2;

	  for(int i=0;i<total_rules;i++)
	  {
  		for(it=m_rewritingElementsVector[i]->m_rewritersVector.begin();it!=m_rewritingElementsVector[i]->m_rewritersVector.end();it++)
  		{
  			Rewriter *rw = *(it);
  			RewriteBuf = rw->getRewriteBufferFor(m_compilerInstance->getSourceManager().getMainFileID());
  			raw_ostream *OS = m_rewritingElementsVector[i]->m_outStreamsVector[0];
  			*(OS) << string(RewriteBuf->begin(), RewriteBuf->end());

  	//  	    i++;
  		}
	  }
  }

//private:
//  CompilerInstance 			*m_compilerInstance;
//  ASTContext       			*Context;
//  SourceManager    			*m_sourceManager;
//  std::vector<Rewriter*>	 m_rewritersVector;
//  llvm::StringRef 			 m_inputFile;
//  PolcaPragmaTable			*m_pragmaTable;
//  SymbolTable				*m_symbolTable;
//
//  int 						 m_traversalId;
//  bool						 m_firstTraverseVisit;

};

////////////////////////////////////////////////////////////////

#endif
