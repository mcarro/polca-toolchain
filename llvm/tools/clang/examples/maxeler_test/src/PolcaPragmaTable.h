#ifndef _POLCAPRAGMATABLE_H_
#define _POLCAPRAGMATABLE_H_

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

#include "PolcaPragma.h"

using namespace std;
using namespace clang;
using namespace sema;

class PolcaPragmaTable {
public:
	PolcaPragmaTable();
	
	~PolcaPragmaTable();
	
	vector<PolcaPragma*> getPragmaTable();

	
	void insertPolcaPragma(PolcaPragma *pragma);

	PolcaPragma* findPolcaPragmaById(string pragmaId);

	PolcaPragma* findPolcaPragmaByDirective(string pragmaDirective);


private:
	vector<PolcaPragma*>		m_pragmaTable;

//	static const std::vector<string>		s_higherOrdFuncs = {"zipWith3","itn"};

};

#endif
