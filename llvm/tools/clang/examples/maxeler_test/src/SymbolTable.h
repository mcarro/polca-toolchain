#ifndef _SYMBOLTABLE_H_
#define _SYMBOLTABLE_H_

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

using namespace std;
using namespace clang;
using namespace sema;

//enum symbolType {
//  value1,
//  value2,
//  value3,
//  .
//  .
//}SymbolType;

struct Symbol{
	string 	id;
	string	type;
	Stmt*  	rootASTNode;
};

class SymbolTable {
public:
	SymbolTable();
	
	~SymbolTable();
	
	void insertSymbol(Symbol* s);

	Symbol* findSymbol(string symbolId);

	void prettyPrintTable(SourceManager* sourceManager);
	
private:
	vector<Symbol*>		m_symbolTable;

};
#endif
