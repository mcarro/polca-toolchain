#ifndef _POLCAPRAGMA_H_
#define _POLCAPRAGMA_H_

#define PUGIXML_HEADER_ONLY

#include "../pugixml/pugixml.hpp"
#include "../pugixml/pugixml.cpp"
#include "../pugixml/pugixml.hpp"

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

using namespace std;
using namespace clang;
using namespace sema;


typedef enum{
	nil = 0,
	init = 1,
	argument = 2,
	offset = 3,
	argumentType = 4,
//	offset = 4,
//	postOffset = 5,
}ParsingState;

class PolcaPragma {
public:
	PolcaPragma();
	
	~PolcaPragma();
	
	void setPragmaContent(std::vector<string> content);
	
	void setPragmaContentWithChild(std::vector<string> content, PolcaPragma* pp);

	std::vector<string> getPragmaContent();
	
	string getPragmaContentStr();

	void setPragmaSrcLocation(SourceLocation loc);
		
	SourceLocation getPragmaSrcLocation();

	void setNextStmtSrcLocation(SourceLocation loc);
		
	SourceLocation getNextStmtSrcLocation();
	
	void setPragmaId(string id);

	string getPragmaId();

	void setPragmaDirective(string id);

	string getPragmaDirective();

	bool isAssociated();

	void setAssociated(bool value);

	//pugi::xml_document getPragmaMXLSpecification();

	pugi::xml_document				m_pragmaXMLSpecification;

private:
	void generatePragmaSpecification();
	void parsePragmaContent();


	PolcaPragma*					m_pragmaChild;
	std::vector<string>				m_pragmaContent;
	SourceLocation					m_pragmaSrcLocation;
	SourceLocation					m_nextStmtSrcLocation;
	string							m_pragmaDirective;
	string 							m_pragmaId;
	string							m_pragmaSpecification;

	bool							m_associated;

//	static const std::vector<string>		s_higherOrdFuncs = {"zipWith3","itn"};

};

#endif
