#ifndef _POLCAASTVISITOR_H_
#define _POLCAASTVISITOR_H_


#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

#include "PolcaPragma.h"
#include "SymbolTable.h"
#include "PolcaPragmaTable.h"
#include "apply_polca_rules.cpp"

using namespace std;
using namespace clang;
using namespace sema;


struct RewritingElements{

	  std::vector<raw_ostream*>  m_outStreamsVector;
	  std::vector<Rewriter*>	 m_rewritersVector;
	  std::vector<std::string*>	 m_filesContent;
};

class PolcaASTVisitor : public RecursiveASTVisitor<PolcaASTVisitor> {


public:
  explicit PolcaASTVisitor(CompilerInstance *CI,ASTContext *context, SourceManager *sourceManager,PolcaPragmaTable *pragmaTable, SymbolTable *symbolTable, PolcaRules* polcaRules, llvm::StringRef inFile)
  {
	  m_compilerInstance = CI;
	  Context = context;
	  m_sourceManager = sourceManager;
	  m_inputFile = inFile;

	  m_firstTraverseVisit = true;
	  m_traversalId = 0;

	  m_pragmaTable = pragmaTable;
	  m_symbolTable = symbolTable;

	  m_polcaRules = polcaRules;

//	  m_rewriter = rewriter;

  }

  virtual void initializeRewriters(){}


  virtual bool VisitStmt(Stmt *statement)
  {
	  if(m_firstTraverseVisit)
	  {
		  printf("\n\n\n");
		  printf("/*******************************************************/\n");
		  printf("/                 Traversing input AST                  /\n");
		  printf("/*******************************************************/\n");
		  printf("\n");

		  m_firstTraverseVisit = false;

		  //std::cout << "BEFORE\n";
		  (m_pragmaTable->findPolcaPragmaByDirective("itn")->m_pragmaXMLSpecification).print(std::cout);
		  m_polcaRules->apply_rules_doc("input/polca_rules.xml", &(m_pragmaTable->findPolcaPragmaByDirective("itn")->m_pragmaXMLSpecification));

		  //m_polcaRulesNames = {"chunkZipWith","chunkZipWithPar","chunkItnZipWith","chunkItnZipWithPar"};

		  m_polcaRulesNames.emplace_back ("z3_to_map");
		  m_polcaRulesNames.emplace_back ("z3_to_mapP");
		  m_polcaRulesNames.emplace_back ("itn_z3_to_map");
		  m_polcaRulesNames.emplace_back ("itn_z3_to_mapP");

		  initializeRewriters();

	  }

		unsigned lineSrcLocation = m_sourceManager->getSpellingLineNumber(statement->getSourceRange().getBegin());

		PolcaPragma *pp = NULL;
		std::vector<PolcaPragma*>::iterator it;
		std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
		for(it = v.begin(); it != v.end(); ++it) {
			pp = *it;
			SourceLocation sl = pp->getNextStmtSrcLocation();
			if( (m_sourceManager->isInMainFile(statement->getLocStart())) && (lineSrcLocation == m_sourceManager->getSpellingLineNumber(sl)) && !(pp->isAssociated()) )
			{
				llvm::outs() << m_traversalId << " - Stmt with POLCA Pragma found!!" << "\n";
				llvm::outs() << "\tAttached pragma: " << pp->getPragmaContentStr() << "\n";
//				statement->dump(llvm::outs(),*m_sourceManager);

				Symbol* s = new Symbol();

				if(isa<ForStmt>(statement)){
					s->id = pp->getPragmaDirective();
					s->type = std::string("ForStmt");
				}else{
				//if(isa<CompoundStmt>(statement)){
					s->id = pp->getPragmaId();
					s->type = std::string("Block");
				}

				s->rootASTNode = statement;
				statement->printPretty(llvm::outs(), 0, PrintingPolicy(Context->getLangOpts()));

				m_symbolTable->insertSymbol(s);

				pp->setAssociated(true);
			}
		}

	  return true;
  }

  virtual bool VisitDecl(Decl* D)
  {
	  if(isa<FunctionDecl>(D))
	  {
		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
	  	  DeclarationName name = nameInfo.getName();
	  	  if( m_sourceManager->isInMainFile(D->getSourceRange().getBegin()) )
	  	  {

				unsigned lineSrcLocation = m_sourceManager->getSpellingLineNumber(D->getSourceRange().getBegin());

	  		  	PolcaPragma *pp = NULL;
	  		    std::vector<PolcaPragma*>::iterator it;
	  		    std::vector<PolcaPragma*> v = m_pragmaTable->getPragmaTable();
				for(it = v.begin(); it != v.end(); ++it) {
					pp = *it;
					SourceLocation sl = pp->getNextStmtSrcLocation();
//					llvm::outs() << m_traversalId << " - function: " << name.getAsString() << " found!!" << "\n";
					if( (lineSrcLocation == m_sourceManager->getSpellingLineNumber(sl)) && !(pp->isAssociated()) )
					{
						llvm::outs() << m_traversalId << " - function: " << name.getAsString() << " found!!" << "\n";
						llvm::outs() << "\tAttached pragma: " << pp->getPragmaContentStr() << "\n";

						//llvm::outs() << "\tLocation: " << D->getSourceRange().getBegin().printToString(*m_sourceManager) << "  " << m_sourceManager->getSpellingLineNumber(D->getSourceRange().getBegin()) << "\n";
		//				D->dump(llvm::outs());

						Symbol* s = new Symbol();
						s->id = name.getAsString();
						s->type = std::string("Function");
						s->rootASTNode = D->getBody();

						D->getBody()->printPretty(llvm::outs(), 0, PrintingPolicy(Context->getLangOpts()));
//						D->print(llvm::outs(), PrintingPolicy(Context->getLangOpts()));

						m_symbolTable->insertSymbol(s);

						m_traversalId++;

						pp->setAssociated(true);
					}
//					D->dump(llvm::outs());
				}

	  	  }

	  }

	  return true;
  }

//  virtual std::vector<Rewriter*> getRewriters(){
//	  return m_rewritersVector;
//  }

  virtual void createOutputStreams()
  {}

  virtual void dumpOutputStreams()
  {}

protected:
  CompilerInstance 					*m_compilerInstance;
  ASTContext       					*Context;
  SourceManager    					*m_sourceManager;

  llvm::StringRef 			 		 m_inputFile;
  PolcaPragmaTable					*m_pragmaTable;
  SymbolTable						*m_symbolTable;
  PolcaRules						*m_polcaRules;
  std::vector<const char*>			m_polcaRulesNames;

  std::vector<RewritingElements*>	 m_rewritingElementsVector;

  int 						 m_traversalId;
  bool						 m_firstTraverseVisit;

//  string 			m_preForLoop;
//  string 			m_forLoop;
//  string 			m_postForLoop;
//  static assignmentSide m_assignmentSide;
};

////////////////////////////////////////////////////////////////

#endif
