#ifndef _POLCAASTVISITORMAXJ_H_
#define _POLCAASTVISITORMAXJ_H_

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

#include "PolcaPragma.h"
#include "SymbolTable.h"
#include "PolcaPragmaTable.h"
#include "PolcaASTVisitor.cpp"

using namespace std;
using namespace clang;
using namespace sema;

typedef enum{
	KERNEL = 0,
	MANAGER,
	CPU,
} outputFileType;

class PolcaASTVisitorMaxj : public PolcaASTVisitor {


public:
  explicit PolcaASTVisitorMaxj(CompilerInstance *CI
		  ,ASTContext *context
		  , SourceManager *sourceManager
		  ,PolcaPragmaTable *pragmaTable
		  , SymbolTable *symbolTable
		  , PolcaRules* polcaRules
		  , llvm::StringRef inFile):PolcaASTVisitor(CI,context,sourceManager,pragmaTable,symbolTable,polcaRules,inFile)
  {}

  void initializeRewriters()
  {

	  for(int i=0;i<m_polcaRules->v.size();i++)
	  {
		  std::string* str;

		  if(i<2)
		  {
			  RewritingElements*  re = new RewritingElements();

			  for(int i=0;i<3;i++)
			  {
				  str = new std::string();
				  re->m_filesContent.push_back(str);
			  }

			  m_rewritingElementsVector.push_back(re);
		  }
	  }

	  writeFilesHeaders();
  }

  void dumpFilesContent()
  {
//	  SourceLocation s;
//	  m_rewritersVector[0]->InsertText(m_initOutputSourceLocation,
//			  	  	  	  	 //m_kernelContent.c_str(),
//			  	  	  	  	 "KK de la vaca!!!!",
//                             true, true);
  }

  void writeKernelHeader()
  {
	  for(int i=0;i<m_polcaRules->v.size();i++)
	  {
		  if(i==0)
		  {
	  m_rewritingElementsVector[i]->m_filesContent[KERNEL]->append(" \
	  package heat;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;\n \
	  class HeatKernel extends Kernel {\n \
	  	protected HeatKernel(KernelParameters parameters) {\n \
	  		super(parameters);\n\n \
	  		DFEVar inStream = io.input(\"heatmap\", dfeFloat(8,24));\n \
			DFEVar l = constant.var( dfeFloat(8,24),0);\n \
			DFEVar c = constant.var( dfeFloat(8,24),0);\n \
			DFEVar r = constant.var( dfeFloat(8,24),0);\n\n \
			l  = stream.offset(inStream,-1);\n \
			c  = stream.offset(inStream, 0);\n \
			r  = stream.offset(inStream, 1);\n\n \
	  ");
		  }
		  if(i==1)
		  {
	  m_rewritingElementsVector[i]->m_filesContent[KERNEL]->append(" \
	  package heat;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;\n \
	  class HeatKernel extends Kernel {\n \
		protected HeatKernel(KernelParameters parameters) {\n \
			super(parameters);\n\n \
			int numPipes = 12;\n\n \
			DFEVectorType<DFEVar> vt = new DFEVectorType<DFEVar>( dfeFloat(8,24),numPipes);\n \
			DFEVector<DFEVar> inStream = io.input(\"heatmap\", vt);\n\n \
			DFEVector<DFEVar> l = constant.var( dfeFloat(8,24),0);\n \
			DFEVector<DFEVar> c = constant.var( dfeFloat(8,24),0);\n \
			DFEVector<DFEVar> r = constant.var( dfeFloat(8,24),0);\n\n \
			l  = stream.offsetStriped(inStream, -1);\n \
			c  = stream.offsetStriped(inStream, 0);\n \
			r  = stream.offsetStriped(inStream, +1);\n\n \
	  ");
		  }
	  }

//	  dumpFilesContent();
  }

  void writeManagerHeader()
  {
	  for(int i=0;i<m_polcaRules->v.size();i++)
	  {
		  if(i==0)
		  {
	  m_rewritingElementsVector[i]->m_filesContent[MANAGER]->append(" \
	  package heat;\n\n \
	  import com.maxeler.maxcompiler.v2.build.EngineParameters;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;\n \
	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;\n \
	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;\n \
	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;\n \
	  import com.maxeler.maxcompiler.v2.managers.standard.IOLink.IODestination;\n \
	  import com.maxeler.maxcompiler.v2.managers.standard.Manager;\n\n \
	  class HeatManager {\n\n \
		public static void main(String[] args) {\n \
			Manager manager = new Manager(new EngineParameters(args));\n \
			Kernel kernel = new HeatKernel(manager.makeKernelParameters());\n \
			manager.setKernel(kernel);\n \
			manager.setIO(IOType.ALL_CPU);\n \
			manager.createSLiCinterface();\n \
			manager.build();\n \
		}\n \
	  }\n \
	  ");
		  }
		  if(i==1)
		  {
	  m_rewritingElementsVector[i]->m_filesContent[MANAGER]->append(" \
	  package heat;\n\n \
	  import com.maxeler.maxcompiler.v2.build.EngineParameters;\n \
	  import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;\n \
	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;\n \
	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;\n \
	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;\n \
	  import com.maxeler.maxcompiler.v2.managers.standard.IOLink.IODestination;\n \
	  import com.maxeler.maxcompiler.v2.managers.standard.Manager;\n\n \
	  class HeatManager {\n\n \
		public static void main(String[] args) {\n \
			Manager manager = new Manager(new EngineParameters(args));\n \
			Kernel kernel = new HeatKernel(manager.makeKernelParameters());\n \
			manager.setKernel(kernel);\n \
			manager.setIO(IOType.ALL_CPU);\n\n \
			manager.createSLiCinterface(new EngineInterface(\"parallel\") {\n \
				{\n \
				int numPipes = 12;\n \
				InterfaceParam length = addParam(\"length\", CPUTypes.INT32);\n \
				InterfaceParam heatmap = addParam(\"heatmap\", CPUTypes.INT32);\n \
				InterfaceParam heatmap_tmp = addParam(\"heatmap_tmp\", CPUTypes.INT32);\n \
				ignoreAll(Direction.IN_OUT);\n\n \
				setTicks(\"HeatKernel\", length / numPipes);\n \
				}\n \
			});\n \
			manager.build();\n \
		}\n \
	  }\n \
	  ");
		  }
	  }

  }

  void writeCPUHeader(){
	  for(int i=0;i<m_polcaRules->v.size();i++)
	  {
		  if(i<2)
		  {
	  m_rewritingElementsVector[i]->m_filesContent[CPU]->append(" \
#include <math.h>\n \
#include <stdio.h>\n \
#include <stdlib.h>\n\n \
#include \"Maxfiles.h\"\n \
#include <MaxSLiCInterface.h>\n\n \
int n_iter;\n \
int n_elem;\n\n");
		  }
	  }
  }

  void writeFilesHeaders()
  {
	  writeKernelHeader();
	  writeManagerHeader();
	  writeCPUHeader();
  }

  bool VisitStmt(Stmt *statement)
  {

      PolcaASTVisitor::VisitStmt(statement);


	  return true;
  }

  void processKernelFunction(Decl* D)
  {
	  	  Symbol* s;


		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
	  	  DeclarationName name = nameInfo.getName();


			  s = m_symbolTable->findSymbol(name.getAsString());
			  if(s)
			  {
//				  std::cout << "Symbol found!!!\n";
				  PolcaPragma *pp = m_pragmaTable->findPolcaPragmaById(s->id);
				  if(pp)
				  {
					  if(pp->getPragmaDirective().compare("kernel") == 0)
					  {
	//					  std::cout << "Pragma Polca found!!!\n";
						  string str;
						  llvm::raw_string_ostream rso(str);
						  CompoundStmt *cmdStmt = cast<CompoundStmt>(s->rootASTNode);



						  cmdStmt->body_back()->printPretty(rso, 0, PrintingPolicy(Context->getLangOpts()));
						  string aux = rso.str();
						  aux.replace(aux.begin(),aux.begin()+6,"c = ");
						  m_rewritingElementsVector[0]->m_filesContent[KERNEL]->append("\t\t"+aux+"\n");
						  m_rewritingElementsVector[0]->m_filesContent[KERNEL]->append(" \
\t\t\tio.output(\"heatmap_tmp\", c, inStream.getType());\n \
	  	}\n \
	  }\n");

						  m_rewritingElementsVector[1]->m_filesContent[KERNEL]->append("\t\t"+aux+"\n");
						  m_rewritingElementsVector[1]->m_filesContent[KERNEL]->append(" \
\t\t\tio.output(\"heatmap_tmp\", c, inStream.getType());\n \
	  	}\n \
	  }\n");
					  }
				  }
			  }



  }

  void processInitFunction(Decl* D)
  {
	  	  Symbol* s;


		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
	  	  DeclarationName name = nameInfo.getName();


			  s = m_symbolTable->findSymbol(name.getAsString());
			  if(s)
			  {
//				  std::cout << "Symbol found!!!\n";
				  PolcaPragma *pp = m_pragmaTable->findPolcaPragmaById(s->id);
				  if(pp)
				  {
					  if(pp->getPragmaId().compare("init") == 0)
					  {

						  string str;
						  llvm::raw_string_ostream rso(str);
//						  CompoundStmt *cmdStmt = cast<CompoundStmt>(s->rootASTNode);



//						  cmdStmt->body_back()->printPretty(rso, 0, PrintingPolicy(Context->getLangOpts()));
						  D->print(rso, PrintingPolicy(Context->getLangOpts()));
						  string aux = rso.str();

						  m_rewritingElementsVector[0]->m_filesContent[CPU]->append(aux+"\n");

						  m_rewritingElementsVector[1]->m_filesContent[CPU]->append(aux+"\n");

					  }
				  }
			  }



  }

  void processPrintCellsFunction(Decl* D)
    {
  	  	  Symbol* s;


  		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
  	  	  DeclarationName name = nameInfo.getName();

		  if(name.getAsString().compare("printCells") == 0)
		  {
			  string str;
			  llvm::raw_string_ostream rso(str);

			  D->print(rso, PrintingPolicy(Context->getLangOpts()));
			  string aux = rso.str();

			  m_rewritingElementsVector[0]->m_filesContent[CPU]->append(aux+"\n");

			  m_rewritingElementsVector[1]->m_filesContent[CPU]->append(aux+"\n");

		  }



    }

  void processHeatspreadFunction(Decl* D)
      {
    	  	  Symbol* s;



    		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
    	  	  DeclarationName name = nameInfo.getName();



    			  if(name.getAsString().compare("heatspread") == 0)
    			  {


  //				  string str;
  //				  llvm::raw_string_ostream rso(str);
  //
  //
  //				  D->print(rso, PrintingPolicy(Context->getLangOpts()));
  //				  string aux = rso.str();
    				  for(int i=0;i<m_polcaRules->v.size();i++)
    				  {
    					  if(i==0)
    					  {
    						m_rewritingElementsVector[i]->m_filesContent[CPU]->append("\
void heatspread(float *heatmap,float *heatmap_tmp)\n \
{\n \
  	   heat(heatmap,n_elem,heatmap_tmp);\n\n \
  	   heatmap_tmp[n_elem-1] = 0.0;\n \
  	   heatmap_tmp[0] = 0.0;\n\n \
  	   memcpy((void*)heatmap, (void*)heatmap_tmp, n_elem * sizeof(float));\n \
}\n\n");

  //				  	  	  m_rewritingElementsVector[i]->m_filesContent[CPU]->append(aux+"\n");
    					  }
    					  if(i==1)
    					  {
      					m_rewritingElementsVector[i]->m_filesContent[CPU]->append("\
void heatspread(float *heatmap,float *heatmap_tmp) {\n \
{\n \
  	   heat_parallel(heatmap,n_elem,heatmap_tmp);\n\n \
  	   heatmap_tmp[n_elem-1] = 0.0;\n \
  	   heatmap_tmp[0] = 0.0;\n\n \
  	   memcpy((void*)heatmap, (void*)heatmap_tmp, n_elem * sizeof(float));\n \
}\n\n");

  //  						  m_rewritingElementsVector[i]->m_filesContent[CPU]->append(aux+"\n");
    					  }

    				  }

    			  }



      }

  void processMainFunction(Decl* D)
    {
  	  	  Symbol* s;



  		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
  	  	  DeclarationName name = nameInfo.getName();



  			  if(name.getAsString().compare("main") == 0)
  			  {


//				  string str;
//				  llvm::raw_string_ostream rso(str);
//
//
//				  D->print(rso, PrintingPolicy(Context->getLangOpts()));
//				  string aux = rso.str();
  				  for(int i=0;i<m_polcaRules->v.size();i++)
  				  {
  					  if(i==0)
  					  {
  						m_rewritingElementsVector[i]->m_filesContent[CPU]->append("\
int main(void) {\n \
	int i;\n \
	float *heatmap,*heatmap_tmp;\n\n \
	init(&heatmap,&heatmap_tmp);\n\n\n \
	for(i=0; i<n_iter; i++)\n \
	{\n \
		heatspread(heatmap,heatmap_tmp);\n \
	}\n\n \
	printCells(heatmap, n_iter);\n \
	free(heatmap);\n \
	free(heatmap_tmp);\n\n \
	return 0;\n \
}\n\n");

//				  	  	  m_rewritingElementsVector[i]->m_filesContent[CPU]->append(aux+"\n");
  					  }
  					  if(i==1)
  					  {
    					m_rewritingElementsVector[i]->m_filesContent[CPU]->append("\
int main(void) {\n \
	int i;\n \
	float *heatmap,*heatmap_tmp;\n\n \
	init(&heatmap,&heatmap_tmp);\n\n\n \
	for(i=0; i<n_iter; i++)\n \
	{\n \
		heatspread(heatmap,heatmap_tmp);\n \
	}\n\n \
	printCells(heatmap, n_iter);\n \
	free(heatmap);\n \
	free(heatmap_tmp);\n\n \
	return 0;\n \
}\n\n");

//  						  m_rewritingElementsVector[i]->m_filesContent[CPU]->append(aux+"\n");
  					  }

  				  }

  			  }



    }

  bool VisitDecl(Decl* D)
  {
	  PolcaASTVisitor::VisitDecl(D);


	  if(isa<FunctionDecl>(D))
	  {
		  DeclarationNameInfo nameInfo = ((FunctionDecl*)D)->getNameInfo();
	  	  DeclarationName name = nameInfo.getName();

	  	  if( m_sourceManager->isInMainFile(D->getSourceRange().getBegin()) )
	  	  {
	  		processKernelFunction(D);
	  		processInitFunction(D);
	  		processHeatspreadFunction(D);
	  		processPrintCellsFunction(D);
	  		processMainFunction(D);
	  	  }

	  }

	  return true;
  }

//  std::vector<Rewriter*> getRewriters(){
//
//	  return m_rewritersVector;
//  }

  void createOutputStreams()
  {
	  raw_ostream *OS;
	  char buffer[255];


	  for(int i=0;i<m_polcaRules->v.size();i++)
	  {

		  if(i<2)
		  {
		  	  sprintf(buffer, "./output/maxj/rule_%s/heatmapKernel", m_polcaRulesNames[i]);
			  OS = m_compilerInstance->createDefaultOutputFile(false, buffer, "java");
			  m_rewritingElementsVector[i]->m_outStreamsVector.push_back(OS);

			  sprintf(buffer, "./output/maxj/rule_%s/heatmapManager", m_polcaRulesNames[i]);
			  OS = m_compilerInstance->createDefaultOutputFile(false, buffer, "java");
			  m_rewritingElementsVector[i]->m_outStreamsVector.push_back(OS);

			  sprintf(buffer, "./output/maxj/rule_%s/heatmapCPUCode", m_polcaRulesNames[i]);
			  OS = m_compilerInstance->createDefaultOutputFile(false, buffer, "c");
			  m_rewritingElementsVector[i]->m_outStreamsVector.push_back(OS);
		  }
	  }

  }

  void dumpOutputStreams()
  {
	    raw_ostream *OS;
	    std::string* str;

		createOutputStreams();

		  for(int i=0;i<m_polcaRules->v.size();i++)
		  {

			  if(i<2)
			  {

				OS = m_rewritingElementsVector[i]->m_outStreamsVector[KERNEL];
				str = m_rewritingElementsVector[i]->m_filesContent[KERNEL];
				*(OS) << *(str);

				OS = m_rewritingElementsVector[i]->m_outStreamsVector[MANAGER];
				str = m_rewritingElementsVector[i]->m_filesContent[MANAGER];
				*(OS) << *(str);

				OS = m_rewritingElementsVector[i]->m_outStreamsVector[CPU];
				str = m_rewritingElementsVector[i]->m_filesContent[CPU];
				*(OS) << *(str);
			  }
		  }

  }

protected:
  std::string 			m_kernelContent;
  std::string 			m_managerContent;


  SourceLocation 		m_initOutputSourceLocation;
};

////////////////////////////////////////////////////////////////

#endif
