#include "PolcaPragmaTable.h"

PolcaPragmaTable::PolcaPragmaTable()
{}
	
PolcaPragmaTable::~PolcaPragmaTable()
{}
	
vector<PolcaPragma*> PolcaPragmaTable::getPragmaTable()
{
	return m_pragmaTable;
}

	
void PolcaPragmaTable::insertPolcaPragma(PolcaPragma *pragma)
{
	m_pragmaTable.push_back(pragma);
}

PolcaPragma* PolcaPragmaTable::findPolcaPragmaById(string pragmaId)
{
	PolcaPragma* pp = NULL;
	vector<PolcaPragma*>::iterator it;

	for(it=m_pragmaTable.begin();it!=m_pragmaTable.end();it++)
	{
		pp = *(it);

		if(pp->getPragmaId().compare(pragmaId) == 0)
			break;
	}

	return pp;
}



PolcaPragma* PolcaPragmaTable::findPolcaPragmaByDirective(string pragmaDirective)
{
	PolcaPragma* pp = NULL;
	vector<PolcaPragma*>::iterator it;

	for(it=m_pragmaTable.begin();it!=m_pragmaTable.end();it++)
	{
		pp = *(it);

		if(pp->getPragmaDirective().compare(pragmaDirective) == 0)
			break;
	}

	return pp;
}
