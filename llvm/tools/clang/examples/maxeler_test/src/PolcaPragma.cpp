#include "PolcaPragma.h"

PolcaPragma::PolcaPragma()
{
	m_associated = false;
}
	
PolcaPragma::~PolcaPragma(){}

void PolcaPragma::setPragmaContent(std::vector<string> content){
		m_pragmaContent = content;
		generatePragmaSpecification();
	}
	
void PolcaPragma::setPragmaContentWithChild(std::vector<string> content, PolcaPragma* pp){
		m_pragmaChild = pp;
		setPragmaContent(content);
	}

std::vector<string> PolcaPragma::getPragmaContent(){
	return m_pragmaContent;
}
	
string PolcaPragma::getPragmaContentStr(){
	string str;

	std::vector<string>::iterator it;
	for(it = m_pragmaContent.begin(); it != m_pragmaContent.end(); ++it) {
		str.append(*(it)+" ");
//		llvm::outs() << s << " ";
	}
//	llvm::outs() << "\n";
	return str;
}

void PolcaPragma::setPragmaSrcLocation(SourceLocation loc){
	m_pragmaSrcLocation = loc;
	}
	
SourceLocation PolcaPragma::getPragmaSrcLocation(){
	return m_pragmaSrcLocation;
}

void PolcaPragma::setNextStmtSrcLocation(SourceLocation loc){
	m_nextStmtSrcLocation = loc;
	}
	
SourceLocation PolcaPragma::getNextStmtSrcLocation(){
	return m_nextStmtSrcLocation;
}

void PolcaPragma::setPragmaId(string id){
	m_pragmaId = id;
}

string PolcaPragma::getPragmaId(){
	return m_pragmaId;
}

void PolcaPragma::setPragmaDirective(string id){
	m_pragmaDirective = id;
}

string PolcaPragma::getPragmaDirective(){
	return m_pragmaDirective;
}

bool PolcaPragma::isAssociated(){
	return m_associated;
}

void PolcaPragma::setAssociated(bool value){
	m_associated = value;
}

//pugi::xml_document PolcaPragma::getPragmaMXLSpecification()
//{
//	return m_pragmaXMLSpecification;
//}

void PolcaPragma::generatePragmaSpecification(){
	string s;

	std::vector<string>::iterator it;

	it = m_pragmaContent.begin();
	s = *(it);

	if( (s.compare("zipWith3") == 0) || (s.compare("itn") == 0) ){
		parsePragmaContent();
	}

}

void PolcaPragma::parsePragmaContent(){
	string currentStr,prevStr,cumulativeStr;
	pugi::xml_node currentNode,rootNode,currentSlideNode;
	std::vector<string>::iterator it;

	int argOrder = 1;
	ParsingState state = nil;

	for(it = m_pragmaContent.begin(); it != m_pragmaContent.end(); ++it) {
		currentStr = *(it);

		switch(state){
			case nil:
				currentNode = m_pragmaXMLSpecification.append_child("spec");
				currentNode.append_attribute("id") = m_pragmaDirective.c_str();

				currentNode = currentNode.append_child("call");
				currentNode.append_attribute("function") = currentStr.c_str();
				rootNode = currentNode;

//				m_pragmaSpecification.append("<call function=");
//				m_pragmaSpecification.append(currentStr);
//				m_pragmaSpecification.append(">\n");
				if(currentStr.compare("zipWith3") == 0)
					state = init;
				if(currentStr.compare("itn") == 0)
					state = init;
				break;
			case init:
				state = argument;
				currentNode = rootNode.append_child("arg");
				currentNode.append_attribute("ord") = to_string(argOrder).c_str();

				if(prevStr.compare("itn") == 0)
					currentNode = currentNode.append_copy(m_pragmaChild->m_pragmaXMLSpecification.first_child());
				else{

					currentNode = currentNode.append_child("block");
					currentNode.append_attribute("name") = currentStr.c_str();
				}

				argOrder++;
				break;
			case argument:
				if( (currentStr.compare("[")==0) ){

					currentNode = currentNode.append_child("call");
					currentSlideNode = currentNode;
					currentNode.append_attribute("function") = "slide";

					currentNode = currentNode.append_child("arg");
					currentNode.append_attribute("ord") = "1";

					currentNode = currentNode.append_child("var");
					currentNode.append_attribute("name") = prevStr.c_str();


//					m_pragmaSpecification.append("\t\t<call function='slide'>\n");
//					m_pragmaSpecification.append("\t\t\t<arg ord='1'><var name = '"+prevStr+"'></var></arg>\n");
					state = offset;

					break;
				}
				if( (currentStr.compare(":")==0) ){
					if( (prevStr.compare("]")!=0) ){
						currentNode = currentNode.append_child("var");
						currentNode.append_attribute("name") = prevStr.c_str();
					}

					state = argumentType;
//					m_pragmaSpecification.append(cumulativeStr);
					break;
				}
				if( (currentStr.compare("]")==0) )
					break;
				if( (currentStr.compare("\n")==0) )
					break;

				currentNode = rootNode.append_child("arg");
				currentNode.append_attribute("ord") = to_string(argOrder).c_str();
				argOrder++;


				break;
			case offset:
				if( (currentStr.compare("]")==0) ){

					// We need to extract the offset from annotation, so for the following cases:
					// [i+1] -> +1 (offset equals to +1)
					// [i-1] -> -1 (offset equals to -1)
					// [i]   ->  0 (offset equals to 0)
					size_t pos;
					pos = cumulativeStr.find_first_of('+');
					if(pos == string::npos){
						pos = cumulativeStr.find_first_of('-');
					}

					if(pos != string::npos)
						cumulativeStr.replace(0,pos,"");
					else
						cumulativeStr = "0";


//					if(cumulativeStr.compare("") == 0)

					currentNode = currentSlideNode.append_child("arg");
					currentNode.append_attribute("ord") = "2";

					currentNode = currentNode.append_child("cons");
					currentNode.append_attribute("value") = cumulativeStr.c_str();


//					m_pragmaSpecification.append("\t\t\t<arg ord='2'>"+cumulativeStr+"</arg>\n");
//					m_pragmaSpecification.append("\t\t</call>\n");
//					m_pragmaSpecification.append("\t</arg>\n");
					cumulativeStr = "";
					state = argument;
					break;
				}

				cumulativeStr.append(currentStr);
				break;
			case argumentType:
				state = argument;
		}

		prevStr = *(it);



	}

//	m_pragmaSpecification.append("</call>\n");
//	llvm::outs() << m_pragmaSpecification << "\n";


	m_pragmaXMLSpecification.print(std::cout);
}
