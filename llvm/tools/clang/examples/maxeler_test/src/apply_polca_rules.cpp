#ifndef _APPLYPOLCARULES_
#define _APPLYPOLCARULES_

#define PUGIXML_HEADER_ONLY


#include "../pugixml/pugixml.hpp"
#include "../pugixml/pugixml.cpp"
#include "../pugixml/pugixml.hpp"


#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

//#define DEBUG



struct rewriting {
	const pugi::char_t* pattern;
	const pugi::char_t* value;
    const pugi::char_t* attribute;
};

class PolcaRules {

public:

//std::vector<pugi::xml_document> d;
std::vector<pugi::xml_node*> v;
std::vector<std::string> files;
pugi::xml_document *doc1;

int num_reglas;

PolcaRules(){}

~PolcaRules(){}

void dummyFunction()
{
	char buffer[30];

	num_reglas = 4;
	int nodeId = 1;
	for(int i=0;i<num_reglas;i++)
	{
	doc1 = new pugi::xml_document();

	sprintf(buffer, "%d", nodeId);
	pugi::xml_node node1 = doc1->append_child(buffer);
	nodeId++;

	sprintf(buffer, "%d", nodeId);
	pugi::xml_node node2 = doc1->append_child(buffer);
	nodeId++;

	v.push_back(doc1);

	}

	//return v;
}


void apply_rules_doc(char* file_rules,pugi::xml_document *specification)
{
    std::string file_name = "output/rules_files/original_specification.xml";
    //specification->print(std::cout);
    specification->save_file(file_name.c_str());
    apply_rules(file_rules,file_name.c_str());
}

void apply_rules(char* file_rules,const char* file_input)
{

    //std::vector<std::string> returned;
    //std::vector<std::string>* myvector;
    //returned = new std::vector<pugi::xml_node>();

    // Check the number of parameters
    // if (argc < 3) {
    //     // Tell the user how to run the program
    //     std::cerr << "Usage: " << argv[0] << " RULES_FILE SPECIFICATION_FILE" << std::endl;
    //     return 1;
    // }



    //read the rules
    pugi::xml_document rules;
    if (!rules.load_file(file_rules)) {std::cout << "Can't read "<< file_rules <<" document.\n";return;}

    pugi::xpath_node_set node_rules = rules.select_nodes("/rule");   

    for (pugi::xpath_node_set::const_iterator it_rule = node_rules.begin(); it_rule != node_rules.end(); ++it_rule)
    {
        pugi::xml_node rule_to_write = (*it_rule).node();

        //store and load file to handle a problem with xpath selection over partial nodes
        std::ofstream temp_rule_file ("output/rules_files/temp_rule.xml");
        rule_to_write.print(temp_rule_file, "", pugi::format_declaration );
        temp_rule_file.close();
        pugi::xml_document rule;
        if (!rule.load_file("output/rules_files/temp_rule.xml")) {std::cout << "Can't read temp_rule.xml document.\n";return;}

#ifdef DEBUG
        std::cout << "CURRENT RULE\n";
        rule.print(std::cout);
#endif 

        std::string rule_id = rule.first_child().attribute("id").value();

        //search and store the antecedent query
        pugi::xpath_node antecedent = rule.select_single_node("//antecedent");
        const pugi::char_t* antecedent_query = antecedent.node().child_value();
#ifdef DEBUG
        std::cout << "ANTECEDENT\n";
        std::cout << antecedent_query << "\n";
#endif

        //read the specification
        pugi::xml_document specification;
        if (!specification.load_file(file_input)) {std::cout << "Can't read "<< file_input << " document.\n";return;}
        

        //search nodes matching the antecedent in the specification
        pugi::xpath_node_set matches = specification.select_nodes(antecedent_query);

        //search rewritings in the rule
        pugi::xpath_node_set rewritings = rule.select_nodes("//rewriting");
        int num_rewritings = rewritings.size();
        rewriting rews[num_rewritings]; 

#ifdef DEBUG
        std::cout << "REWRITINGS\n\n";
#endif
        int i = 0;
        //store each rewriting in the list
        for (pugi::xpath_node_set::const_iterator it = rewritings.begin(); it != rewritings.end(); ++it)
        {
            pugi::xpath_node node = *it;
            pugi::xml_node pattern_node = node.node().first_child();
            pugi::xml_node value_node = pattern_node.next_sibling();
            rews[i].pattern = pattern_node.child_value();
            rews[i].value = value_node.child_value();
            pugi::xml_node attribute_node = value_node.next_sibling();
            if(attribute_node)
                rews[i].attribute = attribute_node.child_value();
            else 
                rews[i].attribute = "";
#ifdef DEBUG
            if (rews[i].attribute != "")
                std::cout << "Rewriting " << i <<"\nPattern: " << rews[i].pattern << "\nValue: " << rews[i].value << "\nAttribute: " << rews[i].attribute << "\n\n";
            else
                std::cout << "Rewriting " << i <<"\nPattern: " << rews[i].pattern << "\nValue: " << rews[i].value << "\n\n";
#endif   
            i++;
        }

#ifdef DEBUG
        std::cout << "Total rewritings: " << num_rewritings << "\n";
        std::cout << "\nEND_REWRITINGS\n\n";
#endif

        //search and store the rule's schema. Should be written into a file and read each iteration in order to avoid problems when substitutions are done.
        pugi::xml_node original_schema = rule.select_single_node("//schema/*").node();
        std::ofstream schema_file ("output/rules_files/schema.xml");
        original_schema.print(schema_file, "", pugi::format_declaration );
        schema_file.close();
#ifdef DEBUG
        std::cout << "SCHEMA\n";
        original_schema.print(std::cout);
#endif

        //loop over all the those nodes matching the antecedent
        for (pugi::xpath_node_set::const_iterator it = matches.begin(); it != matches.end(); ++it)
        {
            pugi::xml_node current_node = (*it).node();


            //PROBLEM: current_node is not pointing to the matched node, instead it points to the whole document
            //SOLVED: Create a temporal file create a new independent node reading this file
            //Additionally, we have needed some modification in the library (hpp and cpp). Search for Tamarit in order to find them.
            std::ofstream temp_file ("output/rules_files/temp.xml");
            current_node.print(temp_file, "", pugi::format_declaration );
            temp_file.close();
            pugi::xml_document temp_current_node;
            if (!temp_current_node.load_file("output/rules_files/temp.xml")) {std::cout << "Can't read temp.xml document.\n";return;}

#ifdef DEBUG
                std::cout << "MATCHING SPECIFICATION NODE\n";
                current_node.print(std::cout);
#endif

            //read the schema file
            pugi::xml_document schema;
            if (!schema.load_file("output/rules_files/schema.xml")) {std::cout << "Can't read schema.xml document.\n";return;}

            //Loop over the rewriting rules in the rule
            for(int num_rew = 0; num_rew < num_rewritings; num_rew++)
            {
                //Search the node value (in the new document created to solve the problem with function)
                pugi::xml_node node_value = temp_current_node.select_single_node(rews[num_rew].value).node();
#ifdef DEBUG
                std::cout << "Perform rewriting " << num_rew << "\n";
                node_value.print(std::cout);
#endif
                //Loop over those nodes matching the pattern
                pugi::xpath_node_set nodes_pattern = schema.select_nodes(rews[num_rew].pattern);
                for (pugi::xpath_node_set::const_iterator it_pat = nodes_pattern.begin(); it_pat != nodes_pattern.end(); ++it_pat)
                {
                    pugi::xml_node node_pattern = (*it_pat).node();
#ifdef DEBUG
                    node_pattern.print(std::cout);
#endif
                    if(rews[num_rew].attribute == "")
                    {
                        //Substitute pattern node by value node
                        node_pattern.parent().insert_copy_after(node_value, node_pattern);
                        node_pattern.parent().remove_child(node_pattern);
                    }
                    else
                    {
                        node_pattern.append_copy(node_value.attribute(rews[num_rew].attribute));
                    }
                }
            }
            current_node.parent().insert_copy_before(schema.first_child(), current_node);
            current_node.parent().remove_child(current_node);
#ifdef DEBUG
            std::cout << "\nREWRITTEN SCHEMA\n";
            schema.print(std::cout);
            std::cout << "\nREWRITTEN SPECIFICATION\n";
            specification.print(std::cout);
#endif

            //we store the result in a file named polca_consequent_rule_IDRULE
            std::string save_file_name = "output/rules_files/polca_consequent_rule_" + rule_id + ".xml";
            specification.save_file(save_file_name.c_str());

            pugi::xml_document* new_specification = new pugi::xml_document();
            if (!new_specification->load_file(save_file_name.c_str())) {std::cout << "Can't read consequent document.\n";return;}

            // pugi::xml_document* pointer_to_new_specification = &new_specification;
            // pointer_to_new_specification.print(std::cout);

            v.push_back(new_specification);



            // std:string prova = "ola";

            // myvector->push_back(prova);

            //files.push_back(save_file_name);
        }


    }

    // for(int j=0;j < myvector->size();j++)
    // {
    //  std::string node = myvector->at(j);
    //  std::cout<<node;
    // }

    // int cont = 0;
    // for(int j=0;j < v.size();j++)
    // {
    //     std::cout << "Regla "<< cont << "\n";
    //     pugi::xml_node* node = v.at(j);
    //     node->print(std::cout);
    //     cont++;
    // }

    //return returned;
}

};
#endif
