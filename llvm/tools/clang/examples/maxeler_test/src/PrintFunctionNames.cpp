//===- PrintFunctionNames.cpp ---------------------------------------------===//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//
//===----------------------------------------------------------------------===//
//
// Example clang plugin which simply prints the names of all the top-level decls
// in the input file.
//
//===----------------------------------------------------------------------===//

#define PUGIXML_HEADER_ONLY

#include "../pugixml/pugixml.hpp"
#include "../pugixml/pugixml.cpp"
#include "../pugixml/pugixml.hpp"

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Lex/Pragma.h"
#include "clang/Lex/Preprocessor.h"

#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"

#include "clang/Sema/Sema.h"
#include "clang/Sema/SemaConsumer.h"
#include "clang/Sema/ExternalSemaSource.h"
//#include "clang/AST/OperationKinds.h"
#include "clang/Parse/ParseAST.h"

//#include "sema.h"
#include "PolcaPragma.h"
#include "SymbolTable.h"
#include "PolcaPragmaTable.h"

#include "PolcaASTVisitorMaxj.cpp"
#include "PolcaASTVisitorMpi.cpp"
#include "PolcaASTVisitorOpenMP.cpp"
#include "apply_polca_rules.cpp"

using namespace std;
using namespace clang;
using namespace sema;

typedef enum{
	none = 0,
	openmp,
	mpi,
	maxj,
}ArchType;


ArchType					archType;

namespace {


class PolcaPragmaHandler : public PragmaHandler{
public:
	explicit PolcaPragmaHandler(PolcaPragmaTable *table) {
		  printf("\n\n\n");
		  printf("/*******************************************************/\n");
		  printf("/                 Preprocessing input code              /\n");
		  printf("/*******************************************************/\n");
		  printf("\n");

		  m_pragmaTable = table;

	}

	explicit PolcaPragmaHandler(StringRef name) : PragmaHandler(name) {}

	StringRef getName()
	{
		return "polca";
	}

	void HandlePragma(Preprocessor &PP, PragmaIntroducerKind Introducer, Token &FirstToken)
	{
		//IdentifierInfo* idInfo = FirstToken.getIdentifierInfo();

		printf("Pragma POLCA directive found!!\n");//,idInfo->getNameStart(),FirstToken.getLength());

		//char* literalData = FirstToken.getLiteralData();
		SourceLocation initialSrcLocation = FirstToken.getLocation().getLocWithOffset(-1);
		std::string S;
		Token Result;
//		S = FirstToken.getLocation().getLocWithOffset(-1).printToString(PP.getSourceManager()); //PP.getSpelling(FirstToken);
//		printf("\t%s\n",S.c_str());
//
//		PP.Lex(Result);
//		S = PP.getSpelling(Result);
//		printf("\t%s\n",S.c_str());

//		SourceLocation sl = Result.getLocation();
//		S = sl.printToString(PP.getSourceManager());
//		printf("\t%s\n",S.c_str());
	
		std::vector<string> pragmaContent;
		std::string endPragmaTok("\n");
		
		int iteration = 0;
		string pragmaDirective, pragmaId;
		while(S.compare(endPragmaTok) != 0)
		{
//			pragmaContent.append(" "+S);
			PP.Lex(Result);
			S = PP.getSpelling(Result);
			pragmaContent.push_back(S);

			if(iteration==0)
				pragmaDirective = S;
			if(iteration==1)
				pragmaId = S;

			iteration++;
		}
		
		SourceLocation pragmaFollowingLocation = Result.getLocation().getLocWithOffset(+1);
		S = pragmaFollowingLocation.printToString(PP.getSourceManager());
		printf("\tPragmaLocation:   %s\n",S.c_str());
		printf("\tPragma directive: %s\n",pragmaDirective.c_str());
		printf("\tPragma Id:        %s\n",pragmaId.c_str());

		PolcaPragma *pp = new PolcaPragma();
		pp->setPragmaId(pragmaId);
		pp->setPragmaDirective(pragmaDirective);
		if(pragmaDirective.compare("itn")==0){
			string s("zipWith3");
			pp->setPragmaContentWithChild(pragmaContent,m_pragmaTable->findPolcaPragmaByDirective(s));
		}else
			pp->setPragmaContent(pragmaContent);
		pp->setPragmaSrcLocation(initialSrcLocation);
		pp->setNextStmtSrcLocation(pragmaFollowingLocation);

		m_pragmaTable->insertPolcaPragma(pp);
		llvm::outs() << "\t" << pp->getPragmaContentStr() << "\n";

	}

private:
	PolcaPragmaTable *m_pragmaTable;

};

// Adapted from Douglas Gregor's presentation (slide 8):
// http://llvm.org/devmtg/2011-11/Gregor_ExtendingClang.pdf
//class PolcaPPHandler : public PPCallbacks {
//  //SourceManager& SM;
// public:
//
//  explicit PolcaPPHandler(SourceManager& sm) {} //: SM(sm) {}
//
//  void PragmaDirective(SourceLocation Loc, PragmaIntroducerKind Introducer)
//  {
//	  printf("Pragma directive found!!\n");
//  }
//
//  void PragmaMessage(SourceLocation Loc, StringRef Namespace, PragmaMessageKind Kind, StringRef Str)
//  {
//	  printf("PragmaMessage directive found!!\n");
//  }
//
//};

////////////////////////////////////////////////////////////////

class FindNamedClassConsumer : public clang::ASTConsumer {
public:
//  explicit FindNamedClassConsumer(CompilerInstance *CI, Rewriter *rewriter, llvm::StringRef inFile)
  explicit FindNamedClassConsumer(CompilerInstance *CI, PolcaPragmaTable *pragmaTable, SymbolTable *symbolTable, PolcaRules* polcaRules, llvm::StringRef inFile)
  {
//	  m_visitor = new PolcaASTVisitor(CI, &(CI->getASTContext()), &(CI->getSourceManager()), pragmaTable, symbolTable, inFile );

	  switch(archType)
	  {
	  case maxj:
		  m_visitor = new PolcaASTVisitorMaxj(CI, &(CI->getASTContext()), &(CI->getSourceManager()), pragmaTable, symbolTable, polcaRules, inFile );
		  break;
	  case openmp:
		  m_visitor = new PolcaASTVisitorOpenMP(CI, &(CI->getASTContext()), &(CI->getSourceManager()), pragmaTable, symbolTable, polcaRules, inFile );
		  break;
	  case mpi:
		  m_visitor = new PolcaASTVisitorMpi(CI, &(CI->getASTContext()), &(CI->getSourceManager()), pragmaTable, symbolTable, polcaRules, inFile );
		  break;
	  default:
		  break;
	  }

  }

  ~FindNamedClassConsumer(){}

  void HandleTranslationUnit(clang::ASTContext &Context) {
    m_visitor->TraverseDecl(Context.getTranslationUnitDecl());
  }

//  std::vector<Rewriter*> getRewriters(){
//	  return m_visitor->getRewriters();
//  }

  void dumpOutputStreams()
  {
	  m_visitor->dumpOutputStreams();
  }

private:
  PolcaASTVisitor *m_visitor;
};

////////////////////////////////////////////////////////////////

class PrintFunctionNamesAction : public PluginASTAction {
//public:


protected:
  ASTConsumer *CreateASTConsumer(CompilerInstance &CI, llvm::StringRef inFile) {
	llvm::outs() << "Input file: " << inFile << "\n\n\n";

	m_polcaRules = new PolcaRules();

	//m_pragmaTable->findPolcaPragmaByDirective("itn")->m_pragmaXMLSpecification;
	//m_polcaRules->apply_rules("input/polca_rules.xml", "input/polca.xml");

//	std::vector<pugi::xml_node*>::iterator it;
//	int i=0;
//	for(it=m_polcaRules->v.begin();it!=m_polcaRules->v.end();it++)
//	{
//		pugi::xml_node *d = *(it);
//		std::cout << "Document " << i << "\n";
//		d->print(std::cout);
//		i++;
//	}

	m_symbolTable = new SymbolTable();
	m_astConsumer = new FindNamedClassConsumer(&CI,m_pragmaTable,m_symbolTable,m_polcaRules, inFile);

	return m_astConsumer;
  }

  // This is where the action happens. By modifying the CompilerInstance, you
  // can access most of the interesting stuff that Clang does.
  bool BeginSourceFileAction(CompilerInstance& CI, llvm::StringRef) {
	m_pragmaTable = new PolcaPragmaTable();

    Preprocessor &PP = CI.getPreprocessor();
//    PP.addPPCallbacks(new PolcaPPHandler(CI.getSourceManager()));
    PP.AddPragmaHandler(new PolcaPragmaHandler(m_pragmaTable));

    return true;
  }

  void EndSourceFileAction(){
	CompilerInstance &CI = getCompilerInstance();

//	printf("\n\n\n");
//	symbolTable.prettyPrintTable(&(CI.getSourceManager()));

	m_astConsumer->dumpOutputStreams();

  }


//  void ExecuteAction(){
//	CompilerInstance &CI = getCompilerInstance();
//
//	//	    CodeCompleteConsumer *CompletionConsumer = 0;
//	//	    if (CI.hasCodeCompletionConsumer())
//	//	    	CompletionConsumer = &CI.getCodeCompletionConsumer();
//	//
//	//	    CI.createSema(getTranslationUnitKind(),  CompletionConsumer);
//
//	PolcaSema *PC = new PolcaSema(CI.getPreprocessor(),
//			CI.getASTContext(),
//			CI.getASTConsumer());
//
//	CI.setSema(PC);
//
////
////	clang::ASTConsumer cons = CI.getASTConsumer();
////	if (clang::SemaConsumer *SC = dyn_cast<clang::SemaConsumer>(&cons)) {
////		SC->InitializeSema(*(PC));
////	}
//
////	if (PolcaSema *polcaS = (PolcaSema*) &CI.getSema() ) {
////		polcaS->printMsg();
////	}
//
//
////	if (ExternalASTSource *External = CI.getASTContext().getExternalSource()) {
////		if(ExternalSemaSource *ExternalSema = dyn_cast<ExternalSemaSource>(External))
////			ExternalSema->InitializeSema(*(PC));
////		External->StartTranslationUnit(&CI.getASTConsumer());
////	}
//
//	ParseAST(CI.getSema(), CI.getFrontendOpts().ShowStats,
//		CI.getFrontendOpts().SkipFunctionBodies);
//  }

  bool ParseArgs(const CompilerInstance &CI,
                 const std::vector<std::string>& args) {
    archType = none;

    for (unsigned i = 0, e = args.size(); i != e; ++i) {
      llvm::errs() << "PrintFunctionNames arg = " << args[i] << "\n";

      // Example error handling.
      if (args[i] == "-an-error") {
        DiagnosticsEngine &D = CI.getDiagnostics();
        unsigned DiagID = D.getCustomDiagID(DiagnosticsEngine::Error,
                                            "invalid argument '%0'");
        D.Report(DiagID) << args[i];
        return false;
      }

      if(args[i] == "--target=maxj")
    	  archType = maxj;

      if(args[i] == "--target=mpi")
    	  archType = mpi;

      if(args[i] == "--target=openmp")
    	  archType = openmp;

    }

    //llvm::outs() << "pasa1\n";

    
//	std::vector<std::string> consequents_file_names = apply_rules("input/polca_rules.xml", "input/polca.xml");

	// std::vector<std::string>::iterator it;

 //  	for(it=consequents_file_names.begin();it!=consequents_file_names.end();it++)
 //  	{
 //  		std::string consequent_file_name = *(it);
 //  		llvm::outs() << consequent_file_name << "\n";
 //  		pugi::xml_document consequent;
 //        if (!consequent.load_file(consequent_file_name.c_str())) {std::cout << "Can't read "<< consequent_file_name <<" document.\n";return false;}
 //        consequent.print(std::cout);
          
 //  	}	

//	llvm::outs() << "Number of consequents: " << consequents_file_names.size() << "\n";

  	// for(int j=0;j < value->size();j++)
  	// {
  	// 	pugi::xml_node node = value->at(j);
  	// 	//node.print(std::cout);
  	// }

    //llvm::outs() << "pasa2 -> " << value->size() << "\n";


    if(archType == none){
        DiagnosticsEngine &D = CI.getDiagnostics();
        unsigned DiagID = D.getCustomDiagID(DiagnosticsEngine::Error,
                                            "The architecture type must be provided. Available options:\n\n\t--target=maxj\n\t--target=mpi\n\t--target=openmp");
        D.Report(DiagID);
        return false;
    }

    if (args.size() && args[0] == "help")
      PrintHelp(llvm::errs());

    return true;
  }
  void PrintHelp(llvm::raw_ostream& ros) {
    ros << "Help for PrintFunctionNames plugin goes here\n";
  }

private:
//  Rewriter *m_rewriter;

  PolcaPragmaTable			*m_pragmaTable;
  SymbolTable				*m_symbolTable;
  FindNamedClassConsumer 	*m_astConsumer;
  std::vector<raw_ostream*>  m_outStreamsVector;

  PolcaRules 				*m_polcaRules;

};

}

static FrontendPluginRegistry::Add<PrintFunctionNamesAction>
X("print-fns", "print function names");
