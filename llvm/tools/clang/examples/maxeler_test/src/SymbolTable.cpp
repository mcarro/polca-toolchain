#include "SymbolTable.h"

SymbolTable::SymbolTable(){}
	
SymbolTable::~SymbolTable(){}

void SymbolTable::insertSymbol(Symbol* s)
{
	m_symbolTable.push_back(s);
}

Symbol* SymbolTable::findSymbol(string symbolId)
{
	Symbol* s = NULL;

	std::vector<Symbol*>::iterator it;

	for(it = m_symbolTable.begin(); it != m_symbolTable.end(); ++it) {
		if((*it)->id.compare(symbolId) == 0)
		{
			s = *it;
			break;
		}
	}

//	printf("Error: symbol %s not found!!!\n",symbolId.c_str());
	return s;
}

void SymbolTable::prettyPrintTable(SourceManager* sourceManager)
{
	  printf("\n\n\n");
	  printf("/*******************************************************/\n");
	  printf("/                 Symbol Table content                  /\n");
	  printf("/*******************************************************/\n");
	  printf("\n");
//	  printf("Symbol Table content: \n\n");

	std::vector<Symbol*>::iterator it;
	for(it = m_symbolTable.begin(); it != m_symbolTable.end(); ++it) {
		llvm::outs() << "--------------------------------------------------------------------------" << "\n";
		llvm::outs() << "Symbol Id: " <<  (*it)->id << "\t\t\t( type: " << (*it)->type << ")\n\n";
		(*it)->rootASTNode->dump(llvm::outs(),*sourceManager);
		llvm::outs() << "--------------------------------------------------------------------------" << "\n";
		llvm::outs() << "\n\n";
	}
	llvm::outs() << "\n\n";

}
