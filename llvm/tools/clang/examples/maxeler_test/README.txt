This is a simple example demonstrating how to use clang's facility for
providing AST consumers using a plugin.

---------------------------- LLVM ----------------------------

In order to compile this plugin, LLVM and Clang should be built first. Instructions about how to do it can be found at: http://clang.llvm.org/get_started.html

----
Note that LLVM, Clang, extra Clang Tools and Compiler-rt are already downloaded and contained in this repository so you can focus on LLVM and Clang compilation instructions.

--------------------------- Plugin ---------------------------

Before compiling the plugin comment the following line in the file $LLVM_ROOT/Makefile.rules

 CXX.Flags += -fno-exceptions

Build the plugin by running `make` in this directory.

Once the plugin is built, you can run it using (these paths are dependent on the actual compilation target (Release or Debug) and build path. In this case "Debug+Asserts" is on the LLVM root directory):
--
Linux:
$ ../../../../Debug+Asserts/bin/clang -cc1 -load ../../../../Debug+Asserts/lib/PrintFunctionNames.so -plugin print-fns input/test.c

Mac:
$ ../../../../Debug+Asserts/bin/clang -cc1 -load ../../../../Debug+Asserts/lib/PrintFunctionNames.dylib -plugin print-fns input/test.c
