/* printf example */
#include <stdio.h>

int main()
{
   printf ("Test program: %d %f \n", 1,3.0);

#pragma polca maxj_stream C
   int C[5];

#pragma polca maxj_stream B
   int B[5];

   int count;
	for (count=0; count<5 ; count += 1)
	{
	   B[count] = C[count] + 1;
	   //printf("Increment A[%d] and return onto B[%d]\n",count,count);
	}
	printf("Program finished!!\n");

   return 0;
}
