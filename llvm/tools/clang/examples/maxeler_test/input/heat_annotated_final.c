// 1-dimensional heat dissipation

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int n_iter;
int n_elem;

#define K 0.1

#pragma polca def init heatmap:o
void init(float **heatmap,float **heatmap_tmp) {
  n_elem = 21;
  n_iter = 10;

  *heatmap = malloc(sizeof(float) * n_elem);
  *heatmap_tmp = malloc(sizeof(float) * n_elem);

  int i;
  for(i=0; i<n_elem; i++) {
    (*heatmap)[i] = 0.0;
  }

  (*heatmap)[n_elem/2] = 100.0;
}


#pragma polca kernel g l:i c:i r:i
float g(float l, float c, float r) {
  return c + K * (l + r - 2 * c);
}

  
void heatspread(float *heatmap,float *heatmap_tmp) {
  int i;

#pragma polca zipWith3 G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
  for(i=1; i<n_elem-1; i++)
    {
#pragma polca def G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
      heatmap_tmp[i] = g(heatmap[i-1], heatmap[i], heatmap[i+1]);
    }

#pragma polca def B
  {
    heatmap_tmp[n_elem-1] = 0.0;
    heatmap_tmp[0] = 0.0;
  }

#pragma polca MemCopy heatmap_tmp:i heatmap:i
{
  memcpy((void*)heatmap, (void*)heatmap_tmp, n_elem * sizeof(float));
}
}


void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<n_elem; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}


int main(void) {
  int i;
  float *heatmap,*heatmap_tmp;

#pragma polca def INIT heatmap:o
  init(&heatmap, &heatmap_tmp);
  
#pragma polca itn HEATSPREAD INIT N_ITER heatmap:o
  for(i=0; i<n_iter; i++)
  {
#pragma polca def HEATSPREAD heatmap:i heatmap:o
    heatspread(heatmap, heatmap_tmp);
    //printCells(heatmap, i+1);
  }

  printCells(heatmap, n_iter);
  free(heatmap);
  free(heatmap_tmp);

  return 0;
}
