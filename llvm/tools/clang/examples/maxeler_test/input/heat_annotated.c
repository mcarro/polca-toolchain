// 1-dimensional heat dissipation

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define N_ITER 10
#define K 0.1

int n_elem;


#pragma polca def init heatmap:o
void init(float **heatmap,float **heatmap_tmp) {
  int i;

  *heatmap = malloc(sizeof(float) * n_elem);
  *heatmap_tmp = malloc(sizeof(float) * n_elem);

  for(i=0; i<n_elem; i++) {
    (*heatmap)[i] = 0.0;
  }

  (*heatmap)[10] = 100.0;
}


#pragma polca kernel g l:i c:i r:i
float g(float l, float c, float r) {
  return c + K * (l + r - 2 * c);
}

  
void heatspread(float *heatmap,float *heatmap_tmp) {
  int i;
// Should the user also annotate the G for the zipwith3?
// In that case the toolchain will search for a G block to link with
#pragma polca zipWith3 G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
  for(i=1; i<n_elem-1; i++)
    {
#pragma polca def G heatmap[i-1]:i heatmap[i]:i heatmap[i+1]:i heatmap_tmp[i]:o
      heatmap_tmp[i] = g(heatmap[i-1], heatmap[i], heatmap[i+1]);
    }

#pragma polca def B
  {
    heatmap_tmp[n_elem-1] = 0.0;
    heatmap_tmp[0] = 0.0;
  }

#pragma polca def MemCopy heatmap_tmp:i heatmap:i
  memcpy((void*)heatmap, (void*)heatmap_tmp, n_elem * sizeof(float));
}


void printCells(float *heatmap, int n) {
  int i;

  printf("i %05d:", n);
  for(i=0; i<n_elem; i++) {
    printf(" %10.6f", heatmap[i]);
  }

  printf("\n");
}


int main(void) {
  int i;
  float *heatmap,*heatmap_tmp;
  
  n_elem = 21;
  
#pragma polca itn HEATSPREAD INIT N_ITER heatmap:o
  {

#pragma polca def INIT heatmap:o
    init(&heatmap, &heatmap_tmp);

for(i=0; i<N_ITER; i++)
    {
#pragma polca def HEATSPREAD heatmap:i heatmap:o
      heatspread(heatmap, heatmap_tmp);
      //printCells(heatmap, i+1);
    }
  }

  printCells(heatmap, N_ITER);
  free(heatmap);
  free(heatmap_tmp);

  return 0;
}
