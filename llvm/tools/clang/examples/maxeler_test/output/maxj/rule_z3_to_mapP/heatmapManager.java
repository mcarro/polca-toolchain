 	  package heat;

 	  import com.maxeler.maxcompiler.v2.build.EngineParameters;
 	  import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
 	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.CPUTypes;
 	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.EngineInterface;
 	  import com.maxeler.maxcompiler.v2.managers.engine_interfaces.InterfaceParam;
 	  import com.maxeler.maxcompiler.v2.managers.standard.IOLink.IODestination;
 	  import com.maxeler.maxcompiler.v2.managers.standard.Manager;

 	  class HeatManager {

 		public static void main(String[] args) {
 			Manager manager = new Manager(new EngineParameters(args));
 			Kernel kernel = new HeatKernel(manager.makeKernelParameters());
 			manager.setKernel(kernel);
 			manager.setIO(IOType.ALL_CPU);

 			manager.createSLiCinterface(new EngineInterface("parallel") {
 				{
 				int numPipes = 12;
 				InterfaceParam length = addParam("length", CPUTypes.INT32);
 				InterfaceParam heatmap = addParam("heatmap", CPUTypes.INT32);
 				InterfaceParam heatmap_tmp = addParam("heatmap_tmp", CPUTypes.INT32);
 				ignoreAll(Direction.IN_OUT);

 				setTicks("HeatKernel", length / numPipes);
 				}
 			});
 			manager.build();
 		}
 	  }
 	  