 	  package heat;
 	  import com.maxeler.maxcompiler.v2.kernelcompiler.Kernel;
 	  import com.maxeler.maxcompiler.v2.kernelcompiler.KernelParameters;
 	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.base.DFEVar;
 	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVector;
 	  import com.maxeler.maxcompiler.v2.kernelcompiler.types.composite.DFEVectorType;
 	  class HeatKernel extends Kernel {
 		protected HeatKernel(KernelParameters parameters) {
 			super(parameters);

 			int numPipes = 12;

 			DFEVectorType<DFEVar> vt = new DFEVectorType<DFEVar>( dfeFloat(8,24),numPipes);
 			DFEVector<DFEVar> inStream = io.input("heatmap", vt);

 			DFEVector<DFEVar> l = constant.var( dfeFloat(8,24),0);
 			DFEVector<DFEVar> c = constant.var( dfeFloat(8,24),0);
 			DFEVector<DFEVar> r = constant.var( dfeFloat(8,24),0);

 			l  = stream.offsetStriped(inStream, -1);
 			c  = stream.offsetStriped(inStream, 0);
 			r  = stream.offsetStriped(inStream, +1);

 	  		c =  c + 0.10000000000000001 * (l + r - 2 * c);

 			io.output("heatmap_tmp", c, inStream.getType());
 	  	}
 	  }
