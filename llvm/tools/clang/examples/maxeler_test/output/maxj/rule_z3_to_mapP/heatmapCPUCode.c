 #include <math.h>
 #include <stdio.h>
 #include <stdlib.h>

 #include "Maxfiles.h"
 #include <MaxSLiCInterface.h>

 int n_iter;
 int n_elem;

void init(float **heatmap, float **heatmap_tmp) {
    n_elem = 21;
    n_iter = 10;
    *heatmap = malloc(sizeof(float) * n_elem);
    *heatmap_tmp = malloc(sizeof(float) * n_elem);
    int i;
    for (i = 0; i < n_elem; i++) {
        (*heatmap)[i] = 0.;
    }
    (*heatmap)[n_elem / 2] = 100.;
}


void heatspread(float *heatmap,float *heatmap_tmp) {
 {
   	   heat_parallel(heatmap,n_elem,heatmap_tmp);

   	   heatmap_tmp[n_elem-1] = 0.0;
   	   heatmap_tmp[0] = 0.0;

   	   memcpy((void*)heatmap, (void*)heatmap_tmp, n_elem * sizeof(float));
 }

void printCells(float *heatmap, int n) {
    int i;
    printf("i %05d:", n);
    for (i = 0; i < n_elem; i++) {
        printf(" %10.6f", heatmap[i]);
    }
    printf("\n");
}


int main(void) {
 	int i;
 	float *heatmap,*heatmap_tmp;

 	init(&heatmap,&heatmap_tmp);


 	for(i=0; i<n_iter; i++)
 	{
 		heatspread(heatmap,heatmap_tmp);
 	}

 	printCells(heatmap, n_iter);
 	free(heatmap);
 	free(heatmap_tmp);

 	return 0;
 }

