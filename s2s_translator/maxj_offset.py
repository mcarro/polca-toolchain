#!/usr/bin/python

import numpy as np
from sklearn import datasets, linear_model
import itertools as it
import sys

nBodies = 6
DIM = 3

READ_THEN_WRITE = 0 
WRITE_THEN_READ = 1

def search_li_system(A,b):

    newA = None
    newb = None

    # remove invariant columns to avoid having an indeterminate system of equations
    # we don't want to remove the last column corresponding to the independent
    # term, so iterate up to #columns-1
    columns2remove = []
    for c in range(A.shape[1]-1):
        l = list(A[:,c])
        if all(l[0] == elem for elem in l):
            columns2remove.append(c)

    remainColumns = [c for c in range(A.shape[1]) if c not in columns2remove]
    # print remainColumns

    A = np.delete(A, columns2remove, 1)


    tuples = list(it.combinations(range(A.shape[0]), A.shape[1]))
    # print tuples

    for tuple in tuples:
        newA = None
        for index in tuple:
            if newA == None:
                newA = np.array([A[index,:]])
            else:
                newA = np.concatenate((newA,[A[index,:]]))

        if np.linalg.det(newA) != 0:
            # print "First set of linear independent equations %s" % (str(tuple))
            # print newA
            for index in tuple:
                if newb == None:
                    newb = np.array([b[index]])
                else:
                    newb = np.concatenate((newb,[b[index]]))
            # print newb

            return (newA,newb,remainColumns)

    return (newA,newb,remainColumns)

def update_matrix(A,b,readValue,currentTick,tup):

    offset = currentTick - readValue

    # List with the coefficients of the current equation
    currentRow = [x for x in tup]
    # append 1 to add the independent term of the equation
    currentRow.append(1)

    if A==None and b==None:
        # A = np.array([[i,j,k,w]])
        A = np.array([currentRow])
        b = np.array([offset])
    else:
        # A = np.concatenate((A,[[i,j,k,w]]))
        A = np.concatenate((A,[currentRow]))
        b = np.concatenate((b,[offset]))

    return A,b



def compute_system_inner_var(condRead,posRead,condWrite,posWrite,iterRanges,nestedVars,varDim,tick_rw_order):

    A = None
    b = None

    # Careful with the following replacement since it is syntactic and not semantic
    # i.e. the replacement is done without knowing if what is replaced is actually
    # a variable. Should be done using AST info...
    for i in range(len(nestedVars)):
        # Replace by tup[] since in the 'for tup in iterTuples:' below the name of
        # the variable to iterate is 'tup'
        repl = "tup[%d]" % i
        condRead  = condRead.replace(nestedVars[i],repl)
        posRead   = posRead.replace(nestedVars[i],repl)
        condWrite = condWrite.replace(nestedVars[i],repl)
        posWrite  = posWrite.replace(nestedVars[i],repl)

    iterSpace = []
    dimensions = []
    for limit in iterRanges:
        iterSpace.append(range(limit[0],limit[1]+1))
        dimensions.append((limit[1]-limit[0])+1)

    iterTuples = it.product(*iterSpace)

    # create empty list of size varDim
    varBuffer = [None] * varDim

    for tup in iterTuples:
        # (i,j,k,w) = tup
        
        currentTick = 0
        for ind in range(len(tup)):
            currentVar = tup[ind]
            for ind2 in range(ind+1,len(dimensions)):
                currentVar *= dimensions[ind2] 
            currentTick += currentVar

        if eval(condWrite) and eval(condRead):
            if tick_rw_order == WRITE_THEN_READ:
                # First write
                varBuffer[eval(posWrite)] = currentTick
                # then read
                readValue = varBuffer[eval(posRead)]
                # and update the matrix
                A,b = update_matrix(A,b,readValue,currentTick,tup)
            elif tick_rw_order == READ_THEN_WRITE:
                # first read
                readValue = varBuffer[eval(posRead)]
                # and update the matrix
                A,b = update_matrix(A,b,readValue,currentTick,tup)
                # then write
                varBuffer[eval(posWrite)] = currentTick
        else: # default
            if eval(condWrite):
                varBuffer[eval(posWrite)] = currentTick
            if eval(condRead):
                readValue = varBuffer[eval(posRead)]
                A,b = update_matrix(A,b,readValue,currentTick,tup)

    return A,b

def compute_system_input_stream(condRead,posRead,iterRanges,nestedVars):

    A = None
    b = None

    # Careful with the following replacement since it is syntactic and not semantic
    # i.e. the replacement is done without knowing if what is replaced is actually
    # a variable. Should be done using AST info...
    for i in range(len(nestedVars)):
        # Replace by tup[] since in the 'for tup in iterTuples:' below the name of
        # the variable to iterate is 'tup'
        repl = "tup[%d]" % i
        condRead  = condRead.replace(nestedVars[i],repl)
        posRead   = posRead.replace(nestedVars[i],repl)

    iterSpace = []
    dimensions = []
    for limit in iterRanges:
        iterSpace.append(range(limit[0],limit[1]+1))
        dimensions.append((limit[1]-limit[0])+1)

    iterTuples = it.product(*iterSpace)

    for tup in iterTuples:
        # (i,j,k,w) = tup
        
        currentTick = 0
        for ind in range(len(tup)):
            currentVar = tup[ind]
            for ind2 in range(ind+1,len(dimensions)):
                currentVar *= dimensions[ind2] 
            currentTick += currentVar


        if eval(condRead):
            readValue = eval(posRead)
            A,b = update_matrix(A,b,readValue,currentTick,tup)

    return A,b



def solve_system(A,b,newA,newb):
    try:
        x = np.linalg.solve(newA, newb)

        # print "\nOffset func."
        print '\tCoefficients:', x
    except np.linalg.LinAlgError as err:
        print('Error: ',err)

        x = np.linalg.lstsq(A,b)
        # print "\nOffset func. for"
        # print '\tCoefficients:', x



        # # Create linear regression object
        # regr = linear_model.LinearRegression()

        # regr.fit(A, b)
        # # The coefficients
        # print "\nAproximated (linReg) offset func. for d[w]"
        # print '\tCoefficients:', regr.coef_
        # print '\tIndep.  term:', regr.intercept_

    return x


def offsetFunction(cofficients,remainVars,indexNestedVars):

    index = 0
    sys.stdout.write("\nFunction: ")
    for i in range(len(indexNestedVars)):
        if i in remainVars:
            # if cofficients[index] != 0.0:
            sys.stdout.write("%d * %s + " % (cofficients[index],indexNestedVars[i]))            
            index +=1

    # print independent term
    sys.stdout.write("%d " % (cofficients[len(cofficients)-1]))            

    sys.stdout.write("\n\n")


def computeInnerVarOffsets(statementList,iterRanges,nestedVars):

    for access in statementList:
        print "Computing read offset for ", access[0]

        condRead      = access[1]
        posRead       = access[2]
        condWrite     = access[3]
        posWrite      = access[4]
        varDim        = access[5]
        tick_rw_order = access[6]
        A,b = compute_system_inner_var(condRead,posRead,condWrite,posWrite,iterRanges,nestedVars,varDim,tick_rw_order)
    
        liSystem = search_li_system(A,b)

        x = solve_system(A,b,liSystem[0],liSystem[1])
        offsetFunction(x,liSystem[2],nestedVars)

        print "#####################################################"


def computeInputStreamOffsets(inputStreamAccesses,iterRanges,nestedVars):

    for access in inputStreamAccesses:

        print "Computing input stream offset for ", access[0]

        condRead      = access[1]
        posRead       = access[2]
        A,b = compute_system_input_stream(condRead,posRead,iterRanges,nestedVars)

        liSystem = search_li_system(A,b)

        x = solve_system(A,b,liSystem[0],liSystem[1])
        offsetFunction(x,liSystem[2],nestedVars)

        print "#####################################################"


######################################################################

iterRanges = []
iterRanges.append((0,nBodies-1))
iterRanges.append((0,nBodies-1))
iterRanges.append((0,DIM-1))
iterRanges.append((0,DIM-1))

nestedVars = []
nestedVars.append(("i"))
nestedVars.append(("j"))
nestedVars.append(("k"))
nestedVars.append(("w"))

# ######################################################################


# values for program nbody_2arr_7_54.c

print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++")
print("\tOffsets for program nbody_2arr_7_54.c")
print("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n")


statementList = []
statementList.append(["d[0]",'w==0 and k==2','0','w==0','k',3,WRITE_THEN_READ])
statementList.append(["d[1]",'w==0 and k==2','1','w==0','k',3,WRITE_THEN_READ])
statementList.append(["d[2]",'w==0 and k==2','2','w==0','k',3,WRITE_THEN_READ])

statementList.append(["distSqr",'w==0 and k==2','0','w==0 and k==2','0',1,WRITE_THEN_READ])
statementList.append(["invDist",'w==0 and k==2','0','w==0 and k==2','0',1,WRITE_THEN_READ])
statementList.append(["invDist3",'k==2','0','w==0 and k==2','0',1,WRITE_THEN_READ])

statementList.append(["d[w]",'k==2','w','w==0','k',3,WRITE_THEN_READ])
statementList.append(["F[w]",'k==2 and j!=0','w','k==2','w',3,READ_THEN_WRITE])

computeInnerVarOffsets(statementList,iterRanges,nestedVars)

inputStreamAccesses = []
inputStreamAccesses.append(["pos[i*3+k]","w==0","i*3+k"])
inputStreamAccesses.append(["pos[j*3+k]","w==0","j*3+k"])

computeInputStreamOffsets(inputStreamAccesses,iterRanges,nestedVars)

# ######################################################################

# values for program nbody_6arr_4_15.c

# print("\n\n+++++++++++++++++++++++++++++++++++++++++++++++++++++")
# print("\tOffsets for program nbody_6arr_4_15.c")
# print("+++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n")


# iterRanges = []
# iterRanges.append((0,nBodies-1))
# iterRanges.append((0,nBodies-1))

# nestedVars = []
# nestedVars.append(("i"))
# nestedVars.append(("j"))

# statementList = []
# #                       condRead,posRead,condWrite,posWrite,varDim,tick_rw_order
# statementList.append(["dx",'True','0','True','0',1,WRITE_THEN_READ])
# statementList.append(["dy",'True','0','True','0',1,WRITE_THEN_READ])
# statementList.append(["dz",'True','0','True','0',1,WRITE_THEN_READ])

# statementList.append(["distSqr",'True','0','True','0',1,WRITE_THEN_READ])
# statementList.append(["invDist",'True','0','True','0',1,WRITE_THEN_READ])
# statementList.append(["invDist3",'True','0','True','0',1,WRITE_THEN_READ])

# # These statements are translated by Maxj template to compute summation
# statementList.append(["Fx",'j!=0','0','True','0',1,READ_THEN_WRITE])
# statementList.append(["Fy",'j!=0','0','True','0',1,READ_THEN_WRITE])
# statementList.append(["Fz",'j!=0','0','True','0',1,READ_THEN_WRITE])

# statementList.append(["Fx",'j==5','0','True','0',1,WRITE_THEN_READ])
# statementList.append(["Fy",'j==5','0','True','0',1,WRITE_THEN_READ])
# statementList.append(["Fz",'j==5','0','True','0',1,WRITE_THEN_READ])

# computeInnerVarOffsets(statementList,iterRanges,nestedVars)

# inputStreamAccesses = []
# inputStreamAccesses.append(["x[i]","True","i"])
# inputStreamAccesses.append(["x[j]","True","j"])

# inputStreamAccesses.append(["y[i]","True","i"])
# inputStreamAccesses.append(["y[j]","True","j"])

# inputStreamAccesses.append(["z[i]","True","i"])
# inputStreamAccesses.append(["z[j]","True","j"])


# computeInputStreamOffsets(inputStreamAccesses,iterRanges,nestedVars)






















# def compute_system_inner_var(condRead,condWrite):

#     A = None
#     b = None

#     wrt = []
#     read = []
#     current = 0
#     for i in range(0,nBodies):
#         for j in range(0,nBodies):
#             for k in range(0,DIM):
#                 for w in range(0,DIM):
#                     if eval(condWrite):
#                         wrt.append(i*nBodies*DIM*DIM+j*DIM*DIM+k*DIM+w)
#                     if eval(condRead):
#                         read.append(i*nBodies*DIM*DIM+j*DIM*DIM+k*DIM+w)
#                         # print "%d, %d, %d, %d: %d - %d" % (i,j,k,w,wrt[current],read[current])
#                         if A==None and b==None:
#                             # A = np.array([[i,j,k,w]])
#                             A = np.array([[i,j,k,w,1]])
#                             b = np.array([read[current]-wrt[current]])
#                             print i,j,k,w,read[current],wrt[current]
#                         else:
#                             # A = np.concatenate((A,[[i,j,k,w]]))
#                             A = np.concatenate((A,[[i,j,k,w,1]]))
#                             b = np.concatenate((b,[read[current]-wrt[current]]))
#                             if len(b)==16:
#                                 print read[current],wrt[current]
#                         current += 1

#     # remove invariant columns to avoid noise in the system resolution
#     # we don't want to remove the last column corresponding to the independent
#     # term, so iterate up to #columns-1
#     columns2remove = []
#     for c in range(A.shape[1]-1):
#         l = list(A[:,c])
#         if all(l[0] == elem for elem in l):
#             columns2remove.append(c)

#     A = np.delete(A, columns2remove, 1)

#     return A,b
