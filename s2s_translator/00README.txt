
A program to help in mechanizing the generation of maxj code:
    
    * It computes read offsets of input streams and loop inner
      variables
    
    * It requires as inputs for computing each read offset:
      - Information about ranges of the loop nest
      - Read/Write conditions in code (based on loop nest variables)
      - Read/Write positions in loop inner var
      - Dimension of inner var (=1. scalar, >1. vectors)
      - Flag to know if a write precedes the read or viceversa, in
        case both are performed within the same tick
    
    * Based on inputs, mentioned above, and the cartesian product
      resulting from loop nest, it computes a system of equations. The
      solution of the system will be the coefficients of a function
      that computes the inner var read offset. This offset will be
      "plugged in" into the Maxj code to access the DFEVar variables.

      For ex.: if the code to be translated consists of 3 nested
               loops.  We have 3 iteration variables (e.g. i,j,k), one
               per loop. Then the offset function for each read access
               to an array will be computed as a linear combination of
               the loop nest variables:

	       a1 * i + a2 * j + a3 * k + a4
